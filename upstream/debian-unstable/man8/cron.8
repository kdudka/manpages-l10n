'\" t
.\"     Title: Cron
.\"    Author: Paul Vixie <paul@vix.com>
.\" Generator: DocBook XSL Stylesheets vsnapshot <http://docbook.sf.net/>
.\"      Date: 03/26/2024
.\"    Manual: cron User Manual
.\"    Source: cron
.\"  Language: English
.\"
.TH "CRON" "8" "03/26/2024" "cron" "cron User Manual"
.\" -----------------------------------------------------------------
.\" * Define some portability stuff
.\" -----------------------------------------------------------------
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.\" http://bugs.debian.org/507673
.\" http://lists.gnu.org/archive/html/groff/2009-02/msg00013.html
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.ie \n(.g .ds Aq \(aq
.el       .ds Aq '
.\" -----------------------------------------------------------------
.\" * set default formatting
.\" -----------------------------------------------------------------
.\" disable hyphenation
.nh
.\" disable justification (adjust text to left margin only)
.ad l
.\" -----------------------------------------------------------------
.\" * MAIN CONTENT STARTS HERE *
.\" -----------------------------------------------------------------
.SH "NAME"
cron \- daemon to execute scheduled commands (Vixie Cron)
.SH "SYNOPSIS"
.HP \w'\fBcron\fR\ 'u
\fBcron\fR [\fB\-f\fR] [\fB\-l\fR] [\fB\-L\ \fR\fB\fIloglevel\fR\fR] [\fB\-n\ \fR\fB\fIfqdn\fR\fR] [\fB\-x\ \fR\fB\fIdebugflags\fR\fR]
.HP \w'\fBcron\fR\ 'u
\fBcron\fR [\fB\-N\fR]
.SH "DESCRIPTION"
.PP
\fBcron\fR
is started automatically from
/etc/init\&.d
on entering multi\-user runlevels\&.
.SH "OPTIONS"
.PP
\fB\-f\fR
.RS 4
Stay in foreground mode, don\*(Aqt daemonize\&.
.RE
.PP
\fB\-l\fR
.RS 4
Enable LSB compliant names for /etc/cron\&.d files\&. This setting, however, does not affect the parsing of files under
/etc/cron\&.hourly,
/etc/cron\&.daily,
/etc/cron\&.weekly
or
/etc/cron\&.monthly\&.
.RE
.PP
\fB\-n \fR\fB\fIfqdn\fR\fR
.RS 4
Include the FQDN in the subject when sending mails\&. By default, cron will abbreviate the hostname\&.
.RE
.PP
\fB\-N \fR
.RS 4
Run cron jobs Now, immediately, and exit\&. This option is useful to perform tests\&.
.RE
.PP
\fB\-L \fR\fB\fIloglevel\fR\fR
.RS 4
Tell cron what to log about
\fIjobs\fR
(errors are logged regardless of this value) as the sum of the following values:
.PP
1
.RS 4
will log the start of all cron jobs
.RE
.PP
2
.RS 4
will log the end of all cron jobs
.RE
.PP
4
.RS 4
will log all failed jobs (exit status != 0)
.RE
.PP
8
.RS 4
will log the process number of all cron jobs
.RE
.sp
The default is to log the start of all jobs (1)\&. Logging will be disabled if levels is set to zero (0)\&. A value of fifteen (15) will select all options\&.
.RE
.PP
\fB\-x \fR\fB\fIdebugflags\fR\fR
.RS 4
Tell cron to be more verbose and output debugging information; debugflags is the sum of those values:
.PP
1
.RS 4
"ext": \&.\&.\&.
.RE
.PP
2
.RS 4
"sch": \&.\&.\&.
.RE
.PP
4
.RS 4
"proc": \&.\&.\&.
.RE
.PP
8
.RS 4
"pars": \&.\&.\&.
.RE
.PP
16
.RS 4
"load": \&.\&.
.RE
.PP
32
.RS 4
"misc": \&.\&.\&.
.RE
.PP
64
.RS 4
"test": \&.\&.\&.
.RE
.PP
128
.RS 4
"bit": \&.\&.\&.
.RE
.sp
.RE
.SH "NOTES"
.PP
\fBcron\fR
searches its spool area (/var/spool/cron/crontabs/) for crontab files (which are named after accounts in
/etc/passwd); crontabs found are loaded into memory\&. Note that crontabs in this directory should not be accessed directly \- the
\fBcrontab\fR
command should be used to access and update them\&.
.PP
\fBcron\fR
also reads
/etc/crontab, which is in a slightly different format (see
\fBcrontab\fR(5))\&. In Debian, the content of
/etc/crontab
is predefined to run programs under
/etc/cron\&.hourly,
/etc/cron\&.daily,
/etc/cron\&.weekly
and
/etc/cron\&.monthly\&. This configuration is specific to Debian, see the note under DEBIAN SPECIFIC below\&.
.PP
Additionally, in Debian,
\fBcron\fR
reads the files in the
/etc/cron\&.d
directory\&.
\fBcron\fR
treats the files in
/etc/cron\&.d
as in the same way as the
/etc/crontab
file (they follow the special format of that file, i\&.e\&. they include the
\fIuser\fR
field)\&. However, they are independent of
/etc/crontab: they do not, for example, inherit environment variable settings from it\&. This change is specific to Debian see the note under
\fIDEBIAN SPECIFIC\fR
below\&.
.PP
Like
/etc/crontab, the files in the
/etc/cron\&.d
directory are monitored for changes\&. The system administrator may create cron jobs in
/etc/cron\&.d/
with file names like "local" or "local\-foo"\&.
.PP
/etc/crontab
and the files in
/etc/cron\&.d
must be owned by root, and must not be group\- or other\-writable\&. In contrast to the spool area, the files under
/etc/cron\&.d
or the files under
/etc/cron\&.hourly,
/etc/cron\&.daily,
/etc/cron\&.weekly
and
/etc/cron\&.monthly
may also be symlinks, provided that both the symlink and the file it points to are owned by root\&. The files under
/etc/cron\&.d
do not need to be executable, while the files under
/etc/cron\&.hourly,
/etc/cron\&.daily,
/etc/cron\&.weekly
and
/etc/cron\&.monthly
do, as they are run by run\-parts (see
\fBrun-parts\fR(8)
for more information)\&.
.PP
\fBcron\fR
then wakes up every minute, examining all stored crontabs, checking each command to see if it should be run in the current minute\&. When executing commands, any output is mailed to the owner of the crontab (or to the user named in the MAILTO environment variable in the crontab, if such exists) from the owner of the crontab (or from the email address given in the MAILFROM environment variable in the crontab, if such exists)\&. The children copies of cron running these processes have their name coerced to uppercase, as will be seen in the syslog and ps output\&.
.PP
Additionally,
\fBcron\fR
checks each minute to see if its spool directory\*(Aqs modtime (or the modtime on the
/etc/crontab
file) has changed, and if it has,
\fBcron\fR
will then examine the modtime on all crontabs and reload those which have changed\&. Thus
\fBcron\fR
need not be restarted whenever a crontab file is modified\&. Note that the
\fBcrontab\fR(1)
command updates the modtime of the spool directory whenever it changes a crontab\&.
.PP
Special considerations exist when the clock is changed by less than 3 hours, for example at the beginning and end of daylight savings time\&. If the time has moved forwards, those jobs which would have run in the time that was skipped will be run soon after the change\&. Conversely, if the time has moved backwards by less than 3 hours, those jobs that fall into the repeated time will not be re\-run\&.
.PP
Only jobs that run at a particular time (not specified as @hourly, nor with \*(Aq*\*(Aq in the hour or minute specifier) are affected\&. Jobs which are specified with wildcards are run based on the new time immediately\&.
.PP
Clock changes of more than 3 hours are considered to be corrections to the clock, and the new time is used immediately\&.
.PP
\fBcron\fR
logs its action to the syslog facility \*(Aqcron\*(Aq, and logging may be controlled using the standard
\fBsyslogd\fR(8)
facility\&.
.SH "ENVIRONMENT"
.PP
If configured in
\fI/etc/default/cron\fR
in Debian systems, the
\fBcron\fR
daemon localisation settings environment can be managed through the use of
\fI/etc/environment\fR
or through the use of
\fI/etc/default/locale\fR
with values from the latter overriding values from the former\&. These files are read and they will be used to setup the LANG, LC_ALL, and LC_CTYPE environment variables\&. These variables are then used to set the charset of mails, which defaults to \*(AqC\*(Aq\&.
.PP
This does
\fINOT\fR
affect the environment of tasks running under cron\&. For more information on how to modify the environment of tasks, consult
\fBcrontab\fR(5)\&.
.PP
The daemon will use, if present, the definition from
\fI/etc/localtime\fR
for the timezone\&.
.PP
The environment can be redefined in user\*(Aqs crontab definitions but cron will only handle tasks in a single timezone\&.
.SH "DEBIAN SPECIFIC"
.PP
Debian introduces some changes to cron that were not originally available upstream\&. The most significant changes introduced are:
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Support for
/etc/cron\&.{hourly,daily,weekly,monthly}
via/etc/crontab,
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Support for
/etc/cron\&.d
(drop\-in dir for package crontabs),
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
PAM support,
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
SELinux support,
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
auditlog support,
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
DST and other time\-related changes/fixes,
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
SGID crontab(1) instead of SUID root,
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Debian\-specific file locations and commands,
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
Debian\-specific configuration (/etc/default/cron),
.RE
.sp
.RS 4
.ie n \{\
\h'-04'\(bu\h'+03'\c
.\}
.el \{\
.sp -1
.IP \(bu 2.3
.\}
numerous other smaller features and fixes\&.
.RE
.PP
Support for
/etc/cron\&.hourly,
/etc/cron\&.daily,
/etc/cron\&.weekly
and
/etc/cron\&.monthly
is provided in Debian through the default setting of the
/etc/crontab
file (see the system\-wide example in
\fBcrontab\fR(5))\&. The default system\-wide crontab contains four tasks: run every hour, every day, every week and every month\&. Each of these tasks will execute
\fBrun\-parts\fR
providing each one of the directories as an argument\&. These tasks are disabled if
\fBanacron\fR
is installed (except for the hourly task) to prevent conflicts between both daemons\&.
.PP
As described above, the files under these directories have to pass some sanity checks including the following: be executable, be owned by root, not be writable by group or other and, if symlinks, point to files owned by root\&. Additionally, the file names must conform to the filename requirements of
\fBrun\-parts\fR: they must be entirely made up of letters, digits and can only contain the special signs underscores (\*(Aq_\*(Aq) and hyphens (\*(Aq\-\*(Aq)\&. Any file that does not conform to these requirements will not be executed by
\fBrun\-parts\fR\&. For example, any file containing dots will be ignored\&. This is done to prevent cron from running any of the files that are left by the Debian package management system when handling files in /etc/cron\&.d/ as configuration files (i\&.e\&. files ending in \&.dpkg\-dist, \&.dpkg\-orig, \&.dpkg\-old, and \&.dpkg\-new)\&.
.PP
This feature can be used by system administrators and packages to include tasks that will be run at defined intervals\&. Files created by packages in these directories should be named after the package that supplies them\&.
.PP
Support for
/etc/cron\&.d
is included in the
\fBcron\fR
daemon itself, which handles this location as the system\-wide crontab spool\&. This directory can contain any file defining tasks following the format used in
/etc/crontab, i\&.e\&. unlike the user cron spool, these files must provide the username to run the task as in the task definition\&.
.PP
Files in this directory have to be owned by root, do not need to be executable (they are configuration files, just like
/etc/crontab) and must conform to the same naming convention as used by
\fBrun-parts\fR(8)
: they must consist solely of upper\- and lower\-case letters, digits, underscores, and hyphens\&. This means that they
\fIcannot\fR
contain any dots\&. If the
\fI\-l\fR
option is specified to
\fBcron\fR
(this option can be setup through
/etc/default/cron, see below), then they must conform to the LSB namespace specification, exactly as in the
\fI\-\-lsbsysinit\fR
option in
\fBrun\-parts\fR\&.
.PP
The intended purpose of this feature is to allow packages that require finer control of their scheduling than the
/etc/cron\&.{hourly,daily,weekly,monthly}
directories to add a crontab file to
/etc/cron\&.d\&. Such files should be named after the package that supplies them\&.
.PP
Also, the default configuration of
\fBcron\fR
is controlled by
/etc/default/cron
which is read by the init\&.d script that launches the
\fBcron\fR
daemon\&. This file determines whether cron will read the system\*(Aqs environment variables and makes it possible to add additional options to the
\fBcron\fR
program before it is executed, either to configure its logging or to define how it will treat the files under
/etc/cron\&.d\&.
.SH "SEE ALSO"
.PP
\fBcrontab\fR(1),
\fBcrontab\fR(5),
\fBrun-parts\fR(8)
.SH "AUTHORS"
.PP
\fBPaul Vixie\fR <\&paul@vix\&.com\&>
.RS 4
Wrote this manpage (1994)\&.
.RE
.PP
\fBSteve Greenland\fR <\&stevegr@debian\&.org\&>
.RS 4
Maintained the package (1996\-2005)\&.
.RE
.PP
\fBJavier Fern\('andez\-Sanguino Pe\(~na\fR <\&jfs@debian\&.org\&>
.RS 4
Maintained the package (2005\-2014)\&.
.RE
.PP
\fBChristian Kastner\fR <\&ckk@debian\&.org\&>
.RS 4
Maintained the package (2010\-2016)\&.
.RE
.PP
\fBGeorges Khaznadar\fR <\&georgesk@debian\&.org\&>
.RS 4
Maintained the package (2022\-2024)\&.
.RE
.SH "COPYRIGHT"
.br
Copyright \(co 1994 Paul Vixie
.br
.PP
Distribute freely, except: don\*(Aqt remove my name from the source or documentation (don\*(Aqt take credit for my work), mark your changes (don\*(Aqt get me blamed for your possible bugs), don\*(Aqt alter or remove this notice\&. May be sold if buildable source is provided to buyer\&. No warranty of any kind, express or implied, is included with this software; use at your own risk, responsibility for damages (if any) to anyone resulting from the use of this software rests entirely with the user\&.
.PP
Since year 1994, many modifications were made in this manpage, authored by Debian Developers which maintained
cron; above is a short list, more information can be found in the file
/usr/share/doc/cron/copyright\&.
.sp

# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-05-01 15:48+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "proc_pid_task"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-08-15"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "/proc/pid/task/, /proc/tid/, /proc/thread-self/ - thread information"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "I</proc/>pidI</task/> (since Linux 2.6.0)"
msgstr ""

#.  Precisely: Linux 2.6.0-test6
#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"This is a directory that contains one subdirectory for each thread in the "
"process.  The name of each subdirectory is the numerical thread ID (I<tid>)  "
"of the thread (see B<gettid>(2))."
msgstr ""

#.  in particular: "children" :/
#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Within each of these subdirectories, there is a set of files with the same "
"names and contents as under the I</proc/>pid directories.  For attributes "
"that are shared by all threads, the contents for each of the files under the "
"I<task/>tid subdirectories will be the same as in the corresponding file in "
"the parent I</proc/>pid directory (e.g., in a multithreaded process, all of "
"the I<task/>tidI</cwd> files will have the same value as the I</proc/>pidI</"
"cwd> file in the parent directory, since all of the threads in a process "
"share a working directory).  For attributes that are distinct for each "
"thread, the corresponding files under I<task/>tid may have different values "
"(e.g., various fields in each of the I<task/>tidI</status> files may be "
"different for each thread), or they might not exist in I</proc/>pid at all."
msgstr ""

#.  The following was still true as at kernel 2.6.13
#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"In a multithreaded process, the contents of the I</proc/>pidI</task> "
"directory are not available if the main thread has already terminated "
"(typically by calling B<pthread_exit>(3))."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "I</proc/>tidI</>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"There is a numerical subdirectory for each running thread that is not a "
"thread group leader (i.e., a thread whose thread ID is not the same as its "
"process ID); the subdirectory is named by the thread ID.  Each one of these "
"subdirectories contains files and subdirectories exposing information about "
"the thread with the thread ID I<tid>.  The contents of these directories are "
"the same as the corresponding I</proc/>pidI</task/>tid directories."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The I</proc/>tid subdirectories are I<not> visible when iterating through I</"
"proc> with B<getdents>(2)  (and thus are I<not> visible when one uses "
"B<ls>(1)  to view the contents of I</proc>).  However, the pathnames of "
"these directories are visible to (i.e., usable as arguments in)  system "
"calls that operate on pathnames."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "I</proc/thread-self/> (since Linux 3.17)"
msgstr ""

#.  commit 0097875bd41528922fb3bb5f348c53f17e00e2fd
#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"This directory refers to the thread accessing the I</proc> filesystem, and "
"is identical to the I</proc/self/task/>tid directory named by the process "
"thread ID (I<tid>)  of the same thread."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<proc>(5)"
msgstr ""

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

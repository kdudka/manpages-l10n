# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-29 09:35+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "ADJTIME_CONFIG"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "File formats"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"adjtime_config - information about hardware clock setting and drift factor"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "I</etc/adjtime>"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The file I</etc/adjtime> contains descriptive information about the hardware "
"mode clock setting and clock drift factor. The file is read and write by "
"B<hwclock>(8); and read by programs like rtcwake to get RTC time mode."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The file is usually located in I</etc>, but tools like B<hwclock>(8) or "
"B<rtcwake>(8) can use alternative location by command line options if write "
"access to I</etc> is unwanted. The default clock mode is \"UTC\" if the file "
"is missing."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The Hardware Clock is usually not very accurate. However, much of its "
"inaccuracy is completely predictable - it gains or loses the same amount of "
"time every day. This is called systematic drift. The util B<hwclock>(8) "
"keeps the file I</etc/adjtime>, that keeps some historical information. For "
"more details see \"B<The Adjust Function>\" and \"B<The Adjtime File>\" "
"sections from B<hwclock>(8) man page."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The I<adjtime> file is formatted in ASCII."
msgstr ""

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "First line"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Three numbers, separated by blanks:"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<drift factor>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "the systematic drift rate in seconds per day (floating point decimal)"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<last adjust time>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"the resulting number of seconds since 1969 UTC of most recent adjustment or "
"calibration (decimal integer)"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<adjustment status>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "zero (for compatibility with B<clock>(8)) as a floating point decimal"
msgstr ""

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Second line"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<last calibration time>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The resulting number of seconds since 1969 UTC of most recent calibration. "
"Zero if there has been no calibration yet or it is known that any previous "
"calibration is moot (for example, because the Hardware Clock has been found, "
"since that calibration, not to contain a valid time). This is a decimal "
"integer."
msgstr ""

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Third line"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<clock mode>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Supported values are B<UTC> or B<LOCAL>. Tells whether the Hardware Clock is "
"set to Coordinated Universal Time or local time. You can always override "
"this value with options on the B<hwclock>(8) command line."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "FILES"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<hwclock>(8), B<rtcwake>(8)"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<adjtime_config> is part of the util-linux package which can be downloaded "
"from"
msgstr ""

# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-05-01 15:43+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ldconfig"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "ldconfig - configure dynamic linker run-time bindings"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: SY
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "/sbin/ldconfig"
msgstr ""

#.  TODO?: -c, --format, -i, --ignore-aux-cache, --print-cache,
#.  --verbose, -V, --version, -?, --help, --usage
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"[B<-nNvVX>] [B<-C\\~> I<cache>] [B<-f\\~> I<conf>] [B<-r\\~> I<root>] "
"I<directory>\\~.\\|.\\|."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<-l> [B<-v>] I<library>\\~.\\|.\\|."
msgstr ""

#. #-#-#-#-#  archlinux: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  debian-bookworm: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-unstable: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-40: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-rawhide: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-leap-15-6: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-tumbleweed: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-p>"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<\\%ldconfig> creates the necessary links and cache to the most recent "
"shared libraries found in the directories specified on the command line, in "
"the file I</etc/ld.so.conf>, and in the trusted directories, I</lib> and I</"
"usr/lib>.  On some 64-bit architectures such as x86-64, I</lib> and I</usr/"
"lib> are the trusted directories for 32-bit libraries, while I</lib64> and "
"I</usr/lib64> are used for 64-bit libraries."
msgstr ""

#.  Support for libc4 and libc5 dropped in
#.  8ee878592c4a642937152c8308b8faef86bcfc40 (2022-07-14) as "obsolete
#.  for over twenty years".
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The cache is used by the run-time linker, I<ld.so> or I<ld-linux.so>.  B<\\"
"%ldconfig> checks the header and filenames of the libraries it encounters "
"when determining which versions should have their links updated.  B<\\"
"%ldconfig> should normally be run by the superuser as it may require write "
"permission on some root owned directories and files."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<\\%ldconfig> will look only at files that are named I<lib*.so*> (for "
"regular shared objects) or I<ld-*.so*> (for the dynamic loader itself).  "
"Other files will be ignored.  Also, B<\\%ldconfig> expects a certain pattern "
"to how the symbolic links are set up, like this example, where the middle "
"file (B<libfoo.so.1> here) is the SONAME for the library:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "libfoo.so -E<gt> libfoo.so.1 -E<gt> libfoo.so.1.12\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Failure to follow this pattern may result in compatibility issues after an "
"upgrade."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. #-#-#-#-#  archlinux: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  debian-unstable: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-40: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-rawhide: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-leap-15-6: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-tumbleweed: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--format=>I<fmt>"
msgstr ""

#. #-#-#-#-#  archlinux: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  debian-bookworm: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-unstable: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-40: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-rawhide: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-leap-15-6: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-tumbleweed: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-c\\~>I<fmt>"
msgstr ""

#.  commit 45eca4d141c047950db48c69c8941163d0a61fcd
#.  commit cad64f778aced84efdaa04ae64f8737b86f063ab
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"(Since glibc 2.2)  Use cache format I<fmt>, which is one of B<old>, B<new>, "
"or B<\\%compat>.  Since glibc 2.32, the default is B<new>.  Before that, it "
"was B<\\%compat>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-C\\~>I<cache>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Use I<cache> instead of I</etc/ld.so.cache>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-f\\~>I<conf>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Use I<conf> instead of I</etc/ld.so.conf>."
msgstr ""

#. #-#-#-#-#  archlinux: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  debian-unstable: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-40: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-rawhide: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-leap-15-6: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-tumbleweed: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--ignore-aux-cache>"
msgstr ""

#. #-#-#-#-#  archlinux: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  debian-bookworm: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-unstable: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-40: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-rawhide: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-leap-15-6: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-tumbleweed: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>"
msgstr ""

#.  commit 27d9ffda17df4d2388687afd12897774fde39bcc
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "(Since glibc 2.7)  Ignore auxiliary cache file."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-l>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"(Since glibc 2.2)  Interpret each operand as a library name and configure "
"its links.  Intended for use only by experts."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-n>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Process only the directories specified on the command line; don't process "
"the trusted directories, nor those specified in I</etc/ld.so.conf>.  Implies "
"B<-N>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-N>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Don't rebuild the cache.  Unless B<-X> is also specified, links are still "
"updated."
msgstr ""

#. #-#-#-#-#  archlinux: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  debian-unstable: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-40: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-rawhide: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-leap-15-6: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-tumbleweed: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--print-cache>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Print the lists of directories and candidate libraries stored in the current "
"cache."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-r\\~>I<root>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Change to and use I<root> as the root directory."
msgstr ""

#. #-#-#-#-#  archlinux: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  debian-unstable: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-40: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-rawhide: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-leap-15-6: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-tumbleweed: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--verbose>"
msgstr ""

#. #-#-#-#-#  archlinux: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  debian-bookworm: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-unstable: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-40: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-rawhide: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-leap-15-6: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-tumbleweed: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Verbose mode.  Print current version number, the name of each directory as "
"it is scanned, and any links that are created.  Overrides quiet mode."
msgstr ""

#. #-#-#-#-#  archlinux: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  debian-unstable: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-40: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-rawhide: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-leap-15-6: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-tumbleweed: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr ""

#. #-#-#-#-#  archlinux: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  debian-bookworm: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-unstable: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-40: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-rawhide: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-leap-15-6: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-tumbleweed: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Print program version."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-X>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Don't update links.  Unless B<-N> is also specified, the cache is still "
"rebuilt."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I</lib/ld.so>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "is the run-time linker/loader."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/ld.so.conf>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"contains a list of directories, one per line, in which to search for "
"libraries."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/ld.so.cache>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"contains an ordered list of libraries found in the directories specified in "
"I</etc/ld.so.conf>, as well as those found in the trusted directories."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<ldd>(1), B<ld.so>(8)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-01-07"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"(Since glibc 2.2)  Interpret each operand as a libary name and configure its "
"links.  Intended for use only by experts."
msgstr ""

#. type: TH
#: fedora-40
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-11"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""

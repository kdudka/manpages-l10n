# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-29 09:57+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "WIPEFS"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "System Administration"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "wipefs - wipe a signature from a device"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<wipefs> [options] I<device>..."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<wipefs> [B<--backup>] B<-o> I<offset device>..."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<wipefs> [B<--backup>] B<-a> I<device>..."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<wipefs> can erase filesystem, raid or partition-table signatures (magic "
"strings) from the specified I<device> to make the signatures invisible for "
"libblkid. B<wipefs> does not erase the filesystem itself nor any other data "
"from the device."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"When used without any options, B<wipefs> lists all visible filesystems and "
"the offsets of their basic signatures. The default output is subject to "
"change. So whenever possible, you should avoid using default outputs in your "
"scripts. Always explicitly define expected columns by using B<--output> "
"I<columns-list> in environments where a stable output is required."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<wipefs> calls the B<BLKRRPART> ioctl when it has erased a partition-table "
"signature to inform the kernel about the change. The ioctl is called as the "
"last step and when all specified signatures from all specified devices are "
"already erased. This feature can be used to wipe content on partitions "
"devices as well as partition table on a disk device, for example by B<wipefs "
"-a /dev/sdc1 /dev/sdc2 /dev/sdc>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Note that some filesystems and some partition tables store more magic "
"strings on the device (e.g., FAT, ZFS, GPT). The B<wipefs> command (since "
"v2.31) lists all the offset where a magic strings have been detected."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"When option B<-a> is used, all magic strings that are visible for "
"B<libblkid>(3) are erased. In this case the B<wipefs> scans the device again "
"after each modification (erase) until no magic string is found."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Note that by default B<wipefs> does not erase nested partition tables on non-"
"whole disk devices. For this the option B<--force> is required."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-a>, B<--all>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Erase all available signatures. The set of erased signatures can be "
"restricted with the B<-t> option."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-b>, B<--backup>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Create a signature backup to the file I<$HOME/wipefs-E<lt>devnameE<gt>-"
"E<lt>offsetE<gt>.bak>. For more details see the B<EXAMPLE> section."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-f>, B<--force>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Force erasure, even if the filesystem is mounted. This is required in order "
"to erase a partition-table signature on a block device."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-J>, B<--json>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Use JSON output format."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--lock>[=I<mode>]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Use exclusive BSD lock for device or file it operates. The optional argument "
"I<mode> can be B<yes>, B<no> (or 1 and 0) or B<nonblock>. If the I<mode> "
"argument is omitted, it defaults to B<\"yes\">. This option overwrites "
"environment variable B<$LOCK_BLOCK_DEVICE>. The default is not to use any "
"lock at all, but it\\(cqs recommended to avoid collisions with udevd or "
"other tools."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-i>, B<--noheadings>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Do not print a header line."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-O>, B<--output> I<list>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Specify which output columns to print. Use B<--help> to get a list of all "
"supported columns."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-n>, B<--no-act>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Causes everything to be done except for the B<write>(2) call."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-o>, B<--offset> I<offset>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Specify the location (in bytes) of the signature which should be erased from "
"the device. The I<offset> number may include a \"0x\" prefix; then the "
"number will be interpreted as a hex value. It is possible to specify "
"multiple B<-o> options."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The I<offset> argument may be followed by the multiplicative suffixes KiB "
"(=1024), MiB (=1024*1024), and so on for GiB, TiB, PiB, EiB, ZiB and YiB "
"(the \"iB\" is optional, e.g., \"K\" has the same meaning as \"KiB\"), or "
"the suffixes KB (=1000), MB (=1000*1000), and so on for GB, TB, PB, EB, ZB "
"and YB."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-p>, B<--parsable>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Print out in parsable instead of printable format. Encode all potentially "
"unsafe characters of a string to the corresponding hex value prefixed by "
"\\(aq\\(rsx\\(aq."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-q>, B<--quiet>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Suppress any messages after a successful signature wipe."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-t>, B<--types> I<list>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Limit the set of printed or erased signatures. More than one type may be "
"specified in a comma-separated list. The list or individual types can be "
"prefixed with \\(aqno\\(aq to specify the types on which no action should be "
"taken. For more details see B<mount>(8)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "ENVIRONMENT"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "LIBBLKID_DEBUG=all"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "enables B<libblkid>(3) debug output."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "LOCK_BLOCK_DEVICE=E<lt>modeE<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"use exclusive BSD lock. The mode is \"1\" or \"0\". See B<--lock> for more "
"details."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<wipefs /dev/sda>*"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Prints information about sda and all partitions on sda."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<wipefs --all --backup /dev/sdb>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Erases all signatures from the device I</dev/sdb> and creates a signature "
"backup file I<~/wipefs-sdb-E<lt>offsetE<gt>.bak> for each signature."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<dd if=~/wipefs-sdb-0x00000438.bak of=/dev/sdb seek=$((0x00000438)) bs=1 "
"conv=notrunc>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Restores an ext2 signature from the backup file I<~/wipefs-sdb-0x00000438."
"bak>."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<blkid>(8), B<findfs>(8)"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<wipefs> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""

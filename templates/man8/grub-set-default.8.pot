# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-29 09:41+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB-SET-DEFAULT"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "March 2024"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.12-2"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "System Administration Utilities"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux
msgid "grub-set-default - set the saved default boot entry for GRUB"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<grub-set-default> [I<\\,OPTION\\/>] I<\\,MENU_ENTRY\\/>"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Set the default boot menu entry for GRUB.  This requires setting "
"GRUB_DEFAULT=saved in I<\\,/etc/default/grub\\/>."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print this message and exit"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print the version information and exit"
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--boot-directory>=I<\\,DIR\\/>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"expect GRUB images under the directory DIR/grub instead of the I<\\,/boot/"
"grub\\/> directory"
msgstr ""

#. type: Plain text
#: archlinux
msgid "MENU_ENTRY is a number, a menu item title or a menu item identifier."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<grub-reboot>(8), B<grub-editenv>(1)"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"The full documentation for B<grub-set-default> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-set-default> programs are properly "
"installed at your site, the command"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<info grub-set-default>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "should give you access to the complete manual."
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "November 2006"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "grub-set-default - Set the default boot entry for GRUB"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<grub-set-default> [I<OPTION>] entry"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Set the default boot entry for GRUB."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-h, --help>"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-v, --version>"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<--root-directory=DIR>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Use the directory DIR instead of the root directory"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "ENTRY is a number (counting from 0) or the special keyword `default\\'."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>"
msgstr ""

#.  Always quote multiple words for .SH
#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<grub>(8), B<update-grub>(8)."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"This manual page was written by Patrick Schoenfeld E<lt>schoenfeld@in-medias-"
"res.comE<gt>, for the Debian project (but may be used by others)."
msgstr ""

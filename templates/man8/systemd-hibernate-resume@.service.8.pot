# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-01-07 12:13+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "SYSTEMD-HIBERNATE-RESUME@\\&.SERVICE"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd 253"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "systemd-hibernate-resume@.service"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"systemd-hibernate-resume@.service, systemd-hibernate-resume - Resume from "
"hibernation"
msgstr ""

#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid "systemd-hibernate-resume@\\&.service"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid "/usr/lib/systemd/systemd-hibernate-resume"
msgstr ""

#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"systemd-hibernate-resume@\\&.service initiates the resume from "
"hibernation\\&. It is instantiated with the device to resume from as the "
"template argument\\&."
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"systemd-hibernate-resume only supports the in-kernel hibernation "
"implementation, see \\m[blue]B<Swap suspend>\\m[]\\&\\s-2\\u[1]\\d\\s+2\\&. "
"Internally, it works by writing the major:minor of specified device node to /"
"sys/power/resume\\&."
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"Failing to initiate a resume is not an error condition\\&. It may mean that "
"there was no resume image (e\\&. g\\&. if the system has been simply powered "
"off and not hibernated)\\&. In such case, the boot is ordinarily "
"continued\\&."
msgstr ""

#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid "B<systemd>(1), B<systemd-hibernate-resume-generator>(8)"
msgstr ""

#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: IP
#: mageia-cauldron
#, no-wrap
msgid " 1."
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid "Swap suspend"
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid "\\%https://docs.kernel.org/power/swsusp.html"
msgstr ""

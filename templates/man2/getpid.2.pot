# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-05-01 15:40+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "getpid"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "getpid, getppid - get process identification"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<pid_t getpid(void);>\n"
"B<pid_t getppid(void);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<getpid>()  returns the process ID (PID) of the calling process.  (This is "
"often used by routines that generate unique temporary filenames.)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<getppid>()  returns the process ID of the parent of the calling process.  "
"This will be either the ID of the process that created this process using "
"B<fork>(), or, if that process has already terminated, the ID of the process "
"to which this process has been reparented (either B<init>(1)  or a "
"\"subreaper\" process defined via the B<prctl>(2)  B<PR_SET_CHILD_SUBREAPER> "
"operation)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "These functions are always successful."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On Alpha, instead of a pair of B<getpid>()  and B<getppid>()  system calls, "
"a single B<getxpid>()  system call is provided, which returns a pair of PID "
"and parent PID.  The glibc B<getpid>()  and B<getppid>()  wrapper functions "
"transparently deal with this.  See B<syscall>(2)  for details regarding "
"register mapping."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2001, 4.3BSD, SVr4."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "C library/kernel differences"
msgstr ""

#
#
#
#
#.  The following program demonstrates this "feature":
#.  #define _GNU_SOURCE
#.  #include <sys/syscall.h>
#.  #include <sys/wait.h>
#.  #include <stdint.h>
#.  #include <stdio.h>
#.  #include <stdlib.h>
#.  #include <unistd.h>
#.  int
#.  main(int argc, char *argv[])
#.  {
#.     /* The following statement fills the getpid() cache */
#.     printf("parent PID = %ld
#. ", (intmax_t) getpid());
#.     if (syscall(SYS_fork) == 0) {
#.         if (getpid() != syscall(SYS_getpid))
#.             printf("child getpid() mismatch: getpid()=%jd; "
#.                     "syscall(SYS_getpid)=%ld
#. ",
#.                     (intmax_t) getpid(), (long) syscall(SYS_getpid));
#.         exit(EXIT_SUCCESS);
#.     }
#.     wait(NULL);
#. }
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"From glibc 2.3.4 up to and including glibc 2.24, the glibc wrapper function "
"for B<getpid>()  cached PIDs, with the goal of avoiding additional system "
"calls when a process calls B<getpid>()  repeatedly.  Normally this caching "
"was invisible, but its correct operation relied on support in the wrapper "
"functions for B<fork>(2), B<vfork>(2), and B<clone>(2): if an application "
"bypassed the glibc wrappers for these system calls by using B<syscall>(2), "
"then a call to B<getpid>()  in the child would return the wrong value (to be "
"precise: it would return the PID of the parent process).  In addition, there "
"were cases where B<getpid>()  could return the wrong value even when "
"invoking B<clone>(2)  via the glibc wrapper function.  (For a discussion of "
"one such case, see BUGS in B<clone>(2).)  Furthermore, the complexity of the "
"caching code had been the source of a few bugs within glibc over the years."
msgstr ""

#.  commit c579f48edba88380635ab98cb612030e3ed8691e
#.  https://sourceware.org/glibc/wiki/Release/2.25#pid_cache_removal
#.  FIXME .
#.  Review progress of https://bugzilla.redhat.com/show_bug.cgi?id=1469757
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Because of the aforementioned problems, since glibc 2.25, the PID cache is "
"removed: calls to B<getpid>()  always invoke the actual system call, rather "
"than returning a cached value."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the caller's parent is in a different PID namespace (see "
"B<pid_namespaces>(7)), B<getppid>()  returns 0."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"From a kernel perspective, the PID (which is shared by all of the threads in "
"a multithreaded process)  is sometimes also known as the thread group ID "
"(TGID).  This contrasts with the kernel thread ID (TID), which is unique for "
"each thread.  For further details, see B<gettid>(2)  and the discussion of "
"the B<CLONE_THREAD> flag in B<clone>(2)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<clone>(2), B<fork>(2), B<gettid>(2), B<kill>(2), B<exec>(3), "
"B<mkstemp>(3), B<tempnam>(3), B<tmpfile>(3), B<tmpnam>(3), "
"B<credentials>(7), B<pid_namespaces>(7)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-01-22"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, 4.3BSD, SVr4."
msgstr ""

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""

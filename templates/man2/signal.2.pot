# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-05-01 15:53+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "signal"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "signal - ANSI C signal handling"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>signal.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<typedef void (*sighandler_t)(int);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<sighandler_t signal(int >I<signum>B<, sighandler_t >I<handler>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<WARNING>: the behavior of B<signal>()  varies across UNIX versions, and "
"has also varied historically across different versions of Linux.  B<Avoid "
"its use>: use B<sigaction>(2)  instead.  See I<Portability> below."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<signal>()  sets the disposition of the signal I<signum> to I<handler>, "
"which is either B<SIG_IGN>, B<SIG_DFL>, or the address of a programmer-"
"defined function (a \"signal handler\")."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the signal I<signum> is delivered to the process, then one of the "
"following happens:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "*"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If the disposition is set to B<SIG_IGN>, then the signal is ignored."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the disposition is set to B<SIG_DFL>, then the default action associated "
"with the signal (see B<signal>(7))  occurs."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the disposition is set to a function, then first either the disposition "
"is reset to B<SIG_DFL>, or the signal is blocked (see I<Portability> below), "
"and then I<handler> is called with argument I<signum>.  If invocation of the "
"handler caused the signal to be blocked, then the signal is unblocked upon "
"return from the handler."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The signals B<SIGKILL> and B<SIGSTOP> cannot be caught or ignored."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<signal>()  returns the previous value of the signal handler.  On failure, "
"it returns B<SIG_ERR>, and I<errno> is set to indicate the error."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<signum> is invalid."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr ""

#.  libc4 and libc5 define
#.  .IR SignalHandler ;
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The use of I<sighandler_t> is a GNU extension, exposed if B<_GNU_SOURCE> is "
"defined; glibc also defines (the BSD-derived)  I<sig_t> if B<_BSD_SOURCE> "
"(glibc 2.19 and earlier)  or B<_DEFAULT_SOURCE> (glibc 2.19 and later)  is "
"defined.  Without use of such a type, the declaration of B<signal>()  is the "
"somewhat harder to read:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<void ( *>I<signal>B<(int >I<signum>B<, void (*>I<handler>B<)(int)) ) (int);>\n"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Portability"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The only portable use of B<signal>()  is to set a signal's disposition to "
"B<SIG_DFL> or B<SIG_IGN>.  The semantics when using B<signal>()  to "
"establish a signal handler vary across systems (and POSIX.1 explicitly "
"permits this variation); B<do not use it for this purpose.>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"POSIX.1 solved the portability mess by specifying B<sigaction>(2), which "
"provides explicit control of the semantics when a signal handler is invoked; "
"use that interface instead of B<signal>()."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "C89, POSIX.1-2001."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In the original UNIX systems, when a handler that was established using "
"B<signal>()  was invoked by the delivery of a signal, the disposition of the "
"signal would be reset to B<SIG_DFL>, and the system did not block delivery "
"of further instances of the signal.  This is equivalent to calling "
"B<sigaction>(2)  with the following flags:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "sa.sa_flags = SA_RESETHAND | SA_NODEFER;\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"System\\ V also provides these semantics for B<signal>().  This was bad "
"because the signal might be delivered again before the handler had a chance "
"to reestablish itself.  Furthermore, rapid deliveries of the same signal "
"could result in recursive invocations of the handler."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"BSD improved on this situation, but unfortunately also changed the semantics "
"of the existing B<signal>()  interface while doing so.  On BSD, when a "
"signal handler is invoked, the signal disposition is not reset, and further "
"instances of the signal are blocked from being delivered while the handler "
"is executing.  Furthermore, certain blocking system calls are automatically "
"restarted if interrupted by a signal handler (see B<signal>(7)).  The BSD "
"semantics are equivalent to calling B<sigaction>(2)  with the following "
"flags:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "sa.sa_flags = SA_RESTART;\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The situation on Linux is as follows:"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The kernel's B<signal>()  system call provides System\\ V semantics."
msgstr ""

#
#. #-#-#-#-#  archlinux: signal.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  System V semantics are also provided if one uses the separate
#.  .BR sysv_signal (3)
#.  function.
#.  .IP \[bu]
#.  The
#.  .BR signal ()
#.  function in Linux libc4 and libc5 provide System\ V semantics.
#.  If one on a libc5 system includes
#.  .I <bsd/signal.h>
#.  instead of
#.  .IR <signal.h> ,
#.  then
#.  .BR signal ()
#.  provides BSD semantics.
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: signal.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  System V semantics are also provided if one uses the separate
#.  .BR sysv_signal (3)
#.  function.
#.  .IP *
#.  The
#.  .BR signal ()
#.  function in Linux libc4 and libc5 provide System\ V semantics.
#.  If one on a libc5 system includes
#.  .I <bsd/signal.h>
#.  instead of
#.  .IR <signal.h> ,
#.  then
#.  .BR signal ()
#.  provides BSD semantics.
#. type: Plain text
#. #-#-#-#-#  debian-unstable: signal.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  System V semantics are also provided if one uses the separate
#.  .BR sysv_signal (3)
#.  function.
#.  .IP \[bu]
#.  The
#.  .BR signal ()
#.  function in Linux libc4 and libc5 provide System\ V semantics.
#.  If one on a libc5 system includes
#.  .I <bsd/signal.h>
#.  instead of
#.  .IR <signal.h> ,
#.  then
#.  .BR signal ()
#.  provides BSD semantics.
#. type: Plain text
#. #-#-#-#-#  fedora-40: signal.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  System V semantics are also provided if one uses the separate
#.  .BR sysv_signal (3)
#.  function.
#.  .IP \[bu]
#.  The
#.  .BR signal ()
#.  function in Linux libc4 and libc5 provide System\ V semantics.
#.  If one on a libc5 system includes
#.  .I <bsd/signal.h>
#.  instead of
#.  .IR <signal.h> ,
#.  then
#.  .BR signal ()
#.  provides BSD semantics.
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: signal.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  System V semantics are also provided if one uses the separate
#.  .BR sysv_signal (3)
#.  function.
#.  .IP \[bu]
#.  The
#.  .BR signal ()
#.  function in Linux libc4 and libc5 provide System\ V semantics.
#.  If one on a libc5 system includes
#.  .I <bsd/signal.h>
#.  instead of
#.  .IR <signal.h> ,
#.  then
#.  .BR signal ()
#.  provides BSD semantics.
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: signal.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  System V semantics are also provided if one uses the separate
#.  .BR sysv_signal (3)
#.  function.
#.  .IP \[bu]
#.  The
#.  .BR signal ()
#.  function in Linux libc4 and libc5 provide System\ V semantics.
#.  If one on a libc5 system includes
#.  .I <bsd/signal.h>
#.  instead of
#.  .IR <signal.h> ,
#.  then
#.  .BR signal ()
#.  provides BSD semantics.
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-6: signal.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  System V semantics are also provided if one uses the separate
#.  .BR sysv_signal (3)
#.  function.
#.  .IP *
#.  The
#.  .BR signal ()
#.  function in Linux libc4 and libc5 provide System\ V semantics.
#.  If one on a libc5 system includes
#.  .I <bsd/signal.h>
#.  instead of
#.  .IR <signal.h> ,
#.  then
#.  .BR signal ()
#.  provides BSD semantics.
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: signal.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  System V semantics are also provided if one uses the separate
#.  .BR sysv_signal (3)
#.  function.
#.  .IP \[bu]
#.  The
#.  .BR signal ()
#.  function in Linux libc4 and libc5 provide System\ V semantics.
#.  If one on a libc5 system includes
#.  .I <bsd/signal.h>
#.  instead of
#.  .IR <signal.h> ,
#.  then
#.  .BR signal ()
#.  provides BSD semantics.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"By default, in glibc 2 and later, the B<signal>()  wrapper function does not "
"invoke the kernel system call.  Instead, it calls B<sigaction>(2)  using "
"flags that supply BSD semantics.  This default behavior is provided as long "
"as a suitable feature test macro is defined: B<_BSD_SOURCE> on glibc 2.19 "
"and earlier or B<_DEFAULT_SOURCE> in glibc 2.19 and later.  (By default, "
"these macros are defined; see B<feature_test_macros>(7)  for details.)  If "
"such a feature test macro is not defined, then B<signal>()  provides "
"System\\ V semantics."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The effects of B<signal>()  in a multithreaded process are unspecified."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"According to POSIX, the behavior of a process is undefined after it ignores "
"a B<SIGFPE>, B<SIGILL>, or B<SIGSEGV> signal that was not generated by "
"B<kill>(2)  or B<raise>(3).  Integer division by zero has undefined result.  "
"On some architectures it will generate a B<SIGFPE> signal.  (Also dividing "
"the most negative integer by -1 may generate B<SIGFPE>.)  Ignoring this "
"signal might lead to an endless loop."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"See B<sigaction>(2)  for details on what happens when the disposition "
"B<SIGCHLD> is set to B<SIG_IGN>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"See B<signal-safety>(7)  for a list of the async-signal-safe functions that "
"can be safely called from inside a signal handler."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<kill>(1), B<alarm>(2), B<kill>(2), B<pause>(2), B<sigaction>(2), "
"B<signalfd>(2), B<sigpending>(2), B<sigprocmask>(2), B<sigsuspend>(2), "
"B<bsd_signal>(3), B<killpg>(3), B<raise>(3), B<siginterrupt>(3), "
"B<sigqueue>(3), B<sigsetops>(3), B<sigvec>(3), B<sysv_signal>(3), "
"B<signal>(7)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, C99."
msgstr ""

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""

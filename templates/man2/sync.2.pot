# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-05-01 15:55+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "sync"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "sync, syncfs - commit filesystem caches to disk"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<void sync(void);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int syncfs(int >I<fd>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<sync>():"
msgstr ""

#.     || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"    _XOPEN_SOURCE E<gt>= 500\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<syncfs>():"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "    _GNU_SOURCE\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<sync>()  causes all pending modifications to filesystem metadata and "
"cached file data to be written to the underlying filesystems."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<syncfs>()  is like B<sync>(), but synchronizes just the filesystem "
"containing file referred to by the open file descriptor I<fd>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<syncfs>()  returns 0 on success; on error, it returns -1 and sets I<errno> "
"to indicate the error."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<sync>()  is always successful."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<syncfs>()  can fail for at least the following reasons:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<fd> is not a valid file descriptor."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EIO>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"An error occurred during synchronization.  This error may relate to data "
"written to any file on the filesystem, or on metadata related to the "
"filesystem itself."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSPC>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Disk space was exhausted while synchronizing."
msgstr ""

#. type: TQ
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<EDQUOT>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Data was written to a file on NFS or another filesystem which does not "
"allocate space at the time of a B<write>(2)  system call, and some previous "
"write failed due to insufficient storage space."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"According to the standard specification (e.g., POSIX.1-2001), B<sync>()  "
"schedules the writes, but may return before the actual writing is done.  "
"However Linux waits for I/O completions, and thus B<sync>()  or B<syncfs>()  "
"provide the same guarantees as B<fsync>()  called on every file in the "
"system or filesystem respectively."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<sync>()"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<syncfs>()"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Linux."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2001, SVr4, 4.3BSD."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Linux 2.6.39, glibc 2.14."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Since glibc 2.2.2, the Linux prototype for B<sync>()  is as listed above, "
"following the various standards.  In glibc 2.2.1 and earlier, it was \"int "
"sync(void)\", and B<sync>()  always returned 0."
msgstr ""

#.  commit 735e4ae5ba28c886d249ad04d3c8cc097dad6336
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In mainline kernel versions prior to Linux 5.8, B<syncfs>()  will fail only "
"when passed a bad file descriptor (B<EBADF>).  Since Linux 5.8, B<syncfs>()  "
"will also report an error if one or more inodes failed to be written back "
"since the last B<syncfs>()  call."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Before Linux 1.3.20, Linux did not wait for I/O to complete before returning."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<sync>(1), B<fdatasync>(2), B<fsync>(2)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: TP
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "B<ENOSPC>, B<EDQUOT>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<syncfs>()  first appeared in Linux 2.6.39; library support was added in "
"glibc 2.14."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<sync>(): POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<syncfs>()  is Linux-specific."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""

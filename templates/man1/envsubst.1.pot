# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-29 09:38+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "ENVSUBST"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "September 2023"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "GNU gettext-runtime 0.22.2"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "envsubst - substitutes environment variables in shell format strings"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<envsubst> [I<\\,OPTION\\/>] [I<\\,SHELL-FORMAT\\/>]"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Substitutes the values of environment variables."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "Operation mode:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--variables>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "output the variables occurring in SHELL-FORMAT"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "Informative output:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "display this help and exit"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "output version information and exit"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"In normal operation mode, standard input is copied to standard output, with "
"references to environment variables of the form $VARIABLE or ${VARIABLE} "
"being replaced with the corresponding values.  If a SHELL-FORMAT is given, "
"only those environment variables that are referenced in SHELL-FORMAT are "
"substituted; otherwise all environment variables references occurring in "
"standard input are substituted."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"When B<--variables> is used, standard input is ignored, and the output "
"consists of the environment variables that are referenced in SHELL-FORMAT, "
"one per line."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Written by Bruno Haible."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"Report bugs in the bug tracker at E<lt>https://savannah.gnu.org/projects/"
"gettextE<gt> or by email to E<lt>bug-gettext@gnu.orgE<gt>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr ""

#. type: Plain text
#: archlinux mageia-cauldron opensuse-tumbleweed
msgid ""
"Copyright \\(co 2003-2023 Free Software Foundation, Inc.  License GPLv3+: "
"GNU GPL version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The full documentation for B<envsubst> is maintained as a Texinfo manual.  "
"If the B<info> and B<envsubst> programs are properly installed at your site, "
"the command"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<info envsubst>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "February 2023"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "GNU gettext-runtime 0.21"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Copyright \\(co 2003-2020 Free Software Foundation, Inc.  License GPLv3+: "
"GNU GPL version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"
msgstr ""

#. type: TH
#: debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "February 2024"
msgstr ""

#. type: TH
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "GNU gettext-runtime 0.22.5"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "October 2022"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU gettext-runtime 0.21.1"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Copyright \\(co 2003-2022 Free Software Foundation, Inc.  License GPLv3+: "
"GNU GPL version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"
msgstr ""

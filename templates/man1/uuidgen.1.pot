# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-29 09:57+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "UUIDGEN"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "uuidgen - create a new UUID value"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<uuidgen> [options]"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<uuidgen> program creates (and prints) a new universally unique "
"identifier (UUID) using the B<libuuid>(3) library. The new UUID can "
"reasonably be considered unique among all UUIDs created on the local system, "
"and among UUIDs created on other systems in the past and in the future."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"There are three types of UUIDs which B<uuidgen> can generate: time-based "
"UUIDs, random-based UUIDs, and hash-based UUIDs. By default B<uuidgen> will "
"generate a random-based UUID if a high-quality random number generator is "
"present. Otherwise, it will choose a time-based UUID. It is possible to "
"force the generation of one of these first two UUID types by using the B<--"
"random> or B<--time> options."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The third type of UUID is generated with the B<--md5> or B<--sha1> options, "
"followed by B<--namespace> I<namespace> and B<--name> I<name>. The "
"I<namespace> may either be a well-known UUID, or else an alias to one of the "
"well-known UUIDs defined in RFC 4122, that is B<@dns>, B<@url>, B<@oid>, or "
"B<@x500>. The I<name> is an arbitrary string value. The generated UUID is "
"the digest of the concatenation of the namespace UUID and the name value, "
"hashed with the MD5 or SHA1 algorithms. It is, therefore, a predictable "
"value which may be useful when UUIDs are being used as handles or nonces for "
"more complex values or values which shouldn\\(cqt be disclosed directly. See "
"the RFC for more information."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-r>, B<--random>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Generate a random-based UUID. This method creates a UUID consisting mostly "
"of random bits. It requires that the operating system has a high quality "
"random number generator, such as I</dev/random>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-t>, B<--time>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Generate a time-based UUID. This method creates a UUID based on the system "
"clock plus the system\\(cqs ethernet hardware address, if present."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-m>, B<--md5>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Use MD5 as the hash algorithm."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-s>, B<--sha1>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Use SHA1 as the hash algorithm."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-n>, B<--namespace> I<namespace>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Generate the hash with the I<namespace> prefix. The I<namespace> is UUID, or "
"\\(aq@ns\\(aq where \"ns\" is well-known predefined UUID addressed by "
"namespace name (see above)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-N>, B<--name> I<name>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Generate the hash of the I<name>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-x>, B<--hex>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Interpret name I<name> as a hexadecimal string."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "OSF DCE 1.1"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "uuidgen --sha1 --namespace @dns --name \"www.example.com\""
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<uuidgen> was written by Andreas Dilger for B<libuuid>(3)."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<uuidparse>(1), B<libuuid>(3),"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<uuidgen> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""

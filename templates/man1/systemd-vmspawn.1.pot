# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-05-01 15:57+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SYSTEMD-VMSPAWN"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "systemd 255"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "systemd-vmspawn"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid "systemd-vmspawn - Spawn an OS in a virtual machine"
msgstr ""

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<systemd-vmspawn> [OPTIONS...] [ARGS...]"
msgstr ""

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"B<systemd-vmspawn> may be used to start a virtual machine from an OS "
"image\\&. In many ways it is similar to B<systemd-nspawn>(1), but it "
"launches a full virtual machine instead of using namespaces\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Note: on Ubuntu/Debian derivatives systemd-vmspawn requires the user to be "
"in the \"kvm\" group to use the VSock options\\&."
msgstr ""

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"The excess arguments are passed as extra kernel command line arguments using "
"SMBIOS\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "The following options are understood:"
msgstr ""

#. type: SS
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Image Options"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<-i>, B<--image=>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Root file system disk image (or device node) for the virtual machine\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "Added in version 255\\&."
msgstr ""

#. type: SS
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Host Configuration"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<--qemu-smp=>I<SMP>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Configures the number of CPUs to start the virtual machine with\\&. Defaults "
"to 1\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<--qemu-mem=>I<MEM>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Configures the amount of memory to start the virtual machine with\\&. "
"Defaults to 2G\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<--qemu-kvm=>I<BOOL>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Configures whether to use KVM\\&. If the option is not specified KVM support "
"will be detected automatically\\&. If true, KVM is always used, and if "
"false, KVM is never used\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<--qemu-vsock=>I<BOOL>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "Configure whether to use VSock networking\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"If the option is not specified VSock support will be detected "
"automatically\\&. If yes is specified VSocks are always used, and vice versa "
"if no is set VSocks are never used\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<--vsock-cid=>I<CID>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "Configure vmspawn to use a specific CID for the guest\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"If the option is not specified or an empty argument is supplied the guest "
"will be assigned a random CID\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Valid CIDs are in the range B<3> to B<4294967294> (B<0xFFFF_FFFE>)\\&. CIDs "
"outside of this range are reserved\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<--qemu-gui>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "Start QEMU in graphical mode\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<--secure-boot=>I<BOOL>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "Configure whether to search for firmware which supports Secure Boot\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"If the option is not specified the first firmware which is detected will be "
"used\\&. If the option is set to yes then the first firmware with Secure "
"Boot support will be selected\\&. If no is specified then the first firmware "
"without Secure Boot will be selected\\&."
msgstr ""

#. type: SS
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "System Identity Options"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<-M>, B<--machine=>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Sets the machine name for this container\\&. This name may be used to "
"identify this container during its runtime (for example in tools like "
"B<machinectl>(1)  and similar)\\&."
msgstr ""

#. type: SS
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Credentials"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<--load-credential=>I<ID>:I<PATH>, B<--set-credential=>I<ID>:I<VALUE>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Pass a credential to the container\\&. These two options correspond to the "
"I<LoadCredential=> and I<SetCredential=> settings in unit files\\&. See "
"B<systemd.exec>(5)  for details about these concepts, as well as the syntax "
"of the option\\*(Aqs arguments\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid ""
"In order to embed binary data into the credential data for B<--set-"
"credential=>, use C-style escaping (i\\&.e\\&.  \"\\en\" to embed a newline, "
"or \"\\ex00\" to embed a B<NUL> byte)\\&. Note that the invoking shell might "
"already apply unescaping once, hence this might require double escaping!"
msgstr ""

#. type: SS
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Other"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<--no-pager>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "Do not pipe output into a pager\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "Print a short help text and exit\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<--version>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "Print a short version string and exit\\&."
msgstr ""

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "I<$SYSTEMD_LOG_LEVEL>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"The maximum log level of emitted messages (messages with a higher log level, "
"i\\&.e\\&. less important ones, will be suppressed)\\&. Either one of (in "
"order of decreasing importance)  B<emerg>, B<alert>, B<crit>, B<err>, "
"B<warning>, B<notice>, B<info>, B<debug>, or an integer in the range "
"0\\&...7\\&. See B<syslog>(3)  for more information\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "I<$SYSTEMD_LOG_COLOR>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"A boolean\\&. If true, messages written to the tty will be colored according "
"to priority\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"This setting is only useful when messages are written directly to the "
"terminal, because B<journalctl>(1)  and other tools that display logs will "
"color messages based on the log level on their own\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "I<$SYSTEMD_LOG_TIME>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"A boolean\\&. If true, console log messages will be prefixed with a "
"timestamp\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"This setting is only useful when messages are written directly to the "
"terminal or a file, because B<journalctl>(1)  and other tools that display "
"logs will attach timestamps based on the entry metadata on their own\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "I<$SYSTEMD_LOG_LOCATION>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"A boolean\\&. If true, messages will be prefixed with a filename and line "
"number in the source code where the message originates\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Note that the log location is often attached as metadata to journal entries "
"anyway\\&. Including it directly in the message text can nevertheless be "
"convenient when debugging programs\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "I<$SYSTEMD_LOG_TID>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"A boolean\\&. If true, messages will be prefixed with the current numerical "
"thread ID (TID)\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Note that the this information is attached as metadata to journal entries "
"anyway\\&. Including it directly in the message text can nevertheless be "
"convenient when debugging programs\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "I<$SYSTEMD_LOG_TARGET>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"The destination for log messages\\&. One of B<console> (log to the attached "
"tty), B<console-prefixed> (log to the attached tty but with prefixes "
"encoding the log level and \"facility\", see B<syslog>(3), B<kmsg> (log to "
"the kernel circular log buffer), B<journal> (log to the journal), B<journal-"
"or-kmsg> (log to the journal if available, and to kmsg otherwise), B<auto> "
"(determine the appropriate log target automatically, the default), B<null> "
"(disable log output)\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "I<$SYSTEMD_LOG_RATELIMIT_KMSG>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Whether to ratelimit kmsg or not\\&. Takes a boolean\\&. Defaults to "
"\"true\"\\&. If disabled, systemd will not ratelimit messages written to "
"kmsg\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "I<$SYSTEMD_PAGER>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Pager to use when B<--no-pager> is not given; overrides I<$PAGER>\\&. If "
"neither I<$SYSTEMD_PAGER> nor I<$PAGER> are set, a set of well-known pager "
"implementations are tried in turn, including B<less>(1)  and B<more>(1), "
"until one is found\\&. If no pager implementation is discovered no pager is "
"invoked\\&. Setting this environment variable to an empty string or the "
"value \"cat\" is equivalent to passing B<--no-pager>\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Note: if I<$SYSTEMD_PAGERSECURE> is not set, I<$SYSTEMD_PAGER> (as well as "
"I<$PAGER>) will be silently ignored\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "I<$SYSTEMD_LESS>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "Override the options passed to B<less> (by default \"FRSXMK\")\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "Users might want to change two options in particular:"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<K>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"This option instructs the pager to exit immediately when Ctrl+C is "
"pressed\\&. To allow B<less> to handle Ctrl+C itself to switch back to the "
"pager command prompt, unset this option\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"If the value of I<$SYSTEMD_LESS> does not include \"K\", and the pager that "
"is invoked is B<less>, Ctrl+C will be ignored by the executable, and needs "
"to be handled by the pager\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<X>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"This option instructs the pager to not send termcap initialization and "
"deinitialization strings to the terminal\\&. It is set by default to allow "
"command output to remain visible in the terminal even after the pager "
"exits\\&. Nevertheless, this prevents some pager functionality from working, "
"in particular paged output cannot be scrolled with the mouse\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid ""
"Note that setting the regular I<$LESS> environment variable has no effect "
"for B<less> invocations by systemd tools\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "See B<less>(1)  for more discussion\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "I<$SYSTEMD_LESSCHARSET>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Override the charset passed to B<less> (by default \"utf-8\", if the "
"invoking terminal is determined to be UTF-8 compatible)\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron
msgid ""
"Note that setting the regular I<$LESSCHARSET> environment variable has no "
"effect for B<less> invocations by systemd tools\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "I<$SYSTEMD_PAGERSECURE>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Takes a boolean argument\\&. When true, the \"secure\" mode of the pager is "
"enabled; if false, disabled\\&. If I<$SYSTEMD_PAGERSECURE> is not set at "
"all, secure mode is enabled if the effective UID is not the same as the "
"owner of the login session, see B<geteuid>(2)  and "
"B<sd_pid_get_owner_uid>(3)\\&. In secure mode, B<LESSSECURE=1> will be set "
"when invoking the pager, and the pager shall disable commands that open or "
"create new files or start new subprocesses\\&. When I<$SYSTEMD_PAGERSECURE> "
"is not set at all, pagers which are not known to implement secure mode will "
"not be used\\&. (Currently only B<less>(1)  implements secure mode\\&.)"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Note: when commands are invoked with elevated privileges, for example under "
"B<sudo>(8)  or B<pkexec>(1), care must be taken to ensure that unintended "
"interactive features are not enabled\\&. \"Secure\" mode for the pager may "
"be enabled automatically as describe above\\&. Setting "
"I<SYSTEMD_PAGERSECURE=0> or not removing it from the inherited environment "
"allows the user to invoke arbitrary commands\\&. Note that if the "
"I<$SYSTEMD_PAGER> or I<$PAGER> variables are to be honoured, "
"I<$SYSTEMD_PAGERSECURE> must be set too\\&. It might be reasonable to "
"completely disable the pager using B<--no-pager> instead\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "I<$SYSTEMD_COLORS>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Takes a boolean argument\\&. When true, B<systemd> and related utilities "
"will use colors in their output, otherwise the output will be monochrome\\&. "
"Additionally, the variable can take one of the following special values: "
"\"16\", \"256\" to restrict the use of colors to the base 16 or 256 ANSI "
"colors, respectively\\&. This can be specified to override the automatic "
"decision based on I<$TERM> and what the console is connected to\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "I<$SYSTEMD_URLIFY>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"The value must be a boolean\\&. Controls whether clickable links should be "
"generated in the output for terminal emulators supporting this\\&. This can "
"be specified to override the decision that B<systemd> makes based on "
"I<$TERM> and other conditions\\&."
msgstr ""

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<Example\\ \\&1.\\ \\&Run an Arch Linux VM image generated by mkosi>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"$ mkosi -d arch -p systemd -p linux --autologin -o image\\&.raw -f build\n"
"$ systemd-vmspawn --image=image\\&.raw\n"
msgstr ""

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"If an error occurred the value errno is propagated to the return code\\&. If "
"EXIT_STATUS is supplied by the running image that is returned\\&. Otherwise "
"EXIT_SUCCESS is returned\\&."
msgstr ""

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<systemd>(1), B<mkosi>(1)"
msgstr ""

#. type: Plain text
#: fedora-40 opensuse-tumbleweed
msgid "systemd-vmspawn - Spawn an OS in a virtual machine\\&."
msgstr ""

#. type: Plain text
#: fedora-40 opensuse-tumbleweed
msgid ""
"In order to embed binary data into the credential data for B<--set-"
"credential=>, use C-style escaping (i\\&.e\\&.  \"\\en\" to embed a newline, "
"or \"\\ex00\" to embed a B<NUL> byte)\\&. Note that the invoking shell might "
"already apply unescaping once, hence this might require double escaping!\\&."
msgstr ""

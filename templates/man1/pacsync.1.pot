# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-05-01 15:47+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#.  ========================================================================
#. type: IX
#: archlinux
#, no-wrap
msgid "Title"
msgstr ""

#.  ========================================================================
#. type: IX
#: archlinux
#, no-wrap
msgid "PACSYNC 1"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "PACSYNC"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "2024-04-16"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "pacutils"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "pacsync"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pacsync - update sync databases"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Header"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"\\& pacsync [options] [E<lt>syncdbE<gt>]...  \\& pacsync (--help|--version)"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Update sync databases.  If no I<syncdb> names are provided all databases "
"will by updated."
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--config>=I<path>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "Item"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "--config=path"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Set an alternate configuration file path."
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--dbext>=I<extension>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "--dbext=extension"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Set an alternate sync database extension."
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--dbpath>=I<path>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "--dbpath=path"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Set an alternate database path."
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--logfile>=I<path>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "--logfile=path"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Set an alternate log file path."
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--no-timeout>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "--no-timeout"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Disable low-speed timeouts for downloads."
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--root>=I<path>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "--root=path"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Set an alternate installation root."
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--sysroot>=I<path>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "--sysroot=path"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Set an alternate system root.  See B<pacutils-sysroot>\\|(7)."
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--debug>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "--debug"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Display additional debugging information."
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--updated>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "--updated"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Return true only if a database was actually updated."
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--force>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "--force"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Update databases even if already up-to-date."
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--help>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "--help"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Display usage information and exit."
msgstr ""

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--version>"
msgstr ""

#. type: IX
#: archlinux
#, no-wrap
msgid "--version"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Display version information and exit."
msgstr ""

# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-29 09:37+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "COLCRT"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "colcrt - filter nroff output for CRT previewing"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<colcrt> [options] [I<file> ...]"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<colcrt> provides virtual half-line and reverse line feed sequences for "
"terminals without such capability, and on which overstriking is destructive. "
"Half-line characters and underlining (changed to dashing `-\\(aq) are placed "
"on new lines in between the normal output lines."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<->, B<--no-underlining>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Suppress all underlining. This option is especially useful for previewing "
"I<allboxed> tables from B<tbl>(1)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-2>, B<--half-lines>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Causes all half-lines to be printed, effectively double spacing the output. "
"Normally, a minimal space output format is used which will suppress empty "
"lines. The program never suppresses two consecutive empty lines, however. "
"The B<-2> option is useful for sending output to the line printer when the "
"output contains superscripts and subscripts which would otherwise be "
"partially invisible."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The B<colcrt> command appeared in 3.0BSD."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Should fold underlines onto blanks even with the B<-> option so that a true "
"underline character would show."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Can\\(cqt back up more than 102 lines."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"General overstriking is lost; as a special case \\(aq|\\(aq overstruck with "
"\\(aq-\\(aq or underline becomes \\(aq+\\(aq."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Lines are trimmed to 132 characters."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Some provision should be made for processing superscripts and subscripts in "
"documents which are already double-spaced."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "A typical use of B<colcrt> would be:"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<tbl exum2.n | nroff -ms | colcrt - | more>"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<col>(1), B<more>(1), B<nroff>(1), B<troff>(1), B<ul>(1)"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<colcrt> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""

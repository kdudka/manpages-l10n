# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-01 17:00+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "MAKECHROOTPKG"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "02/14/2024"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \" "
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \""
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux
msgid "makechrootpkg - Build a PKGBUILD in a given chroot environment"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "makechrootpkg [OPTIONS] -r E<lt>chrootdirE<gt> [--] [makepkg args]"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Run this script in a directory containing a PKGBUILD to build a package "
"inside a clean chroot\\&. Arguments passed to this script after the end-of-"
"options marker (--) will be passed to makepkg\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"The chroot dir consists of the following directories: E<lt>chrootdirE<gt>/"
"{root, copy} but only \"root\" is required by default\\&. The working copy "
"will be created as needed"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"The chroot \"root\" directory must be created via the following command: "
"mkarchroot E<lt>chrootdirE<gt>/root base-devel"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"This script reads {SRC,SRCPKG,PKG,LOG}DEST, MAKEFLAGS and PACKAGER from "
"makepkg\\&.conf(5), if those variables are not part of the environment\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Default makepkg args: --syncdeps --noconfirm --log --holdver --skipinteg"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-h>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Show this usage message"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-c>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Clean the chroot before building"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-d> E<lt>dirE<gt>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Bind directory into build chroot as read-write"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-D> E<lt>dirE<gt>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Bind directory into build chroot as read-only"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-u>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Update the working copy of the chroot before building This is useful for "
"rebuilds without dirtying the pristine chroot"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-r> E<lt>dirE<gt>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "The chroot dir to use"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-I> E<lt>pkgE<gt>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Install a package into the working copy of the chroot"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-l> E<lt>copyE<gt>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"The directory to use as the working copy of the chroot Useful for "
"maintaining multiple copies Default: $USER"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-n>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Run namcap on the build package"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-C>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Run checkpkg on the build package"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-T>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Build in a temporary directory"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-U>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Run makepkg as a specified user"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-x> E<lt>whenE<gt>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Inspect chroot after build, possible modes are I<never> (default), I<always> "
"or I<failure>"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "HOMEPAGE"
msgstr ""

#. type: Plain text
#: archlinux
msgid "I<https://gitlab\\&.archlinux\\&.org/archlinux/devtools>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Please report bugs and feature requests in the issue tracker\\&. Please do "
"your best to provide a reproducible test case for bugs\\&."
msgstr ""

# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-29 09:47+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "PRLIMIT"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "prlimit - get and set process resource limits"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<prlimit> [options] [B<--resource>[=I<limits>]] [B<--pid> I<PID>]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<prlimit> [options] [B<--resource>[=I<limits>]] I<command> [I<argument>...]"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Given a process ID and one or more resources, B<prlimit> tries to retrieve "
"and/or modify the limits."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"When I<command> is given, B<prlimit> will run this command with the given "
"arguments."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The I<limits> parameter is composed of a soft and a hard value, separated by "
"a colon (:), in order to modify the existing values. If no I<limits> are "
"given, B<prlimit> will display the current values. If one of the values is "
"not given, then the existing one will be used. To specify the unlimited or "
"infinity limit (B<RLIM_INFINITY>), the -1 or \\(aqunlimited\\(aq string can "
"be passed."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Because of the nature of limits, the soft limit must be lower or equal to "
"the high limit (also called the ceiling). To see all available resource "
"limits, refer to the B<RESOURCE OPTIONS> section."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "I<soft>:_hard_ Specify both limits."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "I<soft>: Specify only the soft limit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ":I<hard> Specify only the hard limit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "I<value> Specify both limits to the same value."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "GENERAL OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--noheadings>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Do not print a header line."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-o, --output> I<list>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Define the output columns to use. If no output arrangement is specified, "
"then a default set is used. Use B<--help> to get a list of all supported "
"columns."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-p, --pid>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Specify the process id; if none is given, the running process will be used."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--raw>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Use the raw output format."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--verbose>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Verbose mode."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "RESOURCE OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-c, --core>[=I<limits>]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Maximum size of a core file."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-d, --data>[=I<limits>]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Maximum data size."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-e, --nice>[=I<limits>]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Maximum nice priority allowed to raise."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-f, --fsize>[=I<limits>]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Maximum file size."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-i, --sigpending>[=I<limits>]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Maximum number of pending signals."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-l, --memlock>[=I<limits>]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Maximum locked-in-memory address space."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-m, --rss>[=I<limits>]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Maximum Resident Set Size (RSS)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-n, --nofile>[=I<limits>]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Maximum number of open files."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-q, --msgqueue>[=I<limits>]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Maximum number of bytes in POSIX message queues."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-r, --rtprio>[=I<limits>]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Maximum real-time priority."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-s, --stack>[=I<limits>]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Maximum size of the stack."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-t, --cpu>[=I<limits>]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "CPU time, in seconds."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-u, --nproc>[=I<limits>]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Maximum number of processes."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-v, --as>[=I<limits>]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Address space limit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-x, --locks>[=I<limits>]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Maximum number of file locks held."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-y, --rttime>[=I<limits>]"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Timeout for real-time tasks."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<prlimit>(2) system call is supported since Linux 2.6.36, older kernels "
"will break this program."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<prlimit --pid 13134>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display limit values for all current resources."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<prlimit --pid 13134 --rss --nofile=1024:4095>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Display the limits of the RSS, and set the soft and hard limits for the "
"number of open files to 1024 and 4095, respectively."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<prlimit --pid 13134 --nproc=512:>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Modify only the soft limit for the number of processes."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<prlimit --pid $$ --nproc=unlimited>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Set for the current process both the soft and ceiling values for the number "
"of processes to unlimited."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<prlimit --cpu=10 sort -u hugefile>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Set both the soft and hard CPU time limit to ten seconds and run B<sort>(1)."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "- In memory of Dennis M. Ritchie."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<ulimit>(1p), B<prlimit>(2)"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<prlimit> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""

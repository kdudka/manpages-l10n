# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-29 09:47+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "PF2AFM"
msgstr ""

#. type: TH
#: archlinux debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "06 March 2024"
msgstr ""

#. type: TH
#: archlinux debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "10.03.0"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "Ghostscript"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"pf2afm - Make an AFM file from Postscript (PFB/PFA/PFM) font files using "
"ghostscript"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
msgid "B<pf2afm> I<fontfilename>"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"This script invokes B<gs>(1)  to make an AFM file from PFB / PFA and "
"(optionally) PFM files.  Output goes to I<fontfilename.afm>, which must not "
"already exist."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
msgid "gs(1)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
msgid "pf2afm.ps in the Ghostscript lib directory."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "VERSION"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron opensuse-tumbleweed
msgid "This document was last revised for Ghostscript version 10.03.0."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Artifex Software, Inc. are the primary maintainers of Ghostscript.  This "
"manpage by George Ferguson."
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "21 September 2022"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "10.00.0"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "This document was last revised for Ghostscript version 10.00.0."
msgstr ""

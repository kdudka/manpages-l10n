# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-29 09:44+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "LSLOGINS"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "lslogins - display information about known users in the system"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<lslogins> [options] [B<-s>|B<-u>[=I<UID>]] [B<-g> I<groups>] [B<-l> "
"I<logins>] [I<username>]"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Examine the wtmp and btmp logs, I</etc/shadow> (if necessary) and I</passwd> "
"and output the desired data."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The optional argument I<username> forces B<lslogins> to print all available "
"details about the specified user only. In this case the output format is "
"different than in case of B<-l> or B<-g> and unknown is I<username> reported "
"as an error."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The default action is to list info about all the users in the system."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-a>, B<--acc-expiration>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Display data about the date of last password change and the account "
"expiration date (see B<shadow>(5) for more info). (Requires root privileges.)"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--btmp-file> I<path>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Alternate path for btmp."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-c>, B<--colon-separate>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Separate info about each user with a colon instead of a newline."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-e>, B<--export>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Output data in the format of NAME=VALUE. See also option B<--shell>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-f>, B<--failed>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display data about the users\\(aq last failed login attempts."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-G>, B<--supp-groups>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Show information about supplementary groups."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-g>, B<--groups>=I<groups>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Only show data of users belonging to I<groups>. More than one group may be "
"specified; the list has to be comma-separated. Unknown group names are "
"ignored."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Note that the relation between user and group may be invisible for the "
"primary group if the user is not explicitly specified as group member (e.g., "
"in I</etc/group>). If the command B<lslogins> scans for groups then it uses "
"the groups database only, and the user database with primary GID is not used "
"at all."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-L>, B<--last>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Display data containing information about the users\\(aq last login sessions."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-l>, B<--logins>=I<logins>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Only show data of users with a login specified in I<logins> (user names or "
"user IDs). More than one login may be specified; the list has to be comma-"
"separated. Unknown login names are ignored."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-n>, B<--newline>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display each piece of information on a separate line."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--noheadings>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Do not print a header line."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--notruncate>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Don\\(cqt truncate output."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-o>, B<--output> I<list>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Specify which output columns to print. The default list of columns may be "
"extended if I<list> is specified in the format I<+list>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--output-all>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Output all available columns. B<--help> to get a list of all supported "
"columns."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-p>, B<--pwd>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display information related to login by password (see also B<-afL>)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-r>, B<--raw>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Raw output (no columnation)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-s>, B<--system-accs>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Show system accounts. These are by default all accounts with a UID between "
"101 and 999 (inclusive), with the exception of either nobody or nfsnobody "
"(UID 65534). This hardcoded default may be overwritten by parameters "
"B<SYS_UID_MIN> and B<SYS_UID_MAX> in the file I</etc/login.defs>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--time-format> I<type>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Display dates in short, full or iso format. The default is short, this time "
"format is designed to be space efficient and human readable."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-u>, B<--user-accs>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Show user accounts. These are by default all accounts with UID above 1000 "
"(inclusive), with the exception of either nobody or nfsnobody (UID 65534). "
"This hardcoded default maybe overwritten by parameters UID_MIN and UID_MAX "
"in the file I</etc/login.defs>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--wtmp-file> I<path>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Alternate path for wtmp."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--lastlog> I<path>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Alternate path for B<lastlog>(8)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<y->, B<--shell>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The column name will be modified to contain only characters allowed for "
"shell variable identifiers. This is usable, for example, with B<--export>. "
"Note that this feature has been automatically enabled for B<--export> in "
"version 2.37, but due to compatibility issues, now it\\(cqs necessary to "
"request this behavior by B<--shell>."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-Z>, B<--context>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Display the users\\(aq security context."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-z>, B<--print0>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Delimit user entries with a nul character, instead of a newline."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "0"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "if OK,"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "1"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "if incorrect arguments specified,"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "2"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "if a serious error occurs (e.g., a corrupt log)."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The default UID thresholds are read from I</etc/login.defs>."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<lslogins> utility is inspired by the B<logins> utility, which first "
"appeared in FreeBSD 4.10."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<group>(5), B<passwd>(5), B<shadow>(5), B<utmp>(5)"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<lslogins> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""

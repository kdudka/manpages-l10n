# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-05-01 15:44+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "malloc_info"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2024-02-26"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "malloc_info - export malloc state to a stream"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>malloc.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int malloc_info(int >I<options>B<, FILE *>I<stream>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<malloc_info>()  function exports an XML string that describes the "
"current state of the memory-allocation implementation in the caller.  The "
"string is printed on the file stream I<stream>.  The exported string "
"includes information about all arenas (see B<malloc>(3))."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "As currently implemented, I<options> must be zero."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On success, B<malloc_info>()  returns 0.  On failure, it returns -1, and "
"I<errno> is set to indicate the error."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<options> was nonzero."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<malloc_info>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "GNU."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "glibc 2.10."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The memory-allocation information is provided as an XML string (rather than "
"a C structure)  because the information may change over time (according to "
"changes in the underlying implementation).  The output XML string includes a "
"version field."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<open_memstream>(3)  function can be used to send the output of "
"B<malloc_info>()  directly into a buffer in memory, rather than to a file."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<malloc_info>()  function is designed to address deficiencies in "
"B<malloc_stats>(3)  and B<mallinfo>(3)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The program below takes up to four command-line arguments, of which the "
"first three are mandatory.  The first argument specifies the number of "
"threads that the program should create.  All of the threads, including the "
"main thread, allocate the number of blocks of memory specified by the second "
"argument.  The third argument controls the size of the blocks to be "
"allocated.  The main thread creates blocks of this size, the second thread "
"created by the program allocates blocks of twice this size, the third thread "
"allocates blocks of three times this size, and so on."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The program calls B<malloc_info>()  twice to display the memory-allocation "
"state.  The first call takes place before any threads are created or memory "
"allocated.  The second call is performed after all threads have allocated "
"memory."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In the following example, the command-line arguments specify the creation of "
"one additional thread, and both the main thread and the additional thread "
"allocate 10000 blocks of memory.  After the blocks of memory have been "
"allocated, B<malloc_info>()  shows the state of two allocation arenas."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<getconf GNU_LIBC_VERSION>\n"
"glibc 2.13\n"
"$ B<./a.out 1 10000 100>\n"
"============ Before allocating blocks ============\n"
"E<lt>malloc version=\"1\"E<gt>\n"
"E<lt>heap nr=\"0\"E<gt>\n"
"E<lt>sizesE<gt>\n"
"E<lt>/sizesE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"135168\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"135168\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"135168\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"135168\"/E<gt>\n"
"E<lt>/heapE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"135168\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"135168\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"135168\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"135168\"/E<gt>\n"
"E<lt>/mallocE<gt>\n"
"\\&\n"
"============ After allocating blocks ============\n"
"E<lt>malloc version=\"1\"E<gt>\n"
"E<lt>heap nr=\"0\"E<gt>\n"
"E<lt>sizesE<gt>\n"
"E<lt>/sizesE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"1081344\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"1081344\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"1081344\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"1081344\"/E<gt>\n"
"E<lt>/heapE<gt>\n"
"E<lt>heap nr=\"1\"E<gt>\n"
"E<lt>sizesE<gt>\n"
"E<lt>/sizesE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"1032192\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"1032192\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"1032192\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"1032192\"/E<gt>\n"
"E<lt>/heapE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"2113536\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"2113536\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"2113536\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"2113536\"/E<gt>\n"
"E<lt>/mallocE<gt>\n"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>err.hE<gt>\n"
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>malloc.hE<gt>\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"static size_t        blockSize;\n"
"static size_t        numThreads;\n"
"static unsigned int  numBlocks;\n"
"\\&\n"
"static void *\n"
"thread_func(void *arg)\n"
"{\n"
"    int tn = (int) arg;\n"
"\\&\n"
"    /* The multiplier \\[aq](2 + tn)\\[aq] ensures that each thread (including\n"
"       the main thread) allocates a different amount of memory. */\n"
"\\&\n"
"    for (unsigned int j = 0; j E<lt> numBlocks; j++)\n"
"        if (malloc(blockSize * (2 + tn)) == NULL)\n"
"            err(EXIT_FAILURE, \"malloc-thread\");\n"
"\\&\n"
"    sleep(100);         /* Sleep until main thread terminates. */\n"
"    return NULL;\n"
"}\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int        sleepTime;\n"
"    pthread_t  *thr;\n"
"\\&\n"
"    if (argc E<lt> 4) {\n"
"        fprintf(stderr,\n"
"                \"%s num-threads num-blocks block-size [sleep-time]\\en\",\n"
"                argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    numThreads = atoi(argv[1]);\n"
"    numBlocks = atoi(argv[2]);\n"
"    blockSize = atoi(argv[3]);\n"
"    sleepTime = (argc E<gt> 4) ? atoi(argv[4]) : 0;\n"
"\\&\n"
"    thr = calloc(numThreads, sizeof(*thr));\n"
"    if (thr == NULL)\n"
"        err(EXIT_FAILURE, \"calloc\");\n"
"\\&\n"
"    printf(\"============ Before allocating blocks ============\\en\");\n"
"    malloc_info(0, stdout);\n"
"\\&\n"
"    /* Create threads that allocate different amounts of memory. */\n"
"\\&\n"
"    for (size_t tn = 0; tn E<lt> numThreads; tn++) {\n"
"        errno = pthread_create(&thr[tn], NULL, thread_func,\n"
"                               (void *) tn);\n"
"        if (errno != 0)\n"
"            err(EXIT_FAILURE, \"pthread_create\");\n"
"\\&\n"
"        /* If we add a sleep interval after the start-up of each\n"
"           thread, the threads likely won\\[aq]t contend for malloc\n"
"           mutexes, and therefore additional arenas won\\[aq]t be\n"
"           allocated (see malloc(3)). */\n"
"\\&\n"
"        if (sleepTime E<gt> 0)\n"
"            sleep(sleepTime);\n"
"    }\n"
"\\&\n"
"    /* The main thread also allocates some memory. */\n"
"\\&\n"
"    for (unsigned int j = 0; j E<lt> numBlocks; j++)\n"
"        if (malloc(blockSize) == NULL)\n"
"            err(EXIT_FAILURE, \"malloc\");\n"
"\\&\n"
"    sleep(2);           /* Give all threads a chance to\n"
"                           complete allocations. */\n"
"\\&\n"
"    printf(\"\\en============ After allocating blocks ============\\en\");\n"
"    malloc_info(0, stdout);\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<mallinfo>(3), B<malloc>(3), B<malloc_stats>(3), B<mallopt>(3), "
"B<open_memstream>(3)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<malloc_info>()  was added in glibc 2.10."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "This function is a GNU extension."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"$ B<getconf GNU_LIBC_VERSION>\n"
"glibc 2.13\n"
"$ B<./a.out 1 10000 100>\n"
"============ Before allocating blocks ============\n"
"E<lt>malloc version=\"1\"E<gt>\n"
"E<lt>heap nr=\"0\"E<gt>\n"
"E<lt>sizesE<gt>\n"
"E<lt>/sizesE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"135168\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"135168\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"135168\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"135168\"/E<gt>\n"
"E<lt>/heapE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"135168\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"135168\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"135168\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"135168\"/E<gt>\n"
"E<lt>/mallocE<gt>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"============ After allocating blocks ============\n"
"E<lt>malloc version=\"1\"E<gt>\n"
"E<lt>heap nr=\"0\"E<gt>\n"
"E<lt>sizesE<gt>\n"
"E<lt>/sizesE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"1081344\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"1081344\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"1081344\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"1081344\"/E<gt>\n"
"E<lt>/heapE<gt>\n"
"E<lt>heap nr=\"1\"E<gt>\n"
"E<lt>sizesE<gt>\n"
"E<lt>/sizesE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"1032192\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"1032192\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"1032192\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"1032192\"/E<gt>\n"
"E<lt>/heapE<gt>\n"
"E<lt>total type=\"fast\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>total type=\"rest\" count=\"0\" size=\"0\"/E<gt>\n"
"E<lt>system type=\"current\" size=\"2113536\"/E<gt>\n"
"E<lt>system type=\"max\" size=\"2113536\"/E<gt>\n"
"E<lt>aspace type=\"total\" size=\"2113536\"/E<gt>\n"
"E<lt>aspace type=\"mprotect\" size=\"2113536\"/E<gt>\n"
"E<lt>/mallocE<gt>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"#include E<lt>err.hE<gt>\n"
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>malloc.hE<gt>\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"static size_t        blockSize;\n"
"static size_t        numThreads;\n"
"static unsigned int  numBlocks;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"static void *\n"
"thread_func(void *arg)\n"
"{\n"
"    int tn = (int) arg;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    /* The multiplier \\[aq](2 + tn)\\[aq] ensures that each thread (including\n"
"       the main thread) allocates a different amount of memory. */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    for (unsigned int j = 0; j E<lt> numBlocks; j++)\n"
"        if (malloc(blockSize * (2 + tn)) == NULL)\n"
"            err(EXIT_FAILURE, \"malloc-thread\");\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    sleep(100);         /* Sleep until main thread terminates. */\n"
"    return NULL;\n"
"}\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int        sleepTime;\n"
"    pthread_t  *thr;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    if (argc E<lt> 4) {\n"
"        fprintf(stderr,\n"
"                \"%s num-threads num-blocks block-size [sleep-time]\\en\",\n"
"                argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    numThreads = atoi(argv[1]);\n"
"    numBlocks = atoi(argv[2]);\n"
"    blockSize = atoi(argv[3]);\n"
"    sleepTime = (argc E<gt> 4) ? atoi(argv[4]) : 0;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    thr = calloc(numThreads, sizeof(*thr));\n"
"    if (thr == NULL)\n"
"        err(EXIT_FAILURE, \"calloc\");\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    printf(\"============ Before allocating blocks ============\\en\");\n"
"    malloc_info(0, stdout);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "    /* Create threads that allocate different amounts of memory. */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    for (size_t tn = 0; tn E<lt> numThreads; tn++) {\n"
"        errno = pthread_create(&thr[tn], NULL, thread_func,\n"
"                               (void *) tn);\n"
"        if (errno != 0)\n"
"            err(EXIT_FAILURE, \"pthread_create\");\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"        /* If we add a sleep interval after the start-up of each\n"
"           thread, the threads likely won\\[aq]t contend for malloc\n"
"           mutexes, and therefore additional arenas won\\[aq]t be\n"
"           allocated (see malloc(3)). */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"        if (sleepTime E<gt> 0)\n"
"            sleep(sleepTime);\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "    /* The main thread also allocates some memory. */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    for (unsigned int j = 0; j E<lt> numBlocks; j++)\n"
"        if (malloc(blockSize) == NULL)\n"
"            err(EXIT_FAILURE, \"malloc\");\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    sleep(2);           /* Give all threads a chance to\n"
"                           complete allocations. */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    printf(\"\\en============ After allocating blocks ============\\en\");\n"
"    malloc_info(0, stdout);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""

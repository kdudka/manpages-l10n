# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-05-01 15:50+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "putenv"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "putenv - change or add an environment variable"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr ""

#.  Not: const char *
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int putenv(char *>I<string>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<putenv>():"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"    _XOPEN_SOURCE\n"
"        || /* glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _SVID_SOURCE\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<putenv>()  function adds or changes the value of environment "
"variables.  The argument I<string> is of the form I<name>=I<value>.  If "
"I<name> does not already exist in the environment, then I<string> is added "
"to the environment.  If I<name> does exist, then the value of I<name> in the "
"environment is changed to I<value>.  The string pointed to by I<string> "
"becomes part of the environment, so altering the string changes the "
"environment."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<putenv>()  function returns zero on success.  On failure, it returns a "
"nonzero value, and I<errno> is set to indicate the error."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Insufficient space to allocate new environment."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<putenv>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe const:env"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "POSIX.1-2001, SVr2, 4.3BSD-Reno."
msgstr ""

#. #-#-#-#-#  archlinux: putenv.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .P
#.  Description for libc4, libc5, glibc:
#.  If the argument \fIstring\fP is of the form \fIname\fP,
#.  and does not contain an \[aq]=\[aq] character, then the variable \fIname\fP
#.  is removed from the environment.
#.  If
#.  .BR putenv ()
#.  has to allocate a new array \fIenviron\fP,
#.  and the previous array was also allocated by
#.  .BR putenv (),
#.  then it will be freed.
#.  In no case will the old storage associated
#.  to the environment variable itself be freed.
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: putenv.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .LP
#.  Description for libc4, libc5, glibc:
#.  If the argument \fIstring\fP is of the form \fIname\fP,
#.  and does not contain an \[aq]=\[aq] character, then the variable \fIname\fP
#.  is removed from the environment.
#.  If
#.  .BR putenv ()
#.  has to allocate a new array \fIenviron\fP,
#.  and the previous array was also allocated by
#.  .BR putenv (),
#.  then it will be freed.
#.  In no case will the old storage associated
#.  to the environment variable itself be freed.
#. type: Plain text
#. #-#-#-#-#  debian-unstable: putenv.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .P
#.  Description for libc4, libc5, glibc:
#.  If the argument \fIstring\fP is of the form \fIname\fP,
#.  and does not contain an \[aq]=\[aq] character, then the variable \fIname\fP
#.  is removed from the environment.
#.  If
#.  .BR putenv ()
#.  has to allocate a new array \fIenviron\fP,
#.  and the previous array was also allocated by
#.  .BR putenv (),
#.  then it will be freed.
#.  In no case will the old storage associated
#.  to the environment variable itself be freed.
#. type: Plain text
#. #-#-#-#-#  fedora-40: putenv.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .P
#.  Description for libc4, libc5, glibc:
#.  If the argument \fIstring\fP is of the form \fIname\fP,
#.  and does not contain an \[aq]=\[aq] character, then the variable \fIname\fP
#.  is removed from the environment.
#.  If
#.  .BR putenv ()
#.  has to allocate a new array \fIenviron\fP,
#.  and the previous array was also allocated by
#.  .BR putenv (),
#.  then it will be freed.
#.  In no case will the old storage associated
#.  to the environment variable itself be freed.
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: putenv.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .P
#.  Description for libc4, libc5, glibc:
#.  If the argument \fIstring\fP is of the form \fIname\fP,
#.  and does not contain an \[aq]=\[aq] character, then the variable \fIname\fP
#.  is removed from the environment.
#.  If
#.  .BR putenv ()
#.  has to allocate a new array \fIenviron\fP,
#.  and the previous array was also allocated by
#.  .BR putenv (),
#.  then it will be freed.
#.  In no case will the old storage associated
#.  to the environment variable itself be freed.
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: putenv.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .P
#.  Description for libc4, libc5, glibc:
#.  If the argument \fIstring\fP is of the form \fIname\fP,
#.  and does not contain an \[aq]=\[aq] character, then the variable \fIname\fP
#.  is removed from the environment.
#.  If
#.  .BR putenv ()
#.  has to allocate a new array \fIenviron\fP,
#.  and the previous array was also allocated by
#.  .BR putenv (),
#.  then it will be freed.
#.  In no case will the old storage associated
#.  to the environment variable itself be freed.
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-6: putenv.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .LP
#.  Description for libc4, libc5, glibc:
#.  If the argument \fIstring\fP is of the form \fIname\fP,
#.  and does not contain an \[aq]=\[aq] character, then the variable \fIname\fP
#.  is removed from the environment.
#.  If
#.  .BR putenv ()
#.  has to allocate a new array \fIenviron\fP,
#.  and the previous array was also allocated by
#.  .BR putenv (),
#.  then it will be freed.
#.  In no case will the old storage associated
#.  to the environment variable itself be freed.
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: putenv.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .P
#.  Description for libc4, libc5, glibc:
#.  If the argument \fIstring\fP is of the form \fIname\fP,
#.  and does not contain an \[aq]=\[aq] character, then the variable \fIname\fP
#.  is removed from the environment.
#.  If
#.  .BR putenv ()
#.  has to allocate a new array \fIenviron\fP,
#.  and the previous array was also allocated by
#.  .BR putenv (),
#.  then it will be freed.
#.  In no case will the old storage associated
#.  to the environment variable itself be freed.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<putenv>()  function is not required to be reentrant, and the one in "
"glibc 2.0 is not, but the glibc 2.1 version is."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Since glibc 2.1.2, the glibc implementation conforms to SUSv2: the pointer "
"I<string> given to B<putenv>()  is used.  In particular, this string becomes "
"part of the environment; changing it later will change the environment.  "
"(Thus, it is an error to call B<putenv>()  with an automatic variable as the "
"argument, then return from the calling function while I<string> is still "
"part of the environment.)  However, from glibc 2.0 to glibc 2.1.1, it "
"differs: a copy of the string is used.  On the one hand this causes a memory "
"leak, and on the other hand it violates SUSv2."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The 4.3BSD-Reno version, like glibc 2.0, uses a copy; this is fixed in all "
"modern BSDs."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "SUSv2 removes the I<const> from the prototype, and so does glibc 2.1.3."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The GNU C library implementation provides a nonstandard extension.  If "
"I<string> does not include an equal sign:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "putenv(\"NAME\");\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "then the named variable is removed from the caller's environment."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<clearenv>(3), B<getenv>(3), B<setenv>(3), B<unsetenv>(3), B<environ>(7)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Since glibc 2.1.2, the glibc implementation conforms to SUSv2: the pointer "
"I<string> given to B<putenv>()  is used.  In particular, this string becomes "
"part of the environment; changing it later will change the environment.  "
"(Thus, it is an error to call B<putenv>()  with an automatic variable as the "
"argument, then return from the calling function while I<string> is still "
"part of the environment.)  However, from glibc 2.0 to glibc 2.1.1, it "
"differs:r a copy of the string is used.  On the one hand this causes a "
"memory leak, and on the other hand it violates SUSv2."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "The 4.4BSD version, like glibc 2.0, uses a copy."
msgstr ""

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "POSIX.1-2001, SVr4, 4.3BSD."
msgstr ""

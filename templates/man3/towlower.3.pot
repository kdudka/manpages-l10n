# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-05-01 15:58+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "towlower"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "towlower, towlower_l - convert a wide character to lowercase"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>wctype.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<wint_t towlower(wint_t >I<wc>B<);>\n"
"B<wint_t towlower_l(wint_t >I<wc>B<, locale_t >I<locale>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<towlower_l>():"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.10:\n"
"        _XOPEN_SOURCE E<gt>= 700\n"
"    Before glibc 2.10:\n"
"        _GNU_SOURCE\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<towlower>()  function is the wide-character equivalent of the "
"B<tolower>(3)  function.  If I<wc> is an uppercase wide character, and there "
"exists a lowercase equivalent in the current locale, it returns the "
"lowercase equivalent of I<wc>.  In all other cases, I<wc> is returned "
"unchanged."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<towlower_l>()  function performs the same task, but performs the "
"conversion based on the character type information in the locale specified "
"by I<locale>.  The behavior of B<towlower_l>()  is undefined if I<locale> is "
"the special locale object B<LC_GLOBAL_LOCALE> (see B<duplocale>(3))  or is "
"not a valid locale object handle."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The argument I<wc> must be representable as a I<wchar_t> and be a valid "
"character in the locale or be the value B<WEOF>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<wc> was convertible to lowercase, B<towlower>()  returns its lowercase "
"equivalent; otherwise it returns I<wc>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ""

#. #-#-#-#-#  archlinux: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  debian-unstable: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-40: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-rawhide: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  mageia-cauldron: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-leap-15-6: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-tumbleweed: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<towlower>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe locale"
msgstr ""

#. #-#-#-#-#  archlinux: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  debian-unstable: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-40: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-rawhide: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  mageia-cauldron: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-leap-15-6: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-tumbleweed: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<towlower_l>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "C11, POSIX.1-2008 (XSI)."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "C99, POSIX.1-2001 (XSI).  Obsolete in POSIX.1-2008 (XSI)."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "glibc 2.3.  POSIX.1-2008."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The behavior of these functions depends on the B<LC_CTYPE> category of the "
"locale."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"These functions are not very appropriate for dealing with Unicode "
"characters, because Unicode knows about three cases: upper, lower, and title "
"case."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<iswlower>(3), B<towctrans>(3), B<towupper>(3), B<locale>(7)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The B<towlower_l>()  function first appeared in glibc 2.3."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<towlower>(): C99, POSIX.1-2001 (XSI); present as an XSI extension in "
"POSIX.1-2008, but marked obsolete."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<towlower_l>(): POSIX.1-2008."
msgstr ""

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""

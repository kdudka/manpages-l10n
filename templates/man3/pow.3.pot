# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-05-01 15:48+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "pow"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "pow, powf, powl - power functions"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Math library (I<libm>, I<-lm>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<double pow(double >I<x>B<, double >I<y>B<);>\n"
"B<float powf(float >I<x>B<, float >I<y>B<);>\n"
"B<long double powl(long double >I<x>B<, long double >I<y>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<powf>(), B<powl>():"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "These functions return the value of I<x> raised to the power of I<y>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On success, these functions return the value of I<x> to the power of I<y>."
msgstr ""

#.  The range error is generated at least as far back as glibc 2.4
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the result overflows, a range error occurs, and the functions return "
"B<HUGE_VAL>, B<HUGE_VALF>, or B<HUGE_VALL>, respectively, with the "
"mathematically correct sign."
msgstr ""

#. #-#-#-#-#  archlinux: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  POSIX.1 does not specify the sign of the zero,
#.  but https://www.sourceware.org/bugzilla/show_bug.cgi?id=2678
#.  points out that the zero has the wrong sign in some cases.
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  POSIX.1 does not specify the sign of the zero,
#.  but http://sources.redhat.com/bugzilla/show_bug.cgi?id=2678
#.  points out that the zero has the wrong sign in some cases.
#. type: Plain text
#. #-#-#-#-#  debian-unstable: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  POSIX.1 does not specify the sign of the zero,
#.  but https://www.sourceware.org/bugzilla/show_bug.cgi?id=2678
#.  points out that the zero has the wrong sign in some cases.
#. type: Plain text
#. #-#-#-#-#  fedora-40: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  POSIX.1 does not specify the sign of the zero,
#.  but https://www.sourceware.org/bugzilla/show_bug.cgi?id=2678
#.  points out that the zero has the wrong sign in some cases.
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  POSIX.1 does not specify the sign of the zero,
#.  but https://www.sourceware.org/bugzilla/show_bug.cgi?id=2678
#.  points out that the zero has the wrong sign in some cases.
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  POSIX.1 does not specify the sign of the zero,
#.  but https://www.sourceware.org/bugzilla/show_bug.cgi?id=2678
#.  points out that the zero has the wrong sign in some cases.
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-6: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  POSIX.1 does not specify the sign of the zero,
#.  but https://www.sourceware.org/bugzilla/show_bug.cgi?id=2678
#.  points out that the zero has the wrong sign in some cases.
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  POSIX.1 does not specify the sign of the zero,
#.  but https://www.sourceware.org/bugzilla/show_bug.cgi?id=2678
#.  points out that the zero has the wrong sign in some cases.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If result underflows, and is not representable, a range error occurs, and "
"0.0 with the appropriate sign is returned."
msgstr ""

#.  pow(\(+-0, <0 [[odd]]) = HUGE_VAL*
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<x> is +0 or -0, and I<y> is an odd integer less than 0, a pole error "
"occurs and B<HUGE_VAL>, B<HUGE_VALF>, or B<HUGE_VALL>, is returned, with the "
"same sign as I<x>."
msgstr ""

#.  pow(\(+-0, <0 [[!odd]]) = HUGE_VAL*
#.  The pole error is generated at least as far back as glibc 2.4
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<x> is +0 or -0, and I<y> is less than 0 and not an odd integer, a pole "
"error occurs and +B<HUGE_VAL>, +B<HUGE_VALF>, or +B<HUGE_VALL>, is returned."
msgstr ""

#.  pow(\(+-0, >0 [[odd]]) = \(+-0
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<x> is +0 (-0), and I<y> is an odd integer greater than 0, the result is "
"+0 (-0)."
msgstr ""

#.  pow(\(+-0, >0 [[!odd]]) = +0
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<x> is 0, and I<y> greater than 0 and not an odd integer, the result is "
"+0."
msgstr ""

#.  pow(-1, \(+-INFINITY) = 1.0
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<x> is -1, and I<y> is positive infinity or negative infinity, the "
"result is 1.0."
msgstr ""

#.  pow(+1, y) = 1.0
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If I<x> is +1, the result is 1.0 (even if I<y> is a NaN)."
msgstr ""

#.  pow(x, \(+-0) = 1.0
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If I<y> is 0, the result is 1.0 (even if I<x> is a NaN)."
msgstr ""

#.  pow(<0, y) = NaN
#.  The domain error is generated at least as far back as glibc 2.4
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<x> is a finite value less than 0, and I<y> is a finite noninteger, a "
"domain error occurs, and a NaN is returned."
msgstr ""

#.  pow(|x|<1, -INFINITY) = INFINITY
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the absolute value of I<x> is less than 1, and I<y> is negative infinity, "
"the result is positive infinity."
msgstr ""

#.  pow(|x|>1, -INFINITY) = +0
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the absolute value of I<x> is greater than 1, and I<y> is negative "
"infinity, the result is +0."
msgstr ""

#.  pow(|x|<1, INFINITY) = +0
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the absolute value of I<x> is less than 1, and I<y> is positive infinity, "
"the result is +0."
msgstr ""

#.  pow(|x|>1, INFINITY) = INFINITY
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the absolute value of I<x> is greater than 1, and I<y> is positive "
"infinity, the result is positive infinity."
msgstr ""

#.  pow(-INFINITY, <0 [[odd]]) = -0
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<x> is negative infinity, and I<y> is an odd integer less than 0, the "
"result is -0."
msgstr ""

#.  pow(-INFINITY, <0 [[!odd]]) = +0
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<x> is negative infinity, and I<y> less than 0 and not an odd integer, "
"the result is +0."
msgstr ""

#.  pow(-INFINITY, >0 [[odd]]) = -INFINITY
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<x> is negative infinity, and I<y> is an odd integer greater than 0, the "
"result is negative infinity."
msgstr ""

#.  pow(-INFINITY, >0 [[!odd]]) = INFINITY
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<x> is negative infinity, and I<y> greater than 0 and not an odd "
"integer, the result is positive infinity."
msgstr ""

#.  pow(INFINITY, <0) = +0
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If I<x> is positive infinity, and I<y> less than 0, the result is +0."
msgstr ""

#.  pow(INFINITY, >0) = INFINITY
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<x> is positive infinity, and I<y> greater than 0, the result is "
"positive infinity."
msgstr ""

#.  pow(NaN, y) or pow(x, NaN) = NaN
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Except as specified above, if I<x> or I<y> is a NaN, the result is a NaN."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#
#
#
#. #-#-#-#-#  archlinux: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . review status of this error
#.  longstanding bug report for glibc:
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=369
#.  For negative x, and -large and +large y, glibc 2.8 gives incorrect
#.  results
#.  pow(-0.5,-DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-overflow (ERANGE, FE_OVERFLOW); +INF)
#.  pow(-1.5,-DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-underflow (ERANGE, FE_UNDERFLOW); +0)
#.  pow(-0.5,DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-underflow (ERANGE, FE_UNDERFLOW); +0)
#.  pow(-1.5,DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-overflow (ERANGE, FE_OVERFLOW); +INF)
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . review status of this error
#.  longstanding bug report for glibc:
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=369
#.  For negative x, and -large and +large y, glibc 2.8 gives incorrect
#.  results
#.  pow(-0.5,-DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-overflow (ERANGE, FE_OVERFLOW); +INF)
#.  pow(-1.5,-DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-underflow (ERANGE, FE_UNDERFLOW); +0)
#.  pow(-0.5,DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-underflow (ERANGE, FE_UNDERFLOW); +0)
#.  pow(-1.5,DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-overflow (ERANGE, FE_OVERFLOW); +INF)
#. type: Plain text
#. #-#-#-#-#  debian-unstable: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . review status of this error
#.  longstanding bug report for glibc:
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=369
#.  For negative x, and -large and +large y, glibc 2.8 gives incorrect
#.  results
#.  pow(-0.5,-DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-overflow (ERANGE, FE_OVERFLOW); +INF)
#.  pow(-1.5,-DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-underflow (ERANGE, FE_UNDERFLOW); +0)
#.  pow(-0.5,DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-underflow (ERANGE, FE_UNDERFLOW); +0)
#.  pow(-1.5,DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-overflow (ERANGE, FE_OVERFLOW); +INF)
#. type: Plain text
#. #-#-#-#-#  fedora-40: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . review status of this error
#.  longstanding bug report for glibc:
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=369
#.  For negative x, and -large and +large y, glibc 2.8 gives incorrect
#.  results
#.  pow(-0.5,-DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-overflow (ERANGE, FE_OVERFLOW); +INF)
#.  pow(-1.5,-DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-underflow (ERANGE, FE_UNDERFLOW); +0)
#.  pow(-0.5,DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-underflow (ERANGE, FE_UNDERFLOW); +0)
#.  pow(-1.5,DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-overflow (ERANGE, FE_OVERFLOW); +INF)
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . review status of this error
#.  longstanding bug report for glibc:
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=369
#.  For negative x, and -large and +large y, glibc 2.8 gives incorrect
#.  results
#.  pow(-0.5,-DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-overflow (ERANGE, FE_OVERFLOW); +INF)
#.  pow(-1.5,-DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-underflow (ERANGE, FE_UNDERFLOW); +0)
#.  pow(-0.5,DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-underflow (ERANGE, FE_UNDERFLOW); +0)
#.  pow(-1.5,DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-overflow (ERANGE, FE_OVERFLOW); +INF)
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . review status of this error
#.  longstanding bug report for glibc:
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=369
#.  For negative x, and -large and +large y, glibc 2.8 gives incorrect
#.  results
#.  pow(-0.5,-DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-overflow (ERANGE, FE_OVERFLOW); +INF)
#.  pow(-1.5,-DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-underflow (ERANGE, FE_UNDERFLOW); +0)
#.  pow(-0.5,DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-underflow (ERANGE, FE_UNDERFLOW); +0)
#.  pow(-1.5,DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-overflow (ERANGE, FE_OVERFLOW); +INF)
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-6: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . review status of this error
#.  longstanding bug report for glibc:
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=369
#.  For negative x, and -large and +large y, glibc 2.8 gives incorrect
#.  results
#.  pow(-0.5,-DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-overflow (ERANGE, FE_OVERFLOW); +INF)
#.  pow(-1.5,-DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-underflow (ERANGE, FE_UNDERFLOW); +0)
#.  pow(-0.5,DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-underflow (ERANGE, FE_UNDERFLOW); +0)
#.  pow(-1.5,DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-overflow (ERANGE, FE_OVERFLOW); +INF)
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . review status of this error
#.  longstanding bug report for glibc:
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=369
#.  For negative x, and -large and +large y, glibc 2.8 gives incorrect
#.  results
#.  pow(-0.5,-DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-overflow (ERANGE, FE_OVERFLOW); +INF)
#.  pow(-1.5,-DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-underflow (ERANGE, FE_UNDERFLOW); +0)
#.  pow(-0.5,DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-underflow (ERANGE, FE_UNDERFLOW); +0)
#.  pow(-1.5,DBL_MAX)=nan
#.  EDOM FE_INVALID nan; fail-errno fail-except fail-result;
#.  FAIL (expected: range-error-overflow (ERANGE, FE_OVERFLOW); +INF)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"See B<math_error>(7)  for information on how to determine whether an error "
"has occurred when calling these functions."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The following errors can occur:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Domain error: I<x> is negative, and I<y> is a finite noninteger"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<errno> is set to B<EDOM>.  An invalid floating-point exception "
"(B<FE_INVALID>)  is raised."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Pole error: I<x> is zero, and I<y> is negative"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<errno> is set to B<ERANGE> (but see BUGS).  A divide-by-zero floating-"
"point exception (B<FE_DIVBYZERO>)  is raised."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Range error: the result overflows"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<errno> is set to B<ERANGE>.  An overflow floating-point exception "
"(B<FE_OVERFLOW>)  is raised."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Range error: the result underflows"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<errno> is set to B<ERANGE>.  An underflow floating-point exception "
"(B<FE_UNDERFLOW>)  is raised."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<pow>(),\n"
"B<powf>(),\n"
"B<powl>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "C99, POSIX.1-2001."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "The variant returning I<double> also conforms to SVr4, 4.3BSD, C89."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Historical bugs (now fixed)"
msgstr ""

#.  https://sourceware.org/bugzilla/show_bug.cgi?id=13932
#.  commit c3d466cba1692708a19c6ff829d0386c83a0c6e5
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Before glibc 2.28, on some architectures (e.g., x86-64)  B<pow>()  may be "
"more than 10,000 times slower for some inputs than for other nearby inputs.  "
"This affects only B<pow>(), and not B<powf>()  nor B<powl>().  This problem "
"was fixed in glibc 2.28."
msgstr ""

#. #-#-#-#-#  archlinux: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=3866
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=3866
#. type: Plain text
#. #-#-#-#-#  debian-unstable: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=3866
#. type: Plain text
#. #-#-#-#-#  fedora-40: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=3866
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=3866
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=3866
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-6: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=3866
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=3866
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A number of bugs in the glibc implementation of B<pow>()  were fixed in "
"glibc 2.16."
msgstr ""

#
#. #-#-#-#-#  archlinux: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6776
#.  or possibly 2.9, I haven't found the source code change
#.  and I don't have a 2.9 system to test
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6776
#.  or possibly 2.9, I haven't found the source code change
#.  and I don't have a 2.9 system to test
#. type: Plain text
#. #-#-#-#-#  debian-unstable: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6776
#.  or possibly 2.9, I haven't found the source code change
#.  and I don't have a 2.9 system to test
#. type: Plain text
#. #-#-#-#-#  fedora-40: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6776
#.  or possibly 2.9, I haven't found the source code change
#.  and I don't have a 2.9 system to test
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6776
#.  or possibly 2.9, I haven't found the source code change
#.  and I don't have a 2.9 system to test
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6776
#.  or possibly 2.9, I haven't found the source code change
#.  and I don't have a 2.9 system to test
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-6: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6776
#.  or possibly 2.9, I haven't found the source code change
#.  and I don't have a 2.9 system to test
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: pow.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6776
#.  or possibly 2.9, I haven't found the source code change
#.  and I don't have a 2.9 system to test
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In glibc 2.9 and earlier, when a pole error occurs, I<errno> is set to "
"B<EDOM> instead of the POSIX-mandated B<ERANGE>.  Since glibc 2.10, glibc "
"does the right thing."
msgstr ""

#.  Actually, glibc 2.3.2 is the earliest test result I have; so yet
#.  to confirm if this error occurs only in glibc 2.3.2.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In glibc 2.3.2 and earlier, when an overflow or underflow error occurs, "
"glibc's B<pow>()  generates a bogus invalid floating-point exception "
"(B<FE_INVALID>)  in addition to the overflow or underflow exception."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<cbrt>(3), B<cpow>(3), B<sqrt>(3)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "C99, POSIX.1-2001, POSIX.1-2008."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The variant returning I<double> also conforms to SVr4, 4.3BSD."
msgstr ""

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""

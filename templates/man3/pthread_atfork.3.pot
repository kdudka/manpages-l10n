# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-05-01 15:49+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "pthread_atfork"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr ""

#. type: SH
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "pthread_atfork - register fork handlers"
msgstr ""

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "POSIX threads library (I<libpthread>, I<-lpthread>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>pthread.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int pthread_atfork(void (*>I<prepare>B<)(void), void (*>I<parent>B<)(void),>\n"
"B<                   void (*>I<child>B<)(void));>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The B<pthread_atfork>()  function registers fork handlers that are to be "
"executed when B<fork>(2)  is called by any thread in a process.  The "
"handlers are executed in the context of the thread that calls B<fork>(2)."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Three kinds of handler can be registered:"
msgstr ""

#. type: IP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"I<prepare> specifies a handler that is executed in the parent process before "
"B<fork>(2)  processing starts."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"I<parent> specifies a handler that is executed in the parent process after "
"B<fork>(2)  processing completes."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"I<child> specifies a handler that is executed in the child process after "
"B<fork>(2)  processing completes."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"Any of the three arguments may be NULL if no handler is needed in the "
"corresponding phase of B<fork>(2)  processing."
msgstr ""

#. type: SH
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"On success, B<pthread_atfork>()  returns zero.  On error, it returns an "
"error number.  B<pthread_atfork>()  may be called multiple times by a "
"process to register additional handlers.  The handlers for each phase are "
"called in a specified order: the I<prepare> handlers are called in reverse "
"order of registration; the I<parent> and I<child> handlers are called in the "
"order of registration."
msgstr ""

#. type: SH
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: TP
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Could not allocate memory to record the fork handler list entry."
msgstr ""

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr ""

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr ""

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"When B<fork>(2)  is called in a multithreaded process, only the calling "
"thread is duplicated in the child process.  The original intention of "
"B<pthread_atfork>()  was to allow the child process to be returned to a "
"consistent state.  For example, at the time of the call to B<fork>(2), other "
"threads may have locked mutexes that are visible in the user-space memory "
"duplicated in the child.  Such mutexes would never be unlocked, since the "
"threads that placed the locks are not duplicated in the child.  The intent "
"of B<pthread_atfork>()  was to provide a mechanism whereby the application "
"(or a library)  could ensure that mutexes and other process and thread state "
"would be restored to a consistent state.  In practice, this task is "
"generally too difficult to be practicable."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"After a B<fork>(2)  in a multithreaded process returns in the child, the "
"child should call only async-signal-safe functions (see B<signal-"
"safety>(7))  until such time as it calls B<execve>(2)  to execute a new "
"program."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"POSIX.1 specifies that B<pthread_atfork>()  shall not fail with the error "
"B<EINTR>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<fork>(2), B<atexit>(3), B<pthreads>(7)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "PTHREAD_ATFORK"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "LinuxThreads"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "pthread_atfork - register handlers to be called at fork(2) time"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<#include E<lt>pthread.hE<gt>>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<int pthread_atfork(void (*>I<prepare>B<)(void), void (*>I<parent>B<)"
"(void), void (*>I<child>B<)(void));>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<pthread_atfork> registers handler functions to be called just before and "
"just after a new process is created with B<fork>(2). The I<prepare> handler "
"will be called from the parent process, just before the new process is "
"created. The I<parent> handler will be called from the parent process, just "
"before B<fork>(2) returns. The I<child> handler will be called from the "
"child process, just before B<fork>(2) returns."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"One or several of the three handlers I<prepare>, I<parent> and I<child> can "
"be given as B<NULL>, meaning that no handler needs to be called at the "
"corresponding point."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<pthread_atfork> can be called several times to install several sets of "
"handlers. At B<fork>(2) time, the I<prepare> handlers are called in LIFO "
"order (last added with B<pthread_atfork>, first called before B<fork>), "
"while the I<parent> and I<child> handlers are called in FIFO order (first "
"added, first called)."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"To understand the purpose of B<pthread_atfork>, recall that B<fork>(2)  "
"duplicates the whole memory space, including mutexes in their current "
"locking state, but only the calling thread: other threads are not running in "
"the child process.  The mutexes are not usable after the B<fork> and must be "
"initialized with I<pthread_mutex_init> in the child process.  This is a "
"limitation of the current implementation and might or might not be present "
"in future versions."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<pthread_atfork> returns 0 on success and a non-zero error code on error."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "insufficient memory available to register the handlers."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Xavier Leroy E<lt>Xavier.Leroy@inria.frE<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<fork>(2), B<pthread_mutex_lock>(3), B<pthread_mutex_unlock>(3)."
msgstr ""

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""

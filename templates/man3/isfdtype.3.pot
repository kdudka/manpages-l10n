# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-05-01 15:42+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "isfdtype"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "isfdtype - test file type of a file descriptor"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>sys/stat.hE<gt>>\n"
"B<#include E<lt>sys/socket.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int isfdtype(int >I<fd>B<, int >I<fdtype>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<isfdtype>():"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.20:\n"
"        _DEFAULT_SOURCE\n"
"    Before glibc 2.20:\n"
"        _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<isfdtype>()  function tests whether the file descriptor I<fd> refers "
"to a file of type I<fdtype>.  The I<fdtype> argument specifies one of the "
"B<S_IF*> constants defined in I<E<lt>sys/stat.hE<gt>> and documented in "
"B<stat>(2)  (e.g., B<S_IFREG>)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<isfdtype>()  function returns 1 if the file descriptor I<fd> is of "
"type I<fdtype> and 0 if it is not.  On failure, -1 is returned and I<errno> "
"is set to indicate the error."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<isfdtype>()  function can fail with any of the same errors as "
"B<fstat>(2)."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Portable applications should use B<fstat>(2)  instead."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "None."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"It appeared in the draft POSIX.1g standard.  It is present on OpenBSD and "
"Tru64 UNIX (where the required header file in both cases is just I<E<lt>sys/"
"stat.hE<gt>>, as shown in the POSIX.1g draft)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<fstat>(2)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-10-30"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<isfdtype>()  function is not specified in any standard, but did appear "
"in the draft POSIX.1g standard.  It is present on OpenBSD and Tru64 UNIX "
"(where the required header file in both cases is just I<E<lt>sys/stat."
"hE<gt>>, as shown in the POSIX.1g draft), and possibly other systems."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""

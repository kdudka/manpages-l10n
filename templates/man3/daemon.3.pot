# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-05-01 15:37+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "daemon"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "daemon - run in the background"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int daemon(int >I<nochdir>B<, int >I<noclose>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<daemon>():"
msgstr ""

#.              commit 266865c0e7b79d4196e2cc393693463f03c90bd8
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.21:\n"
"        _DEFAULT_SOURCE\n"
"    In glibc 2.19 and 2.20:\n"
"        _DEFAULT_SOURCE || (_XOPEN_SOURCE && _XOPEN_SOURCE E<lt> 500)\n"
"    Up to and including glibc 2.19:\n"
"        _BSD_SOURCE || (_XOPEN_SOURCE && _XOPEN_SOURCE E<lt> 500)\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<daemon>()  function is for programs wishing to detach themselves from "
"the controlling terminal and run in the background as system daemons."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<nochdir> is zero, B<daemon>()  changes the process's current working "
"directory to the root directory (\"/\"); otherwise, the current working "
"directory is left unchanged."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<noclose> is zero, B<daemon>()  redirects standard input, standard "
"output, and standard error to I</dev/null>; otherwise, no changes are made "
"to these file descriptors."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#.  not .IR in order not to underline _
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"(This function forks, and if the B<fork>(2)  succeeds, the parent calls "
"B<_exit>(2), so that further errors are seen by the child only.)  On success "
"B<daemon>()  returns zero.  If an error occurs, B<daemon>()  returns -1 and "
"sets I<errno> to any of the errors specified for the B<fork>(2)  and "
"B<setsid>(2)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<daemon>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "A similar function appears on the BSDs."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The glibc implementation can also return -1 when I</dev/null> exists but is "
"not a character device with the expected major and minor numbers.  In this "
"case, I<errno> need not be set."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "None."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "4.4BSD."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr ""

#.  FIXME . https://sourceware.org/bugzilla/show_bug.cgi?id=19144
#.  Tested using a program that uses daemon() and then opens an
#.  otherwise unused console device (/dev/ttyN) that does not
#.  have an associated getty process.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The GNU C library implementation of this function was taken from BSD, and "
"does not employ the double-fork technique (i.e., B<fork>(2), B<setsid>(2), "
"B<fork>(2))  that is necessary to ensure that the resulting daemon process "
"is not a session leader.  Instead, the resulting daemon I<is> a session "
"leader.  On systems that follow System V semantics (e.g., Linux), this means "
"that if the daemon opens a terminal that is not already a controlling "
"terminal for another session, then that terminal will inadvertently become "
"the controlling terminal for the daemon."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<fork>(2), B<setsid>(2), B<daemon>(7), B<logrotate>(8)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Not in POSIX.1.  A similar function appears on the BSDs.  The B<daemon>()  "
"function first appeared in 4.4BSD."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""

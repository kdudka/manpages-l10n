# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Antonio Aneiros <aneiros@ctv.es>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-02-15 17:55+0100\n"
"PO-Revision-Date: 1999-05-04 23:07+0200\n"
"Last-Translator: Antonio Aneiros <aneiros@ctv.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.4.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "ARP"
msgstr "ARP"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "2008-10-03"
msgstr "3 Octubre 2008"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "net-tools"
msgstr "net-tools"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Linux System Administrator's Manual"
msgstr "Manual del Administrador del Sistema Linux"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid "arp - manipulate the system ARP cache"
msgstr "arp - manipula la cache ARP del sistema"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, fuzzy
#| msgid "B<arp> [B<-vn>] [B<-H type>] [B<-i if>] B<-a> [B<hostname>]"
msgid "B<arp> [B<-vn>] [B<-H> I<type>] [B<-i> I<if>] [B<-ae>] [I<hostname>]"
msgstr "B<arp> [B<-vn>] [B<-H type>] [B<-i if>] B<-a> [B<hostname>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, fuzzy
#| msgid "B<arp> [B<-v>] [B<-i if>] B<-d hostname> [B<pub>]"
msgid "B<arp> [B<-v>] [B<-i> I<if>] B<-d> I<hostname> [B<pub>]"
msgstr "B<arp> [B<-v>] [B<-i if>] B<-d hostname> [B<pub>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, fuzzy
#| msgid ""
#| "B<arp> [B<-v>] [B<-H type>] [B<-i if>] B<-s hostname hw_addr> [B<temp>]"
msgid ""
"B<arp> [B<-v>] [B<-H> I<type>] [B<-i> I<if>] B<-s> I<hostname hw_addr> "
"[B<temp>]"
msgstr ""
"B<arp> [B<-v>] [B<-H type>] [B<-i if>] B<-s hostname hw_addr> [B<temp>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, fuzzy
#| msgid ""
#| "B<arp> [B<-v>] [B<-H type>] [B<-i if>] B<-s hostname hw_addr> [B<netmask "
#| "nm>] B<pub>"
msgid ""
"B<arp> [B<-v>] [B<-H> I<type>] [B<-i> I<if>] B<-s> I<hostname hw_addr> "
"[B<netmask> I<nm>] B<pub>"
msgstr ""
"B<arp> [B<-v>] [B<-H type>] [B<-i if>] B<-s hostname hw_addr> [B<netmask "
"nm>] B<pub>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, fuzzy
#| msgid ""
#| "B<arp> [B<-v>] [B<-H type>] [B<-i if>] B<-Ds hostname ifa> [B<netmask "
#| "nm>] B<pub>"
msgid ""
"B<arp> [B<-v>] [B<-H> I<type>] [B<-i> I<if>] B<-Ds> I<hostname> I<ifname> "
"[B<netmask> I<nm>] B<pub>"
msgstr ""
"B<arp> [B<-v>] [B<-H type>] [B<-i if>] B<-Ds hostname ifa> [B<netmask nm>] "
"B<pub>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, fuzzy
#| msgid "B<arp> [B<-vnD>] [B<-H type>] [B<-i if>] B<-f [filename]>"
msgid "B<arp> [B<-vnD>] [B<-H> I<type>] [B<-i> I<if>] B<-f> [I<filename>]"
msgstr "B<arp> [B<-vnD>] [B<-H type>] [B<-i if>] B<-f [filename]>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<Arp> manipulates or displays the kernel's IPv4 network neighbour cache. It "
"can add entries to the table, delete one or display the current content."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<ARP> stands for Address Resolution Protocol, which is used to find the "
"media access control address of a network neighbour for a given IPv4 Address."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "MODES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<arp> with no mode specifier will print the current content of the table. "
"It is possible to limit the number of entries printed, by specifying an "
"hardware address type, interface name or host address."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<arp -d> I<address> will delete a ARP table entry. Root or netadmin "
"privilege is required to do this. The entry is found by IP address. If a "
"hostname is given, it will be resolved before looking up the entry in the "
"ARP table."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, fuzzy
#| msgid ""
#| "class, but for most classes one can assume that the usual presentation "
#| "can be used.  For the Ethernet class, this is 6 bytes in hexadecimal, "
#| "separated by colons. When adding proxy arp entries (that is those with "
#| "the B<pub>lish flag set a B<netmask> may be specified to proxy arp for "
#| "entire subnets. This is not good practice, but is supported by older "
#| "kernels because it can be useful. If the B<temp> flag is not supplied "
#| "entries will be permanent stored into the ARP cache."
msgid ""
"B<arp -s> I<address hw_addr> is used to set up a new table entry. The format "
"of the I<hw_addr> parameter is dependent on the hardware class, but for most "
"classes one can assume that the usual presentation can be used.  For the "
"Ethernet class, this is 6 bytes in hexadecimal, separated by colons. When "
"adding proxy arp entries (that is those with the B<pub>lish flag set) a "
"B<netmask> may be specified to proxy arp for entire subnets. This is not "
"good practice, but is supported by older kernels because it can be useful. "
"If the B<temp> flag is not supplied entries will be permanent stored into "
"the ARP cache. To simplify setting up entries for one of your own network "
"interfaces, you can use the B<arp -Ds> I<address ifname> form. In that case "
"the hardware address is taken from the interface with the specified name."
msgstr ""
"El formato de la dirección hardware depende de la clase de hardware, pero "
"para la mayoría de las clases se puede asumir la presentación normal. Para "
"la clase Ethernet, ésta supone 6 bytes en formato hexadecimal, separados por "
"dos puntos(:). Al añadir entradas arp tipo proxy (es decir, aquellas con la "
"opción B<pub>lish activada), se puede especificar una B<netmask> (máscara de "
"red) para así hacer arp proxy a subredes enteras. El proxy arp no resulta un "
"buen protocolo, pero a veces resulta de utilidad. Si no se proporciona la "
"opción B<temp> las entradas se almacenarán permanentemente en la cache ARP."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr "OPCIONES"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<-v, --verbose>"
msgstr "B<-v, --verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid "Tell the user what is going on by being verbose."
msgstr "Informa al usuario de lo que ocurre de manera extendida."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<-n, --numeric>"
msgstr "B<-n, --numeric>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"shows numerical addresses instead of trying to determine symbolic host, port "
"or user names."
msgstr ""
"muestra direcciones numéricas en vez de tratar de determinar nombres "
"simbólicos de servidores, puertos o nombres de usuario."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<-H>I< type>, B<--hw-type>I< type>, B<-t>I< type>"
msgstr "B<-H>I< tipo>, B<--hw-type>I< tipo>, B<-t>I< tipo>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"When setting or reading the ARP cache, this optional parameter tells B<arp> "
"which class of entries it should check for.  The default value of this "
"parameter is B<ether> (i.e. hardware code 0x01 for IEEE 802.3 10Mbps "
"Ethernet).  Other values might include network technologies such as ARCnet "
"(B<arcnet>)  , PROnet (B<pronet>)  , AX.25 (B<ax25>)  and NET/ROM "
"(B<netrom>)."
msgstr ""
"Al configurar o leer la cache ARP, esta opción le dice a B<arp> qué clase de "
"entradas debe buscar. El valor por defecto es B<ether> (es decir, código "
"hardware 0x01 para IEEE 802.3 10Mbps Ethernet).  Otros valores incluyen "
"tecnologías de red como las siguientes ARCnet (B<arcnet>), PROnet "
"(B<pronet>), AX.25 (B<ax25>)  y NET/ROM (B<netrom>)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<-a>"
msgstr "B<-a>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid "Use alternate BSD style output format (with no fixed columns)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<-e>"
msgstr "B<-e>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid "Use default Linux style output format (with fixed columns)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "B<-D, --use-device>"
msgstr "B<-D, --use-device>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"Instead of a hw_addr, the given argument is the name of an interface.  "
"B<arp> will use the MAC address of that interface for the table entry. This "
"is usually the best option to set up a proxy ARP entry to yourself."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<-i If, --device If>"
msgid "B<-i>I< If>, B<--device>I< If>"
msgstr "B<-i If, --device If>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"Select an interface. When dumping the ARP cache only entries matching the "
"specified interface will be printed. When setting a permanent or B<temp> ARP "
"entry this interface will be associated with the entry; if this option is "
"not used, the kernel will guess based on the routing table. For B<pub> "
"entries the specified interface is the interface on which ARP requests will "
"be answered."
msgstr ""
"Selecciona un interfaz. Al vaciar la cache ARP se mostrarán solamente "
"aquellas entradas que correspondan al interfaz. Se usará una configuración "
"de entrada ARP temporal o permanente para el dispositivo especificado. Si no "
"se especifica uno, el kernel lo deducirá a partir de la tabla de "
"encaminamiento. Para entradas tipo B<pub> el interfaz especificado será el "
"usado para responder peticiones ARP."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<NOTE:> This has to be different from the interface to which the IP "
"datagrams will be routed.  B<NOTE:> As of kernel 2.2.0 it is no longer "
"possible to set an ARP entry for an entire subnet. Linux instead does "
"automagic proxy arp when a route exists and it is forwarding. See B<arp>(7)  "
"for details. Also the B<dontpub> option which is available for delete and "
"set operations cannot be used with 2.4 and newer kernels."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<-f filename, --file filename>"
msgid "B<-f>I< filename>, B<--file>I< filename>"
msgstr "B<-f filename, --file filename>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, fuzzy
#| msgid ""
#| "set up.  The name of the data file is very often I</etc/ethers>, but this "
#| "is not official. If no filename is specified /etc/ethers is used as "
#| "default."
msgid ""
"Similar to the B<-s> option, only this time the address info is taken from "
"file I<filename>.  This can be used if ARP entries for a lot of hosts have "
"to be set up.  The name of the data file is very often I</etc/ethers>, but "
"this is not official. If no filename is specified I</etc/ethers> is used as "
"default."
msgstr ""
"Se puede usar esta opción si han de configurarse las entradas ARP de muchos "
"servidores. A menudo el nombre del archivo de datos es I</etc/ethers>, pero "
"esto no es oficial."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"The format of the file is simple; it only contains ASCII text lines with a "
"hostname, and a hardware address separated by whitespace. Additionally the "
"B<pub>,B< temp> andB< netmask> flags can be used."
msgstr ""
"El formato del archivo es simple; solamente contiene líneas de texto ASCII "
"con un nombre de servidor (hostname) y una dirección hardware separados por "
"un espacio en blanco. Adicionalmente se pueden usar las opciones B<pub>,B< "
"temp> yB< netmask>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"In all places where a B<hostname> is expected, one can also enter an B<IP "
"address> in dotted-decimal notation."
msgstr ""
"En todos los lugares donde se espera un nombre de servidor, se puede "
"proporcionar también una B<dirección IP> en notación decimal separada por "
"puntos."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"As a special case for compatibility the order of the hostname and the "
"hardware address can be exchanged."
msgstr ""
"Cada entrada completa de la cache ARP se marcará con la opción B<C>, las "
"entradas permanentes se marcan con B<M> y las entradas publicadas tienen la "
"marca B<P>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"Each complete entry in the ARP cache will be marked with the B<C> flag. "
"Permanent entries are marked with B<M> and published entries have the B<P> "
"flag."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "EXAMPLES"
msgstr "EJEMPLOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid "B</usr/sbin/arp -i eth0 -Ds 10.0.0.2 eth1 pub>"
msgstr "B</usr/sbin/arp -i eth0 -Ds 10.0.0.2 eth1 pub>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"This will answer ARP requests for 10.0.0.2 on eth0 with the MAC address for "
"eth1."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid "B</usr/sbin/arp -i eth1 -d 10.0.0.1>"
msgstr "B</usr/sbin/arp -i eth1 -d 10.0.0.1>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"Delete the ARP table entry for 10.0.0.1 on interface eth1. This will match "
"published proxy ARP entries and permanent entries."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "FILES"
msgstr "ARCHIVOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid "I</proc/net/arp>"
msgstr "I</proc/net/arp>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid "I</etc/networks>"
msgstr "I</etc/networks>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid "I</etc/hosts>"
msgstr "I</etc/hosts>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid "I</etc/ethers>"
msgstr "I</etc/ethers>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron
msgid "B<ethers>(5), B<rarp>(8), B<route>(8), B<ifconfig>(8), B<netstat>(8)"
msgstr "B<ethers>(5), B<rarp>(8), B<route>(8), B<ifconfig>(8), B<netstat>(8)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "AUTHORS"
msgstr "AUTORES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, fuzzy
#| msgid ""
#| "Fred N. van Kempen, E<lt>waltje@uwalt.nl.mugnet.orgE<gt> with a lot of "
#| "improvements from net-tools Maintainer Bernd Eckenfels E<lt>net-"
#| "tools@lina.inka.deE<gt>."
msgid ""
"Fred N. van Kempen E<lt>waltje@uwalt.nl.mugnet.orgE<gt>, Bernd Eckenfels "
"E<lt>net-tools@lina.inka.deE<gt>."
msgstr ""
"Fred N. van Kempen, E<lt>waltje@uwalt.nl.mugnet.orgE<gt> con un montón de "
"mejoras del encargado del mantenimiento de las herramientas de red Bernd "
"Eckenfels E<lt>net-tools@lina.inka.deE<gt>."

#. type: SH
#: fedora-40 fedora-rawhide
#, no-wrap
msgid "NOTE"
msgstr "NOTA"

#. type: Plain text
#: fedora-40 fedora-rawhide
msgid "This program is obsolete. For replacement check B<ip neigh>."
msgstr ""

#. type: Plain text
#: fedora-40 fedora-rawhide
msgid "B<ip(8)>"
msgstr "B<ip(8)>"

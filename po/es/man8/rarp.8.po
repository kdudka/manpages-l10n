# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Antonio Aneiros <aneiros@ctv.es>, 1999.
# Marcos Fouces <marcos@debian.org>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:45+0200\n"
"PO-Revision-Date: 2022-05-04 00:47+0200\n"
"Last-Translator: Marcos Fouces <marcos@debian.org>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "RARP"
msgstr "RARP"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "2008-10-03"
msgstr "3 Octubre 2008"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "net-tools"
msgstr "net-tools"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "Linux System Administrator's Manual"
msgstr "Manual del Administrador del Sistema Linux"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "rarp - manipulate the system RARP table"
msgstr "rarp - manipula la tabla RARP del sistema"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<rarp [-V] [--version] [-h] [--help]>"
msgstr "B<rarp [-V] [--version] [-h] [--help]>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<rarp -a>"
msgstr "B<rarp -a>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<rarp [-v] -d hostname ...>"
msgstr "B<rarp [-v] -d hostname ...>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<rarp [-v] [-t type] -s hostname hw_addr>"
msgstr "B<rarp [-v] [-t type] -s hostname hw_addr>"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "NOTE"
msgstr "NOTA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"This program is obsolete.  From version 2.3, the Linux kernel no longer "
"contains RARP support.  For a replacement RARP daemon, see I<ftp://ftp."
"dementia.org/pub/net-tools>"
msgstr ""
"Este programa está obsoleto. El soporte para RARP no se incluye desde la "
"versión 2.3 de Linux. Puede encontrar un sustituto del demonio RARP en "
"I<ftp://ftp.dementia.org/pub/net-tools>"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"B<Rarp> manipulates the kernel's RARP table in various ways.  The primary "
"options are clearing an address mapping entry and manually setting up one.  "
"For debugging purposes, the B<rarp> program also allows a complete dump of "
"the RARP table."
msgstr ""
"B<Rarp> manipula la tabla RARP del núcleo de varias maneras. Las opciones "
"primarias son las de borrar una entrada de mapeado de dirección y establecer "
"una nueva manualmente. Para propósitos de depuración, el programa B<arp> "
"permite también un vaciado total de la tabla RARP."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "OPTIONS"
msgstr "OPCIONES"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-V>"
msgstr "B<-V>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Display the version of RARP in use."
msgstr "Muestra la versión de RARP en uso."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-v>"
msgstr "B<-v>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Tell the user what is going on by being verbose."
msgstr "Muestra al usuario lo que sucede de manera ampliada."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-t type>"
msgstr "B<-t type>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"When setting or reading the RARP table, this optional parameter tells "
"B<rarp> which class of entries it should check for.  The default value of "
"this parameter is B<ether> (i.e. hardware code B<0x01> for B<IEEE 802.3 "
"10Mbps Ethernet .> Other values might include network technologies such as "
"B<AX.25 (ax25)> and B<NET/ROM (netrom).>"
msgstr ""
"Al establecer o leer la tabla RARP, este parámetro opcional le dice a "
"B<rarp> qué clase de entradas debe buscar. El valor por defecto de este "
"parámetro es B<ether> (es decir, el código de hardware B<0x01> para la "
"B<IEEE 802.3 10Mbps Ethernet.> Otros valores pueden incluir también "
"tecnologías de red como B<AX.25 (ax25)> y B<NET/ROM (netrom).>"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-a>"
msgstr "B<-a>"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--list>"
msgstr "B<--list>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Lists the entries in the RARP table."
msgstr "Lista las entradas de la tabla RARP."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-d hostname>"
msgstr "B<-d hostname>"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--delete hostname>"
msgstr "B<--delete hostname>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Remove all RARP entries for the specified host."
msgstr ""
"Borra todas las entradas de la tabla RARP para el ordenador especificado."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-s hostname hw_addr>"
msgstr "B<-s hostname hw_addr>"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--set hostname hw_addr>"
msgstr "B<--set hostname hw_addr>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"Create a RARP address mapping entry for host B<hostname> with hardware "
"address set to B<hw_addr>.  The format of the hardware address is dependent "
"on the hardware class, but for most classes one can assume that the usual "
"presentation can be used.  For the Ethernet class, this is 6 bytes in "
"hexadecimal, separated by colons."
msgstr ""
"Crea una entrada con la dirección RARP para el equipo B<nombre_equipo>, con "
"dirección de hardware B<hw_addr>.El formato de la dirección de hardware "
"depende del tipo de hardware, pero para la mayoría de ellos se puede asumir "
"la presentación normal. Para el tipo Ethernet, éste consiste en 6 bytes en "
"formato hexadecimal, separados por dos puntos (:)."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "WARNING"
msgstr "AVISO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"Some systems (notably older Suns) assume that the host replying to a RARP "
"query can also provide other remote boot services. Therefore never "
"gratuitously add rarp entries unless you wish to meet the wrath of the "
"network administrator."
msgstr ""
"Algunos sistemas (en particular sistemas Sun antiguos) asumen que el "
"ordenador que responde a una petición RARP puede pproporcionar también otros "
"servicios de arranque remoto. Por lo tanto, no se debe añadir nunca una "
"entrada RARP de modo gratuito, a no ser que se desee provocar la ira del "
"administrador de red."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "FILES"
msgstr "ARCHIVOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "I</proc/net/rarp,>"
msgstr "I</proc/net/rarp,>"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<arp>(8), B<route>(8), B<ifconfig>(8), B<netstat>(8)"
msgstr "B<arp>(8), B<route>(8), B<ifconfig>(8), B<netstat>(8)"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "AUTHORS"
msgstr "AUTORES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Ross D. Martin, E<lt>martin@trcsun3.eas.asu.eduE<gt>"
msgstr "Ross D. Martin, E<lt>martin@trcsun3.eas.asu.eduE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Fred N. van Kempen, E<lt>waltje@uwalt.nl.mugnet.orgE<gt>"
msgstr "Fred N. van Kempen, E<lt>waltje@uwalt.nl.mugnet.orgE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Phil Blundell, E<lt>Philip.Blundell@pobox.comE<gt>"
msgstr "Phil Blundell, E<lt>Philip.Blundell@pobox.comE<gt>"

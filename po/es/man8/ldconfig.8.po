# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Luis M. Garcia <luismaria.garcia@hispalinux.es>, 2004.
# Marcos Fouces <marcos@debian.org>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-05-01 15:43+0200\n"
"PO-Revision-Date: 2020-11-05 19:17+0100\n"
"Last-Translator: Marcos Fouces <marcos@debian.org>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.4.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ldconfig"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-10-31"
msgstr "31 Octubre 2023"

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr "Páginas de Manual de Linux 6.7"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "ldconfig - configure dynamic linker run-time bindings"
msgstr ""
"ldconfig - configura vínculos del enlazador dinámico durante la ejecución"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: SY
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B</sbin/ldconfig> B<-p>"
msgid "/sbin/ldconfig"
msgstr "B</sbin/ldconfig> B<-p>"

#.  TODO?: -c, --format, -i, --ignore-aux-cache, --print-cache,
#.  --verbose, -V, --version, -?, --help, --usage
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B</sbin/ldconfig> [B<-nNvXV>] [B<-f> I<conf>] [B<-C> I<cache>] [B<-r> "
#| "I<root>] I<directory>..."
msgid ""
"[B<-nNvVX>] [B<-C\\~> I<cache>] [B<-f\\~> I<conf>] [B<-r\\~> I<root>] "
"I<directory>\\~.\\|.\\|."
msgstr ""
"B</sbin/ldconfig> [B<-nNvXV>] [B<-f> I<conf>] [B<-C> I<caché>] [B<-r> "
"I<raíz>] I<directorio>..."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "B</sbin/ldconfig> B<-l> [B<-v>] I<library>..."
msgid "B<-l> [B<-v>] I<library>\\~.\\|.\\|."
msgstr "B</sbin/ldconfig> B<-l> [B<-v>] I<biblioteca>..."

#. #-#-#-#-#  archlinux: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  debian-bookworm: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-unstable: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-40: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-rawhide: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-leap-15-6: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-tumbleweed: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-p>"
msgstr "B<-p>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<ldconfig> creates the necessary links and cache to the most recent "
#| "shared libraries found in the directories specified on the command line, "
#| "in the file I</etc/ld.so.conf>, and in the trusted directories, I</lib> "
#| "and I</usr/lib> (on some 64-bit architectures such as x86-64, I</lib> and "
#| "I</usr/lib> are the trusted directories for 32-bit libraries, while I</"
#| "lib64> and I</usr/lib64> are used for 64-bit libraries)."
msgid ""
"B<\\%ldconfig> creates the necessary links and cache to the most recent "
"shared libraries found in the directories specified on the command line, in "
"the file I</etc/ld.so.conf>, and in the trusted directories, I</lib> and I</"
"usr/lib>.  On some 64-bit architectures such as x86-64, I</lib> and I</usr/"
"lib> are the trusted directories for 32-bit libraries, while I</lib64> and "
"I</usr/lib64> are used for 64-bit libraries."
msgstr ""
"B<ldconfig> crea los vínculos y caché necesarios a las bibliotecas "
"compartidas más recientes que se encuentren en los directorios especificados "
"en la línea de órdenes, tanto en el archivo I</etc/ld.so.conf> como en los "
"directorios de confianza (I</lib> y I</usr/lib>). En algunas arquitecturas "
"de 64 bits, éstos sería para las librerías de 32 bits mientras que para las "
"de 64 bits se usarían  I</lib64> y I</usr/lib64>."

#.  Support for libc4 and libc5 dropped in
#.  8ee878592c4a642937152c8308b8faef86bcfc40 (2022-07-14) as "obsolete
#.  for over twenty years".
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The cache is used by the run-time linker, I<ld.so> or I<ld-linux.so>.  "
#| "B<ldconfig> checks the header and filenames of the libraries it "
#| "encounters when determining which versions should have their links "
#| "updated."
msgid ""
"The cache is used by the run-time linker, I<ld.so> or I<ld-linux.so>.  B<\\"
"%ldconfig> checks the header and filenames of the libraries it encounters "
"when determining which versions should have their links updated.  B<\\"
"%ldconfig> should normally be run by the superuser as it may require write "
"permission on some root owned directories and files."
msgstr ""
"La caché es utilizada por el enlazador durante la ejecución, I<ld.so> o I<ld-"
"linux.so>. B<ldconfig> comprueba la cabecera y los nombres de fichero de las "
"bibliotecas que va encontrando cuando determina qué versiones deberían "
"actualizar sus vínculos."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Note that B<ldconfig> will only look at files that are named I<lib*.so*> "
#| "(for regular shared objects) or I<ld-*.so*> (for the dynamic loader "
#| "itself).  Other files will be ignored.  Also, B<ldconfig> expects a "
#| "certain pattern to how the symlinks are set up, like this example, where "
#| "the middle file (B<libfoo.so.1> here) is the SONAME for the library:"
msgid ""
"B<\\%ldconfig> will look only at files that are named I<lib*.so*> (for "
"regular shared objects) or I<ld-*.so*> (for the dynamic loader itself).  "
"Other files will be ignored.  Also, B<\\%ldconfig> expects a certain pattern "
"to how the symbolic links are set up, like this example, where the middle "
"file (B<libfoo.so.1> here) is the SONAME for the library:"
msgstr ""
"Observe que B<ldconfig> solo tendrá en cuenta archivos que coincidan con "
"estos patrones:  I<lib*.so*> (para los objetos compartidos) or I<ld-*.so*> "
"(para el cargador propiamente dicho). B<ldconfig> espera una forma concreta "
"para configuración de los enlaces dinamicos. En este ejemplo el archivo del "
"medio (B<libfoo.so.1> ) es el SONAME de la biblioteca:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "libfoo.so -E<gt> libfoo.so.1 -E<gt> libfoo.so.1.12\n"
msgstr "libfoo.so -E<gt> libfoo.so.1 -E<gt> libfoo.so.1.12\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Failure to follow this pattern may result in compatibility issues after an "
"upgrade."
msgstr ""
"Si no se sigue ese patrón, es posible que tenga problemas de compatibilidad "
"después de actualizar."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPCIONES"

#. #-#-#-#-#  archlinux: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  debian-unstable: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-40: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-rawhide: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-leap-15-6: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-tumbleweed: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<--time-format> I<format>"
msgid "B<--format=>I<fmt>"
msgstr "B<--time-format>I< formato>"

#. #-#-#-#-#  archlinux: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  debian-bookworm: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-unstable: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-40: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-rawhide: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-leap-15-6: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-tumbleweed: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<-u >I<fd>"
msgid "B<-c\\~>I<fmt>"
msgstr "B<-u >I<fd>"

#.  commit 45eca4d141c047950db48c69c8941163d0a61fcd
#.  commit cad64f778aced84efdaa04ae64f8737b86f063ab
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "(Since glibc 2.2)  Cache format to use: I<old>, I<new>, or I<compat>.  "
#| "Since glibc 2.32, the default is I<new>.  Before that, it was I<compat>."
msgid ""
"(Since glibc 2.2)  Use cache format I<fmt>, which is one of B<old>, B<new>, "
"or B<\\%compat>.  Since glibc 2.32, the default is B<new>.  Before that, it "
"was B<\\%compat>."
msgstr ""
"A partir de la versión 2.2 de glibc, el formato de cache es el siguiente: "
"I<old>, I<new>, o I<compat>. A partir de la versión 2.32, por defecto será "
"I<new> y anteriormente era I<compat>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<-C >I<cache>"
msgid "B<-C\\~>I<cache>"
msgstr "B<-C >I<caché>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Use I<cache> instead of I</etc/ld.so.cache>."
msgstr "Utiliza I<caché> en vez de I</etc/ld.so.cache>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<-f >I<conf>"
msgid "B<-f\\~>I<conf>"
msgstr "B<-f >I<conf>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Use I<conf> instead of I</etc/ld.so.conf>."
msgstr "Utiliza I<conf> en vez de I</etc/ld.so.conf>."

#. #-#-#-#-#  archlinux: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  debian-unstable: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-40: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-rawhide: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-leap-15-6: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-tumbleweed: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<-i>, B<--ignore-aux-cache>"
msgid "B<--ignore-aux-cache>"
msgstr "B<-i>, B<--ignore-aux-cache>"

#. #-#-#-#-#  archlinux: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  debian-bookworm: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-unstable: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-40: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-rawhide: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-leap-15-6: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-tumbleweed: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>"
msgstr "B<-i>"

#.  commit 27d9ffda17df4d2388687afd12897774fde39bcc
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "(Since glibc 2.7)  Ignore auxiliary cache file."
msgstr ""
"A partir de la versión 2.7 de glibc se ignora el archivo de caché auxiliar."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-l>"
msgstr "B<-l>"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "(Since glibc 2.2)  Library mode.  Manually link individual libraries.  "
#| "Intended for use by experts only."
msgid ""
"(Since glibc 2.2)  Interpret each operand as a library name and configure "
"its links.  Intended for use only by experts."
msgstr ""
"A partir de la versión 2.2 de glibc, Modo biblioteca. Crea vínculos "
"manualmente en bibliotecas individuales. Para usuarios avanzados."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-n>"
msgstr "B<-n>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Process only the directories specified on the command line.  Don't "
#| "process the trusted directories, nor those specified in I</etc/ld.so."
#| "conf>.  Implies B<-N>."
msgid ""
"Process only the directories specified on the command line; don't process "
"the trusted directories, nor those specified in I</etc/ld.so.conf>.  Implies "
"B<-N>."
msgstr ""
"Procesa solo los directorios especificados en la línea de órdenes. No "
"procesa los directorios de confianza, ni los especificados en I</etc/ld.so."
"conf>. Implica B<-N>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-N>"
msgstr "B<-N>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Don't rebuild the cache.  Unless B<-X> is also specified, links are still "
"updated."
msgstr ""
"No reconstruir la caché. A menos que se especifique también B<-X>, los "
"vínculos sí son actualizados."

#. #-#-#-#-#  archlinux: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  debian-unstable: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-40: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-rawhide: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-leap-15-6: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-tumbleweed: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<-p>, B<--print-cache>"
msgid "B<--print-cache>"
msgstr "B<-p>, B<--print-cache>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Print the lists of directories and candidate libraries stored in the current "
"cache."
msgstr ""
"Imprime las listas de directorios y bibliotecas candidatas almcenadas en la "
"caché actual."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<-r >I<root>"
msgid "B<-r\\~>I<root>"
msgstr "B<-r >I<raíz>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Change to and use I<root> as the root directory."
msgstr "Cambia al directorio I<raíz> y lo utiliza como directorio raíz."

#. #-#-#-#-#  archlinux: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  debian-unstable: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-40: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-rawhide: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-leap-15-6: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-tumbleweed: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--verbose>"
msgstr "B<--verbose>"

#. #-#-#-#-#  archlinux: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  debian-bookworm: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-unstable: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-40: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-rawhide: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-leap-15-6: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-tumbleweed: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>"
msgstr "B<-v>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Verbose mode.  Print current version number, the name of each directory as "
"it is scanned, and any links that are created.  Overrides quiet mode."
msgstr ""
"Modo verboso. Imprime el número de versión actual, el nombre de cada "
"directorio a medida que se examina, y los enlaces que se crean. Tiene "
"preferencia sobre el modo silencioso."

#. #-#-#-#-#  archlinux: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  debian-unstable: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-40: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-rawhide: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-leap-15-6: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-tumbleweed: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. #-#-#-#-#  archlinux: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  debian-bookworm: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-unstable: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-40: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  fedora-rawhide: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  opensuse-leap-15-6: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-tumbleweed: ldconfig.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>"
msgstr "B<-V>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Print program version."
msgstr "Imprime la versión del programa"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-X>"
msgstr "B<-X>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Don't update links.  Unless B<-N> is also specified, the cache is still "
"rebuilt."
msgstr ""
"No actualizar los enlaces. A menos que se especifique también B<-N>, la "
"caché se reconstruye."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "ARCHIVOS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I</lib/ld.so>"
msgstr "I</lib/ld.so>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "Run-time linker/loader."
msgid "is the run-time linker/loader."
msgstr "Enlazador/cargador en tiempo de ejecución."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/ld.so.conf>"
msgstr "I</etc/ld.so.conf>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "File containing a list of directories, one per line, in which to search "
#| "for libraries."
msgid ""
"contains a list of directories, one per line, in which to search for "
"libraries."
msgstr ""
"Fichero que contiene una lista de directorios, uno en cada línea, dónde se "
"buscarán las librerías."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/ld.so.cache>"
msgstr "I</etc/ld.so.cache>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "File containing an ordered list of libraries found in the directories "
#| "specified in I</etc/ld.so.conf>, as well as those found in the trusted "
#| "directories."
msgid ""
"contains an ordered list of libraries found in the directories specified in "
"I</etc/ld.so.conf>, as well as those found in the trusted directories."
msgstr ""
"Fichero que contiene una lista ordenada de bibliotecas encontradas en los "
"directorios especificados en B</etc/ld.so.conf> y en los directorios "
"confiables."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<ldd>(1), B<ld.so>(8)"
msgstr "B<ldd>(1), B<ld.so>(8)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-01-07"
msgstr "7 Enero 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de Manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "(Since glibc 2.2)  Library mode.  Manually link individual libraries.  "
#| "Intended for use by experts only."
msgid ""
"(Since glibc 2.2)  Interpret each operand as a libary name and configure its "
"links.  Intended for use only by experts."
msgstr ""
"A partir de la versión 2.2 de glibc, Modo biblioteca. Crea vínculos "
"manualmente en bibliotecas individuales. Para usuarios avanzados."

#. type: TH
#: fedora-40
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Páginas de Manual de Linux 6.06"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-11"
msgstr "11 Marzo 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Páginas de Manual de Linux 6.04"

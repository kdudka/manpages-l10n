# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Gerardo Aburruzaga García <gerardo.aburruzaga@uca.es>, 1998.
# Miguel Pérez Ibars <mpi79470@alu.um.es>, 2004.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-05-01 15:53+0200\n"
"PO-Revision-Date: 2004-09-29 19:53+0200\n"
"Last-Translator: Miguel Pérez Ibars <mpi79470@alu.um.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "setresuid"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-10-31"
msgstr "31 Octubre 2023"

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr "Páginas de Manual de Linux 6.7"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "setresuid, setresgid - set real, effective and saved user or group ID"
msgid "setresuid, setresgid - set real, effective, and saved user or group ID"
msgstr ""
"setresuid, setresgid - establecen el UID o GID efectivo, real y salvado"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca Estándar C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<#define _GNU_SOURCE>         /* See feature_test_macros(7) */\n"
#| "B<#include E<lt>stdio.hE<gt>>\n"
msgid ""
"B<#define _GNU_SOURCE>         /* See feature_test_macros(7) */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#define _GNU_SOURCE>         /* Vea feature_test_macros(7) */\n"
"B<#include E<lt>stdio.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int setresuid(uid_t >I<ruid>B<, uid_t >I<euid>B<, uid_t >I<suid>B<);>"
msgid ""
"B<int setresuid(uid_t >I<ruid>B<, uid_t >I<euid>B<, uid_t >I<suid>B<);>\n"
"B<int setresgid(gid_t >I<rgid>B<, gid_t >I<egid>B<, gid_t >I<sgid>B<);>\n"
msgstr "B<int setresuid(uid_t >I<ruid>B<, uid_t >I<euid>B<, uid_t >I<suid>B<);>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<setresuid> sets the real user ID, the effective user ID, and the saved "
#| "set-user-ID of the current process."
msgid ""
"B<setresuid>()  sets the real user ID, the effective user ID, and the saved "
"set-user-ID of the calling process."
msgstr ""
"B<setresuid> establece el UID real, el UID efectivo y el SETUID salvado del "
"proceso actual."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Unprivileged user processes (i.e., processes with each of real, effective "
#| "and saved user ID nonzero) may change the real, effective and saved user "
#| "ID, each to one of: the current uid, the current effective uid or the "
#| "current saved uid."
msgid ""
"An unprivileged process may change its real UID, effective UID, and saved "
"set-user-ID, each to one of: the current real UID, the current effective "
"UID, or the current saved set-user-ID."
msgstr ""
"Los procesos de usuarios no privilegiados (esto es, procesos con cada uno de "
"los UIDs real, efectivo y salvado distintos de cero) pueden cambiar el UID "
"real, efectivo y salvado, cada uno a uno de: el UID actual, el UID efectivo "
"actual o el UID salvado actual."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A privileged process (on Linux, one having the B<CAP_SETUID> capability)  "
"may set its real UID, effective UID, and saved set-user-ID to arbitrary "
"values."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If one of the arguments equals -1, the corresponding value is not changed."
msgstr ""
"Si uno de los argumentos es igual a -1, el valor correspondiente no se "
"cambia."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Regardless of what changes are made to the real UID, effective UID, and "
"saved set-user-ID, the filesystem UID is always set to the same value as the "
"(possibly new) effective UID."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Completely analogously, B<setresgid> sets the real, effective and saved "
#| "group ID's of the current process, with the same restrictions for "
#| "processes with each of real, effective and saved user ID nonzero."
msgid ""
"Completely analogously, B<setresgid>()  sets the real GID, effective GID, "
"and saved set-group-ID of the calling process (and always modifies the "
"filesystem GID to be the same as the effective GID), with the same "
"restrictions for unprivileged processes."
msgstr ""
"De forma completamente análoga, B<setresgid> establece el GID real, efectivo "
"y salvado del proceso en curso, con las mismas restricciones para procesos "
"con cada uno de los UIDs real, efectivo y salvado distintos de cero."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DEVUELTO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"En caso de éxito se devuelve cero. En caso de error se devuelve -1, y "
"I<errno> se configura para indicar el error."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<Note>: there are cases where B<setresuid>()  can fail even when the caller "
"is UID 0; it is a grave security error to omit checking for a failure return "
"from B<setresuid>()."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERRORES"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EAGAIN>"
msgstr "B<EAGAIN>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The call would change the caller's real UID (i.e., I<ruid> does not match "
"the caller's real UID), but there was a temporary failure allocating the "
"necessary kernel data structures."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<ruid> does not match the caller's real UID and this call would bring the "
"number of processes belonging to the real user ID I<ruid> over the caller's "
"B<RLIMIT_NPROC> resource limit.  Since Linux 3.1, this error case no longer "
"occurs (but robust applications should check for this error); see the "
"description of B<EAGAIN> in B<execve>(2)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"One or more of the target user or group IDs is not valid in this user "
"namespace."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The calling process is not privileged (did not have the necessary capability "
"in its user namespace)  and tried to change the IDs to values that are not "
"permitted.  For B<setresuid>(), the necessary capability is B<CAP_SETUID>; "
"for B<setresgid>(), it is B<CAP_SETGID>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONES"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "C library/kernel differences"
msgstr "Diferencias núcleo / biblioteca C"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"At the kernel level, user IDs and group IDs are a per-thread attribute.  "
"However, POSIX requires that all threads in a process share the same "
"credentials.  The NPTL threading implementation handles the POSIX "
"requirements by providing wrapper functions for the various system calls "
"that change process UIDs and GIDs.  These wrapper functions (including those "
"for B<setresuid>()  and B<setresgid>())  employ a signal-based technique to "
"ensure that when one thread changes credentials, all of the other threads in "
"the process also change their credentials.  For details, see B<nptl>(7)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "ESTÁNDARES"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "None."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIAL"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Linux 2.1.44, glibc 2.3.2.  HP-UX, FreeBSD."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The original Linux B<setresuid>()  and B<setresgid>()  system calls "
"supported only 16-bit user and group IDs.  Subsequently, Linux 2.4 added "
"B<setresuid32>()  and B<setresgid32>(), supporting 32-bit IDs.  The glibc "
"B<setresuid>()  and B<setresgid>()  wrapper functions transparently deal "
"with the variations across kernel versions."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<getresuid>(2), B<getuid>(2), B<setfsgid>(2), B<setfsuid>(2), "
"B<setreuid>(2), B<setuid>(2), B<capabilities>(7), B<credentials>(7), "
"B<user_namespaces>(7)"
msgstr ""
"B<getresuid>(2), B<getuid>(2), B<setfsgid>(2), B<setfsuid>(2), "
"B<setreuid>(2), B<setuid>(2), B<capabilities>(7), B<credentials>(7), "
"B<user_namespaces>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-04"
msgstr "4 Diciembre 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de Manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm
msgid "These calls are available under Linux since Linux 2.1.44."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"These calls are nonstandard; they also appear on HP-UX and some of the BSDs."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "Under HP-UX and FreeBSD the prototype is found in I<E<lt>unistd.hE<gt>>.  "
#| "Under Linux there is so far no include file giving the prototype - this "
#| "is a glibc bug. Programs using this system call must add the prototype "
#| "themselves."
msgid ""
"Under HP-UX and FreeBSD, the prototype is found in I<E<lt>unistd.hE<gt>>.  "
"Under Linux, the prototype is provided since glibc 2.3.2."
msgstr ""
"Bajo HP-UX y FreeBSD el prototipo se encuentra en I<E<lt>unistd.hE<gt>>.  "
"Bajo Linux no hay hasta ahora fichero de cabecera que incluya el prototipo - "
"ésto es un fallo de glibc. Los programas que usen esta llamada al sistema "
"deben añadir el prototipo ellos mismos."

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Páginas de Manual de Linux 6.06"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30 Marzo 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Páginas de Manual de Linux 6.04"

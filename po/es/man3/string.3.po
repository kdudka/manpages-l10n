# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Gerardo Aburruzaga García <gerardo.aburruzaga@uca.es>, 1998.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-05-01 15:54+0200\n"
"PO-Revision-Date: 1998-03-11 19:55+0200\n"
"Last-Translator: Gerardo Aburruzaga García <gerardo.aburruzaga@uca.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "I<string>"
msgid "string"
msgstr "I<cadena>"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-11-14"
msgstr "14 Noviembre 2023"

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr "Páginas de Manual de Linux 6.7"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"stpcpy, strcasecmp, strcat, strchr, strcmp, strcoll, strcpy, strcspn, "
"strdup, strfry, strlen, strncat, strncmp, strncpy, strncasecmp, strpbrk, "
"strrchr, strsep, strspn, strstr, strtok, strxfrm, index, rindex - string "
"operations"
msgstr ""
"stpcpy, strcasecmp, strcat, strchr, strcmp, strcoll, strcpy, strcspn, "
"strdup, strfry, strlen, strncat, strncmp, strncpy, strncasecmp, strpbrk, "
"strrchr, strsep, strspn, strstr, strtok, strxfrm, index, rindex - "
"operaciones con cadenas de caracteres"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca Estándar C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<#include E<lt>strings.hE<gt>>"
msgstr "B<#include E<lt>strings.hE<gt>>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int strcasecmp(const char *>I<s1>B<, const char *>I<s2>B<);>"
msgstr "B<int strcasecmp(const char *>I<s1>B<, const char *>I<s2>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Compare the strings I<s1> and I<s2> ignoring case."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int strncasecmp(const char *>I<s1>B<, const char *>I<s2>B<, size_t >I<n>B<);>"
msgid "B<int strncasecmp(const char >I<s1>B<[.>I<n>B<], const char >I<s2>B<[.>I<n>B<], size_t >I<n>B<);>"
msgstr "B<int strncasecmp(const char *>I<s1>B<, const char *>I<s2>B<, size_t >I<n>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Compare the first I<n> bytes of the strings I<s1> and I<s2> ignoring case."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<char *index(const char *>I<s>B<, int >I<c>B<);>"
msgstr "B<char *index(const char *>I<s>B<, int >I<c>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Identical to B<strchr>(3)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<char *rindex(const char *>I<s>B<, int >I<c>B<);>"
msgstr "B<char *rindex(const char *>I<s>B<, int >I<c>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Identical to B<strrchr>(3)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>string.hE<gt>>"
msgstr "B<#include E<lt>string.hE<gt>>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<char *stpcpy(char *>I<dest>B<, const char *>I<src>B<);>"
msgid "B<char *stpcpy(char *restrict >I<dest>B<, const char *restrict >I<src>B<);>"
msgstr "B<char *stpcpy(char *>I<dest>B<, const char *>I<src>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Copy a string from I<src> to I<dest>, returning a pointer to the end of the "
"resulting string at I<dest>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<char *strcat(char *>I<dest>B<, const char *>I<src>B<);>"
msgid "B<char *strcat(char *restrict >I<dest>B<, const char *restrict >I<src>B<);>"
msgstr "B<char *strcat(char *>I<dest>B<, const char *>I<src>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Append the string I<src> to the string I<dest>, returning a pointer I<dest>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<char *strchr(const char *>I<s>B<, int >I<c>B<);>"
msgstr "B<char *strchr(const char *>I<s>B<, int >I<c>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Return a pointer to the first occurrence of the character I<c> in the string "
"I<s>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int strcmp(const char *>I<s1>B<, const char *>I<s2>B<);>"
msgstr "B<int strcmp(const char *>I<s1>B<, const char *>I<s2>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Compare the strings I<s1> with I<s2>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int strcoll(const char *>I<s1>B<, const char *>I<s2>B<);>"
msgstr "B<int strcoll(const char *>I<s1>B<, const char *>I<s2>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Compare the strings I<s1> with I<s2> using the current locale."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<char *strcpy(char *>I<dest>B<, const char *>I<src>B<);>"
msgid "B<char *strcpy(char *restrict >I<dest>B<, const char *restrict >I<src>B<);>"
msgstr "B<char *strcpy(char *>I<dest>B<, const char *>I<src>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Copy the string I<src> to I<dest>, returning a pointer to the start of "
"I<dest>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<size_t strcspn(const char *>I<s>B<, const char *>I<reject>B<);>"
msgstr "B<size_t strcspn(const char *>I<s>B<, const char *>I<reject>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Calculate the length of the initial segment of the string I<s> which does "
"not contain any of bytes in the string I<reject>,"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<char *strdup(const char *>I<s>B<);>"
msgstr "B<char *strdup(const char *>I<s>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Return a duplicate of the string I<s> in memory allocated using B<malloc>(3)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<char *strfry(char *>I<string>B<);>"
msgstr "B<char *strfry(char *>I<string>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Randomly swap the characters in I<string>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<size_t strlen(const char *>I<s>B<);>"
msgstr "B<size_t strlen(const char *>I<s>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Return the length of the string I<s>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<char *strncat(char *>I<dest>B<, const char *>I<src>B<, size_t >I<n>B<);>"
msgid ""
"B<char *strncat(char >I<dest>B<[restrict strlen(.>I<dest>B<) + .>I<n>B< + 1],>\n"
"B<       const char >I<src>B<[restrict .>I<n>B<],>\n"
"B<       size_t >I<n>B<);>\n"
msgstr "B<char *strncat(char *>I<dest>B<, const char *>I<src>B<, size_t >I<n>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Append at most I<n> bytes from the unterminated string I<src> to the string "
"I<dest>, returning a pointer to I<dest>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int strncmp(const char *>I<s1>B<, const char *>I<s2>B<, size_t >I<n>B<);>"
msgid "B<int strncmp(const char >I<s1>B<[.>I<n>B<], const char >I<s2>B<[.>I<n>B<], size_t >I<n>B<);>"
msgstr "B<int strncmp(const char *>I<s1>B<, const char *>I<s2>B<, size_t >I<n>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Compare at most I<n> bytes of the strings I<s1> and I<s2>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<char *strpbrk(const char *>I<s>B<, const char *>I<accept>B<);>"
msgstr "B<char *strpbrk(const char *>I<s>B<, const char *>I<accept>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Return a pointer to the first occurrence in the string I<s> of one of the "
"bytes in the string I<accept>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<char *strrchr(const char *>I<s>B<, int >I<c>B<);>"
msgstr "B<char *strrchr(const char *>I<s>B<, int >I<c>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Return a pointer to the last occurrence of the character I<c> in the string "
"I<s>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<char *strsep(char **>I<stringp>B<, const char *>I<delim>B<);>"
msgid "B<char *strsep(char **restrict >I<stringp>B<, const char *restrict >I<delim>B<);>"
msgstr "B<char *strsep(char **>I<stringp>B<, const char *>I<delim>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Extract the initial token in I<stringp> that is delimited by one of the "
"bytes in I<delim>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<size_t strspn(const char *>I<s>B<, const char *>I<accept>B<);>"
msgstr "B<size_t strspn(const char *>I<s>B<, const char *>I<accept>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Calculate the length of the starting segment in the string I<s> that "
"consists entirely of bytes in I<accept>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<char *strstr(const char *>I<haystack>B<, const char *>I<needle>B<);>"
msgstr "B<char *strstr(const char *>I<haystack>B<, const char *>I<needle>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Find the first occurrence of the substring I<needle> in the string "
"I<haystack>, returning a pointer to the found substring."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<char *strtok(char *>I<s>B<, const char *>I<delim>B<);>"
msgid "B<char *strtok(char *restrict >I<s>B<, const char *restrict >I<delim>B<);>"
msgstr "B<char *strtok(char *>I<s>B<, const char *>I<delim>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Extract tokens from the string I<s> that are delimited by one of the bytes "
"in I<delim>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<size_t strxfrm(char *>I<dest>B<, const char *>I<src>B<, size_t >I<n>B<);>"
msgid ""
"B<size_t strxfrm(char >I<dest>B<[restrict .>I<n>B<], const char >I<src>B<[restrict .>I<n>B<],>\n"
"B<        size_t >I<n>B<);>\n"
msgstr "B<size_t strxfrm(char *>I<dest>B<, const char *>I<src>B<, size_t >I<n>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Transforms I<src> to the current locale and copies the first I<n> bytes to "
"I<dest>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<char *strncpy(char *>I<dest>B<, const char *>I<src>B<, size_t >I<n>B<);>"
msgid ""
"B<char *strncpy(char >I<dest>B<[restrict .>I<n>B<], const char >I<src>B<[restrict .>I<n>B<],>\n"
"B<       size_t >I<n>B<);>\n"
msgstr "B<char *strncpy(char *>I<dest>B<, const char *>I<src>B<, size_t >I<n>B<);>"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Fill a fixed-size buffer with leading non-null bytes from a source array, "
"padding with null bytes as needed."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The string functions perform string operations on NUL-terminated "
#| "strings.  See the individual man pages for descriptions of each function."
msgid ""
"The string functions perform operations on null-terminated strings.  See the "
"individual man pages for descriptions of each function."
msgstr ""
"Las funciones de manejo de cadenas de caracteres realizan operaciones en "
"cadenas de caracteres acabadas en el carácer de código cero. Consulte las "
"páginas individuales del Manual para ver las descripciones de cada función."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<index>(3), B<rindex>(3), B<stpcpy>(3), B<strcasecmp>(3), B<strcat>(3), "
#| "B<strchr>(3), B<strcmp>(3), B<strcoll>(3), B<strcpy>(3), B<strcspn>(3), "
#| "B<strdup>(3), B<strfry>(3), B<strlen>(3), B<strncasecmp>(3), "
#| "B<strncat>(3), B<strncmp>(3), B<strncpy>(3), B<strpbrk>(3), "
#| "B<strrchr>(3), B<strsep>(3), B<strspn>(3), B<strstr>(3), B<strtok>(3), "
#| "B<strxfrm>(3)"
msgid ""
"B<bstring>(3), B<stpcpy>(3), B<strcasecmp>(3), B<strcat>(3), B<strchr>(3), "
"B<strcmp>(3), B<strcoll>(3), B<strcpy>(3), B<strcspn>(3), B<strdup>(3), "
"B<strfry>(3), B<strlen>(3), B<strncasecmp>(3), B<strncat>(3), B<strncmp>(3), "
"B<strncpy>(3), B<strpbrk>(3), B<strrchr>(3), B<strsep>(3), B<strspn>(3), "
"B<strstr>(3), B<strtok>(3), B<strxfrm>(3)"
msgstr ""
"B<index>(3), B<rindex>(3), B<stpcpy>(3), B<strcasecmp>(3), B<strcat>(3), "
"B<strchr>(3), B<strcmp>(3), B<strcoll>(3), B<strcpy>(3), B<strcspn>(3), "
"B<strdup>(3), B<strfry>(3), B<strlen>(3), B<strncasecmp>(3), B<strncat>(3), "
"B<strncmp>(3), B<strncpy>(3), B<strpbrk>(3), B<strrchr>(3), B<strsep>(3), "
"B<strspn>(3), B<strstr>(3), B<strtok>(3), B<strxfrm>(3)"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "2023-01-22"
msgstr "22 Enero 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de Manual de Linux 6.03"

#. type: SS
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "Obsolete functions"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Copy at most I<n> bytes from string I<src> to I<dest>, returning a pointer "
"to the start of I<dest>."
msgstr ""

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Páginas de Manual de Linux 6.06"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Páginas de Manual de Linux 6.04"

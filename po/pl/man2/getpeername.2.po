# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Przemek Borys <pborys@dione.ids.pl>, 1998.
# Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>, 2002.
# Michał Kułach <michal.kulach@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-05-01 15:40+0200\n"
"PO-Revision-Date: 2024-03-11 20:24+0100\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "getpeername"
msgstr "getpeername"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-10-31"
msgstr "31 października 2023 r."

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr "Linux man-pages 6.7"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "getpeername - get name of connected peer socket"
msgstr "getpeername - pobiera nazwę drugiej strony połączonego gniazda"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTEKA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standardowa biblioteka C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/socket.hE<gt>>\n"
msgstr "B<#include E<lt>sys/socket.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int getpeername(int >I<sockfd>B<, struct sockaddr *restrict >I<addr>B<,>\n"
"B<                socklen_t *restrict >I<addrlen>B<);>\n"
msgstr ""
"B<int getpeername(int >I<sockfd>B<, struct sockaddr *restrict >I<addr>B<,>\n"
"B<                socklen_t *restrict >I<addrlen>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<getpeername>()  returns the address of the peer connected to the socket "
"I<sockfd>, in the buffer pointed to by I<addr>.  The I<addrlen> argument "
"should be initialized to indicate the amount of space pointed to by "
"I<addr>.  On return it contains the actual size of the name returned (in "
"bytes).  The name is truncated if the buffer provided is too small."
msgstr ""
"B<getpeername>() zwraca adres drugiej strony równorzędnego połączenia "
"odbywającego się poprzez gniazdo I<sockfd>, w buforze na który wskazuje "
"I<addr>. Argument I<addrlen> powinien być zainicjalizowany tak, aby podawać "
"rozmiar obszaru wskazywanego przez I<addr>. Po zakończeniu, będzie on "
"zawierać rzeczywisty rozmiar zwróconej nazwy (w bajtach). Nazwa jest "
"obcinana, jeśli zadany bufor jest zbyt mały."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The returned address is truncated if the buffer provided is too small; in "
"this case, I<addrlen> will return a value greater than was supplied to the "
"call."
msgstr ""
"Zwracany adres jest przycinany, jeśli udostępniony bufor jest zbyt mały; w "
"tym przypadku I<addrlen> zwróci wartość większą niż była podana w wywołaniu."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "WARTOŚĆ ZWRACANA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"Po pomyślnym zakończeniu zwracane jest zero. Po błędzie zwracane jest -1 i "
"ustawiane I<errno>, wskazując błąd."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "BŁĘDY"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The argument I<sockfd> is not a valid file descriptor."
msgstr "Argument I<sockfd> nie jest prawidłowym deskryptorem."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<addr> argument points to memory not in a valid part of the process "
"address space."
msgstr "Parametr I<addr> wskazuje poza dostępną przestrzeń adresową procesu."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<addrlen> is invalid (e.g., is negative)."
msgstr "I<addrlen> jest nieprawidłowe (np. jest ujemne)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOBUFS>"
msgstr "B<ENOBUFS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Insufficient resources were available in the system to perform the operation."
msgstr ""
"Dostępna ilość zasobów systemowych jest niewystarczająca dla wykonania "
"operacji."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTCONN>"
msgstr "B<ENOTCONN>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The socket is not connected."
msgstr "Gniazdo nie jest podłączone."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTSOCK>"
msgstr "B<ENOTSOCK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The file descriptor I<sockfd> does not refer to a socket."
msgstr "Deskryptor pliku I<sockfd> nie odnosi się do gniazda."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDY"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIA"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2001, SVr4, 4.4BSD (first appeared in 4.2BSD)."
msgstr "POSIX.1-2001, SVr4, 4.4BSD (pojawiło się pierwotnie w 4.2BSD)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "UWAGI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For stream sockets, once a B<connect>(2)  has been performed, either socket "
"can call B<getpeername>()  to obtain the address of the peer socket.  On the "
"other hand, datagram sockets are connectionless.  Calling B<connect>(2)  on "
"a datagram socket merely sets the peer address for outgoing datagrams sent "
"with B<write>(2)  or B<recv>(2).  The caller of B<connect>(2)  can use "
"B<getpeername>()  to obtain the peer address that it earlier set for the "
"socket.  However, the peer socket is unaware of this information, and "
"calling B<getpeername>()  on the peer socket will return no useful "
"information (unless a B<connect>(2)  call was also executed on the peer).  "
"Note also that the receiver of a datagram can obtain the address of the "
"sender when using B<recvfrom>(2)."
msgstr ""
"W przypadku gniazd strumieniowych, po przeprowadzeniu B<connect>(2), każde z "
"gniazd może wywołać B<getpeername>(), aby pozyskać adres drugiego gniazda. Z "
"drugiej strony, gniazda datagramowe są bezpołączeniowe. Wywołanie "
"B<connect>(2) na gnieździe datagramowym jedynie ustawia adres drugiej strony "
"w celu wysyłania datagramów za pomocą B<write>(2) lub B<recv>(2). Odbiorca "
"B<connect>(2)  może użyć B<getpeername>() do pozyskania adresu drugiego "
"gniazda, które ustawiło wcześniej dla gniazda. Jednakże drugie gniazdo nie "
"ma tej wiedzy, zatem wywołanie B<getpeername>() na drugim gnieździe nie "
"zwróci żadnej przydatnej informacji (chyba, że wywołanie B<connect>(2) "
"wykonano również na drugim gnieździe). Proszę zauważyć, że odbiorca "
"datagramu może pozyskać adres wysyłającego za pomocą B<recvfrom>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<accept>(2), B<bind>(2), B<getsockname>(2), B<ip>(7), B<socket>(7), "
"B<unix>(7)"
msgstr ""
"B<accept>(2), B<bind>(2), B<getsockname>(2), B<ip>(7), B<socket>(7), "
"B<unix>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-10-30"
msgstr "30 października 2022 r."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm
msgid ""
"POSIX.1-2001, POSIX.1-2008, SVr4, 4.4BSD (B<getpeername>()  first appeared "
"in 4.2BSD)."
msgstr ""
"SVr4, 4.4BSD (funkcja B<getpeername>() pojawiła się pierwotnie w 4.2BSD)."

#. type: Plain text
#: debian-bookworm
msgid "For background on the I<socklen_t> type, see B<accept>(2)."
msgstr ""
"Więcej informacji o typie I<socklen_t> opisano w podręczniku B<accept>(2)."

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-04-03"
msgstr "3 kwietnia 2023 r."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

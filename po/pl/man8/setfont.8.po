# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Przemek Borys <pborys@dione.ids.pl>, 1999.
# Grzegorz Goławski <grzegol@pld.org.pl>, 2002.
# Robert Luberda <robert@debian.org>, 2017.
# Michał Kułach <michal.kulach@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2024-03-09 15:46+0100\n"
"PO-Revision-Date: 2024-05-05 20:50+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SETFONT"
msgstr "SETFONT"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "11 Feb 2001"
msgstr "11 lutego 2001"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "kbd"
msgstr "kbd"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "setfont - load EGA/VGA console screen font"
msgstr "setfont - ładuje czcionkę konsolową ekranu dla EGA/VGA"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<setfont> [B<-O> I<font+umap.orig>] [B<-o> I<font.orig>] [B<-om> I<cmap."
"orig>] [B<-ou> I<umap.orig>] [B<-> I<N>] [I<font.new ...\" ]> [B<-m> "
"I<cmap>] [B<-u> I<umap>] [B<-C> I<console>] [B<-h> I<H>] [B<-f>] [B<-v>] [B<-"
"V>]"
msgstr ""
"B<setfont> [B<-O> I<font+u-mapa.orig>] [B<-o> I<font.orig>] [B<-om> I<k-mapa."
"orig>] [B<-ou> I<u-mapa.orig>] [B<-> I<N>] [I<font.new ...\" ]> [B<-m> I<k-"
"mapa>] [B<-u> I<u-mapa>] [B<-C> I<konsola>] [B<-h> I<H>] [B<-f>] [B<-v>] [B<-"
"V>]"

#. type: IX
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "setfont command"
msgstr "polecenie setfont"

#. type: IX
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "\\fLsetfont\\fR command"
msgstr "polecenie \\fLsetfont\\fR"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<setfont> command reads a font from the file I<font.new> and loads it "
"into the EGA/VGA character generator, and optionally outputs the previous "
"font.  It can also load various mapping tables and output the previous "
"versions."
msgstr ""
"Polecenie I<setfont> wczytuje czcionkę z pliku I<font.new> i ładuje ją do "
"generatora znaków EGA/VGA oraz opcjonalnie wypisuje poprzednią czcionkę. "
"Może również wczytać różne tablice odwzorowań i wypisać zawartość "
"poprzednich."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If no args are given (or only the option -I<N> for some number I<N>), then a "
"default (8xI<N>)  font is loaded (see below).  One may give several small "
"fonts, all containing a Unicode table, and B<setfont> will combine them and "
"load the union.  Typical use:"
msgstr ""
"Jeżeli nie podano żadnych argumentów (lub tylko opcję -I<N> dla jakiejś "
"liczby I<N>), wtedy ładowana jest domyślna czcionka (8xI<N>)  (zobacz "
"poniżej). Można podać wiele małych czcionek, zawierających tablice "
"unikodowe, a B<setfont> połączy je i wczyta całość. Typowe użycie:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<setfont>"
msgstr "B<setfont>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Load a default font."
msgstr "Wczytuje domyślną czcionkę."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<setfont drdos8x16>"
msgstr "B<setfont drdos8x16>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Load a given font (here the 448-glyph drdos font)."
msgstr ""
"Wczytuje podaną czcionkę (tutaj 448-glifowa [448-glyph] czcionka drdos)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<setfont cybercafe -u cybercafe>"
msgstr "B<setfont cybercafe -u cybercafe>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Load a given font that does not have a Unicode map and provide one "
"explicitly."
msgstr ""
"Wczytuje podaną czcionkę, który nie ma mapy unikodowej, oraz podaną mapę."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<setfont LatArCyrHeb-19 -m 8859-2>"
msgstr "B<setfont LatArCyrHeb-19 -m 8859-2>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Load a given font (here a 512-glyph font combining several character sets) "
"and indicate that one's local character set is ISO 8859-2."
msgstr ""
"Wczytuje podaną czcionkę (tutaj 512-glifowa czcionka łącząca różne zestawy "
"znaków)  i określa, że lokalnym zestawem znaków jest ISO 8859-2."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note: if a font has more than 256 glyphs, only 8 out of 16 colors can be "
"used simultaneously. It can make console perception worse (loss of intensity "
"and even some colors)."
msgstr ""
"Uwaga: jeśli czcionka ma więcej niż 256 glifów, to tylko 8 z 16 kolorów "
"będzie można użyć jednocześnie. Może to pogorszyć postrzeganie konsoli "
"(utrata intensywności albo i nawet niektórych kolorów)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FONT FORMATS"
msgstr "FORMATY CZCIONEK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The standard Linux font format is the PSF font.  It has a header describing "
"font properties like character size, followed by the glyph bitmaps, "
"optionally followed by a Unicode mapping table giving the Unicode value for "
"each glyph.  Several other (obsolete) font formats are recognized.  If the "
"input file has code page format (probably with suffix .cp), containing three "
"fonts with sizes e.g. 8x8, 8x14 and 8x16, then one of the options -8 or -14 "
"or -16 must be used to select one.  Raw font files are binary files of size "
"256*I<N> bytes, containing bit images for each of 256 characters, one byte "
"per scan line, and I<N> bytes per character (0 E<lt> I<N> E<lt>= 32).  Most "
"fonts have a width of 8 bits, but with the framebuffer device (fb)  other "
"widths can be used."
msgstr ""
"Standardowym formatem czcionek w Linuksie jest PSF. Zawiera on nagłówek "
"opisujący własności czcionki, takie jak rozmiar znaku, po którym występuje "
"mapa bitowa glifów, po której opcjonalnie występować może tablica odwzorowań "
"unikodowych, dająca wartość unikodową dla każdego glifu. Rozpoznawanych jest "
"również wiele innych (przestarzałych) formatów.  Gdy plik wejściowy ma "
"format strony kodowej (prawdopodobnie z rozszerzeniem .cp), zawierającej "
"trzy czcionki o rozmiarach np. 8x8, 8x14 i 8x16, wtedy jedna z opcji -8 lub "
"-14 lub -16 musi być podana, by wybrać jeden z nich. Surowe pliki z "
"czcionkami są plikami binarnymi o rozmiarze 256*I<N> bajtów, zawierającymi "
"obrazy bitowe każdego z 256 znaków, po jednym bajcie na każdą skanowaną "
"linię i po I<N> bajtów na znak (0 E<lt> I<N> E<lt>= 32). Większość czcionek "
"ma szerokość 8 bitów, lecz z urządzeniem bufora ramki (fb) użyte mogą być "
"inne szerokości."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FONT HEIGHT"
msgstr "WYSOKOŚĆ CZCIONKI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The program B<setfont> has no built-in knowledge of VGA video modes, but "
"just asks the kernel to load the character ROM of the video card with "
"certain bitmaps. However, since Linux 1.3.1 the kernel knows enough about "
"EGA/VGA video modes to select a different line distance. The default "
"character height will be the number I<N> inferred from the font or specified "
"by option. However, the user can specify a different character height I<H> "
"using the I<-h> option."
msgstr ""
"Program B<setfont> nie zawiera wbudowanych informacji na temat trybów "
"graficznych VGA, ale po prostu prosi jądro o wczytanie pewnej mapy bitowej "
"do pamięci znakowej ROM karty graficznej. Jednak od Linuksa 1.3.1 jądro wie "
"wystarczająco o trybach graficznych EGA/VGA, aby wybrać inną odległość linii "
"(line distance). Domyślną wysokością znaku będzie liczba I<N> wyciągnięta z "
"czcionki lub podana jako opcja. Użytkownik jednak może podać inną wysokość "
"I<H> znaku, używając opcji I<-h>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "CONSOLE MAPS"
msgstr "MAPY KONSOLI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Several mappings are involved in the path from user program output to "
"console display. If the console is in utf8 mode (see B<unicode_start>(1))  "
"then the kernel expects that user program output is coded as UTF-8 (see "
"B<utf-8>(7)), and converts that to Unicode (ucs2).  Otherwise, a translation "
"table is used from the 8-bit program output to 16-bit Unicode values. Such a "
"translation table is called a I<Unicode console map>.  There are four of "
"them: three built into the kernel, the fourth settable using the I<-m> "
"option of B<setfont>.  An escape sequence chooses between these four tables; "
"after loading a I<cmap>, B<setfont> will output the escape sequence Esc ( K "
"that makes it the active translation."
msgstr ""
"W drodze od wyjścia programu użytkownika do wyświetlenia na konsoli "
"zaangażowanych jest kilka odwzorowań. Gdy konsola jest w trybie utf8 (zobacz "
"B<unicode_start>(1)), to wtedy jądro oczekuje, że wyjście programu "
"użytkownika jest kodowane jako UTF-8 (zobacz B<utf-8>(7)) i konwertuje je do "
"Unikodu (ucs2). W przeciwnym razie, 8-bitowe wyjście programu jest "
"zamieniane na 16-bitowe wartości unikodowe za pomocą tablicy translacji. "
"Taka tablica nazywana jest I<unikodową mapą konsoli> (Unicode console map). "
"Są cztery takie tablice: trzy wbudowane w jądro i czwarta, która może być "
"ustawiona, dzięki opcji I<-m> programu B<setfont>. Między tymi tablicami "
"wybiera sekwencja sterująca; po wczytaniu I<cmap>, B<setfont> wyrzuci na "
"wyjściu sekwencję sterującą \"Esc ( K\", co sprawia, że jest to aktywna "
"translacja."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Suitable arguments for the I<-m> option are for example I<8859-1>, "
"I<8859-2>, ..., I<8859-15>, I<cp437>, ..., I<cp1250>."
msgstr ""
"Przykładami argumentów dla opcji I<-m> są I<8859-1>, I<8859-2>, ..., "
"I<8859-15>, I<cp437>, ..., I<cp1250>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Given the Unicode value of the symbol to be displayed, the kernel finds the "
"right glyph in the font using the Unicode mapping info of the font and "
"displays it."
msgstr ""
"Jądro znajduje prawidłowy glif dla podanej wartości unikodowej symbolu, "
"który ma być wyświetlony, używając informacji o odwzorowaniu unikodowym "
"czcionki i wyświetla go."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Old fonts do not have Unicode mapping info, and in order to handle them "
"there are direct-to-font maps (also loaded using I<-m>)  that give a "
"correspondence between user bytes and font positions.  The most common "
"correspondence is the one given in the file I<trivial> (where user byte "
"values are used directly as font positions).  Other correspondences are "
"sometimes preferable since the PC video hardware expects line drawing "
"characters in certain font positions."
msgstr ""
"Stare czcionki nie zawierają informacji o odwzorowaniu unikodowym i dlatego "
"istnieją mapy bezpośrednio-do-czcionki (direct-to-font maps) (wczytywane "
"także dzięki opcji I<-m>), które dają zgodność (correspondence) pomiędzy "
"bajtami użytkownika, a pozycjami czcionek. Najbardziej powszechną zgodnością "
"jest ta podana w pliku I<trivial> (gdzie wartości bajtów użytkownika są "
"używane bezpośrednio jako pozycje czcionek). Czasami preferowane są inne "
"zgodności, gdyż sprzęt PC video oczekuje, że znaki rysowane będą na pewnych "
"pozycjach czcionek."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Giving a I<-m none> argument inhibits the loading and activation of a "
"mapping table.  The previous console map can be saved to a file using the I<-"
"om file> option.  These options of setfont render B<mapscrn>(8)  obsolete. "
"(However, it may be useful to read that man page.)"
msgstr ""
"Gdy podany zostanie argument I<-m none> wczytanie i aktywacja tablicy "
"odwzorowań zostaną powstrzymane. Poprzednia mapa konsoli może być zachowana "
"dzięki opcji I<-om plik>.  Te opcje sprawiają, że B<mapscrn>(8)  jest "
"przestarzały. (Jednak może być użyteczny przy czytaniu tego podręcznika)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "UNICODE FONT MAPS"
msgstr "UNIKODOWE MAPY CZCIONEK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The correspondence between the glyphs in the font and Unicode values is "
"described by a Unicode mapping table.  Many fonts have a Unicode mapping "
"table included in the font file, and an explicit table can be indicated "
"using the I<-u> option. The program B<setfont> will load such a Unicode "
"mapping table, unless a I<-u none> argument is given. The previous Unicode "
"mapping table will be saved as part of the saved font file when the -O "
"option is used. It can be saved to a separate file using the I<-ou file> "
"option.  These options of setfont render B<loadunimap>(8)  obsolete."
msgstr ""
"Zgodność między glifami w czcionce a wartościami unikodowymi jest opisana "
"przez unikodową tablicę odwzorowań. W wielu plikach z czcionkami zawarte są "
"unikodowe tablice odwzorowań, które mogą być wskazane przez opcję I<-u>.  "
"Program B<setfont> wczyta taką unikodową tablicę odwzorowań, chyba że podana "
"została opcja I<-ou none>.  Poprzednia unikodowa tablica odwzorowań będzie "
"zachowana jako część zapisanego pliku z czcionką, gdy użyta będzie opcja -O. "
"Tablica ta może być także zapisana w oddzielnym pliku dzięki opcji I<-ou "
"plik>.  Te opcje sprawiają, że B<loadunimap>(8) jest przestarzały."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The Unicode mapping table should assign some glyph to the `missing "
"character' value U+fffd, otherwise missing characters are not translated, "
"giving a usually very confusing result."
msgstr ""
"Unikodowa tablica odwzorowań powinna przydzielić kilka glifów do "
"\\'brakującego znaku' (missing character) o wartości U+fffd. W przeciwnym "
"razie brakujące znaki nie są tłumaczone, dając pogmatwane rezultaty."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Usually no mapping table is needed, and a Unicode mapping table is already "
"contained in the font (sometimes this is indicated by the .psfu extension), "
"so that most users need not worry about the precise meaning and functioning "
"of these mapping tables."
msgstr ""
"Zazwyczaj nie jest potrzebna tablica odwzorowań, a unikodowa tablica "
"odwzorowań jest już zawarta w foncie (czasami wskazyje na to rozszerzenie ."
"psfu), więc większość użytkowników nie musi się martwić dokładnym znaczeniem "
"i funkcjonalnością tych tablic."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"One may add a Unicode mapping table to a psf font using B<psfaddtable>(1)."
msgstr ""
"Każdy może dodać unikodową tablicę odwzorowań do czcionki psf używając "
"B<psfaddtable>(1)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPCJE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-h >I<H>"
msgstr "B<-h >I<H>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Override font height."
msgstr "Zmień wysokość czcionki."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>"
msgstr "B<-d>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Doubles the size of the font, by replicating all of its pixels vertically "
"and horizontally.  This is suitable for high pixel density (e.g. \"4k\") "
"displays on which the standard fonts are too small to be easily legible.  "
"Due to kernel limitations, this is suitable only for 16x16 or smaller fonts."
msgstr ""
"Podwaja rozmiar czcionki, replikując wszystkie jej piksele pionowo i "
"poziomo. Jest to odpowiednie dla wyświetlaczy o dużej gęstości pikseli (np. "
"\\[Bq]4k\\[rq]), na której standardowe czcionki są zbyt małe, aby były "
"czytelne. Ze względu na ograniczenia w jądrze, jest to odpowiednie tylko dla "
"czcionek 16x16 lub mniejszych."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-m >I<file>"
msgstr "B<-m >I<plik>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Load console map or Unicode console map from I<file>."
msgstr "Wczytuje mapę konsoli lub unikodową mapę konsoli z I<pliku>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-o >I<file>"
msgstr "B<-o >I<plik>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Save previous font in I<file>."
msgstr "Zapisuje poprzednią czcionkę do I<pliku>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-O >I<file>"
msgstr "B<-0 >I<plik>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Save previous font and Unicode map in I<file>."
msgstr "Zapisuje poprzednią czcionkę i mapę unikodową do I<pliku>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-om >I<file>"
msgstr "B<-om >I<plik>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Store console map in I<file>."
msgstr "Zapisuje mapę konsoli do I<pliku>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-ou >I<file>"
msgstr "B<-ou >I<plik>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Save previous Unicode map in I<file>."
msgstr "Zapisuje poprzednią mapę unikodową do I<pliku>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-u >I<file>"
msgstr "B<-u >I<plik>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Load Unicode table describing the font from I<file>."
msgstr "Wczytuje tablicę unikodową opisującą czcionkę z I<pliku>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-C >I<console>"
msgstr "B<-C >I<console>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Set the font for the indicated console. (May require root permissions.)"
msgstr ""
"Ustawia czcionkę dla wskazanej konsoli. (Może wymagać uprawnień "
"administratora)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-f>"
msgstr "B<-f>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Force-load unicode map: Setfont`issues the system call to load the unicode "
"map even if the specified map is empty.  This may be useful in unusual cases."
msgstr ""
"Wymusza załadowanie mapy unikodowej: setfont wysyła wywołanie systemowe w "
"celu załadowania mapy unikodowej nawet, jeśli podana mapa jest pusta. W "
"nietypowych przypadkach może być tu użyteczne."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>"
msgstr "B<-v>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Be verbose."
msgstr "Więcej szczegółów."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>"
msgstr "B<-V>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Print version and exit."
msgstr "Wyświetla informacje o wersji i kończy działanie."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTE"
msgstr "UWAGA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"PC video hardware allows one to use the \"intensity\" bit either to indicate "
"brightness, or to address 512 (instead of 256)  glyphs in the font. So, if "
"the font has more than 256 glyphs, the console will be reduced to 8 (instead "
"of 16) colors."
msgstr ""
"Sprzęt video PC pozwala na użycie bitu \\[Bq]intensywności\\[rq] albo w celu "
"oznaczenia jasności, albo aby pozwolić na 512 (zamiast 256) glifów w "
"czcionce. Tak więc, jeśli czcionka ma więcej niż 256 glifów, to konsola "
"będzie zredukowana do 8 (zamiast 16) kolorów."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "PLIKI"

#. type: TP
#: archlinux opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I</usr/share/kbd/consolefonts>"
msgstr "I</usr/share/kbd/consolefonts>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The default font directory."
msgstr "Domyślny katalog czcionek."

#. type: TP
#: archlinux opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I</usr/share/kbd/unimaps>"
msgstr "I</usr/share/kbd/unimaps>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The default directory for Unicode maps."
msgstr "Domyślny katalog map unikodowych."

#. type: TP
#: archlinux opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I</usr/share/kbd/consoletrans>"
msgstr "I</usr/share/kbd/consoletrans>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The default directory for screen mappings."
msgstr "Domyślny katalog przypisań ekranowych."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The default font is a file I<default> (or I<default8x>N if the -N option was "
"given for some number N)  perhaps with suitable extension (like .psf)."
msgstr ""
"Domyślną czcionką jest plik I<default> (lub I<default8x>N, gdy podana "
"została opcja -N dla jakiejś liczby N), z możliwym odpowiednim rozszerzeniem "
"(takim jak .psf)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<psfaddtable>(1), B<unicode_start>(1), B<loadunimap>(8), B<utf-8>(7), "
"B<mapscrn>(8)"
msgstr ""
"B<psfaddtable>(1), B<unicode_start>(1), B<loadunimap>(8), B<utf-8>(7), "
"B<mapscrn>(8)"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "I</usr/share/consolefonts>"
msgstr "I</usr/share/consolefonts>"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "I</usr/share/unimaps>"
msgstr "I</usr/share/unimaps>"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "I</usr/share/consoletrans>"
msgstr "I</usr/share/consoletrans>"

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I</usr/lib/kbd/consolefonts>"
msgstr "I</usr/lib/kbd/consolefonts>"

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I</usr/lib/kbd/unimaps>"
msgstr "I</usr/lib/kbd/unimaps>"

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I</usr/lib/kbd/consoletrans>"
msgstr "I</usr/lib/kbd/consoletrans>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<setfont> [B<-O> I<font+umap.orig>] [B<-o> I<font.orig>] [B<-om> I<cmap."
"orig>] [B<-ou> I<umap.orig>] [B<-> I<N>] [I<font.new ...\" ]> [B<-m> "
"I<cmap>] [B<-u> I<umap>] [B<-C> I<console>] [B<-h> I<H>] [B<-v>] [B<-V>]"
msgstr ""
"B<setfont> [B<-O> I<font+u-mapa.orig>] [B<-o> I<font.orig>] [B<-om> I<k-mapa."
"orig>] [B<-ou> I<u-mapa.orig>] [B<-> I<N>] [I<font.new ...\" ]> [B<-m> I<k-"
"mapa>] [B<-u> I<u-mapa>] [B<-C> I<konsola>] [B<-h> I<H>] [B<-v>] [B<-V>]"

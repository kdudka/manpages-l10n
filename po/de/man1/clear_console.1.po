# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2023-06-27 19:22+0200\n"
"PO-Revision-Date: 2021-10-09 13:25+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "clear_console"
msgstr "clear_console"

#. type: ds n
#: debian-bookworm debian-unstable
#, no-wrap
msgid "5"
msgstr "5"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<clear_console> - clear the console"
msgstr "B<clear_console> - Leeren der Konsole"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<clear_console>"
msgstr "B<clear_console>"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"B<clear_console> clears your console if this is possible.  It looks in the "
"environment for the terminal type and then in the B<terminfo> database to "
"figure out how to clear the screen. To clear the buffer, it then changes the "
"foreground virtual terminal to another terminal and then back to the "
"original terminal."
msgstr ""
"B<clear_console> leert Ihre Konsole, falls dies möglich ist. Es sucht in der "
"Umgebung nach dem Terminaltyp und dann in der B<terminfo>-Datenbank, um "
"herauszufinden, wie der Bildschirm gelöscht wird. Um den Puffer zu leeren, "
"ändert es dann das virtuelle Vordergrundterminal auf ein anderes Terminal "
"und dann zurück zum ursprünglichen Terminal."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<clear>(1), B<chvt>(1)"
msgstr "B<clear>(1), B<chvt>(1)"

# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.20.0\n"
"POT-Creation-Date: 2024-03-09 15:52+0100\n"
"PO-Revision-Date: 2024-03-09 18:47+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Xpmtoppm User Manual"
msgstr "Xpmtoppm-Benutzerhandbuch"

#. type: TH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "31 December 2011"
msgstr "31. Dezember 2011"

#. type: TH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "netpbm documentation"
msgstr "Netpbm-Dokumentation"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "xpmtoppm - convert an X11 pixmap to a PPM image"
msgstr "xpmtoppm - Ein X11-Rasterbild in ein PPM-Bild konvertieren"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<xpmtoppm>"
msgstr "B<xpmtoppm>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "[B<--alphaout=>{I<alpha-filename>,B<->}] [B<-verbose>]"
msgstr "[B<--alphaout=>{I<Alphadateiname>,B<->}] [B<-verbose>]"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "[I<xpmfile>]"
msgstr "[I<Xpmdatei>]"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

# FIXME B<Netpbm> → B<netpbm>
#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "This program is part of B<Netpbm>(1)  \\&."
msgstr "Dieses Programm ist Teil von B<netpbm>(1)\\&."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<xpbtoppm> reads an X11 pixmap (XPM version 1 or 3) as input and produces a "
"PPM image as output."
msgstr ""
"B<xpbtoppm> liest ein X11-Rasterbild (XPM-Version 1 oder 3) als Eingabe und "
"erstellt ein PPM-Bild als Ausgabe."

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"In addition to the options common to all programs based on libnetpbm\n"
"(most notably B<-quiet>, see \n"
"E<.UR index.html#commonoptions>\n"
" Common Options\n"
"E<.UE>\n"
"\\&), B<xpmtoppm> recognizes the following\n"
"command line options:\n"
msgstr ""
"Zusätzlich zu den Optionen, die alle auf libnetpbm basierende Programme\n"
"akzeptieren (insbesondere B<-quiet>, siehe E<.UR index.html#commonoptions>\n"
" Common Options\n"
"E<.UE>\n"
"\\&), akzeptiert B<xpmtoppm> die folgenden Befehlszeilenoptionen:\n"

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--alphaout=>I<alpha-filename>"
msgstr "B<--alphaout=>I<Alphadateiname>"

# FIXME The second sentence with if not , then does not should be worded positive for easier reading
#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<xpmtoppm> creates a PBM file containing the transparency mask for the "
"image.  If the input image doesn't contain transparency information, the "
"I<alpha-filename> file contains all white (opaque) transparency values.  If "
"you don't specify B<--alphaout>, B<xpmtoppm> does not generate a "
"transparency file, and if the input image has transparency information, "
"B<xpmtoppm> simply discards it."
msgstr ""
"B<xpmtoppm> erstellt eine PBM-Datei, die die Transparenzmaske für das Bild "
"enthält. Falls das Eingabebild keine Transparenzinformationen enthält, wird "
"die Datei I<Alphadateiname> sämtlich weiße (undurchsichtige) "
"Transparenzwerte enthalten. Falls Sie B<--alphaout> nicht angeben, erstellt "
"B<xpmtoppm> keine Transparenzdatei und falls das Ausgabebild über "
"Transparenzinformationen verfügt, verwirft B<xpmtoppm> sie einfach."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If you specify B<-> as the filename, B<xpmtoppm> writes the transparency "
"output to Standard Output and discards the image."
msgstr ""
"Falls Sie B<-> als Dateinamen angeben, schreibt B<xpmtoppm> die "
"Transparenzinformationen in die Standardausgabe und verwirft das Bild."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "See B<pamcomp>(1)  \\& for one way to use the transparency output file."
msgstr ""
"Siehe B<pamcomp>(1)\\& für eine Art, die Transparenzausgabedatei zu "
"verwenden."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<xpmtoppm> can't handle a line longer than 8K characters in the XPM input.  "
"If an input line exceeds this limit, B<xpmtoppm> quits with an error message "
"to that effect.  Before Netpbm 10.30 (October 2005), the limit was 2K."
msgstr ""
"B<xpmtoppm> kann mit Zeilen länger als 8000 Zeichen in der XPM-Eingabe nicht "
"umgehen. Falls eine Eingabezeile diese Beschränkungen überschreitet, beendet "
"sich B<xpmtoppm> mit einer entsprechenden Fehlermeldung. Vor Netpbm 10.30 "
"(Oktober 2005) lag die Beschränkung bei 2000."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--verbose>"
msgstr "B<--verbose>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<xpmtoppm> prints information about its processing on Standard Error."
msgstr ""
"B<xpmtoppm> gibt Informationen über seine Arbeit auf der "
"Standardfehlerausgabe aus."

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIMITATIONS"
msgstr "EINSCHRÄNKUNGEN"

# FIXME as invalid many valid XPM images → many valid XPM images as invalid
#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<xpmtoppm> recognizes only a limited set of the features of XPM Version 3; "
"i.e. it rejects as invalid many valid XPM images."
msgstr ""
"B<xpmtoppm> erkennt nur einen begrenzten Anteil der Funktionalitäten von XPM "
"Version 3; d.h. es weist viele gültige XPM-Bilder als ungültig ab."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The only place a comment block is valid is starting in Column 1 of the line "
"immediately after \"static char ...\"."
msgstr ""
"Die einzige Stelle, an der ein Kommentarblock gültig ist, ist Spalte 1 der "
"Zeile direkt nach »static char …«."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In addition, B<ppmtoxpm> properly recognizes any single-line comment that "
"begins in Column 1 in the color table part of the file."
msgstr ""
"Zusätzlich erkennt B<ppmtoxpm> jeden einzeiligen Kommentar korrekt, der in "
"Spalte 1 im Farbtabellenteil der Datei beginnt."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"There must be for every pixel a default colorname for a color type visual."
msgstr ""
"Für jeden Pixel muss es einen Standardfarbnamen für ein farbfähiges "
"Darstellungssystem geben."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Before Netpbm 10.58 (March 2012), zero bytes per pixel causes the program to "
"fail with a message about premature EOF on input."
msgstr ""
"Vor Netpbm 10.58 (März 2012) führten Null-Byte pro Pixel dazu, dass das "
"Programm mit einer Meldung über vorzeitiges Dateiende (EOF) bei der Eingabe "
"fehlschlug."

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
msgid "B<ppmtoxpm>(1)  \\&, B<pamcomp>(1)  \\&, B<ppm>(1)  \\&"
msgstr "B<ppmtoxpm>(1)\\&, B<pamcomp>(1)\\&, B<ppm>(1)\\&"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Copyright (C) 1991 by Jef Poskanzer."
msgstr "Copyright \\(co 1991 Jef Poskanzer."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Upgraded to work with XPM version 3 by Arnaud Le HorsE<lt>I<lehors@mirsa."
"inria.fr>E<gt>, Tue Apr 9 1991."
msgstr ""
"Arnaud Le HorsE<lt>I<lehors@mirsa.inria.fr>E<gt> aktualisierte das Programm "
"für den Umgang mit XPM Version 3 am Dienstag, 9. April 1991."

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DOCUMENT SOURCE"
msgstr "URSPRUNG DES DOKUMENTS"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This manual page was generated by the Netpbm tool 'makeman' from HTML "
"source.  The master documentation is at"
msgstr ""
"Diese Handbuchseite wurde vom Netpbm-Werkzeug »makeman« aus der HTML-Quelle "
"erstellt. Das Master-Dokument befindet sich unter"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<http://netpbm.sourceforge.net/doc/xpmtoppm.html>"
msgstr "B<http://netpbm.sourceforge.net/doc/xpmtoppm.html>"

#. type: Plain text
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<ppmtoxpm>(1)  \\&, B<pamcomp>(1)  \\&, B<ppm>(5)  \\&"
msgstr "B<ppmtoxpm>(1)\\&, B<pamcomp>(1)\\&, B<ppm>(5)\\&"

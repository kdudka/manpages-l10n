# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-05-01 15:41+0200\n"
"PO-Revision-Date: 2024-03-01 22:12+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.04.1\n"

#. type: TH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB-MKIMAGE"
msgstr "GRUB-MKIMAGE"

#. type: TH
#: fedora-40
#, no-wrap
msgid "February 2024"
msgstr "Februar 2024"

#. type: TH
#: fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GRUB 2.06"
msgstr "GRUB 2.06"

#. type: TH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Dienstprogramme für Benutzer"

#. type: SH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "grub-mkimage - make a bootable image of GRUB"
msgstr "grub-mkimage - ein startfähiges GRUB-Abbild erstellen"

#. type: SH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"B<grub-mkimage> [I<\\,OPTION\\/>...] [I<\\,OPTION\\/>]... [I<\\,MODULES\\/>]"
msgstr ""
"B<grub-mkimage> [I<\\,OPTION\\/>…] [I<\\,OPTION\\/>]… [I<\\,MODULE\\/>]"

#. type: SH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Make a bootable image of GRUB."
msgstr "Erstellt ein startfähiges GRUB-Abbild."

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--config>=I<\\,FILE\\/>"
msgstr "B<-c>, B<--config>=I<\\,DATEI\\/>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "embed FILE as an early config"
msgstr "bettet die angegebene DATEI als Frühkonfiguration ein."

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-C>, B<--compression=>(xz|none|auto)"
msgstr "B<-C>, B<--compression=>(xz|none|auto)"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "choose the compression to use for core image"
msgstr "wählt die für das Speicherkern-Abbild zu verwendende Kompression."

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--disable-shim-lock>"
msgstr "B<--disable-shim-lock>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "disable shim_lock verifier"
msgstr "deaktiviert die shim_lock-Überprüfung."

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--directory>=I<\\,DIR\\/>"
msgstr "B<-d>, B<--directory>=I<\\,VERZEICHNIS\\/>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"use images and modules under DIR [default=/usr/lib/grub/E<lt>platformE<gt>]"
msgstr ""
"verwendet Abbilder und Module im angegebenen VERZEICHNIS (Vorgabe ist /usr/"
"lib/grub/E<lt>PlattformE<gt>)."

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-D>, B<--dtb>=I<\\,FILE\\/>"
msgstr "B<-D>, B<--dtb>=I<\\,DATEI\\/>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "embed FILE as a device tree (DTB)"
msgstr "bettet die angegebene DATEI als »Device tree« (DTB) ein."

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-k>, B<--pubkey>=I<\\,FILE\\/>"
msgstr "B<-k>, B<--pubkey>=I<\\,DATEI\\/>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "embed FILE as public key for PGP signature checking"
msgstr ""
"bettet die angegebene DATEI als öffentlichen Schlüssel für die Überprüfung "
"der PGP-Signatur ein."

# FIXME distance between short and long option
#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-m>,                              B<--memdisk>=I<\\,FILE\\/>"
msgstr "B<-m>,                              B<--memdisk>=I<\\,DATEI\\/>"

# FIXME memdisk → ramdisk?
#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "embed FILE as a memdisk image"
msgstr "bettet die angegebene DATEI als Speichergerät-Abbild ein."

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Implies `-p (memdisk)/boot/grub' and overrides"
msgstr "Impliziert B<-p (Speichergerät)/boot/grub> und setzt jedes"

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "any prefix supplied previously, but the prefix"
msgstr "vorher übergebene Präfix außer Kraft, aber das Präfix"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "itself can be overridden by later options"
msgstr ""
"selbst kann durch später angegebene Optionen außer Kraft gesetzt werden."

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-n>, B<--note>"
msgstr "B<-n>, B<--note>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "add NOTE segment for CHRP IEEE1275"
msgstr "fügt einen NOTE-Teil für CHRP IEEE1275 hinzu."

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output>=I<\\,DATEI\\/>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "output a generated image to FILE [default=stdout]"
msgstr ""
"gibt das erzeugte Abbild in die angegebene DATEI aus (per Vorgabe wird in "
"die Standardausgabe geschrieben)."

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-O>, B<--format>=I<\\,FORMAT\\/>"
msgstr "B<-O>, B<--format>=I<\\,FORMAT\\/>"

# FIXME in FORMAT available formats: → in FORMAT. Available formats:
#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"generate an image in FORMAT available formats: i386-coreboot, i386-"
"multiboot, i386-pc, i386-xen_pvh, i386-pc-pxe, i386-pc-eltorito, i386-efi, "
"i386-ieee1275, i386-qemu, x86_64-efi, i386-xen, x86_64-xen, mipsel-yeeloong-"
"flash, mipsel-fuloong2f-flash, mipsel-loongson-elf, powerpc-ieee1275, "
"sparc64-ieee1275-raw, sparc64-ieee1275-cdcore, sparc64-ieee1275-aout, ia64-"
"efi, mips-arc, mipsel-arc, mipsel-qemu_mips-elf, mips-qemu_mips-flash, "
"mipsel-qemu_mips-flash, mips-qemu_mips-elf, arm-uboot, arm-coreboot-"
"vexpress, arm-coreboot-veyron, arm-efi, arm64-efi, riscv32-efi, riscv64-efi"
msgstr ""
"erzeugt ein Abbild im angegebenen FORMAT. Folgende Formate sind verfügbar: "
"i386-coreboot, i386-multiboot, i386-pc, i386-xen_pvh, i386-pc-pxe, i386-pc-"
"eltorito, i386-efi, i386-ieee1275, i386-qemu, x86_64-efi, i386-xen, x86_64-"
"xen, mipsel-yeeloong-flash, mipsel-fuloong2f-flash, mipsel-loongson-elf, "
"powerpc-ieee1275, sparc64-ieee1275-raw, sparc64-ieee1275-cdcore, sparc64-"
"ieee1275-aout, ia64-efi, mips-arc, mipsel-arc, mipsel-qemu_mips-elf, mips-"
"qemu_mips-flash, mipsel-qemu_mips-flash, mips-qemu_mips-elf, arm-uboot, arm-"
"coreboot-vexpress, arm-coreboot-veyron, arm-efi, arm64-efi, riscv32-efi, "
"riscv64-efi"

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-p>, B<--prefix>=I<\\,DIR\\/>"
msgstr "B<-p>, B<--prefix>=I<\\,VERZEICHNIS\\/>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "set prefix directory"
msgstr "legt das Präfix-Verzeichnis fest."

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--sbat>=I<\\,FILE\\/>"
msgstr "B<-s>, B<--sbat>=I<\\,DATEI\\/>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "SBAT metadata"
msgstr "SBAT-Metadaten"

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-S>, B<--appended-signature-size>=I<\\,SIZE\\/>"
msgstr "B<-S>, B<--appended-signature-size>=I<\\,GRÖSSE\\/>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Add a note segment reserving SIZE bytes for an appended signature"
msgstr ""
"fügt einen Notizbereich hinzu, der die angegebene GRÖSSE an Bytes für die "
"angehängte Signatur reserviert."

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "print verbose messages."
msgstr "gibt ausführliche Meldungen aus."

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-x>, B<--x509>=I<\\,FILE\\/>"
msgstr "B<-x>, B<--x509>=I<\\,DATEI\\/>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "embed FILE as an x509 certificate for appended signature checking"
msgstr ""
"bettet die angegebene DATEI als x509-Zertifikat für die Überprüfung der "
"angehängten Signatur ein."

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "-?, B<--help>"
msgstr "B<-?>, B<--help>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "give this help list"
msgstr "gibt eine kurze Hilfe aus."

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "give a short usage message"
msgstr "gibt eine kurze Meldung zur Verwendung aus."

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "print program version"
msgstr "gibt die Programmversion aus."

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Erforderliche oder optionale Argumente für lange Optionen sind ebenso "
"erforderlich bzw. optional für die entsprechenden Kurzoptionen."

#. type: SH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Melden Sie Fehler (auf Englisch) an E<.MT bug-grub@gnu.org> E<.ME .>"

#. type: SH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<grub-install>(8), B<grub-mkrescue>(1), B<grub-mknetdir>(8)"
msgstr "B<grub-install>(8), B<grub-mkrescue>(1), B<grub-mknetdir>(8)"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The full documentation for B<grub-mkimage> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-mkimage> programs are properly installed "
"at your site, the command"
msgstr ""
"Die vollständige Dokumentation für B<grub-mkimage> wird als ein Texinfo-"
"Handbuch gepflegt. Wenn die Programme B<info>(1) und B<grub-mkimage> auf "
"Ihrem Rechner ordnungsgemäß installiert sind, können Sie mit dem Befehl"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<info grub-mkimage>"
msgstr "B<info grub-mkimage>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "auf das vollständige Handbuch zugreifen."

#. type: TH
#: fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "April 2024"
msgstr "April 2024"

#. type: TH
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "March 2024"
msgstr "März 2024"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "grub-mkimage (GRUB2) 2.12"
msgstr "grub-mkimage (GRUB2) 2.12"

#. type: Plain text
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"use images and modules under DIR [default=/usr/share/grub2/"
"E<lt>platformE<gt>]"
msgstr ""
"verwendet Abbilder und Module im angegebenen VERZEICHNIS (Vorgabe ist /usr/"
"share/grub2/E<lt>PlattformE<gt>)."

# FIXME in FORMAT available formats: → in FORMAT. Available formats:
#. type: Plain text
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"generate an image in FORMAT available formats: i386-coreboot, i386-"
"multiboot, i386-pc, i386-xen_pvh, i386-pc-pxe, i386-pc-eltorito, i386-efi, "
"i386-ieee1275, i386-qemu, x86_64-efi, i386-xen, x86_64-xen, mipsel-yeeloong-"
"flash, mipsel-fuloong2f-flash, mipsel-loongson-elf, powerpc-ieee1275, "
"sparc64-ieee1275-raw, sparc64-ieee1275-cdcore, sparc64-ieee1275-aout, ia64-"
"efi, mips-arc, mipsel-arc, mipsel-qemu_mips-elf, mips-qemu_mips-flash, "
"mipsel-qemu_mips-flash, mips-qemu_mips-elf, arm-uboot, arm-coreboot-"
"vexpress, arm-coreboot-veyron, arm-efi, arm64-efi, loongarch64-efi, riscv32-"
"efi, riscv64-efi"
msgstr ""
"erzeugt ein Abbild im angegebenen FORMAT. Folgende Formate sind verfügbar: "
"i386-coreboot, i386-multiboot, i386-pc, i386-xen_pvh, i386-pc-pxe, i386-pc-"
"eltorito, i386-efi, i386-ieee1275, i386-qemu, x86_64-efi, i386-xen, x86_64-"
"xen, mipsel-yeeloong-flash, mipsel-fuloong2f-flash, mipsel-loongson-elf, "
"powerpc-ieee1275, sparc64-ieee1275-raw, sparc64-ieee1275-cdcore, sparc64-"
"ieee1275-aout, ia64-efi, mips-arc, mipsel-arc, mipsel-qemu_mips-elf, mips-"
"qemu_mips-flash, mipsel-qemu_mips-flash, mips-qemu_mips-elf, arm-uboot, arm-"
"coreboot-vexpress, arm-coreboot-veyron, arm-efi, arm64-efi, loongarch64-efi, "
"riscv32-efi, riscv64-efi"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB2 2.12"
msgstr "GRUB2 2.12"

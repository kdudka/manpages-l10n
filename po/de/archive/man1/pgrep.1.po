# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-02-09 17:05+0100\n"
"PO-Revision-Date: 2021-12-13 08:35+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Bugs: Report translation errors to the Language-Team address.\n"
"X-Generator: Lokalize 21.08.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "PGREP"
msgstr "PGREP"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2020-06-04"
msgstr "4. Juni 2020"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "procps-ng"
msgstr "procps-ng"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "User Commands"
msgstr "Dienstprogramme für Benutzer"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "pgrep, pkill - look up or signal processes based on name and other "
#| "attributes"
msgid ""
"pgrep, pkill, pwait - look up, signal, or wait for processes based on name "
"and other attributes"
msgstr ""
"pgrep, pkill - Prozesse finden oder ein Signal auf Basis des Namens oder "
"anderer Attribute senden"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<pgrep> [options] pattern"
msgstr "B<pgrep> [Optionen] Muster"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<pkill> [options] pattern"
msgstr "B<pkill> [Optionen] Muster"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid "B<pkill> [options] pattern"
msgid "B<pwait> [options] pattern"
msgstr "B<pkill> [Optionen] Muster"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<pgrep> looks through the currently running processes and lists the process "
"IDs which match the selection criteria to stdout.  All the criteria have to "
"match.  For example,"
msgstr ""
"B<pgrep> durchsucht die gegenwärtig laufenden Prozesse und listet die "
"Prozesskennungen (IDs), welche den Auswahlkriterien entsprechen, in der "
"Standardausgabe auf. Übereinstimmungen sind für alle Auswahlkriterien "
"erforderlich. Beispielsweise listet"

#. type: Plain text
#: opensuse-leap-15-6
msgid "$ pgrep -u root sshd"
msgstr "$ pgrep -u root sshd"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"will only list the processes called B<sshd> AND owned by B<root>.  On the "
"other hand,"
msgstr ""
"nur die Prozesse auf, deren Name B<sshd> lautet UND B<root> gehören. "
"Andererseits listet"

#. type: Plain text
#: opensuse-leap-15-6
msgid "$ pgrep -u root,daemon"
msgstr "$ pgrep -u root,daemon"

#. type: Plain text
#: opensuse-leap-15-6
msgid "will list the processes owned by B<root> OR B<daemon>."
msgstr "jene Prozesse auf, die entweder B<root> ODER B<daemon> gehören."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<pkill> will send the specified signal (by default B<SIGTERM>)  to each "
"process instead of listing them on stdout."
msgstr ""
"B<pkill> sendet das angegebene Signal (per Vorgabe B<SIGTERM>) an jeden "
"Prozess, anstatt diese in der Standardausgabe aufzulisten."

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "B<pkill> will send the specified signal (by default B<SIGTERM>)  to each "
#| "process instead of listing them on stdout."
msgid "B<pwait> will wait for each process instead of listing them on stdout."
msgstr ""
"B<pkill> sendet das angegebene Signal (per Vorgabe B<SIGTERM>) an jeden "
"Prozess, anstatt diese in der Standardausgabe aufzulisten."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<->I<signal>"
msgstr "B<->I<Signal>"

#. type: TQ
#: opensuse-leap-15-6
#, no-wrap
msgid "B<--signal> I<signal>"
msgstr "B<--signal> I<Signal>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Defines the signal to send to each matched process.  Either the numeric or "
"the symbolic signal name can be used.  (B<pkill> only.)"
msgstr ""
"definiert das Signal, das an jeden passenden Prozess gesendet werden soll. "
"Es werden entweder numerische Angaben oder der symbolische Signalname "
"akzeptiert (nur für B<pkill>)."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-c>, B<--count>"
msgstr "B<-c>, B<--count>"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "Suppress normal output; instead print a count of matching processes.  "
#| "When count does not match anything, e.g. returns zero, the command will "
#| "return non-zero value."
msgid ""
"Suppress normal output; instead print a count of matching processes.  When "
"count does not match anything, e.g. returns zero, the command will return "
"non-zero value. Note that for pkill and pwait, the count is the number of "
"matching processes, not the processes that were successfully signaled or "
"waited for."
msgstr ""
"unterdrückt normale Ausgaben und gibt stattdessen die Anzahl der passenden "
"Prozesse aus. Wenn keine Übereinstimmungen gefunden werden, liefert der "
"Befehl einen von 0 verschiedenen Rückgabewert."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-d>, B<--delimiter> I<delimiter>"
msgstr "B<-d>, B<--delimiter> I<Trenner>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Sets the string used to delimit each process ID in the output (by default a "
"newline).  (B<pgrep> only.)"
msgstr ""
"legt die in der Ausgabe als Trenner für die Prozesskennungen zu verwendende "
"Zeichenkette fest. Vorgabe ist ein Zeilenumbruch (gilt nur für B<pgrep>)."

#. type: TP
#: opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid "B<-e>, B<--ed>"
msgid "B<-e>, B<--echo>"
msgstr "B<-e>, B<--ed>"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid "List the process name as well as the process ID.  (B<pgrep> only.)"
msgid "Display name and PID of the process being killed.  (B<pkill> only.)"
msgstr ""
"listet sowohl den Prozessnamen als auch die Prozesskennung auf (nur für "
"B<pgrep>)."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-f>, B<--full>"
msgstr "B<-f>, B<--full>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The I<pattern> is normally only matched against the process name.  When B<-"
"f> is set, the full command line is used."
msgstr ""
"Das I<Muster> wird normalerweise nur auf den Prozessnamen angewendet. Wenn "
"B<-f> gesetzt ist, wird die vollständige Befehlszeile verwendet."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-g>, B<--pgroup> I<pgrp>,..."
msgstr "B<-g>, B<--pgroup> I<Prozessgruppe>, …"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "Only match processes in the process group IDs listed.  Process group 0 is "
#| "translated into B<pgrep>'s or B<pkill>'s own process group."
msgid ""
"Only match processes in the process group IDs listed.  Process group 0 is "
"translated into B<pgrep>'s, B<pkill>'s, or B<pwait>'s own process group."
msgstr ""
"sucht nur nach passenden Prozessen, die in den Prozessgruppenkennungen "
"aufgelistet sind. Die Prozessgruppe 0 wird in die eigene Prozessgruppe von "
"B<pgrep> oder B<pkill> übersetzt."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-G>, B<--group> I<gid>,..."
msgstr "B<-G>, B<--group> I<GID>, …"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Only match processes whose real group ID is listed.  Either the numerical or "
"symbolical value may be used."
msgstr ""
"berücksichtigt nur Prozesse, deren reale Gruppenkennung aufgelistet ist. "
"Hier kann entweder der numerische oder der symbolische Wert verwendet werden."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-i>, B<--ignore-case>"
msgstr "B<-i>, B<--ignore-case>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Match processes case-insensitively."
msgstr ""
"sucht nach Prozessen, ohne Groß- oder Kleinschreibung zu berücksichtigen."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-l>, B<--list-name>"
msgstr "B<-l>, B<--list-name>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "List the process name as well as the process ID.  (B<pgrep> only.)"
msgstr ""
"listet sowohl den Prozessnamen als auch die Prozesskennung auf (nur für "
"B<pgrep>)."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-a>, B<--list-full>"
msgstr "B<-a>, B<--list-full>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "List the full command line as well as the process ID.  (B<pgrep> only.)"
msgstr ""
"listet sowohl die vollständige Befehlszeile als auch die Prozesskennung auf "
"(nur für B<pgrep>)."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-n>, B<--newest>"
msgstr "B<-n>, B<--newest>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Select only the newest (most recently started) of the matching processes."
msgstr ""
"wählt nur den neuesten (zuletzt gestarteten) aus den passenden Prozessen aus."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-o>, B<--oldest>"
msgstr "B<-o>, B<--oldest>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Select only the oldest (least recently started) of the matching processes."
msgstr ""
"wählt nur den ältesten (zuerst gestarteten) aus den passenden Prozessen aus."

#. type: TP
#: opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid "B<-d>, B<--delay> I<seconds>"
msgid "B<-O>, B<--older> I<secs>"
msgstr "B<-d>, B<--delay> I<Sekunden>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Select processes older than secs."
msgstr ""

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-P>, B<--parent> I<ppid>,..."
msgstr "B<-P>, B<--parent> I<Eltern-Prozesskennung>, …"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Only match processes whose parent process ID is listed."
msgstr ""
"sucht nur nach Prozessen, von denen die Kennung des übergeordneten Prozesses "
"aufgelistet ist."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-s>, B<--session> I<sid>,..."
msgstr "B<-s>, B<--session> I<Sitzungskennung>, …"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "Only match processes whose process session ID is listed.  Session ID 0 is "
#| "translated into B<pgrep>'s or B<pkill>'s own session ID."
msgid ""
"Only match processes whose process session ID is listed.  Session ID 0 is "
"translated into B<pgrep>'s, B<pkill>'s, or B<pwait>'s own session ID."
msgstr ""
"sucht nur nach passenden Prozessen, die in den Prozess-Sitzungskennungen "
"aufgelistet ist. Die Sitzungskennung 0 wird in die eigene Prozessgruppe von "
"B<pgrep> oder B<pkill> übersetzt."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-t>, B<--terminal> I<term>,..."
msgstr "B<-t>, B<--terminal> I<Terminal>, …"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Only match processes whose controlling terminal is listed.  The terminal "
"name should be specified without the \"/dev/\" prefix."
msgstr ""
"sucht nur nach passenden Prozessen, deren steuerndes Terminal aufgelistet "
"ist. Der Terminalname sollte ohne das Präfix »dev« angegeben werden."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-u>, B<--euid> I<euid>,..."
msgstr "B<-u>, B<--euid> I<EUID>, …"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Only match processes whose effective user ID is listed.  Either the "
"numerical or symbolical value may be used."
msgstr ""
"sucht nur nach passenden Prozessen, deren effektive Benutzerkennung "
"aufgelistet ist. Sie können entweder numerische oder symbolische Werte "
"verwenden."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-U>, B<--uid> I<uid>,..."
msgstr "B<-U>, B<--uid> I<UID>, …"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Only match processes whose real user ID is listed.  Either the numerical or "
"symbolical value may be used."
msgstr ""
"sucht nur nach passenden Prozessen, deren reale Benutzerkennung aufgelistet "
"ist. Sie können entweder numerische oder symbolische Werte verwenden."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-v>, B<--inverse>"
msgstr "B<-v>, B<--inverse>"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "Negates the matching.  This option is usually used in B<pgrep>'s "
#| "context.  In B<pkill>'s context the short option is disabled to avoid "
#| "accidental usage of the option."
msgid ""
"Negates the matching.  This option is usually used in B<pgrep>'s or "
"B<pwait>'s context.  In B<pkill>'s context the short option is disabled to "
"avoid accidental usage of the option."
msgstr ""
"negiert die Anwendung der Suchkriterien. Diese Option wird üblicherweise mit "
"B<pgrep> verwendet. In B<pkill> ist die Kurzoption deaktiviert, um die "
"zufällige Anwendung dieser Option zu vermeiden."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-w>, B<--lightweight>"
msgstr "B<-w>, B<--lightweight>"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "Shows all thread ids instead of pids in B<pgrep>'s context.  In "
#| "B<pkill>'s context this option is disabled."
msgid ""
"Shows all thread ids instead of pids in B<pgrep>'s or B<pwait>'s context.  "
"In B<pkill>'s context this option is disabled."
msgstr ""
"zeigt bei B<pgrep> alle Thread-Kennungen anstelle der Prozess-Kennungen an. "
"In B<pkill> ist diese Option deaktiviert."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-x>, B<--exact>"
msgstr "B<-x>, B<--exact>"

# FIXME argument formatting
# FIXME line → lines
#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "Only match processes whose names (or command line if -f is specified)  "
#| "B<exactly> match the I<pattern>."
msgid ""
"Only match processes whose names (or command lines if B<-f> is specified)  "
"B<exactly> match the I<pattern>."
msgstr ""
"sucht nur nach passenden Prozessen, deren Namen (oder Befehlszeilen, falls "
"B<-f> angegeben ist) B<exakt> dem I<Muster> entsprechen."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-F>, B<--pidfile> I<file>"
msgstr "B<-F>, B<--pidfile> I<Datei>"

# FIXME I<PID>'s → I<PID>s
#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "Read I<PID>'s from file.  This option is perhaps more useful for B<pkill> "
#| "than B<pgrep>."
msgid ""
"Read I<PID>s from I<file>.  This option is more useful for "
"B<pkill>orB<pwait> than B<pgrep>."
msgstr ""
"liest die I<Prozesskennungen> aus einer Datei. Diese Option ist "
"wahrscheinlich eher für B<pkill> als für B<pgrep> sinnvoll."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-L>, B<--logpidfile>"
msgstr "B<-L>, B<--logpidfile>"

# FIXME argument formatting
#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid "Fail if pidfile (see -F) not locked."
msgid "Fail if pidfile (see B<-F>) not locked."
msgstr "schlägt fehl, wenn die PID-Datei (siehe B<-F>) nicht gesperrt ist."

#. type: TP
#: opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid "B<-P>, B<--parent> I<ppid>,..."
msgid "B<-r>, B<--runstates> I<D,R,S,Z,>..."
msgstr "B<-P>, B<--parent> I<Eltern-Prozesskennung>, …"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid "Only match processes whose parent process ID is listed."
msgid "Match only processes which match the process state."
msgstr ""
"sucht nur nach Prozessen, von denen die Kennung des übergeordneten Prozesses "
"aufgelistet ist."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<--ns >I<pid>"
msgstr "B<--ns >I<PID>"

# FIXME argument formatting
#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "Match processes that belong to the same namespaces. Required to run as "
#| "root to match processes from other users. See --nslist for how to limit "
#| "which namespaces to match."
msgid ""
"Match processes that belong to the same namespaces. Required to run as root "
"to match processes from other users. See B<--nslist> for how to limit which "
"namespaces to match."
msgstr ""
"sucht nach passenden Prozessen, die zu den gleichen Namensräumen gehören. Um "
"Prozesse anderer Benutzer zu finden, sind Root-Rechte erforderlich. In B<--"
"nslist> finden Sie Informationen, wie Sie die Übereinstimmungen im "
"Zusammenhang mit Namensräumen begrenzen können."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<--nslist >I<name>B<,...>"
msgstr "B<--nslist >I<Name>B<, …>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Match only the provided namespaces. Available namespaces: ipc, mnt, net, "
"pid, user,uts."
msgstr ""
"sucht nur in den angegebenen Namensräumen nach Übereinstimmungen. Verfügbare "
"Namensräume: ipc, mnt, net, pid, user, uts."

#. type: TP
#: opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid "B<-q>, B<--queues>"
msgid "B<-q>, B<--queue >I<value>"
msgstr "B<-q>, B<--queues>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Use B<sigqueue(3)> rather than B<kill(2)> and the value argument is used to "
"specify an integer to be sent with the signal. If the receiving process has "
"installed a handler for this signal using the SA_SIGINFO flag to "
"B<sigaction(2)> , then it can obtain this data via the si_value field of the "
"siginfo_t structure."
msgstr ""

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display version information and exit."
msgstr "zeigt Versionsinformationen an und beendet das Programm."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display help and exit."
msgstr "zeigt einen Hilfetext an und beendet das Programm."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "OPERANDS"
msgstr "OPERANDEN"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "I<pattern>"
msgstr "I<Muster>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Specifies an Extended Regular Expression for matching against the process "
"names or command lines."
msgstr ""
"gibt einen erweiterten regulären Ausdruck für die Übereinstimmungen von "
"Prozessnamen oder Befehlszeilen an."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLES"
msgstr "BEISPIELE"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Example 1: Find the process ID of the B<named> daemon:"
msgstr "Beispiel 1: Prozesskennung des B<named>-Daemons suchen:"

#. type: Plain text
#: opensuse-leap-15-6
msgid "$ pgrep -u root named"
msgstr "$ pgrep -u root named"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Example 2: Make B<syslog> reread its configuration file:"
msgstr ""
"Beispiel 2: B<syslog> veranlassen, seine Konfigurationsdatei neu einzulesen:"

#. type: Plain text
#: opensuse-leap-15-6
msgid "$ pkill -HUP syslogd"
msgstr "$ pkill -HUP syslogd"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Example 3: Give detailed information on all B<xterm> processes:"
msgstr ""
"Beispiel 3: Detaillierte Informationen zu allen B<xterm>-Prozessen ausgeben:"

#. type: Plain text
#: opensuse-leap-15-6
msgid "$ ps -fp $(pgrep -d, -x xterm)"
msgstr "$ ps -fp $(pgrep -d, -x xterm)"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Example 4: Make all B<chrome> processes run nicer:"
msgstr "Beispiel 4: Den Nice-Wert für alle B<chrome>-Prozesse erhöhen:"

#. type: Plain text
#: opensuse-leap-15-6
msgid "$ renice +4 $(pgrep chrome)"
msgstr "$ renice +4 $(pgrep chrome)"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "EXIT STATUS"
msgstr "EXIT-STATUS"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "0"
msgstr "0"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "One or more processes matched the criteria. For pkill the process must "
#| "also have been successfully signalled."
msgid ""
"One or more processes matched the criteria. For pkill and pwait, one or more "
"processes must also have been successfully signalled or waited for."
msgstr ""
"Einer oder mehrere Prozesse entsprechen dem Kriterium. Für B<pkill> muss "
"auch ein Signal erfolgreich an den Prozess gesendet worden sein."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "1"
msgstr "1"

#. type: Plain text
#: opensuse-leap-15-6
msgid "No processes matched or none of them could be signalled."
msgstr ""
"Es wurden keine passenden Prozesse gefunden oder an keine von ihnen konnte "
"ein Signal gesendet werden."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "2"
msgstr "2"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Syntax error in the command line."
msgstr "Syntaxfehler in der Befehlszeile."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "3"
msgstr "3"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Fatal error: out of memory etc."
msgstr "Schwerwiegender Fehler: Speicher ausgeschöpft usw."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

# FIXME argument formatting
#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "The process name used for matching is limited to the 15 characters "
#| "present in the output of /proc/I<pid>/stat.  Use the -f option to match "
#| "against the complete command line, /proc/I<pid>/cmdline."
msgid ""
"The process name used for matching is limited to the 15 characters present "
"in the output of /proc/I<pid>/stat.  Use the B<-f> option to match against "
"the complete command line, /proc/I<pid>/cmdline."
msgstr ""
"Der für die Suche verwendete Prozessname ist auf die 15 Zeichen in der "
"Ausgabe von /proc/I<Prozesskennung>/stat beschränkt. Verwenden Sie die "
"Option B<-f>, um stattdessen Übereinstimmungen in der gesamten Befehlszeile "
"(/proc/I<Prozesskennung>/cmdline) zu suchen."

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "The running B<pgrep> or B<pkill> process will never report itself as a "
#| "match."
msgid ""
"The running B<pgrep>, B<pkill>, or B<pwait> process will never report itself "
"as a match."
msgstr ""
"Die laufenden B<pgrep>- oder B<pkill>-Prozesse werden niemals selbst als "
"Treffer gemeldet."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "BUGS"
msgstr "FEHLER"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The options B<-n> and B<-o> and B<-v> can not be combined.  Let me know if "
"you need to do this."
msgstr ""
"Die Optionen B<-n>, B<-o> und B<-v> können nicht kombiniert werden. Bitte "
"informieren Sie die Entwickler, falls das für Ihre Zwecke nötig sein sollte."

#. type: Plain text
#: opensuse-leap-15-6
msgid "Defunct processes are reported."
msgstr "Bereits beendete Prozesse werden gemeldet."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "B<ps>(1), B<regex>(7), B<signal>(7), B<killall>(1), B<skill>(1), "
#| "B<kill>(1), B<kill>(2)"
msgid ""
"B<ps>(1), B<regex>(7), B<signal>(7), B<sigqueue>(3), B<killall>(1), "
"B<skill>(1), B<kill>(1), B<kill>(2)"
msgstr ""
"B<ps>(1), B<regex>(7), B<signal>(7), B<killall>(1), B<skill>(1), B<kill>(1), "
"B<kill>(2)"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: opensuse-leap-15-6
msgid "E<.UR kjetilho@ifi.uio.no> Kjetil Torgrim Homme E<.UE>"
msgstr "E<.UR kjetilho@ifi.uio.no> Kjetil Torgrim Homme E<.UE>"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Please send bug reports to E<.UR procps@freelists.org> E<.UE>"
msgstr ""
"Bitte senden Sie Fehlermeldungen an E<.UR procps@freelists.org> E<.UE>."

# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>, 2010.
# Helge Kreutzmann <debian@helgefjell.de>, 2018, 2019, 2022.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.15.0\n"
"POT-Creation-Date: 2023-12-01 20:34+0100\n"
"PO-Revision-Date: 2022-11-20 06:42+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "HALT"
msgstr "HALT"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "systemd 249"
msgstr "systemd 249"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "halt"
msgstr "halt"

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: opensuse-leap-15-6
msgid "halt, poweroff, reboot - Halt, power-off or reboot the machine"
msgstr ""
"halt, poweroff, reboot - Die Maschine anhalten, ausschalten oder neustarten"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<halt> [OPTIONS...]"
msgstr "B<halt> [OPTIONEN…]"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<poweroff> [OPTIONS...]"
msgstr "B<poweroff> [OPTIONEN…]"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<reboot> [OPTIONS...]"
msgstr "B<reboot> [OPTIONEN…]"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<halt>, B<poweroff>, B<reboot> may be used to halt, power-off, or reboot "
"the machine\\&. All three commands take the same options\\&."
msgstr ""
"B<halt>, B<poweroff>, B<reboot> kann zum Herunterfahren, Ausschalten oder "
"Neustarten der Maschine verwandt werden\\&. Alle drei Befehle akzeptieren "
"die gleichen Optionen\\&."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: opensuse-leap-15-6
msgid "The following options are understood:"
msgstr "Die folgenden Optionen werden verstanden:"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Print a short help text and exit\\&."
msgstr "Zeigt einen kurzen Hilfetext an und beendet das Programm\\&."

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<--halt>"
msgstr "B<--halt>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Halt the machine, regardless of which one of the three commands is "
"invoked\\&."
msgstr ""
"Hält die Maschine an, unabhängig davon, welcher der drei Befehle aufgerufen "
"wurde\\&."

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<-p>, B<--poweroff>"
msgstr "B<-p>, B<--poweroff>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Power-off the machine, regardless of which one of the three commands is "
"invoked\\&."
msgstr ""
"Die Maschine ausschalten, unabhängig davon, welcher der drei Befehle "
"aufgerufen wurde\\&."

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<--reboot>"
msgstr "B<--reboot>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Reboot the machine, regardless of which one of the three commands is "
"invoked\\&."
msgstr ""
"Startet die Maschine neu, unabhängig davon, welcher der drei Befehle "
"verwandt wurde\\&."

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<-f>, B<--force>"
msgstr "B<-f>, B<--force>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Force immediate halt, power-off, or reboot\\&. When specified once, this "
"results in an immediate but clean shutdown by the system manager\\&. When "
"specified twice, this results in an immediate shutdown without contacting "
"the system manager\\&. See the description of B<--force> in B<systemctl>(1)  "
"for more details\\&."
msgstr ""
"Erzwingt sofortiges Anhalten, Ausschalten oder Neustarten\\&. Wird dies "
"einmal angegeben, führt dies zu einem sofortigen aber sauberen "
"Herunterfahren durch den Systemverwalter\\&. Bei doppelter Angabe führt dies "
"zu einem sofortigen Herunterfahren ohne Kontakt zum Diensteverwalter\\&. "
"Lesen Sie die Beschreibung von B<--force> in B<systemctl>(1) für weitere "
"Details\\&."

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<-w>, B<--wtmp-only>"
msgstr "B<-w>, B<--wtmp-only>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Only write wtmp shutdown entry, do not actually halt, power-off, reboot\\&."
msgstr ""
"Nur einen Wtmp-Herunterfahreintrag schreiben, nicht tatsächlich die Maschine "
"anhalten, ausschalten oder neustarten\\&."

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<-d>, B<--no-wtmp>"
msgstr "B<-d>, B<--no-wtmp>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Do not write wtmp shutdown entry\\&."
msgstr "Schreibt keinen Herunterfahreintrag\\&."

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<-n>, B<--no-sync>"
msgstr "B<-n>, B<--no-sync>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Don\\*(Aqt sync hard disks/storage media before halt, power-off, reboot\\&."
msgstr ""
"Die Festplatte/das Speichermedium nicht vor dem Herunterfahren, Ausschalten "
"oder Neustarten synchronisieren\\&."

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<--no-wall>"
msgstr "B<--no-wall>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Do not send wall message before halt, power-off, reboot\\&."
msgstr ""
"Keine Wall-Nachrichten vor dem Anhalten, Herunterfahren oder Neustarten "
"senden\\&."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "EXIT STATUS"
msgstr "EXIT-STATUS"

#. type: Plain text
#: opensuse-leap-15-6
msgid "On success, 0 is returned, a non-zero failure code otherwise\\&."
msgstr ""
"Bei Erfolg wird 0 zurückgegeben, anderenfalls ein Fehlercode ungleich "
"Null\\&."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"These commands are implemented in a way that preserves basic compatibility "
"with the original SysV commands\\&.  B<systemctl>(1)  verbs B<halt>, "
"B<poweroff>, B<reboot> provide the same functionality with some additional "
"features\\&."
msgstr ""
"Diese Befehle sind auf eine Art implementiert, die die grundlegende "
"Kompatibilität zu den ursprünglichen SysV-Befehlen erhalten\\&. Die "
"Unterbefehle B<halt>, B<poweroff>, B<reboot> von B<systemctl>(1) stellen die "
"gleichen Funktionalitäten mit zusätzlichen Funktionen bereit\\&."

# FIXME B<halt> → B<halt>(8)
#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Note that on many SysV systems B<halt> used to be synonymous to B<poweroff>, "
"i\\&.e\\&. both commands would equally result in powering the machine "
"off\\&. systemd is more accurate here, and B<halt> results in halting the "
"machine only (leaving power on), and B<poweroff> is required to actually "
"power it off\\&."
msgstr ""
"Beachten Sie, dass auf vielen SysV-Systemen B<halt>(8) ein Synonmy für "
"B<poweroff> war, d\\&.h\\&. beide Befehle würden gleichermaßen im "
"Ausschalten der Maschine enden\\&. Systemd ist hier genauer und B<halt>(8) "
"führt nur zum Anhalten der Maschine (die eingeschaltet bleibt) und "
"B<poweroff> wird benötigt, um die Maschine tatsächlich auszuschalten\\&."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<systemd>(1), B<systemctl>(1), B<shutdown>(8), B<wall>(1)"
msgstr "B<systemd>(1), B<systemctl>(1), B<shutdown>(8), B<wall>(1)"

# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2020, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-05-01 15:41+0200\n"
"PO-Revision-Date: 2024-03-02 13:08+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.04.1\n"

#. type: TH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB-REBOOT"
msgstr "GRUB-REBOOT"

#. type: TH
#: fedora-40
#, no-wrap
msgid "February 2024"
msgstr "Februar 2024"

#. type: TH
#: fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GRUB 2.06"
msgstr "GRUB 2.06"

#. type: TH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "System Administration Utilities"
msgstr "Systemverwaltungswerkzeuge"

#. type: SH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"grub-reboot - set the default boot entry for GRUB, for the next boot only"
msgstr ""
"grub-reboot - den standardmäßigen Starteintrag für GRUB nur für den nächsten "
"Systemstart festlegen"

#. type: SH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<grub-reboot> [I<\\,OPTION\\/>] I<\\,MENU_ENTRY\\/>"
msgstr "B<grub-reboot> [I<\\,OPTION\\/>] I<\\,MENÜEINTRAG\\/>"

#. type: SH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Set the default boot menu entry for GRUB, for the next boot only."
msgstr ""
"Legt den standardmäßigen Starteintrag für GRUB fest, aber nur für den "
"nächsten Systemstart."

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "print this message and exit"
msgstr "gibt eine Hilfemeldung aus und beendet das Programm."

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "print the version information and exit"
msgstr "gibt die Versionsinformation aus und beendet das Programm."

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--boot-directory>=I<\\,DIR\\/>"
msgstr "B<--boot-directory>=I<\\,VERZEICHNIS\\/>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"expect GRUB images under the directory DIR/grub2 instead of the I<\\,/boot/"
"grub2\\/> directory"
msgstr ""
"erwartet GRUB-Abbilder im Verzeichnis VERZEICHNIS/grub2 anstelle des "
"Verzeichnisses I<\\,/boot/grub2\\/>."

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"MENU_ENTRY is a number, a menu item title or a menu item identifier. Please "
"note that menu items in submenus or sub-submenus require specifying the "
"submenu components and then the menu item component. The titles should be "
"separated using the greater-than character (E<gt>) with no extra spaces. "
"Depending on your shell some characters including E<gt> may need escaping. "
"More information about this is available in the GRUB Manual in the section "
"about the 'default' command."
msgstr ""
"Der MENÜEINTRAG ist entweder eine Zahl oder ein Titel oder Bezeichner eines "
"Menüeintrags. Bitte beachten Sie, dass Einträge in Untermenüs oder deren "
"Untermenüs die Angabe der Untermenü-Komponenten und dann der "
"Menüeintragskomponenten erfordern. Die Titel sollten durch ein Größer-als-"
"Zeichen (E<gt>) ohne zusätzliches Leerzeichen getrennt werden. Abhängig von "
"Ihrer Shell kann es notwendig sein, das E<gt> zu maskieren. Weitere "
"Informationen dazu sind im GRUB-Handbuch im Abschnitt zum »default«-Befehl "
"verfügbar."

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"NOTE: In cases where GRUB cannot write to the environment block, such as "
"when it is stored on an MDRAID or LVM device, the chosen boot menu entry "
"will remain the default even after reboot."
msgstr ""
"ACHTUNG: In jenen Fällen, wenn GRUB nicht in den Umgebungsblock schreiben "
"kann, zum Beispiel wenn diese auf einem MDRAID- oder LVM-Gerät gespeichert "
"ist, dann bleibt der gewählte Startmenüeintrag auch nach einem Neustart die "
"Vorgabe."

#. type: SH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Melden Sie Fehler (auf Englisch) an E<.MT bug-grub@gnu.org> E<.ME .>"

#. type: SH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<grub-set-default>(8), B<grub-editenv>(1)"
msgstr "B<grub-set-default>(8), B<grub-editenv>(1)"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The full documentation for B<grub-reboot> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-reboot> programs are properly installed "
"at your site, the command"
msgstr ""
"Die vollständige Dokumentation für B<grub-reboot> wird als ein Texinfo-"
"Handbuch gepflegt. Wenn die Programme B<info>(1) und B<grub-reboot> auf "
"Ihrem Rechner ordnungsgemäß installiert sind, können Sie mit dem Befehl"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<info grub-reboot>"
msgstr "B<info grub-reboot>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "auf das vollständige Handbuch zugreifen."

#. type: TH
#: fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "April 2024"
msgstr "April 2024"

#. type: TH
#: mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "March 2024"
msgstr "März 2024"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "grub-reboot (GRUB2) 2.12"
msgstr "grub-reboot (GRUB2) 2.12"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB2 2.12"
msgstr "GRUB2 2.12"

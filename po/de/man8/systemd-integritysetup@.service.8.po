# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.15.0\n"
"POT-Creation-Date: 2024-03-01 17:10+0100\n"
"PO-Revision-Date: 2022-10-21 21:16+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SYSTEMD-INTEGRITYSETUP@\\&.SERVICE"
msgstr "SYSTEMD-INTEGRITYSETUP@\\&.SERVICE"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr "systemd 255"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "systemd-integritysetup@.service"
msgstr "systemd-integritysetup@.service"

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"systemd-integritysetup@.service, systemd-integritysetup - Disk integrity "
"protection logic"
msgstr ""
"systemd-integritysetup@.service, systemd-integritysetup - "
"Plattenintegritätsschutzlogik"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid "systemd-integritysetup@\\&.service"
msgstr "systemd-integritysetup@\\&.service"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
msgid "/usr/lib/systemd/systemd-integritysetup"
msgstr "/usr/lib/systemd/systemd-integritysetup"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"systemd-integritysetup@\\&.service is a service responsible for setting up "
"integrity protected block devices\\&. It should be instantiated for each "
"device that requires integrity protection\\&."
msgstr ""
"systemd-integritysetup@\\&.service ist ein Dienst, der für das Einrichten "
"von integritätsgeschützten Blockgeräten verantwortlich ist\\&. Er sollte für "
"jedes Gerät, das Integritätsschutz erfordert, instantiiert werden\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"At early boot and when the system manager configuration is reloaded, entries "
"from B<integritytab>(5)  are converted into systemd-integritysetup@\\&."
"service units by B<systemd-integritysetup-generator>(8)\\&."
msgstr ""
"In der frühen Systemstartphase und wenn die Konfiguration des "
"Systemverwalters neu geladen wird, werden Einträge aus B<integritytab>(5) "
"durch B<systemd-integritysetup-generator>(8) in Units systemd-"
"integritysetup@\\&.service konvertiert\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid "systemd-integritysetup@\\&.service calls B<systemd-integritysetup>\\&."
msgstr ""
"systemd-integritysetup@\\&.service ruft B<systemd-integritysetup> auf\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "COMMANDS"
msgstr "BEFEHLE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid "The following commands are understood by B<systemd-integritysetup>:"
msgstr ""
"Durch B<systemd-integritysetup> werden die folgenden Befehle verstanden:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid "B<attach> I<volume> I<device> [I<key-file|->] [I<option(s)|->]"
msgstr ""
"B<attach> I<Datenträger> I<Gerät> [I<Schlüsseldatei|->] [I<Option(en)|->]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"Create a block device I<volume> using I<device>\\&. See B<integritytab>(5)  "
"and \\m[blue]B<Kernel dm-integrity>\\m[]\\&\\s-2\\u[1]\\d\\s+2 documentation "
"for details\\&."
msgstr ""
"Erstellt ein Blockgerät I<Datenträger> auf I<Gerät>\\&. Siehe "
"B<integritytab>(5) und die Dokumentation \\m[blue]B<Kernel dm-"
"integrity>\\m[]\\&\\s-2\\u[1]\\d\\s+2 für Details\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
msgid "Added in version 250\\&."
msgstr "Hinzugefügt in Version 250\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid "B<detach> I<volume>"
msgstr "B<detach> I<Datenträger>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid "Detach (destroy) the block device I<volume>\\&."
msgstr "Hängt (baut) das Blockgerät I<Datenträger> ab\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid "B<help>"
msgstr "B<help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid "Print short information about command syntax\\&."
msgstr "Gibt eine kurze Information über die Befehlssyntax aus\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<systemd>(1), B<integritytab>(5), B<systemd-integritysetup-generator>(8), "
"B<integritysetup>(8)"
msgstr ""
"B<systemd>(1), B<integritytab>(5), B<systemd-integritysetup-generator>(8), "
"B<integritysetup>(8)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid " 1."
msgstr " 1."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid "Kernel dm-integrity"
msgstr "Kernel dm-integrity"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid "\\%https://docs.kernel.org/admin-guide/device-mapper/dm-integrity.html"
msgstr "\\%https://docs.kernel.org/admin-guide/device-mapper/dm-integrity.html"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "systemd 254"
msgstr "systemd 254"

#. type: Plain text
#: debian-bookworm
msgid "/lib/systemd/systemd-integritysetup"
msgstr "/lib/systemd/systemd-integritysetup"

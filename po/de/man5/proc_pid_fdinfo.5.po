# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>, 2011-2012.
# Dr. Tobias Quathamer <toddy@debian.org>, 2016.
# Chris Leick <c.leick@vollbio.de>, 2017.
# Erik Pfannenstein <debianignatz@gmx.de>, 2017.
# Helge Kreutzmann <debian@helgefjell.de>, 2012-2017, 2019-2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.22.0\n"
"POT-Creation-Date: 2024-05-01 15:48+0200\n"
"PO-Revision-Date: 2024-03-23 15:30+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "proc_pid_fdinfo"
msgstr "proc_pid_fdinfo"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-08-15"
msgstr "15. August 2023"

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr "Linux man-pages 6.7"

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "/proc/pid/fdinfo/ - information about file descriptors"
msgstr "/proc/pid/fdinfo/ - Informationen über Dateideskriptoren"

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "I</proc/>pidI</fdinfo/> (since Linux 2.6.22)"
msgstr "I</proc/>PIDI</fdinfo/> (seit Linux 2.6.22)"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"This is a subdirectory containing one entry for each file which the process "
"has open, named by its file descriptor.  The files in this directory are "
"readable only by the owner of the process.  The contents of each file can be "
"read to obtain information about the corresponding file descriptor.  The "
"content depends on the type of file referred to by the corresponding file "
"descriptor."
msgstr ""
"In diesem Unterverzeichnis stehen die Dateideskriptoren aller von diesem "
"Prozess geöffneten Dateien. Die Dateien in diesem Verzeichnis können nur von "
"dem Eigentümer des Prozesses gelesen werden. Der Inhalt jeder Datei kann "
"gelesen werden, um Informationen über den entsprechenden Dateideskriptor zu "
"bekommen. Der Inhalt hängt von der Art der Datei ab, die von dem "
"entsprechenden Dateideskriptor referenziert wird."

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "For regular files and directories, we see something like:"
msgstr "Für reguläre Dateien und Verzeichnisse ergibt sich etwas der Form:"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"$B< cat /proc/12015/fdinfo/4>\n"
"pos:    1000\n"
"flags:  01002002\n"
"mnt_id: 21\n"
msgstr ""
"$B< cat /proc/12015/fdinfo/4>\n"
"pos:    1000\n"
"flags:  01002002\n"
"mnt_id: 21\n"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "The fields are as follows:"
msgstr "Die Bedeutung der Felder im Einzelnen:"

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "I<pos>"
msgstr "I<pos>"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "This is a decimal number showing the file offset."
msgstr "Dies ist eine Dezimalzahl, die den Dateiversatz zeigt."

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "I<flags>"
msgstr "I<flags>"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"This is an octal number that displays the file access mode and file status "
"flags (see B<open>(2)).  If the close-on-exec file descriptor flag is set, "
"then I<flags> will also include the value B<O_CLOEXEC>."
msgstr ""
"Dies ist eine oktale Zahl, die den Dateizugriffsmodus und die "
"Dateistatusschalter anzeigt (siehe B<open>(2)). Falls der »close-on-exec«-"
"Dateideskriptorschalter gesetzt ist, wird I<flags> auch den Wert "
"B<O_CLOEXEC> enthalten."

#.  commit 1117f72ea0217ba0cc19f05adbbd8b9a397f5ab7
#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Before Linux 3.1, this field incorrectly displayed the setting of "
"B<O_CLOEXEC> at the time the file was opened, rather than the current "
"setting of the close-on-exec flag."
msgstr ""
"Vor Linux 3.1 zeigte dieses Feld inkorrekterweise die Einstellung von "
"B<O_CLOEXEC> zum Zeitpunkt des Öffnens der Datei an, statt den aktuellen "
"Wert des Schalters close-on-exec."

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "I<mnt_id>"
msgstr "I<mnt_id>"

#.  commit 49d063cb353265c3af701bab215ac438ca7df36d
#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"This field, present since Linux 3.15, is the ID of the mount containing this "
"file.  See the description of I</proc/>pidI</mountinfo>."
msgstr ""
"Dieses seit Linux 3.15 vorhandene Feld zeigt die Kennung der Einhängung an, "
"der diese Datei enthält. Siehe die Beschreibung von I</proc/>PIDI</"
"mountinfo>."

#.  commit cbac5542d48127b546a23d816380a7926eee1c25
#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"For eventfd file descriptors (see B<eventfd>(2)), we see (since Linux 3.8)  "
"the following fields:"
msgstr ""
"Für den Eventfd-Dateideskriptor (siehe B<eventfd>(2)) gibt es (seit Linux "
"3.8) die folgenden Felder:"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"pos:\t0\n"
"flags:\t02\n"
"mnt_id:\t10\n"
"eventfd-count:               40\n"
msgstr ""
"pos:\t0\n"
"flags:\t02\n"
"mnt_id:\t10\n"
"eventfd-count:               40\n"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"I<eventfd-count> is the current value of the eventfd counter, in hexadecimal."
msgstr ""
"I<eventfd-count> ist der aktuelle hexadezimale Wert des Eventfd-Zählers."

#.  commit 138d22b58696c506799f8de759804083ff9effae
#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"For epoll file descriptors (see B<epoll>(7)), we see (since Linux 3.8)  the "
"following fields:"
msgstr ""
"Für den Epoll-Dateideskriptor (siehe B<epoll>(7)) gibt es (seit Linux 3.8) "
"die folgenden Felder:"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"pos:\t0\n"
"flags:\t02\n"
"mnt_id:\t10\n"
"tfd:        9 events:       19 data: 74253d2500000009\n"
"tfd:        7 events:       19 data: 74253d2500000007\n"
msgstr ""
"pos:\t0\n"
"flags:\t02\n"
"mnt_id:\t10\n"
"tfd:        9 events:       19 data: 74253d2500000009\n"
"tfd:        7 events:       19 data: 74253d2500000007\n"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Each of the lines beginning I<tfd> describes one of the file descriptors "
"being monitored via the epoll file descriptor (see B<epoll_ctl>(2)  for some "
"details).  The I<tfd> field is the number of the file descriptor.  The "
"I<events> field is a hexadecimal mask of the events being monitored for this "
"file descriptor.  The I<data> field is the data value associated with this "
"file descriptor."
msgstr ""
"Jede mit I<tfd> beginnende Zeile beschreibt einen Dateideskriptor, der mit "
"dem Epoll-Dateideskriptor überwacht wird (siehe B<epoll_ctl>(2) für weitere "
"Details). Das Feld I<tfd> ist die Nummer des Dateideskriptors. Das Feld "
"I<events> ist eine hexadezimale Maske der für diesen Dateideskriptor "
"überwachten Ereignisse. Das Feld I<data> ist der diesem Dateideskriptor "
"zugeordnete Datenwert."

#.  commit 138d22b58696c506799f8de759804083ff9effae
#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"For signalfd file descriptors (see B<signalfd>(2)), we see (since Linux "
"3.8)  the following fields:"
msgstr ""
"Für den Signalfd-Dateideskriptor (siehe B<signalfd>(2)) gibt es (seit Linux "
"3.8) die folgenden Felder:"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"pos:\t0\n"
"flags:\t02\n"
"mnt_id:\t10\n"
"sigmask:\t0000000000000006\n"
msgstr ""
"pos:\t0\n"
"flags:\t02\n"
"mnt_id:\t10\n"
"sigmask:\t0000000000000006\n"

#
#
#
#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"I<sigmask> is the hexadecimal mask of signals that are accepted via this "
"signalfd file descriptor.  (In this example, bits 2 and 3 are set, "
"corresponding to the signals B<SIGINT> and B<SIGQUIT>; see B<signal>(7).)"
msgstr ""
"I<sigmask> ist die hexadezimale Maske der Signale, die über diesen Signalfd-"
"Dateideskriptor akzeptiert werden. (In diesem Beispiel sind die Bits 2 und 3 "
"gesetzt; dies entspricht den Signalen B<SIGINT> und B<SIGQUIT>; siehe "
"B<signal>(7).)"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"For inotify file descriptors (see B<inotify>(7)), we see (since Linux 3.8)  "
"the following fields:"
msgstr ""
"Für Inotify-Dateideskriptoren (siehe B<inotify>(7)) gibt es (seit Linux 3.8) "
"die folgenden Felder:"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"pos:\t0\n"
"flags:\t00\n"
"mnt_id:\t11\n"
"inotify wd:2 ino:7ef82a sdev:800001 mask:800afff ignored_mask:0 fhandle-bytes:8 fhandle-type:1 f_handle:2af87e00220ffd73\n"
"inotify wd:1 ino:192627 sdev:800001 mask:800afff ignored_mask:0 fhandle-bytes:8 fhandle-type:1 f_handle:27261900802dfd73\n"
msgstr ""
"pos:\t0\n"
"flags:\t00\n"
"mnt_id:\t11\n"
"inotify wd:2 ino:7ef82a sdev:800001 mask:800afff ignored_mask:0 fhandle-bytes:8 fhandle-type:1 f_handle:2af87e00220ffd73\n"
"inotify wd:1 ino:192627 sdev:800001 mask:800afff ignored_mask:0 fhandle-bytes:8 fhandle-type:1 f_handle:27261900802dfd73\n"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Each of the lines beginning with \"inotify\" displays information about one "
"file or directory that is being monitored.  The fields in this line are as "
"follows:"
msgstr ""
"Jede der mit »inotify« beginnenden Zeilen zeigt Informationen über eine "
"überwachte Datei oder ein überwachtes Verzeichnis an. Die Felder in dieser "
"Zeile sind wie folgt:"

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "I<wd>"
msgstr "I<wd>"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "A watch descriptor number (in decimal)."
msgstr "Eine Watch-Deskriptornummer (deziaml)"

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "I<ino>"
msgstr "I<ino>"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "The inode number of the target file (in hexadecimal)."
msgstr "Die Inode-Nummer der Zieldatei (hexadezimal)."

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "I<sdev>"
msgstr "I<sdev>"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "The ID of the device where the target file resides (in hexadecimal)."
msgstr ""
"Die Kennung des Gerätes, auf dem sich die Zieldatei befindet (hexadezimal)."

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "I<mask>"
msgstr "I<mask>"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The mask of events being monitored for the target file (in hexadecimal)."
msgstr "Die Maske der für die Zieldatei überwachten Ereignisse (hexadezimal)."

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"If the kernel was built with exportfs support, the path to the target file "
"is exposed as a file handle, via three hexadecimal fields: I<fhandle-bytes>, "
"I<fhandle-type>, and I<f_handle>."
msgstr ""
"Falls der Kernel mit Exportfs-Unterstützung gebaut wurde, ist der Pfad zu "
"der Zieldatei mittels drei hexadezimaler Felder als Datei-Handle "
"offengelegt: I<fhandle-bytes>, I<fhandle-type> und I<f_handle>."

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"For fanotify file descriptors (see B<fanotify>(7)), we see (since Linux "
"3.8)  the following fields:"
msgstr ""
"Für Fanotify-Dateideskriptoren (siehe B<fanotify>(7)) gibt es (seit Linux "
"3.8) die folgenden Felder:"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"pos:\t0\n"
"flags:\t02\n"
"mnt_id:\t11\n"
"fanotify flags:0 event-flags:88002\n"
"fanotify ino:19264f sdev:800001 mflags:0 mask:1 ignored_mask:0 fhandle-bytes:8 fhandle-type:1 f_handle:4f261900a82dfd73\n"
msgstr ""
"pos:\t0\n"
"flags:\t02\n"
"mnt_id:\t11\n"
"fanotify flags:0 event-flags:88002\n"
"fanotify ino:19264f sdev:800001 mflags:0 mask:1 ignored_mask:0 fhandle-bytes:8 fhandle-type:1 f_handle:4f261900a82dfd73\n"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The fourth line displays information defined when the fanotify group was "
"created via B<fanotify_init>(2):"
msgstr ""
"Das vierte Feld zeigt Informationen, die bei der Erstellung der Fanotify-"
"Gruppe mittels B<fanotify_init>(2) definiert wurden:"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The I<flags> argument given to B<fanotify_init>(2)  (expressed in "
"hexadecimal)."
msgstr ""
"Das an B<fanotify_init>(2) übergebene Argument I<flags> (hexadezimal "
"ausgedrückt)."

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "I<event-flags>"
msgstr "I<event-flags>"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The I<event_f_flags> argument given to B<fanotify_init>(2)  (expressed in "
"hexadecimal)."
msgstr ""
"Das an B<fanotify_init>(2) übergebene Argument I<event_f_flags> (hexadezimal "
"ausgedrückt)."

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Each additional line shown in the file contains information about one of the "
"marks in the fanotify group.  Most of these fields are as for inotify, "
"except:"
msgstr ""
"Jede zusätzliche in der Datei gezeigte Zeile enthält Informationen über eine "
"der Markierungen in der Fanotify-Gruppe. Die meisten der Felder sind für "
"Inotify, außer:"

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "I<mflags>"
msgstr "I<mflags>"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "The flags associated with the mark (expressed in hexadecimal)."
msgstr "Die der Markierung zugeordneten Schalter (hexadezimal ausgedrückt)."

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "The events mask for this mark (expressed in hexadecimal)."
msgstr "Die Ereignismaske für diese Markierung (hexadezimal ausgedrückt)."

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "I<ignored_mask>"
msgstr "I<ignored_mask>"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The mask of events that are ignored for this mark (expressed in hexadecimal)."
msgstr ""
"Die Maske der für diese Markierung ignorierten Ereignisse (hexadezimal "
"ausgedrückt)."

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "For details on these fields, see B<fanotify_mark>(2)."
msgstr "Für Details über diese Felder lesen Sie B<fanotify_mark>(2)."

#.  commit af9c4957cf212ad9cf0bee34c95cb11de5426e85
#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"For timerfd file descriptors (see B<timerfd>(2)), we see (since Linux 3.17)  "
"the following fields:"
msgstr ""
"Für den Timerfd-Dateideskriptor (siehe B<timerfd>(2)) gibt es (seit Linux "
"3.17) die folgenden Felder:"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"pos:    0\n"
"flags:  02004002\n"
"mnt_id: 13\n"
"clockid: 0\n"
"ticks: 0\n"
"settime flags: 03\n"
"it_value: (7695568592, 640020877)\n"
"it_interval: (0, 0)\n"
msgstr ""
"pos:    0\n"
"flags:  02004002\n"
"mnt_id: 13\n"
"clockid: 0\n"
"ticks: 0\n"
"settime flags: 03\n"
"it_value: (7695568592, 640020877)\n"
"it_interval: (0, 0)\n"

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "I<clockid>"
msgstr "I<clockid>"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"This is the numeric value of the clock ID (corresponding to one of the "
"B<CLOCK_*> constants defined via I<E<lt>time.hE<gt>>)  that is used to mark "
"the progress of the timer (in this example, 0 is B<CLOCK_REALTIME>)."
msgstr ""
"Dies ist der numerische Wert der Uhrkennung (entsprechend einer der mittels "
"I<E<lt>time.hE<gt>> definierten Konstanten), der zur Markierung des "
"Fortschritts des Timers verwandt wird (in diesem Beispiel ist 0 "
"B<CLOCK_REALTIME>)."

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "I<ticks>"
msgstr "I<ticks>"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"This is the number of timer expirations that have occurred, (i.e., the value "
"that B<read>(2)  on it would return)."
msgstr ""
"Dies ist die Anzahl der aufgetretenen Abläufe des Timers (d.h. dem Wert, den "
"B<read>(2) darauf zurückliefern würde)."

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "I<settime flags>"
msgstr "I<settime flags>"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"This field lists the flags with which the timerfd was last armed (see "
"B<timerfd_settime>(2)), in octal (in this example, both B<TFD_TIMER_ABSTIME> "
"and B<TFD_TIMER_CANCEL_ON_SET> are set)."
msgstr ""
"Dieses Feld führt in oktaler Schreibweise die Schalter auf, mit denen "
"Timerfd letztmalig beladen wurde (siehe B<timerfd_settime>(2)) (in diesem "
"Beispiel sind sowohl B<TFD_TIMER_ABSTIME> als auch "
"B<TFD_TIMER_CANCEL_ON_SET> gesetzt)."

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "I<it_value>"
msgstr "I<it_value>"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"This field contains the amount of time until the timer will next expire, "
"expressed in seconds and nanoseconds.  This is always expressed as a "
"relative value, regardless of whether the timer was created using the "
"B<TFD_TIMER_ABSTIME> flag."
msgstr ""
"Dieses Feld hält die Zeitdauer in Sekunden und Nanosekunden, bis der Timer "
"das nächste Mal ablaufen wird. Der Wert wird immer relativ ausgedrückt, "
"unabhängig davon, ob der Timer mittels des Schalters B<TFD_TIMER_ABSTIME> "
"erstellt wurde."

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "I<it_interval>"
msgstr "I<it_interval>"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"This field contains the interval of the timer, in seconds and nanoseconds.  "
"(The I<it_value> and I<it_interval> fields contain the values that "
"B<timerfd_gettime>(2)  on this file descriptor would return.)"
msgstr ""
"Dieses Feld enthält das Intervall des Timers in Sekunden und Nanosekunden. "
"(Die Felder I<it_value> und I<it_interval> enthalten die Werte, die "
"B<timerfd_gettime>(2) auf diesem Dateideskriptor zurückliefern würde.)"

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<proc>(5)"
msgstr "B<proc>(5)"

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

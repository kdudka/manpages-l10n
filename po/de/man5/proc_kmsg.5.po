# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>, 2011-2012.
# Dr. Tobias Quathamer <toddy@debian.org>, 2016.
# Chris Leick <c.leick@vollbio.de>, 2017.
# Erik Pfannenstein <debianignatz@gmx.de>, 2017.
# Helge Kreutzmann <debian@helgefjell.de>, 2012-2017, 2019-2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.22.0\n"
"POT-Creation-Date: 2024-05-01 15:48+0200\n"
"PO-Revision-Date: 2024-03-23 15:08+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "proc_kmsg"
msgstr "proc_kmsg"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-08-15"
msgstr "15. August 2023"

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr "Linux man-pages 6.7"

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "/proc/kmsg - kernel messages"
msgstr "/proc/kmsg - Kernel-Meldungen"

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "I</proc/kmsg>"
msgstr "I</proc/kmsg>"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"This file can be used instead of the B<syslog>(2)  system call to read "
"kernel messages.  A process must have superuser privileges to read this "
"file, and only one process should read this file.  This file should not be "
"read if a syslog process is running which uses the B<syslog>(2)  system call "
"facility to log kernel messages."
msgstr ""
"Diese Datei kann anstelle des Systemaufrufs B<syslog>(2) benutzt werden, um "
"Meldungen des Kernels zu lesen. Ein Prozess muss Superuser-Privilegien "
"haben, um diese Datei zu lesen und nur ein einziger Prozess sollte dies tun. "
"Die Datei sollte nicht ausgelesen werden, wenn ein Syslog-Prozess läuft, der "
"den Systemaufruf B<syslog>(2) zur Protokollierung benutzt."

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Information in this file is retrieved with the B<dmesg>(1)  program."
msgstr ""
"Die Informationen in dieser Datei können mit B<dmesg>(1) dargestellt werden."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<proc>(5)"
msgstr "B<proc>(5)"

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# ASPLINUX <man@asp-linux.co.kr>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-05-01 15:46+0200\n"
"PO-Revision-Date: 2000-07-29 08:57+0900\n"
"Last-Translator: ASPLINUX <man@asp-linux.co.kr>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "nanosleep"
msgstr "nanosleep"

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2024-03-03"
msgstr "2024년 3월 3일"

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.03"
msgid "Linux man-pages 6.7"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "nanosleep - high-resolution sleep"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "라이브러리"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "표준 C 라이브러리 (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "요약"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>time.hE<gt>>\n"
msgstr "B<#include E<lt>time.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<int nanosleep(const struct timespec *>I<req>B<,>\n"
#| "B<              struct timespec *_Nullable >I<rem>B<);>\n"
msgid ""
"B<int nanosleep(const struct timespec *>I<duration>B<,>\n"
"B<              struct timespec *_Nullable >I<rem>B<);>\n"
msgstr ""
"B<int nanosleep(const struct timespec *>I<req>B<,>\n"
"B<              struct timespec *_Nullable >I<rem>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<nanosleep>():"
msgstr "B<nanosleep>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "    _POSIX_C_SOURCE E<gt>= 199309L\n"
msgstr "    _POSIX_C_SOURCE E<gt>= 199309L\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "설명"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
msgid ""
"B<nanosleep>()  suspends the execution of the calling thread until either at "
"least the time specified in I<*duration> has elapsed, or the delivery of a "
"signal that triggers the invocation of a handler in the calling thread or "
"that terminates the process."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<nanosleep> delays the execution of the program for at least the time "
#| "specified in I<*req>.  The function can return earlier if a signal has "
#| "been delivered to the process. In this case, it returns -1, sets I<errno> "
#| "to B<EINTR>, and writes the remaining time into the structure pointed to "
#| "by I<rem> unless I<rem> is B<NULL>.  The value of I<*rem> can then be "
#| "used to call B<nanosleep> again and complete the specified pause."
msgid ""
"If the call is interrupted by a signal handler, B<nanosleep>()  returns -1, "
"sets I<errno> to B<EINTR>, and writes the remaining time into the structure "
"pointed to by I<rem> unless I<rem> is NULL.  The value of I<*rem> can then "
"be used to call B<nanosleep>()  again and complete the specified pause (but "
"see NOTES)."
msgstr ""
"B<nanosleep> 은 적어도 I<*req> 에 지정된 시간만큼 프로그램 실행을 늦춘다. 시"
"그널이 프로세스로 전달됐다면 그런 기능은 더 빨리 반환할 수 있다. 그 기능은 시"
"그널이 프로세스에 전달되었다면 더 빨리 반환할 수 있다. -1이 반환되는 경우 "
"B<EINTR> 로 I<errno>를 설정하고, I<rem> 이 B<NULL> 이 아니라면 I<rem> 이 가리"
"키는 곳에 남은 시간을 기록한다.  그후 I<*rem> 값은 다시 B<nanosleep> 오출에 "
"사용될 수 있다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The structure I<timespec> is used to specify intervals of time with "
#| "nanosecond precision. It is specified in I<E<lt>time.hE<gt>> and has the "
#| "form"
msgid ""
"The B<timespec>(3)  structure is used to specify intervals of time with "
"nanosecond precision."
msgstr ""
"I<timespec> 은 나노초의 정밀도를 가진 시간 간격을 지정하는데 사용된다.  그것"
"은 I<E<lt>time.hE<gt>> 에 지정되고"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The value of the nanoseconds field must be in the range 0 to 999 999 999."
msgid "The value of the nanoseconds field must be in the range [0, 999999999]."
msgstr "나노초필드 값은 에서 999 999 999]의 범위안에 들어야 한다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Compared to B<sleep>(3)  and B<usleep>(3), B<nanosleep> has the advantage "
#| "of not affecting any signals, it is standardized by POSIX, it provides "
#| "higher timing resolution, and it allows to continue a sleep that has been "
#| "interrupted by a signal more easily."
msgid ""
"Compared to B<sleep>(3)  and B<usleep>(3), B<nanosleep>()  has the following "
"advantages: it provides a higher resolution for specifying the sleep "
"interval; POSIX.1 explicitly specifies that it does not interact with "
"signals; and it makes the task of resuming a sleep that has been interrupted "
"by a signal handler easier."
msgstr ""
"B<sleep>(3)  와 B<usleep>(3)  를 비교한다면, B<nanosleep> 는 모든 POSIX 표준"
"인 시그널에 영향을 주지 않는 잇점이 있다. 그것은 높은 시간 분석을 제공하고, "
"더 쉽게 시그널에 의해 중단된 계속적인 대기상태를 허락한다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "반환값"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
msgid ""
"On successfully sleeping for the requested duration, B<nanosleep>()  returns "
"0.  If the call is interrupted by a signal handler or encounters an error, "
"then it returns -1, with I<errno> set to indicate the error."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "에러"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Problem with copying information from user space."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINTR>"
msgstr "B<EINTR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The pause has been interrupted by a non-blocked signal that was delivered "
#| "to the process. The remaining sleep time has been written into *I<rem> so "
#| "that the process can easily call B<nanosleep> again and continue with the "
#| "pause."
msgid ""
"The pause has been interrupted by a signal that was delivered to the thread "
"(see B<signal>(7)).  The remaining sleep time has been written into I<*rem> "
"so that the thread can easily call B<nanosleep>()  again and continue with "
"the pause."
msgstr ""
"일시 중지는 프로세스에 전달된 비블록화 시그널에 의해 중단된다.  남은 대기 시"
"간은 프로세스가 쉽게 B<nanosleep> 를 다시 호출할 수 있도록 *I<rem>에 기록되"
"고, 일시정지는 계속된다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The value in the I<tv_nsec> field was not in the range 0 to 999\\ 999\\ "
#| "999 or I<tv_sec> was negative."
msgid ""
"The value in the I<tv_nsec> field was not in the range [0, 999999999] or "
"I<tv_sec> was negative."
msgstr ""
"I<tv_nsec> 필드안의 값은 0에서 999\\ 999\\ 999범위가 아니거나 I<tv_sec> 가 음"
"수이다."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "버전S"

#.  See also http://thread.gmane.org/gmane.linux.kernel/696854/
#.  Subject: nanosleep() uses CLOCK_MONOTONIC, should be CLOCK_REALTIME?
#.  Date: 2008-06-22 07:35:41 GMT
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"POSIX.1 specifies that B<nanosleep>()  should measure time against the "
"B<CLOCK_REALTIME> clock.  However, Linux measures the time using the "
"B<CLOCK_MONOTONIC> clock.  This probably does not matter, since the POSIX.1 "
"specification for B<clock_settime>(2)  says that discontinuous changes in "
"B<CLOCK_REALTIME> should not affect B<nanosleep>():"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
msgid ""
"Setting the value of the B<CLOCK_REALTIME> clock via B<clock_settime>(2)  "
"shall have no effect on threads that are blocked waiting for a relative time "
"service based upon this clock, including the B<nanosleep>()  function; ...  "
"Consequently, these time services shall expire when the requested duration "
"elapses, independently of the new or old value of the clock."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "표준"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "이력"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr "POSIX.1-2001."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "As some applications require much more precise pauses (e.g., in order to "
#| "control some time-critical hardware), B<nanosleep> is also capable of "
#| "short high-precision pauses. If the process is scheduled under a real-"
#| "time policy like I<SCHED_FIFO> or I<SCHED_RR>, then pauses of up to 2\\ "
#| "ms will be performed as busy waits with microsecond precision."
msgid ""
"In order to support applications requiring much more precise pauses (e.g., "
"in order to control some time-critical hardware), B<nanosleep>()  would "
"handle pauses of up to 2 milliseconds by busy waiting with microsecond "
"precision when called from a thread scheduled under a real-time policy like "
"B<SCHED_FIFO> or B<SCHED_RR>.  This special extension was removed in Linux "
"2.5.39, and is thus not available in Linux 2.6.0 and later kernels."
msgstr ""
"더욱더 정확한 일시정지가 필요한 응용프로그램처럼 B<nanosleep> 도 또한 매우정"
"확한 일시정지가 가능하다. 프로세스가 I<SCHED_FIFO> 나 I<SCHED_RR> 와 같은 실"
"제시간정책에서 스케쥴링되었다면, 2ms이상의 일시정지는 마이크로초의 정밀도를 "
"가진다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "주의"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
msgid ""
"If the I<duration> is not an exact multiple of the granularity underlying "
"clock (see B<time>(7)), then the interval will be rounded up to the next "
"multiple.  Furthermore, after the sleep completes, there may still be a "
"delay before the CPU becomes free to once again execute the calling thread."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The fact that B<nanosleep>()  sleeps for a relative interval can be "
"problematic if the call is repeatedly restarted after being interrupted by "
"signals, since the time between the interruptions and restarts of the call "
"will lead to drift in the time when the sleep finally completes.  This "
"problem can be avoided by using B<clock_nanosleep>(2)  with an absolute time "
"value."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "버그"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If a program that catches signals and uses B<nanosleep>()  receives signals "
"at a very high rate, then scheduling delays and rounding errors in the "
"kernel's calculation of the sleep interval and the returned I<remain> value "
"mean that the I<remain> value may steadily I<increase> on successive "
"restarts of the B<nanosleep>()  call.  To avoid such problems, use "
"B<clock_nanosleep>(2)  with the B<TIMER_ABSTIME> flag to sleep to an "
"absolute deadline."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In Linux 2.4, if B<nanosleep>()  is stopped by a signal (e.g., B<SIGTSTP>), "
"then the call fails with the error B<EINTR> after the thread is resumed by a "
"B<SIGCONT> signal.  If the system call is subsequently restarted, then the "
"time that the thread spent in the stopped state is I<not> counted against "
"the sleep interval.  This problem is fixed in Linux 2.6.0 and later kernels."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "추가 참조"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<clock_nanosleep>(2), B<restart_syscall>(2), B<sched_setscheduler>(2), "
"B<timer_create>(2), B<sleep>(3), B<timespec>(3), B<usleep>(3), B<time>(7)"
msgstr ""
"B<clock_nanosleep>(2), B<restart_syscall>(2), B<sched_setscheduler>(2), "
"B<timer_create>(2), B<sleep>(3), B<timespec>(3), B<usleep>(3), B<time>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-12"
msgstr "2023년 2월 12일"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm fedora-40 mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid ""
"B<int nanosleep(const struct timespec *>I<req>B<,>\n"
"B<              struct timespec *_Nullable >I<rem>B<);>\n"
msgstr ""
"B<int nanosleep(const struct timespec *>I<req>B<,>\n"
"B<              struct timespec *_Nullable >I<rem>B<);>\n"

#. type: Plain text
#: debian-bookworm fedora-40 mageia-cauldron opensuse-leap-15-6
msgid ""
"B<nanosleep>()  suspends the execution of the calling thread until either at "
"least the time specified in I<*req> has elapsed, or the delivery of a signal "
"that triggers the invocation of a handler in the calling thread or that "
"terminates the process."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-40 mageia-cauldron opensuse-leap-15-6
msgid ""
"On successfully sleeping for the requested interval, B<nanosleep>()  returns "
"0.  If the call is interrupted by a signal handler or encounters an error, "
"then it returns -1, with I<errno> set to indicate the error."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: Plain text
#: debian-bookworm fedora-40 mageia-cauldron opensuse-leap-15-6
msgid ""
"If the interval specified in I<req> is not an exact multiple of the "
"granularity underlying clock (see B<time>(7)), then the interval will be "
"rounded up to the next multiple.  Furthermore, after the sleep completes, "
"there may still be a delay before the CPU becomes free to once again execute "
"the calling thread."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-40 mageia-cauldron opensuse-leap-15-6
msgid ""
"Setting the value of the B<CLOCK_REALTIME> clock via B<clock_settime>(2)  "
"shall have no effect on threads that are blocked waiting for a relative time "
"service based upon this clock, including the B<nanosleep>()  function; ...  "
"Consequently, these time services shall expire when the requested relative "
"interval elapses, independently of the new or old value of the clock."
msgstr ""

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "Old behavior"
msgstr ""

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "2023년 10월 31일"

#. type: TH
#: fedora-40 mageia-cauldron
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.03"
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.03"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "2023년 3월 30일"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

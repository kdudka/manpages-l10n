# Vietnamese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-02-15 17:55+0100\n"
"PO-Revision-Date: 2022-01-18 19:49+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Vietnamese <>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BISON"
msgstr "BISON"

#. type: TH
#: archlinux fedora-40 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "September 2021"
msgstr "Tháng 9 năm 2021"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "GNU Bison 3.8.2"
msgstr "GNU Bison 3.8.2"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Các câu lệnh"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "TÊN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "bison - GNU Project parser generator (yacc replacement)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "TÓM TẮT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<bison> [I<\\,OPTION\\/>]... I<\\,FILE\\/>"
msgstr "B<bison> [I<\\,TÙY_CHỌN\\/>]… I<\\,TẬP_TIN\\/>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "MÔ TẢ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<Bison> is a parser generator in the style of I<yacc>(1).  It should be "
"upwardly compatible with input files designed for I<yacc>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Input files should follow the I<yacc> convention of ending in B<.y>.  Unlike "
"I<yacc>, the generated files do not have fixed names, but instead use the "
"prefix of the input file.  Moreover, if you need to put I<C++> code in the "
"input file, you can end his name by a C++-like extension (.ypp or .y++), "
"then bison will follow your extension to name the output file (.cpp or .c+"
"+).  For instance, a grammar description file named B<parse.yxx> would "
"produce the generated parser in a file named B<parse.tab.cxx>, instead of "
"I<yacc>'s B<y.tab.c> or old I<Bison> version's B<parse.tab.c>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This description of the options that can be given to I<bison> is adapted "
"from the node B<Invocation> in the B<bison.texi> manual, which should be "
"taken as authoritative."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<Bison> supports both traditional single-letter options and mnemonic long "
"option names.  Long option names are indicated with B<--> instead of B<->.  "
"Abbreviations for option names are allowed as long as they are unique.  When "
"a long option takes an argument, like B<--file-prefix>, connect the option "
"name and the argument with B<=>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Generate a deterministic LR or generalized LR (GLR) parser employing "
"LALR(1), IELR(1), or canonical LR(1) parser tables."
msgstr ""
"Tạo ra một bộ phân tích tất định LR hay RL được khái quát hóa dùng bảng phân "
"tích LALR(1), IELR(1), hay canonical LR(1)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too.  "
"The same is true for optional arguments."
msgstr ""
"Mọi đối số bắt buộc phải sử dụng với tùy chọn dài cũng bắt buộc với tùy chọn "
"ngắn. Cũng yêu cầu như thế đối với đối số tùy chọn."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Operation Modes:"
msgstr "Chế độ thao tác:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "hiển thị trợ giúp này rồi thoát"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "đưa ra thông tin phiên bản rồi thoát"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--print-localedir>"
msgstr "B<--print-localedir>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "output directory containing locale-dependent data and exit"
msgstr "xuất thư mục chứa dữ liệu phụ thuộc vào miền địa phương"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--print-datadir>"
msgstr "B<--print-datadir>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
msgid "output directory containing skeletons and XSLT and exit"
msgstr "xuất thư mục chứa khung sườn và XSLT"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-u>, B<--update>"
msgstr "B<-u>, B<--update>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "apply fixes to the source grammar file and exit"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-f>, B<--feature>[=I<\\,FEATURES\\/>]"
msgstr "B<-f>, B<--feature>[=I<\\,TÍNH-NĂNG\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "activate miscellaneous features"
msgstr "kích hoạt các TÍNH-NĂNG"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "FEATURES is a list of comma separated words that can include:"
msgstr "TÍNH-NĂNG là một danh sách các từ cách nhau bằng dấu phẩy, bao gồm:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "caret, diagnostics-show-caret"
msgstr "caret, diagnostics-show-caret"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "show errors with carets"
msgstr "hiển thị lỗi bằng dấu ^"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "fixit, diagnostics-parseable-fixits"
msgstr "fixit, diagnostics-parseable-fixits"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "show machine-readable fixes"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "syntax-only"
msgstr "syntax-only"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "do not generate any file"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "all"
msgstr "all"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "all of the above"
msgstr "tất cả các thứ ở trên"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "none"
msgstr "none"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "disable all of the above"
msgstr "tắt tất cả các thứ ở trên"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Diagnostics:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-W>, B<--warnings>[=I<\\,CATEGORY\\/>]"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "report the warnings falling in CATEGORY"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<--color>[=I<\\,WHEN\\/>]"
msgstr "B<--color>[=I<\\,KHI\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "whether to colorize the diagnostics"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<--style>=I<\\,FILE\\/>"
msgstr "B<--style>=I<\\,TẬP_TIN\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "specify the CSS FILE for colorizer diagnostics"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Warning categories include:"
msgstr "Các kiểu cảnh báo bao gồm:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "conflicts-sr"
msgstr "conflicts-sr"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "S/R conflicts (enabled by default)"
msgstr "xung đột S/R (bật theo mặc định)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "conflicts-rr"
msgstr "conflicts-rr"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "R/R conflicts (enabled by default)"
msgstr "xung đột R/R (bật theo mặc định)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "counterexamples, cex"
msgstr "counterexamples, cex"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "generate conflict counterexamples"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "dangling-alias"
msgstr "dangling-alias"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "string aliases not attached to a symbol"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "deprecated"
msgstr "deprecated"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "obsolete constructs"
msgstr "các chỉ lệnh cũ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "empty-rule"
msgstr "empty-rule"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "empty rules without %empty"
msgstr "quy tắc rỗng không cần %empty"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "midrule-values"
msgstr "midrule-values"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "unset or unused midrule values"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "precedence"
msgstr "precedence"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "useless precedence and associativity"
msgstr "quyền ưu tiên và tính kết hợp vô ích"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "yacc"
msgstr "yacc"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "incompatibilities with POSIX Yacc"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "other"
msgstr "khác"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "all other warnings (enabled by default)"
msgstr "mọi cảnh báo khác (bật theo mặc định)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "all the warnings except 'counterexamples', 'dangling-alias' and 'yacc'"
msgstr "tất cả các cảnh báo ngoại trừ “yacc”, “dangling-alias”, “yacc”"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "no-CATEGORY"
msgstr "no-LOẠI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "turn off warnings in CATEGORY"
msgstr "tắt cảnh báo với LOẠI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "turn off all the warnings"
msgstr "tắt mọi cảnh báo"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "error[=CATEGORY]"
msgstr "error[=LOẠI]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "treat warnings as errors"
msgstr "xử lý cảnh báo dưới dạng lỗi"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "WHEN can be one of the following:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "always, yes"
msgstr "always, yes"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "colorize the output"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "never, no"
msgstr "never, no"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "don't colorize the output"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "auto, tty"
msgstr "auto, tty"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "colorize if the output device is a tty"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
msgid "Tuning the Parser:"
msgstr "Bộ phân tích cú pháp:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-L>, B<--language>=I<\\,LANGUAGE\\/>"
msgstr "B<-L>, B<--language>=I<\\,NGÔN_NGỮ\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "specify the output programming language"
msgstr "chỉ định ngôn ngữ lập trình kết xuất"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-S>, B<--skeleton>=I<\\,FILE\\/>"
msgstr "B<-S>, B<--skeleton>=I<\\,TẬP_TIN\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "specify the skeleton to use"
msgstr "chỉ định khung sườn cần dùng"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--debug>"
msgstr "B<-t>, B<--debug>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
msgid "instrument the parser for tracing same as '-Dparse.trace'"
msgstr "đặt phân tích để gỡ lỗi"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--locations>"
msgstr "B<--locations>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "enable location support"
msgstr "cho phép hỗ trợ về vị trí"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-D>, B<--define=NAME>[=I<\\,VALUE\\/>]"
msgstr "B<-D>, B<--define=TÊN>[=I<\\,GIÁ_TRỊ\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "similar to '%define NAME VALUE'"
msgstr "tương tự như “%define TÊN \"GIÁ_TRỊ\""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-F>, B<--force-define=NAME>[=I<\\,VALUE\\/>]"
msgstr "B<-F>, B<--force-define=TÊN>[=I<\\,GIÁ_TRỊ\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "override '%define NAME VALUE'"
msgstr "đè lên “%define TÊN \"GIÁ_TRỊ\"”"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-p>, B<--name-prefix>=I<\\,PREFIX\\/>"
msgstr "B<-p>, B<--name-prefix>=I<\\,TIỀN_TỐ\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"prepend PREFIX to the external symbols deprecated by '-Dapi.prefix={PREFIX}'"
msgstr ""
"đặt tiền tố nào vào trước ký hiệu ngoài bị phản đối bởi “-Dapi."
"prefix=TIỀN_TỐ”"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-l>, B<--no-lines>"
msgstr "B<-l>, B<--no-lines>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "don't generate '#line' directives"
msgstr "không tạo chỉ thị kiểu “#line”"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-k>, B<--token-table>"
msgstr "B<-k>, B<--token-table>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "include a table of token names"
msgstr "bao gồm bảng các tên thẻ bài"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-y>, B<--yacc>"
msgstr "B<-y>, B<--yacc>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "emulate POSIX Yacc"
msgstr "mô phỏng POSIX Yacc"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
msgid "Output Files:"
msgstr "Kết xuất:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-H>, B<--header>=I<\\,[FILE]\\/>"
msgstr "B<-H>, B<--header>=I<\\,[TẬP_TIN]\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "also produce a header file"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>"
msgstr "B<-d>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "likewise but cannot specify FILE (for POSIX Yacc)"
msgstr "cũng vậy nhưng không thể chỉ định TẬP_TIN (cho POSIX Yacc)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-r>, B<--report>=I<\\,THINGS\\/>"
msgstr "B<-r>, B<--report>=I<\\,CÁI\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "also produce details on the automaton"
msgstr "cũng hiện chi tiết về hàm tự động"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--report-file>=I<\\,FILE\\/>"
msgstr "B<--report-file>=I<\\,TẬP_TIN\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "write report to FILE"
msgstr "ghi báo cáo vào tập tin này"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "same as '--report=state'"
msgstr "tương đương với B<--report=state>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-b>, B<--file-prefix>=I<\\,PREFIX\\/>"
msgstr "B<-b>, B<--file-prefix>=I<\\,TIỀN_TỐ\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "specify a PREFIX for output files"
msgstr "chỉ định tiền tố cho các tập tin kết xuất"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output>=I<\\,TẬP_TIN\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "leave output to FILE"
msgstr "xuất vào tập tin này"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-g>, B<--graph>[=I<\\,FILE\\/>]"
msgstr "B<-g>, B<--graph>[=I<\\,TẬP_TIN\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "also output a graph of the automaton"
msgstr "cũng xuất một đồ thị về hàm tự động"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<--html>[=I<\\,FILE\\/>]"
msgstr "B<--html>[=I<\\,TẬP_TIN\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "also output an HTML report of the automaton"
msgstr "cũng xuất một báo cáo HTML về hàm tự động"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-x>, B<--xml>[=I<\\,FILE\\/>]"
msgstr "B<-x>, B<--xml>[=I<\\,TẬP_TIN\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "also output an XML report of the automaton"
msgstr "cũng xuất một báo cáo XML về hàm tự động"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-M>, B<--file-prefix-map>=I<\\,OLD=NEW\\/> replace prefix OLD with NEW when writing file paths"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "conflicting outputs to file %s"
msgid "in output files"
msgstr "tập tin “%s” có nhiều kết xuất xung đột với nhau"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "THINGS is a list of comma separated words that can include:"
msgstr "CÁI là danh sách những từ được ngăn cách bằng dấu phẩy, bao gồm:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "states"
msgstr "states"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "describe the states"
msgstr "diễn tả các tình trạng"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "itemsets"
msgstr "itemsets"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "complete the core item sets with their closure"
msgstr "tập hợp mục dùng để tự điền với kết thúc nó"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "lookaheads"
msgstr "lookaheads"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "explicitly associate lookahead tokens to items"
msgstr "liên quan rõ ràng mỗi thẻ bài nhìn trước đến mục"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "solved"
msgstr "solved"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "describe shift/reduce conflicts solving"
msgstr "diễn tả việc phân giải sự xung đột kiểu dịch/giảm"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "include all the above information"
msgstr "bao gồm tất cả thông tin trên"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "disable the report"
msgstr "tắt báo cáo"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "TÁC GIẢ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Written by Robert Corbett and Richard Stallman."
msgstr "Tác giả: Robert Corbett và Richard Stallman."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "THÔNG BÁO LỖI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Report bugs to E<lt>bug-bison@gnu.orgE<gt>."
msgstr "Hãy thông báo lỗi cho E<lt>bug-bison@gnu.orgE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "GNU Bison home page: E<lt>https://www.gnu.org/software/bison/E<gt>."
msgstr "Trang chủ GNU Bison: E<lt>https://www.gnu.org/software/bison/E<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"General help using GNU software: E<lt>https://www.gnu.org/gethelp/E<gt>."
msgstr ""
"Hướng dẫn chung về phần mềm GNU: E<lt>https://www.gnu.org/gethelp/E<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "For complete documentation, run: info bison."
msgstr "Để xem toàn bộ tài liệu về phần mềm này, hãy chạy lệnh: info bison."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "BẢN QUYỀN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Copyright \\(co 2021 Free Software Foundation, Inc."
msgstr "Bản quyền \\(co 2021 Tổ chức Phần mềm Tự do."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is free software; see the source for copying conditions.  There is NO "
"warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE."
msgstr ""
"Chương trình này là phần mềm tự do; xem mã nguồn để tìm điều kiện sao chép. "
"KHÔNG CÓ BẢO HÀNH GÌ CẢ, NGAY CẢ KHI NÓ ĐƯỢC BÁN HAY PHÙ HỢP CẢ VỚI MỤC ĐÍCH "
"ĐẶC BIỆT."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "XEM THÊM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<lex>(1), B<flex>(1), B<yacc>(1)."
msgstr "B<lex>(1), B<flex>(1), B<yacc>(1)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The full documentation for B<bison> is maintained as a Texinfo manual.  If "
"the B<info> and B<bison> programs are properly installed at your site, the "
"command"
msgstr ""
"Tài liệu hướng dẫn đầy đủ về B<bison> được bảo trì dưới dạng một sổ tay "
"Texinfo.  Nếu chương trình B<info> và B<bison> được cài đặt đúng ở địa chỉ "
"của bạn thì câu lệnh"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<info bison>"
msgstr "B<info bison>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "nên cho phép bạn truy cập đến toàn bộ sổ tay."

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "September 2022"
msgstr "Tháng 9 năm 2022"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid ""
"Report translation bugs to E<lt>https://translationproject.org/team/E<gt>."
msgstr ""
"Hãy thông báo lỗi dịch cho E<lt>https://translationproject.org/team/vi."
"htmlE<gt>."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "February 2023"
msgstr "Tháng 2 năm 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "January 2015"
msgstr "Tháng 1 năm 2015"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "bison 3.0.4"
msgstr "bison 3.0.4"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Generate a deterministic LR or generalized LR (GLR) parser employing "
"LALR(1), IELR(1), or canonical LR(1) parser tables.  IELR(1) and canonical "
"LR(1) support is experimental."
msgstr ""
"Tạo ra một bộ phân tích tất định LR hay RL được khái quát hóa dùng bảng phân "
"tích LALR(1), IELR(1), hay canonical LR(1). Việc hỗ trợ  IELR(1) và "
"canonical LR(1) chỉ là thử nghiệm."

#. type: SS
#: opensuse-leap-15-6
#, no-wrap
msgid "Operation modes:"
msgstr "Chế độ thao tác:"

#. type: Plain text
#: opensuse-leap-15-6
msgid "output directory containing locale-dependent data"
msgstr "xuất thư mục chứa dữ liệu phụ thuộc vào miền địa phương"

#. type: Plain text
#: opensuse-leap-15-6
msgid "output directory containing skeletons and XSLT"
msgstr "xuất thư mục chứa khung sườn và XSLT"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-f>, B<--feature>[=I<\\,FEATURE\\/>]"
msgstr "B<-f>, B<--feature>[=I<\\,TÍNH-NĂNG\\/>]"

#. type: SS
#: opensuse-leap-15-6
#, no-wrap
msgid "Parser:"
msgstr "Bộ phân tích cú pháp:"

#. type: Plain text
#: opensuse-leap-15-6
msgid "similar to '%define NAME \"VALUE\"'"
msgstr "tương tự như “%define TÊN \"GIÁ_TRỊ\"”"

#. type: Plain text
#: opensuse-leap-15-6
msgid "override '%define NAME \"VALUE\"'"
msgstr "đè lên “%define TÊN \"GIÁ_TRỊ\"”"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"prepend PREFIX to the external symbols deprecated by '-Dapi.prefix=PREFIX'"
msgstr ""
"đặt tiền tố nào vào trước ký hiệu ngoài bị phản đối bởi “-Dapi."
"prefix=TIỀN_TỐ”"

#. type: SS
#: opensuse-leap-15-6
#, no-wrap
msgid "Output:"
msgstr "Kết xuất:"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<--defines>[=I<\\,FILE\\/>]"
msgstr "B<--defines>[=I<\\,TẬP_TIN\\/>]"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"also output an XML report of the automaton (the XML schema is experimental)"
msgstr ""
"cũng xuất một báo cáo XML về hàm tự động (giản đồ XML vẫn còn là thực nghiệm)"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "\\&'midrule-values'"
msgstr "\\&'midrule-values'"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "\\&'yacc'"
msgstr "\\&'yacc'"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "\\&'conflicts-sr'"
msgstr "\\&'conflicts-sr'"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "\\&'conflicts-rr'"
msgstr "\\&'conflicts-rr'"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "\\&'deprecated'"
msgstr "\\&'deprecated'"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "\\&'empty-rule'"
msgstr "\\&'empty-rule'"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "\\&'precedence'"
msgstr "\\&'precedence'"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "\\&'other'"
msgstr "\\&'other'"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "\\&'all'"
msgstr "\\&'all'"

#. type: Plain text
#: opensuse-leap-15-6
msgid "all the warnings except 'yacc'"
msgstr "tất cả các cảnh báo ngoại trừ “yacc”"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "\\&'no-CATEGORY'"
msgstr "\\&'no-LOẠI'"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "\\&'none'"
msgstr "\\&'none'"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "\\&'error[=CATEGORY]'"
msgstr "\\&'error[=LOẠI]'"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "\\&'state'"
msgstr "\\&'state'"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "\\&'itemset'"
msgstr "\\&'itemset'"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "\\&'lookahead'"
msgstr "\\&'lookahead'"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "\\&'solved'"
msgstr "\\&'solved'"

#. type: SS
#: opensuse-leap-15-6
#, no-wrap
msgid "FEATURE is a list of comma separated words that can include:"
msgstr "TÍNH-NĂNG là một danh sách các từ cách nhau bằng dấu phẩy, bao gồm:"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "\\&'caret'"
msgstr "\\&'caret'"

#. type: Plain text
#: opensuse-leap-15-6
msgid "GNU Bison home page: E<lt>http://www.gnu.org/software/bison/E<gt>."
msgstr "Trang chủ GNU Bison: E<lt>https://www.gnu.org/software/bison/E<gt>."

#. type: Plain text
#: opensuse-leap-15-6
msgid "General help using GNU software: E<lt>http://www.gnu.org/gethelp/E<gt>."
msgstr ""
"Hướng dẫn chung về phần mềm GNU: E<lt>https://www.gnu.org/gethelp/E<gt>."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Report translation bugs to E<lt>http://translationproject.org/team/E<gt>."
msgstr ""
"Hãy thông báo lỗi dịch cho E<lt>http://translationproject.org/team/vi."
"htmlE<gt>."

#. type: Plain text
#: opensuse-leap-15-6
msgid "Copyright \\(co 2015 Free Software Foundation, Inc."
msgstr "Bản quyền \\(co 2015 Tổ chức Phần mềm Tự do."

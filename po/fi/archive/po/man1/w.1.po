# Finnish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Tuukka Forssell <taf@jytol.fi>, 1998.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-02-09 17:14+0100\n"
"PO-Revision-Date: 1998-04-08 15:46+0200\n"
"Last-Translator: Tuukka Forssell <taf@jytol.fi>\n"
"Language-Team: Finnish <>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "W"
msgstr "W"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2020-06-04"
msgstr "4. kesäkuuta 2020"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "procps-ng"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "User Commands"
msgstr "Käyttäjän sovellukset"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "NIMI"

#. type: Plain text
#: opensuse-leap-15-6
msgid "w - Show who is logged on and what they are doing."
msgstr ""
"w - Näyttää käyttäjät, jotka ovat kirjautuneena systeemiin sekä mitä he "
"tekevät."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr "YLEISKATSAUS"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid "B<w -> [B<husfV>] [I<user>]"
msgid "B<w> [I<options>] I<user> [...]"
msgstr "B<w -> [B<husfV>] [I<käyttäjä>]"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "KUVAUS"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "B<w > displays information about the users currently on the machine, and "
#| "their processes.  The header shows, in this order, the current time, how "
#| "long the system has been running, how many users are currently logged on, "
#| "and the system load averages for the past 1, 5, and 15 minutes."
msgid ""
"B<w> displays information about the users currently on the machine, and "
"their processes.  The header shows, in this order, the current time, how "
"long the system has been running, how many users are currently logged on, "
"and the system load averages for the past 1, 5, and 15 minutes."
msgstr ""
"B<w > näyttää informaation käyttäjistä jotka ovat parhaillaan kirjautuneena "
"systeemiin ja heidän prosessinsa. Näytettävät tiedot ovat aika, kuinka kauan "
"systeemi on ollut ylhäällä, kuorman sekä kuinka monta käyttäjää on "
"kirjautuneena systeemiin ja keskimääräisen kuorman viimeisen minuutin, "
"viiden ja viidentoista minuutin aikana."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The following entries are displayed for each user: login name, the tty name, "
"the remote host, login time, idle time, JCPU, PCPU, and the command line of "
"their current process."
msgstr ""
"Seuraavat tiedot näytetään jokaiselle käyttäjälle omana kenttänään; "
"käyttäjätunnus, päätteen tunnus, palvelin josta kirjauduttu, JCPU, PCPU ja "
"komentorivin prosessi."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The JCPU time is the time used by all processes attached to the tty.  It "
"does not include past background jobs, but does include currently running "
"background jobs."
msgstr ""
"JCPU aika kertoo ajan minkä prosessit ovat kytkettynä päätteeseen. Se ei "
"kerro aikaisempien sessioiden käynnissä olevia tausta-ajoja, mutta kertoo "
"kyseisestä komentokehotteesta käynnistettyjen tausta-ajojen ajan."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The PCPU time is the time used by the current process, named in the \"what\" "
"field."
msgstr ""
"PCPU kertoo ajan, jonka kehotteessa ajettava prosessi vie. Komennon w antama "
"what-kentän prosessi (PCPU)."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COMMAND-LINE OPTIONS"
msgstr ""

#. type: TP
#: opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid "B<-E>, B<--no-escape>"
msgid "B<-h>, B<--no-header>"
msgstr "B<-E>, B<--no-escape>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Don't print the header."
msgstr "Ei tulosta otsaketta."

#. type: TP
#: opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid "B<-c>, B<--no-create>"
msgid "B<-u>, B<--no-current>"
msgstr "B<-c>, B<--no-create>"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "Ignores the username while figuring out the current process and cpu "
#| "times.  To demonstrate this, do a \"su\" and do a \"w\" and a \"w -u\"."
msgid ""
"Ignores the username while figuring out the current process and cpu times.  "
"To demonstrate this, do a B<su> and do a B<w> and a B<w -u>."
msgstr ""
"Ei huomioi käyttäjätunnusta selvittäessään ajettavaa prosessia ja prosessori "
"aikaa. Voit demonstroida tätä komennolla \"su\" ja ajamalla \"w\" ja \"w -"
"u\"."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-s>, B<--short>"
msgstr "B<-s>, B<--short>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Use the short format.  Don't print the login time, JCPU or PCPU times."
msgstr "Lyhyt formaatti.  Ei näytä kirjautumis-, JCPU- ja PCPU -aikaa."

#. type: TP
#: opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid "B<-c>, B<--no-create>"
msgid "B<-n>, B<--no-truncat>"
msgstr "B<-c>, B<--no-create>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Do not truncate the output format. This option might become renamed in "
"future versions."
msgstr ""

#. type: TP
#: opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid "B<-f>, B<--force>"
msgid "B<-f>, B<--from>"
msgstr "B<-f>, B<--force>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Toggle printing the B<from> (remote hostname) field.  The default as "
"released is for the B<from> field to not be printed, although your system "
"administrator or distribution maintainer may have compiled a version in "
"which the B<from> field is shown by default."
msgstr ""
"Estää tulostamasta B<from> (palvelin josta kirjauduttu) kenttää.  Oletuksena "
"on ettei B<from> kenttää näytetä, mutta jos pääkäyttäjä on kääntänyt "
"version, joka näyttää B<from> kentän oletuksena."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display help text and exit."
msgstr "Näytä tämä ohje ja poistu."

#. type: TP
#: opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid "B<-i>, B<--inode>"
msgid "B<-i>, B<--ip-addr>"
msgstr "B<-i>, B<--inode>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display IP address instead of hostname for B<from> field."
msgstr ""

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display version information."
msgstr "Näyttää versio numeron."

#. type: TP
#: opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid "B<-A>, B<--all>"
msgid "B<-o>, B<--old-style>"
msgstr "B<-A>, B<--all>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Old style output.  Prints blank space for idle times less than one minute."
msgstr ""

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<user >"
msgstr "B<käyttäjä>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Show information about the specified user only."
msgstr "Näyttää tiedot tietystä käyttäjästä."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "ENVIRONMENT"
msgstr "YMPÄRISTÖ"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "PROCPS_USERLEN"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "Override the default width of the username column.  Defaults to 8."
msgstr ""

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "PROCPS_FROMLEN"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "Override the default width of the from column.  Defaults to 16."
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "FILES"
msgstr "TIEDOSTOT"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "I</var/run/utmp>"
msgstr "I</var/run/utmp>"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "I</etc/utmp>\tinformation about who is currently logged on I</"
#| "proc>\tprocess information"
msgid "information about who is currently logged on"
msgstr ""
"I</etc/utmp>\ttiedot sisäänkirjautuneista henkilöistä I</proc>\tprosessien "
"tiedot"

#. type: TP
#: opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid "I</proc/loadavg>"
msgid "I</proc>"
msgstr "I</proc/loadavg>"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid "Display version information."
msgid "process information"
msgstr "Näyttää versio numeron."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr "KATSO MYÖS"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<free>(1), B<ps>(1), B<top>(1), B<uptime>(1), B<utmp>(5), B<who>(1)"
msgstr "B<free>(1), B<ps>(1), B<top>(1), B<uptime>(1), B<utmp>(5), B<who>(1)"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "AUTHORS"
msgstr "TEKIJÄT"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "B<w> was re-written almost entirely by Charles Blake, based on the "
#| "version by Larry Greenfield E<lt>greenfie@gauss.rutgers.eduE<gt> and "
#| "Michael K. Johnson E<lt>johnsonm@redhat.comE<gt>."
msgid ""
"B<w> was re-written almost entirely by Charles Blake, based on the version "
"by E<.UR greenfie@\\:gauss.\\:rutgers.\\:edu> Larry Greenfield E<.UE> and E<."
"UR johnsonm@\\:redhat.\\:com> Michael K. Johnson E<.UE>"
msgstr ""
"B<w> ohjelmoitiin uudelleen Charles Blake toimesta, perustuen Larry "
"Greenfield E<lt>greenfie@gauss.rutgers.eduE<gt> ja Michael K. Johnson "
"E<lt>johnsonm@redhat.comE<gt> versioon."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "REPORTING BUGS"
msgstr "VIRHEISTÄ ILMOITTAMINEN"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid "Please send bug reports to E<lt>procps-bugs@redhat.comE<gt>"
msgid "Please send bug reports to E<.UR procps@freelists.org> E<.UE>"
msgstr ""
"Lähetäthän vikaraportit sähköpostiosoitteeseen E<lt>procps-bugs@redhat."
"comE<gt>"

# Brazilian Portuguese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# André Luiz Fassone <lonely_wolf@ig.com.br>, 2000.
# Roberto Selbach Teixeira <robteix@zaz.com.br>, 2000.
# Rafael Fontenelle <rafaelff@gnome.org>, 2021-2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-05-01 15:46+0200\n"
"PO-Revision-Date: 2022-08-11 10:16-0300\n"
"Last-Translator: Rafael Fontenelle <rafaelff@gnome.org>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1)\n"
"X-Generator: Gtranslator 40.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MV"
msgstr "MV"

#. type: TH
#: archlinux debian-unstable
#, no-wrap
msgid "March 2024"
msgstr "Março de 2024"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.5"
msgstr "GNU coreutils 9.5"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Comandos de usuário"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "mv - move (rename) files"
msgstr "mv - movimenta (renomeia) os arquivos"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<mv> [I<\\,OPTION\\/>]... [I<\\,-T\\/>] I<\\,SOURCE DEST\\/>"
msgstr "B<mv> [I<\\,OPÇÃO\\/>]... [I<\\,-T\\/>] I<\\,ORIGEM DESTINO\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<mv> [I<\\,OPTION\\/>]... I<\\,SOURCE\\/>... I<\\,DIRECTORY\\/>"
msgstr "B<mv> [I<\\,OPÇÃO\\/>]... I<\\,ORIGEM\\/>... I<\\,DIRETÓRIO\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<mv> [I<\\,OPTION\\/>]... I<\\,-t DIRECTORY SOURCE\\/>..."
msgstr "B<mv> [I<\\,OPÇÃO\\/>]... I<\\,-t DIRETÓRIO ORIGEM\\/>..."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Rename SOURCE to DEST, or move SOURCE(s) to DIRECTORY."
msgstr "Renomeia ORIGEM para DESTINO ou move ORIGEM(ns) para DIRETÓRIO."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Argumentos obrigatórios para opções longas também o são para opções curtas."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--backup>[=I<\\,CONTROL\\/>]"
msgstr "B<--backup>[=I<\\,CONTROLE\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "make a backup of each existing destination file"
msgstr "faz uma cópia de segurança de cada arquivo de destino já existente"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-b>"
msgstr "B<-b>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "like B<--backup> but does not accept an argument"
msgstr "como B<--backup>, mas não aceita argumentos"

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--debug>"
msgstr "B<--debug>"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "explain how a file is copied.  Implies B<-v>"
msgstr ""

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<-c>, B<--changes>"
msgid "B<--exchange>"
msgstr "B<-c>, B<--changes>"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "exchange source and destination"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>, B<--force>"
msgstr "B<-f>, B<--force>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "do not prompt before overwriting"
msgstr "não pergunta antes de sobrescrever"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>, B<--interactive>"
msgstr "B<-i>, B<--interactive>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "prompt before overwrite"
msgstr "pergunta antes de sobrescrever"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-n>, B<--no-clobber>"
msgstr "B<-n>, B<--no-clobber>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "do not overwrite an existing file"
msgstr "não sobrescreve um arquivo existente"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If you specify more than one of B<-i>, B<-f>, B<-n>, only the final one "
"takes effect."
msgstr ""
"Se você especificar mais que um dentre B<-i>, B<-f> ou B<-n>, apenas o "
"último terá efeito."

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<--norc>"
msgid "B<--no-copy>"
msgstr "B<--norc>"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid "do not create any files"
msgid "do not copy if renaming fails"
msgstr "não cria arquivo algum"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--strip-trailing-slashes>"
msgstr "B<--strip-trailing-slashes>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "remove any trailing slashes from each SOURCE argument"
msgstr "remove quaisquer barras ao final de cada argumento ORIGEM"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-S>, B<--suffix>=I<\\,SUFFIX\\/>"
msgstr "B<-S>, B<--suffix>=I<\\,SUFIXO\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "override the usual backup suffix"
msgstr "sobrescreve o sufixo comum de cópia de segurança"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--target-directory>=I<\\,DIRECTORY\\/>"
msgstr "B<-t>, B<--target-directory>=I<\\,DIRETÓRIO\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "move all SOURCE arguments into DIRECTORY"
msgstr "move todos os argumentos ORIGEM para o DIRETÓRIO"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-T>, B<--no-target-directory>"
msgstr "B<-T>, B<--no-target-directory>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "treat DEST as a normal file"
msgstr "trata DESTINO como um arquivo normal"

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<--hide>=I<\\,PATTERN\\/>"
msgid "B<--update>[=I<\\,UPDATE\\/>]"
msgstr "B<--hide>=I<\\,PADRÃO\\/>"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"control which existing files are updated; UPDATE={all,none,none-fail,"
"older(default)}."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-u>"
msgstr "B<-u>"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "Equivalent to B<-l>."
msgid "equivalent to B<--update>[=I<\\,older\\/>].  See below"
msgstr "Equivalente a B<-l>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "explain what is being done"
msgstr "explica o que está sendo feito"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-Z>, B<--context>"
msgstr "B<-Z>, B<--context>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "set SELinux security context of destination file to default type"
msgstr ""
"define o contexto de segurança SELinux do arquivo de destino para o tipo "
"padrão"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "mostra esta ajuda e sai"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "informa a versão e sai"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"UPDATE controls which existing files in the destination are replaced.  "
"\\&'all' is the default operation when an B<--update> option is not "
"specified, and results in all existing files in the destination being "
"replaced.  \\&'none' is like the B<--no-clobber> option, in that no files in "
"the destination are replaced, and skipped files do not induce a failure.  "
"\\&'none-fail' also ensures no files are replaced in the destination, but "
"any skipped files are diagnosed and induce a failure.  \\&'older' is the "
"default operation when B<--update> is specified, and results in files being "
"replaced if they're older than the corresponding source file."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The backup suffix is '~', unless set with B<--suffix> or "
"SIMPLE_BACKUP_SUFFIX.  The version control method may be selected via the "
"B<--backup> option or through the VERSION_CONTROL environment variable.  "
"Here are the values:"
msgstr ""
"O sufixo de cópia de segurança é \"~\", a não ser que esteja definido B<--"
"suffix> ou SIMPLE_BACKUP_SUFFIX. O método de controle de versão pode ser "
"definido com B<--backup> ou a variável de ambiente VERSION_CONTROL. Os "
"valores possíveis são:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "none, off"
msgstr "none, off"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "never make backups (even if B<--backup> is given)"
msgstr "nunca faz cópias de segurança (mesmo se B<--backup> for especificado)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "numbered, t"
msgstr "numbered, t"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "make numbered backups"
msgstr "faz cópias de segurança numeradas"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "existing, nil"
msgstr "existing, nil"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "numbered if numbered backups exist, simple otherwise"
msgstr ""
"numeradas se já existirem cópias de segurança numeradas, simples em caso "
"contrário"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "simple, never"
msgstr "simple, never"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "always make simple backups"
msgstr "sempre faz cópias de segurança simples"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Written by Mike Parker, David MacKenzie, and Jim Meyering."
msgstr "Escrito por Mike Parker, David MacKenzie e Jim Meyering."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RELATANDO PROBLEMAS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Página de ajuda do GNU coreutils: E<lt>https://www.gnu.org/software/"
"coreutils/E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Relate erros de tradução para E<lt>https://translationproject.org/team/pt_BR."
"htmlE<gt>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "DIREITOS AUTORAIS"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2024 Free Software Foundation, Inc. Licença GPLv3+: GNU GPL "
"versão 3 ou posterior E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Este é um software livre: você é livre para alterá-lo e redistribuí-lo. NÃO "
"HÁ QUALQUER GARANTIA, na máxima extensão permitida em lei."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<rename>(2)"
msgstr "B<rename>(2)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Full documentation E<lt>https://www.gnu.org/software/coreutils/mvE<gt>"
msgstr ""
"Documentação completa: E<lt>https://www.gnu.org/software/coreutils/mvE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) mv invocation\\(aq"
msgstr "ou disponível localmente via: info \\(aq(coreutils) mv invocation\\(aq"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "September 2022"
msgstr "Setembro de 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: TP
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "B<-u>, B<--update>"
msgstr "B<-u>, B<--update>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"move only when the SOURCE file is newer than the destination file or when "
"the destination file is missing"
msgstr ""
"move somente quando o arquivo de ORIGEM for mais novo do que o de destino ou "
"quando o arquivo de destino não existe"

#. type: Plain text
#: debian-bookworm
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc. Licença GPLv3+: GNU GPL "
"versão 3 ou posterior E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: debian-unstable fedora-40 mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: Plain text
#: debian-unstable fedora-40 mageia-cauldron
msgid ""
"control which existing files are updated; UPDATE={all,none,older(default)}.  "
"See below"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-40 mageia-cauldron
#, fuzzy
#| msgid "Equivalent to B<-l>."
msgid "equivalent to B<--update>[=I<\\,older\\/>]"
msgstr "Equivalente a B<-l>."

#. type: Plain text
#: debian-unstable fedora-40 mageia-cauldron
msgid ""
"UPDATE controls which existing files in the destination are replaced.  "
"\\&'all' is the default operation when an B<--update> option is not "
"specified, and results in all existing files in the destination being "
"replaced.  \\&'none' is similar to the B<--no-clobber> option, in that no "
"files in the destination are replaced, but also skipped files do not induce "
"a failure.  \\&'older' is the default operation when B<--update> is "
"specified, and results in files being replaced if they're older than the "
"corresponding source file."
msgstr ""

#. type: Plain text
#: debian-unstable fedora-40 mageia-cauldron
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc. Licença GPLv3+: GNU GPL "
"versão 3 ou posterior E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: fedora-40 opensuse-leap-15-6
#, no-wrap
msgid "January 2024"
msgstr "Janeiro de 2024"

#. type: TH
#: fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "April 2024"
msgstr "Abril de 2024"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "August 2023"
msgstr "Agosto de 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc. Licença GPLv3+: GNU GPL "
"versão 3 ou posterior E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: opensuse-leap-15-6
msgid "rename(2)"
msgstr "rename(2)"

# Swedish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Göran Uddeborg <goeran@uddeborg.se>, 2023, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-05-01 15:50+0200\n"
"PO-Revision-Date: 2024-05-06 15:19+0200\n"
"Last-Translator: Göran Uddeborg <goeran@uddeborg.se>\n"
"Language-Team: Swedish <tp-sv@listor.tp-sv.se>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "PTX"
msgstr "PTX"

#. type: TH
#: archlinux debian-unstable
#, no-wrap
msgid "March 2024"
msgstr "mars 2024"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.5"
msgstr "GNU coreutils 9.5"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Användarkommandon"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAMN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "ptx - produce a permuted index of file contents"
msgstr "ptx — producera ett permuterat index över filinnehåll"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<ptx> [I<\\,OPTION\\/>]... [I<\\,INPUT\\/>]...  I<\\,(without -G)\\/>"
msgstr "B<ptx> [I<\\,FLAGGA\\/>]... [I<\\,INFIL\\/>]...  I<\\,(utan -G)\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<ptx> I<\\,-G \\/>[I<\\,OPTION\\/>]... [I<\\,INPUT \\/>[I<\\,OUTPUT\\/>]]"
msgstr ""
"B<ptx> I<\\,-G \\/>[I<\\,FLAGGA\\/>]... [I<\\,INFIL \\/>[I<\\,UTFIL\\/>]]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVNING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Output a permuted index, including context, of the words in the input files."
msgstr ""
"Mata ut ett permuterat index, med sammanhang, av orden i indatafilerna."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "With no FILE, or when FILE is -, read standard input."
msgstr "Utan FIL, eller när FIL är -, läs standard in."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Obligatoriska argument till långa flaggor är obligatoriska även för de korta."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-A>, B<--auto-reference>"
msgstr "B<-A>, B<--auto-reference>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output automatically generated references"
msgstr "Skriv ut automatiskt genererade referenser."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-G>, B<--traditional>"
msgstr "B<-G>, B<--traditional>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "behave more like System V 'ptx'"
msgstr "Uppträd mer som System V:s B<ptx>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-F>, B<--flag-truncation>=I<\\,STRING\\/>"
msgstr "B<-F>, B<--flag-truncation>=I<\\,STRÄNG\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "use STRING for flagging line truncations.  The default is '/'"
msgstr "Använd STRÄNG för att markera avhuggna rader. Standard är ”/”."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-M>, B<--macro-name>=I<\\,STRING\\/>"
msgstr "B<-M>, B<--macro-name>=I<\\,STRÄNG\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "macro name to use instead of 'xx'"
msgstr "Makronamn att använda istället för ”xx”."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-O>, B<--format>=I<\\,roff\\/>"
msgstr "B<-O>, B<--format>=I<\\,roff\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "generate output as roff directives"
msgstr "Generera utdata som roff-direktiv."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-R>, B<--right-side-refs>"
msgstr "B<-R>, B<--right-side-refs>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "put references at right, not counted in B<-w>"
msgstr "Skriv referenser till höger, ej med i B<-w>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-S>, B<--sentence-regexp>=I<\\,REGEXP\\/>"
msgstr "B<-S>, B<--sentence-regexp>=I<\\,REGUTTR\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "for end of lines or end of sentences"
msgstr "För radslut eller meningsslut."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-T>, B<--format>=I<\\,tex\\/>"
msgstr "B<-T>, B<--format>=I<\\,tex\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "generate output as TeX directives"
msgstr "Generera utdata som TeX-direktiv."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-W>, B<--word-regexp>=I<\\,REGEXP\\/>"
msgstr "B<-W>, B<--word-regexp>=I<\\,REGUTTR\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "use REGEXP to match each keyword"
msgstr "Använd REGUTTR för att matcha varje nyckelord."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-b>, B<--break-file>=I<\\,FILE\\/>"
msgstr "B<-b>, B<--break-file>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "word break characters in this FILE"
msgstr "Ordmellanrumstecken i denna FIL."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>, B<--ignore-case>"
msgstr "B<-f>, B<--ignore-case>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "fold lower case to upper case for sorting"
msgstr "Gör om gemener till versaler för sortering."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-g>, B<--gap-size>=I<\\,NUMBER\\/>"
msgstr "B<-g>, B<--gap-size>=I<\\,ANTAL\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "gap size in columns between output fields"
msgstr "Mellanrum i kolumner mellan utdatafält."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>, B<--ignore-file>=I<\\,FILE\\/>"
msgstr "B<-i>, B<--ignore-file>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "read ignore word list from FILE"
msgstr "Läs lista av ord att ignorera från FIL."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-o>, B<--only-file>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--only-file>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "read only word list from this FILE"
msgstr "Läs lista av ord att endast använda från FIL."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-r>, B<--references>"
msgstr "B<-r>, B<--references>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "first field of each line is a reference"
msgstr "Första fältet på varje rad är en referens."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<-t>, B<--typeset-mode> - not implemented -"
msgstr "B<-t>, B<--typeset-mode> - ej implementerat -"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-w>, B<--width>=I<\\,NUMBER\\/>"
msgstr "B<-w>, B<--width>=I<\\,ANTAL\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output width in columns, reference excluded"
msgstr "Utmatningsbredd i kolumner, utan referenser."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "visa denna hjälp och avsluta"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "visa versionsinformation och avsluta"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "UPPHOVSMAN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Written by F. Pinard."
msgstr "Skrivet av F. Pinard."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERA FEL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"GNU coreutils hjälp på nätet: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Rapportera anmärkningar på översättningen till E<lt>tp-sv@listor.tp-sv."
"seE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller senare E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Detta är fri programvara: du får fritt ändra och vidaredistribuera den. Det "
"finns INGEN GARANTI, så långt lagen tillåter."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE ÄVEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Full documentation E<lt>https://www.gnu.org/software/coreutils/ptxE<gt>"
msgstr ""
"Fullständig dokumentation E<lt>https://www.gnu.org/software/coreutils/"
"ptxE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) ptx invocation\\(aq"
msgstr ""
"eller tillgängligt lokalt via: info \\(aq(coreutils) ptx invocation\\(aq"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "September 2022"
msgstr "september 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller senare E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: debian-unstable fedora-40 mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: Plain text
#: debian-unstable fedora-40 mageia-cauldron
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller senare E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: fedora-40 opensuse-leap-15-6
#, no-wrap
msgid "January 2024"
msgstr "januari 2024"

#. type: TH
#: fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "April 2024"
msgstr "april 2024"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "August 2023"
msgstr "augusti 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller senare E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

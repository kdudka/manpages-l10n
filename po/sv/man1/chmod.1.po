# Swedish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Göran Uddeborg <goeran@uddeborg.se>, 2023, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-05-01 15:36+0200\n"
"PO-Revision-Date: 2024-05-03 21:56+0200\n"
"Last-Translator: Göran Uddeborg <goeran@uddeborg.se>\n"
"Language-Team: Swedish <tp-sv@listor.tp-sv.se>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "CHMOD"
msgstr "CHMOD"

#. type: TH
#: archlinux debian-unstable
#, no-wrap
msgid "March 2024"
msgstr "mars 2024"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.5"
msgstr "GNU coreutils 9.5"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Användarkommandon"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAMN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "chmod - change file mode bits"
msgstr "chmod — ändra filrättighetsbitar"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<chmod> [I<\\,OPTION\\/>]... I<\\,MODE\\/>[I<\\,,MODE\\/>]... I<\\,FILE\\/"
">..."
msgstr ""
"B<chmod> [I<\\,FLAGGA\\/>]... I<\\,RÄTTIGHET\\/>[I<\\,,RÄTTIGHET\\/>]... "
"I<\\,FIL\\/>..."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<chmod> [I<\\,OPTION\\/>]... I<\\,OCTAL-MODE FILE\\/>..."
msgstr "B<chmod> [I<\\,FLAGGA\\/>]... I<\\,OKTAL-RÄTTIGHET FIL\\/>..."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<chmod> [I<\\,OPTION\\/>]... I<\\,--reference=RFILE FILE\\/>..."
msgstr "B<chmod> [I<\\,FLAGGA\\/>]... I<\\,--reference=RFIL FIL\\/>..."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVNING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This manual page documents the GNU version of B<chmod>.  B<chmod> changes "
"the file mode bits of each given file according to I<mode>, which can be "
"either a symbolic representation of changes to make, or an octal number "
"representing the bit pattern for the new mode bits."
msgstr ""
"Denna manualsida dokumenterar GNU-versionen av B<chmod>. B<chmod> ändrar "
"filrättighetsbitarna på de angivna filerna i enlighet med I<rättighet>, "
"vilket kan vara antingen en symbolisk representation av vilka ändringar som "
"skall göras, eller ett oktalt tal som representerat bitmönstret för de nya "
"lägesbitarna."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The format of a symbolic mode is [ B<ugoa>.\\|.\\|.][[B<-+=>][I<perms>.\\|."
"\\|.].\\|.\\|.], where I<perms> is either zero or more letters from the set "
"B<rwxXst>, or a single letter from the set B<ugo>.  Multiple symbolic modes "
"can be given, separated by commas."
msgstr ""
"Formatet på symboliska rättigheter är [ B<ugoa>.\\|.\\|.][B<-+=>]"
"[I<rättigheter>.\\|.\\|.].\\|.\\|.], där I<rättigheter> är antingen noll "
"eller flera bokstäver från mängden B<rwxXst>, eller en ensam bokstav från "
"mängden B<ugo>. Flera symboliska lägen kan ges, separerade av kommatecken."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A combination of the letters B<ugoa> controls which users' access to the "
"file will be changed: the user who owns it (B<u>), other users in the file's "
"group (B<g>), other users not in the file's group (B<o>), or all users "
"(B<a>).  If none of these are given, the effect is as if (B<a>) were given, "
"but bits that are set in the umask are not affected."
msgstr ""
"En kombination av bokstäverna B<ugoa> styr vilka användares åtkomst till "
"filen som skall ändras: användaren som äger den (B<u>), andra användare i "
"filens grupp (B<g>), andra användare som inte är i filens grupp (B<o>) eller "
"alla användare (B<a>). Om ingen av dessa anges är effekten som om (B<a>) "
"angavs, men bitar som är satta i umask:en påverkas inte."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The operator B<+> causes the selected file mode bits to be added to the "
"existing file mode bits of each file; B<-> causes them to be removed; and "
"B<=> causes them to be added and causes unmentioned bits to be removed "
"except that a directory's unmentioned set user and group ID bits are not "
"affected."
msgstr ""
"Operatorn B<+> gör att de valda rättighetsbitarna läggs till till de "
"befintliga filrättighetsbitarna till varje fil; B<-> gör att de tas bort; "
"och B<=> gör att de läggs till och att bitar som inte nämns tas bort med "
"undantag av att en katalogs ej nämnda sätt-användar- och -grupp-ID-bitar "
"inte påverkas."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The letters B<rwxXst> select file mode bits for the affected users: read "
"(B<r>), write (B<w>), execute (or search for directories)  (B<x>), execute/"
"search only if the file is a directory or already has execute permission for "
"some user (B<X>), set user or group ID on execution (B<s>), restricted "
"deletion flag or sticky bit (B<t>).  Instead of one or more of these "
"letters, you can specify exactly one of the letters B<ugo>: the permissions "
"granted to the user who owns the file (B<u>), the permissions granted to "
"other users who are members of the file's group (B<g>), and the permissions "
"granted to users that are in neither of the two preceding categories (B<o>)."
msgstr ""
"Bokstäverna B<rwxXst> väljer filrättighetsbitar för de påverkade användarna: "
"läs (B<r>), skriv (B<w>), kör (eller sök för kataloger) (B<x>), kör/sök "
"endast om filen är en katalog eller redan har körrättigheter för någon "
"användare (B<X>), sätt-användar- eller -grupp-ID vid körning (B<s>), flagga "
"för begränsade radering eller klistrig byt (B<t>). Istället för en eller "
"flera av dessa bokstäver kan man ange exakt en av bokstäverna B<ugo>: "
"rättigheterna som ges till användaren som äger filen (B<u>), rättigheterna "
"som ges till andra användare som är medlemmar av filens grupp (B<g>) och "
"rättigheterna som ges till användare som inte är i någon av de två "
"föregående katagorierna (B<o>)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A numeric mode is from one to four octal digits (0-7), derived by adding up "
"the bits with values 4, 2, and 1.  Omitted digits are assumed to be leading "
"zeros.  The first digit selects the set user ID (4) and set group ID (2) and "
"restricted deletion or sticky (1) attributes.  The second digit selects "
"permissions for the user who owns the file: read (4), write (2), and execute "
"(1); the third selects permissions for other users in the file's group, with "
"the same values; and the fourth for other users not in the file's group, "
"with the same values."
msgstr ""
"En numerisk rättighet är från en till fyra oktala siffror (0-7), härledda "
"genom att addera ihop bitar med värdena 4, 2 och 1. Uteslutna siffror antas "
"vara inledande nollor. Den första siffran väljer attributen sätt-användar-ID "
"(4) och sätt-grupp-ID (2) och begränsad radering eller klistrig (1). Den "
"andra siffran väljer rättigheter för användaren som äger filen: läs (4), "
"skriv (2) och kör (1); den tredje väljer rättigheter för andra användare i "
"filens grupp, med samma värden; och den fjärde för andra användare som inte "
"är i filens grupp, med samma värden."

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"B<chmod> doesn't change the permissions of symbolic links; the B<chmod> "
"system call cannot change their permissions on most systems, and most "
"systems ignore permissions of symbolic links.  However, for each symbolic "
"link listed on the command line, B<chmod> changes the permissions of the "
"pointed-to file.  In contrast, B<chmod> ignores symbolic links encountered "
"during recursive directory traversals. Options that modify this behavior are "
"described in the B<OPTIONS> section."
msgstr ""
"B<chmod> ändrar inte rättigheterna på symboliska länkar; systemanropet "
"B<chmod> kan inte ändra deras rättigheter på de flesta system, och de flesta "
"system ignorerar rättigheterna på symboliska länkar. Dock, för varje "
"symbolisk länk listad på kommandoraden ändrar B<chmod> rättigheterna hos den "
"utpekade filen. I kontrast till detta ignorerar B<chmod> symboliska länkar "
"som påträffas under rekursiv katalogtraversering. Flaggor som påverkar detta "
"beskrivs i avsnittet B<FLAGGOR>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SETUID AND SETGID BITS"
msgstr "SETUID- OCH SETGID-BITAR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<chmod> clears the set-group-ID bit of a regular file if the file's group "
"ID does not match the user's effective group ID or one of the user's "
"supplementary group IDs, unless the user has appropriate privileges.  "
"Additional restrictions may cause the set-user-ID and set-group-ID bits of "
"I<MODE> or I<RFILE> to be ignored.  This behavior depends on the policy and "
"functionality of the underlying B<chmod> system call.  When in doubt, check "
"the underlying system behavior."
msgstr ""
"B<chmod> nollställer sätt grupp-ID-biten för en normal fil om filens grupp-"
"ID inte stämmer med användarens effektiva grupp-ID eller en av användarens "
"tilläggsgrupps-ID:n, om inte användaren har de motsvarande rättigheterna. "
"Ytterligare begränsningar kan orsaka att sätt-användar-ID- och sätt-grupp-ID-"
"bitarna i I<RÄTTIGHET> eller I<RFIL> ignoreras. Detta beteende beror på "
"policyn och funktionaliteten hos det underliggande systemanropet B<chmod>. "
"Vid oklarhet, kontrollera det underliggande systemets beteende."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"For directories B<chmod> preserves set-user-ID and set-group-ID bits unless "
"you explicitly specify otherwise.  You can set or clear the bits with "
"symbolic modes like B<u+s> and B<g-s>.  To clear these bits for directories "
"with a numeric mode requires an additional leading zero like B<00755>, "
"leading minus like B<-6000>, or leading equals like B<=755>."
msgstr ""
"För kataloger bevarar B<chmod> sätt-användar-ID- och sätt-grupp-ID-bitarna "
"om inte man uttryckligen anger något annat. Man kan sätta eller nollställa "
"bitarna med symboliska rättigheter som B<u+s> och B<g-s>. För att nollställa "
"dessa bitar för kataloger med en numerisk rättighet krävs en ytterligare "
"inledande nolla som B<00755>, inledande minus som B<-6000>, eller inledande "
"likhetstecken som B<=755>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RESTRICTED DELETION FLAG OR STICKY BIT"
msgstr "FLAGGAN FÖR BEGRÄNSAD RADERING ELLER KLISTRIG BIT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The restricted deletion flag or sticky bit is a single bit, whose "
"interpretation depends on the file type.  For directories, it prevents "
"unprivileged users from removing or renaming a file in the directory unless "
"they own the file or the directory; this is called the I<restricted deletion "
"flag> for the directory, and is commonly found on world-writable directories "
"like B</tmp>.  For regular files on some older systems, the bit saves the "
"program's text image on the swap device so it will load more quickly when "
"run; this is called the I<sticky bit>."
msgstr ""
"Flaggan för begränsad radering eller klistrig bit är en enda bit, vars "
"tolkning beror på filtypen. För kataloger förhindrar den oprivilegierade "
"användare från att ta bort eller byta namn på en fil i katalogen om de inte "
"äger filen eller katalogen; detta kallas I<flagga för begränsad radering> "
"för katalogen, och finns ofta på kataloger som är skrivbara för alla såsom "
"B</tmp>. För vanliga filer på en del äldre system sparar biten programmets "
"textavbild på växlingsenheten så att den laddas snabbare när den kör; detta "
"kallas den I<klistriga biten>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "FLAGGOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Change the mode of each FILE to MODE.  With B<--reference>, change the mode "
"of each FILE to that of RFILE."
msgstr ""
"Ändra rättigheterna för varje FIL till RÄTTIGHET. Med B<--reference>, ändra "
"rättigheter för varje FIL till dem hos RFIL."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--changes>"
msgstr "B<-c>, B<--changes>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "like verbose but report only when a change is made"
msgstr "Som B<--verbose> men rapportera bara när ändringar görs."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>, B<--silent>, B<--quiet>"
msgstr "B<-f>, B<--silent>, B<--quiet>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "suppress most error messages"
msgstr "Utelämna de flesta felmeddelanden."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output a diagnostic for every file processed"
msgstr "Skriv ut ett meddelande för varje bearbetad fil."

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<--dereference>"
msgstr "B<--dereference>"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"affect the referent of each symbolic link, rather than the symbolic link "
"itself"
msgstr ""
"ändra referensen för varje symbolisk länk, istället för den symboliska "
"länken själv"

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--no-dereference>"
msgstr "B<-h>, B<--no-dereference>"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "affect each symbolic link, rather than the referent"
msgstr "påverka varje symboliska länk, snarare än det refererade"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--no-preserve-root>"
msgstr "B<--no-preserve-root>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "do not treat '/' specially (the default)"
msgstr "Behandla inte ”/” speciellt (normalfall)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--preserve-root>"
msgstr "B<--preserve-root>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "fail to operate recursively on '/'"
msgstr "Låt bli att arbeta på ”/”."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--reference>=I<\\,RFILE\\/>"
msgstr "B<--reference>=I<\\,RFIL\\/>"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"use RFILE's mode instead of specifying MODE values.  RFILE is always "
"dereferenced if a symbolic link."
msgstr ""
"använd RFILs rättigheter istället för att ange RÄTTIGHETSvärden. RFIL "
"derefereras alltid om det är en symbolisk länk."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-R>, B<--recursive>"
msgstr "B<-R>, B<--recursive>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "change files and directories recursively"
msgstr "Ändra filer och kataloger rekursivt."

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"The following options modify how a hierarchy is traversed when the B<-R> "
"option is also specified.  If more than one is specified, only the final one "
"takes effect. '-H' is the default."
msgstr ""
"Följande flaggor modifierar hur en hierarki traverseras när flaggan B<-R> "
"också anges.  Om mer än en anges har enbart den sista någon verkan. ”-H” är "
"standard."

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-H>"
msgstr "B<-H>"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"if a command line argument is a symbolic link to a directory, traverse it"
msgstr ""
"Om ett kommandoradsargument för ett kommando är en symbolisk länk, följ den."

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-L>"
msgstr "B<-L>"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "traverse every symbolic link to a directory encountered"
msgstr "Följ varje symbolisk länk till en katalog som påträffas."

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<-P>"
msgstr "B<-P>"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "do not traverse any symbolic links"
msgstr "följ inte några symboliska länkar"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "visa denna hjälp och avsluta"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "visa versionsinformation och avsluta"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Each MODE is of the form '[ugoa]*([-+=]([rwxXst]*|[ugo]))+|[-+=][0-7]+'."
msgstr ""
"Varje RÄTTIGHET har formen ”[ugoa]*([-+=]([rwxXst]*|[ugo]))+|[-+=][0-7]+”."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "UPPHOVSMAN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Written by David MacKenzie and Jim Meyering."
msgstr "Skrivet av David MacKenzie och Jim Meyering."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERA FEL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"GNU coreutils hjälp på nätet: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Rapportera anmärkningar på översättningen till E<lt>tp-sv@listor.tp-sv."
"seE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller senare E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Detta är fri programvara: du får fritt ändra och vidaredistribuera den. Det "
"finns INGEN GARANTI, så långt lagen tillåter."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE ÄVEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<chmod>(2)"
msgstr "B<chmod>(2)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/chmodE<gt>"
msgstr ""
"Fullständig dokumentation E<lt>https://www.gnu.org/software/coreutils/"
"chmodE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) chmod invocation\\(aq"
msgstr ""
"eller tillgängligt lokalt via: info \\(aq(coreutils) chmod invocation\\(aq"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "September 2022"
msgstr "september 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 mageia-cauldron opensuse-leap-15-6
msgid ""
"B<chmod> never changes the permissions of symbolic links; the B<chmod> "
"system call cannot change their permissions.  This is not a problem since "
"the permissions of symbolic links are never used.  However, for each "
"symbolic link listed on the command line, B<chmod> changes the permissions "
"of the pointed-to file.  In contrast, B<chmod> ignores symbolic links "
"encountered during recursive directory traversals."
msgstr ""
"B<chmod> ändrar aldrig rättigheterna på symboliska läkar; systemarnopet "
"B<chmod> kan inte ändra deras rättigheter. Detta är inte ett problem "
"eftersom rättigheterna hos en symbolisk länk aldrig används. Dock, för varje "
"symbolisk länk listad på kommandoraden ändrar B<chmod> rättigheterna hos den "
"utpekade filen. I kontrast till detta ignorerar B<chmod> symboliska länkar "
"som påträffas under rekursiv katalogtraversering."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "use RFILE's mode instead of MODE values"
msgstr "Använd RFILs rättigheter istället för ett RÄTTIGHETSvärde."

#. type: Plain text
#: debian-bookworm
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller senare E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: debian-unstable fedora-40 mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: Plain text
#: debian-unstable fedora-40 mageia-cauldron
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller senare E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: fedora-40 opensuse-leap-15-6
#, no-wrap
msgid "January 2024"
msgstr "januari 2024"

#. type: TH
#: fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "April 2024"
msgstr "april 2024"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "August 2023"
msgstr "augusti 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"For directories B<chmod> preserves set-user-ID and set-group-ID bits unless "
"you explicitly specify otherwise.  You can set or clear the bits with "
"symbolic modes like B<u+s> and B<g-s>.  To clear these bits for directories "
"with a numeric mode requires an additional leading zero, or leading = like "
"B<00755> , or B<=755>"
msgstr ""
"För kataloger bevarar B<chmod> sätt-användar-ID- och sätt-grupp-ID-bitarna "
"om man inte uttryckligen anger något annat. Man kan sätta eller nollställa "
"bitarna med symboliska rättigheter som B<u+s> och B<g-s>. För att nollställa "
"dessa bitar för kataloger med en numerisk rättighet krävs en ytterligare "
"inledande nolla eller inledande = som B<00755> eller B<=755>."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller senare E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: opensuse-leap-15-6
msgid "chmod(2)"
msgstr "B<chmod>(2)"

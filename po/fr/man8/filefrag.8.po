# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Gérard Delafond <gerard@delafond.org>
# Frédéric Delanoy <delanoy_f@yahoo.com>, 2000, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2000.
# Sébastien Blanchet, 2002.
# Emmanuel Araman <Emmanuel@araman.org>, 2002.
# Éric Piel <eric.piel@tremplin-utc.net>, 2005.
# Nicolas François <nicolas.francois@centraliens.net>, 2007.
# Romain Doumenc <rd6137@gmail.com>, 2010.
# David Prévot <david@tilapin.org>, 2010-2014.
# Cédric Boutillier <cedric.boutillier@gmail.com>, 2011-2014.
# Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: e2fsprogs\n"
"POT-Creation-Date: 2024-05-01 15:39+0200\n"
"PO-Revision-Date: 2024-03-01 15:14+0100\n"
"Last-Translator: Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: vim\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILEFRAG"
msgstr "FILEFRAG"

#. type: TH
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "February 2023"
msgstr "Février 2023"

#. type: TH
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "E2fsprogs version 1.47.0"
msgstr "E2fsprogs version 1.47.0"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "filefrag - report on file fragmentation"
msgstr "filefrag - Indiquer la fragmentation des fichiers"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<filefrag> [ B<-b>I<blocksize> ] [ B<-BeEkPsvVxX> ] [ I<files...> ]"
msgstr ""
"B<filefrag> [ B<-b>I<taille_bloc> ] [ B<-BeEkPsvVxX> ] [ I<fichiers> ... ]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<filefrag> reports on how badly fragmented a particular file might be.  It "
"makes allowances for indirect blocks for ext2 and ext3 file systems, but can "
"be used on files for any file system."
msgstr ""
"B<filefrag> indique dans quelle mesure un fichier donné peut être fragmenté. "
"Il prend en considération les blocs indirects des systèmes de fichiers ext2 "
"et ext3, mais peut également être utilisé sur d'autres systèmes de fichiers."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<filefrag> program initially attempts to get the extent information "
"using FIEMAP ioctl which is more efficient and faster.  If FIEMAP is not "
"supported then filefrag will fall back to using FIBMAP."
msgstr ""
"Le programme B<filefrag> essaie d'abord d'obtenir les informations à l'aide "
"de l'ioctl FIEMAP qui est plus efficace et plus rapide. Si FIEMAP n'est pas "
"pris en charge, alors B<filefrag> utilisera l'ioctl FIBMAP."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-B>"
msgstr "B<-B>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Force the use of the older FIBMAP ioctl instead of the FIEMAP ioctl for "
"testing purposes."
msgstr ""
"Forcer l'utilisation de l'ancien ioctl FIBMAP plutôt que l'ioctl FIEMAP pour "
"des raisons de test."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-b>I<blocksize>"
msgstr "B<-b>I<taille_bloc>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Use I<blocksize> in bytes, or with [KMG] suffix, up to 1GB for output "
"instead of the file system blocksize.  For compatibility with earlier "
"versions of B<filefrag>, if I<blocksize> is unspecified it defaults to 1024 "
"bytes.  Since I<blocksize> is an optional argument, it must be added without "
"any space after B<-b>."
msgstr ""
"Utiliser I<taille_bloc> en octets, ou avec un suffixe [KMG], jusqu’à 1 Go "
"pour la sortie au lieu de la taille des blocs du système de fichiers. Afin "
"de maintenir la compatibilité avec les versions précédentes de B<filefrag>, "
"si I<taille_bloc> n'est pas précisée, sa valeur par défaut est 1024 octets. "
"Dans la mesure où I<taille_bloc> est un argument optionnel, il peut être "
"ajouté sans espace après B<-b>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-e>"
msgstr "B<-e>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Print output in extent format, even for block-mapped files."
msgstr ""
"Afficher la sortie dans un format étendu, même pour les fichiers placés en "
"bloc."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-E>"
msgstr "B<-E>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Display the contents of ext4's extent status cache.  This feature is not "
"supported on all kernels, and is only supported on ext4 file systems."
msgstr ""
"Afficher le contenu du cache d’état de domaines (extent) d’ext4. Cette "
"fonctionnalité n’est pas gérée par tous les noyaux et n’est uniquement prise "
"en charge que sur les systèmes de fichiers ext4."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-k>"
msgstr "B<-k>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Use 1024-byte blocksize for output (identical to '-b1024')."
msgstr ""
"Utiliser une I<taille_bloc> de 1024 octets pour la sortie (identique à « -"
"b1024 »)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-P>"
msgstr "B<-P>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Pre-load the ext4 extent status cache for the file.  This is not supported "
"on all kernels, and is only supported on ext4 file systems."
msgstr ""
"Précharger le cache d’état de domaines (extent) d’ext4. Cette fonction n’est "
"pas gérée par tous les noyaux et n’est prise en charge que sur les systèmes "
"de fichiers ext4."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>"
msgstr "B<-s>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Sync the file before requesting the mapping."
msgstr "Synchroniser le fichier avant de demander son emplacement."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>"
msgstr "B<-v>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Be verbose when checking for file fragmentation."
msgstr "Être bavard lors de l'affichage de la fragmentation des fichiers."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>"
msgstr "B<-V>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Print version number of program and library.  If given twice, also print the "
"FIEMAP flags that are understood by the current version."
msgstr ""
"Afficher le numéro de version du programme et de la bibliothèque. Si "
"l'option est indiquée deux fois, afficher aussi les drapeaux FIEMAP qui sont "
"compris la version actuelle."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-x>"
msgstr "B<-x>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Display mapping of extended attributes."
msgstr "Afficher les correspondances des attributs étendus."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-X>"
msgstr "B<-X>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Display extent block numbers in hexadecimal format."
msgstr "Afficher les numéros de blocs étendus avec des nombres hexadécimaux."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<filefrag> was written by Theodore Ts'o E<lt>tytso@mit.eduE<gt>."
msgstr "B<filefrag> a été écrit par Theodore Ts'o E<lt>tytso@mit.eduE<gt>."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "April 2024"
msgstr "Avril 2024"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "E2fsprogs version 1.47.1-rc1"
msgstr "E2fsprogs version 1.47.1-rc1"

# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Gérard Delafond <gerard@delafond.org>
# Frédéric Delanoy <delanoy_f@yahoo.com>, 2000, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2000.
# Sébastien Blanchet, 2002.
# Emmanuel Araman <Emmanuel@araman.org>, 2002.
# Éric Piel <eric.piel@tremplin-utc.net>, 2005.
# Nicolas François <nicolas.francois@centraliens.net>, 2007.
# Romain Doumenc <rd6137@gmail.com>, 2010.
# David Prévot <david@tilapin.org>, 2010-2014.
# Cédric Boutillier <cedric.boutillier@gmail.com>, 2011-2014.
# Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fr>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: e2fsprogs\n"
"POT-Creation-Date: 2024-05-01 15:38+0200\n"
"PO-Revision-Date: 2023-07-18 16:08+0200\n"
"Last-Translator: Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: vim\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "E2IMAGE"
msgstr "E2IMAGE"

#. type: TH
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "February 2023"
msgstr "Février 2023"

#. type: TH
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "E2fsprogs version 1.47.0"
msgstr "E2fsprogs version 1.47.0"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "e2image - Save critical ext2/ext3/ext4 file system metadata to a file"
msgstr ""
"e2image — Sauvegarder dans un fichier les métadonnées critiques de systèmes "
"de fichiers ext2/ext3/ext4"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<e2image> [B<-r>|B<-Q> [B<-af>]] [ B<-b> I<superblock> ] [ B<-B> "
"I<blocksize> ] [ B<-cnps> ] [ B<-o> I<src_offset> ] [ B<-O> I<dest_offset> ] "
"I<device> I<image-file>"
msgstr ""
"B<e2image> [B<-r>|B<-Q> [B<-af>]] [ B<-b> I<superbloc> ] [ B<-B> "
"I<taille_bloc> ] [ B<-cnps> ] [ B<-o> I<décalage_src> ] [ B<-O> "
"I<décalage_dest> ] I<device> I<fichier_image>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<e2image> B<-I> I<device> I<image-file>"
msgstr "B<e2image> B<-I> I<périphérique> I<fichier_image>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<e2image> program will save critical ext2, ext3, or ext4 file system "
"metadata located on I<device> to a file specified by I<image-file>.  The "
"image file may be examined by B<dumpe2fs> and B<debugfs>, by using the B<-i> "
"option to those programs.  This can assist an expert in recovering "
"catastrophically corrupted file systems."
msgstr ""
"Le programme B<e2image> permet de sauvegarder dans le fichier "
"I<fichier_image> les métadonnées critiques de systèmes de fichiers ext2, "
"ext3 ou ext4 situés dans I<périphérique> dans un fichier spécifié par "
"I<fichier_image>. Le fichier image peut être examiné par B<dumpe2fs> et "
"B<debugfs>, en utilisant leur option B<-i>. Cela peut permettre à un expert "
"de récupérer un système de fichiers endommagé."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"It is a very good idea to create image files for all file systems on a "
"system and save the partition layout (which can be generated using the "
"B<fdisk -l> command) at regular intervals --- at boot time, and/or every "
"week or so.  The image file should be stored on some file system other than "
"the file system whose data it contains, to ensure that this data is "
"accessible in the case where the file system has been badly damaged."
msgstr ""
"C'est une très bonne idée de créer régulièrement des fichiers image pour "
"tous les systèmes de fichiers d'un système et de sauvegarder régulièrement "
"la structure des partitions (en utilisant la commande B<fdisk\\ -l>), par "
"exemple à chaque démarrage ou toutes les semaines. Le fichier image doit "
"être stocké sur un autre système de fichiers que celui dont les données sont "
"extraites, pour s'assurer que ces données seront disponibles dans le cas où "
"le système de fichiers serait endommagé."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"To save disk space, B<e2image> creates the image file as a sparse file, or "
"in QCOW2 format.  Hence, if the sparse image file needs to be copied to "
"another location, it should either be compressed first or copied using the "
"B<--sparse=always> option to the GNU version of B<cp>(1).  This does not "
"apply to the QCOW2 image, which is not sparse."
msgstr ""
"Pour économiser de l'espace disque, B<e2image> crée l'image comme un fichier "
"creux ou au format QCOW2. De ce fait, si le fichier doit être copié "
"ailleurs, il doit être soit compressé préalablement, soit copié en utilisant "
"l'option B<--sparse=always> de la version GNU de B<cp>(1). Cela ne "
"s'applique pas aux images QCOW2, qui ne sont pas creuses."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The size of an ext2 image file depends primarily on the size of the file "
"systems and how many inodes are in use.  For a typical 10 Gigabyte file "
"system, with 200,000 inodes in use out of 1.2 million inodes, the image file "
"will be approximately 35 Megabytes; a 4 Gigabyte file system with 15,000 "
"inodes in use out of 550,000 inodes will result in a 3 Megabyte image file.  "
"Image files tend to be quite compressible; an image file taking up 32 "
"Megabytes of space on disk will generally compress down to 3 or 4 Megabytes."
msgstr ""
"La taille d'un fichier image ext2 dépend principalement de la taille du "
"système de fichiers et du nombre d'inœuds utilisés. Sur un système typique "
"de 10 gigaoctets, avec 200 000 inœuds utilisés sur 1,2 millions d'inœuds, la "
"taille du fichier image sera d'environ 35 mégaoctets ; un système de "
"fichiers de 4 gigaoctets avec 15 000 inœuds utilisés sur 550 000 donnera un "
"fichier image de 3 mégaoctets. Les fichiers image ont tendance à pouvoir "
"être compressés facilement ; une image prenant 32 mégaoctets sur le disque "
"pourra généralement être compressée en un fichier de 3 ou 4 mégaoctets."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<image-file> is B<->, then the output of B<e2image> will be sent to "
"standard output, so that the output can be piped to another program, such as "
"B<gzip>(1).  (Note that this is currently only supported when creating a raw "
"image file using the B<-r> option, since the process of creating a normal "
"image file, or QCOW2 image currently requires random access to the file, "
"which cannot be done using a pipe."
msgstr ""
"Si I<fichier_image> vaut B<->, la sortie de B<e2image> sera envoyée sur la "
"sortie standard et pourra donc être redirigée vers un autre programme, comme "
"B<gzip>(1). Remarquez que ce n'est actuellement géré que lors de la création "
"d'une image brute en utilisant l'option B<-r> car il est nécessaire de "
"pouvoir accéder aléatoirement dans le fichier pour créer une image normale "
"ou une image QCOW2, ce qui n'est pas possible avec un enchaînement (pipe)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-a>"
msgstr "B<-a>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Include file data in the image file.  Normally B<e2image> only includes fs "
"metadata, not regular file data.  This option will produce an image that is "
"suitable to use to clone the entire FS or for backup purposes.  Note that "
"this option only works with the raw (I<-r>)  or QCOW2 (I<-Q>)  formats.  In "
"conjunction with the B<-r> option it is possible to clone all and only the "
"used blocks of one file system to another device/image file."
msgstr ""
"Inclure les données de fichier dans le fichier image. Normalement, "
"B<e2image> inclut les métadonnées de système de fichiers, pas les données de "
"fichier ordinaire. Cette option produit une image adaptée pour un clonage de "
"système de fichiers entier ou pour des sauvegardes. Il est à remarquer que "
"cette option fonctionne seulement avec les formats bruts (I<-r>) ou QCOW2 "
"(I<-Q>). En conjonction avec l’option\\ B<-r>, il est possible de cloner "
"tous les blocs utilisés, et seulement ceux-ci, d’un système de fichiers vers "
"un autre fichier de périphérique/image."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-b>I< superblock>"
msgstr "B<-b> I<superbloc>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Get image from partition with broken primary superblock by using the "
"superblock located at file system block number I<superblock>.  The partition "
"is copied as-is including broken primary superblock."
msgstr ""
"Obtenir l’image d’une partition avec le superbloc primaire cassé en "
"utilisant le superbloc situé au numéro de bloc I<superbloc> du système de "
"fichiers. La partition est copiée telle quelle, incluant le superbloc "
"primaire cassé."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-B>I< blocksize>"
msgstr "B<-B>I< taille_bloc>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Set the file system blocksize in bytes.  Normally, B<e2image> will search "
"for the superblock at various different block sizes in an attempt to find "
"the appropriate blocksize. This search can be fooled in some cases.  This "
"option forces e2fsck to only try locating the superblock with a particular "
"blocksize. If the superblock is not found, e2image will terminate with a "
"fatal error."
msgstr ""
"Définir la taille de bloc du système de fichiers en octet. Normalement, "
"B<e2fsck> recherchera le superbloc pour des tailles de bloc différentes dans "
"le but de déterminer la taille appropriée des blocs. Cette recherche peut "
"mener à des résultats erronés dans certains cas. Cette option force "
"B<e2fsck> à n'essayer de localiser le superbloc que pour une taille de bloc "
"particulière. Si le superbloc reste introuvable, B<e2image> quittera avec "
"une erreur fatale."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>"
msgstr "B<-c>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Compare each block to be copied from the source I<device> to the "
"corresponding block in the target I<image-file>.  If both are already the "
"same, the write will be skipped.  This is useful if the file system is being "
"cloned to a flash-based storage device (where reads are very fast and where "
"it is desirable to avoid unnecessary writes to reduce write wear on the "
"device)."
msgstr ""
"Comparer chaque bloc à copier à partir de la source I<périphérique> vers le "
"bloc correspondant dans la cible I<fichier_image>. Si les deux sont déjà "
"identiques, l’écriture sera ignorée. Cela est utile si le système de "
"fichiers est en cours de clonage vers un périphérique de stockage de type "
"flash (où les lectures sont très rapides et où il est souhaitable d’éviter "
"des écritures non nécessaires pour éviter l’usure d’écriture sur le "
"périphérique)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>"
msgstr "B<-f>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Override the read-only requirement for the source file system when saving "
"the image file using the B<-r> and B<-Q> options.  Normally, if the source "
"file system is in use, the resulting image file is very likely not going to "
"be useful. In some cases where the source file system is in constant use "
"this may be better than no image at all."
msgstr ""
"Outrepasser l’exigence d’écriture seule pour le système de fichiers source "
"lors de la sauvegarde du fichier image en utilisant les options B<-r> et B<-"
"Q>. Normalement, si le système de fichiers est en cours d’utilisation, le "
"fichier image qui en résulte a beaucoup de chances de ne pas être utile. "
"Dans certains cas où le système de fichiers est utilisé constamment, cela "
"peut être préférable que pas d’image du tout."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-I>"
msgstr "B<-I>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"install the metadata stored in the image file back to the device.  It can be "
"used to restore the file system metadata back to the device in emergency "
"situations."
msgstr ""
"Installer sur le périphérique les métadonnées enregistrées dans le fichier "
"image. Elle permet de restaurer les métadonnées d'un système de fichiers sur "
"le périphérique en cas de nécessité."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<WARNING!!!!> The B<-I> option should only be used as a desperation measure "
"when other alternatives have failed.  If the file system has changed since "
"the image file was created, data B<will> be lost.  In general, you should "
"make another full image backup of the file system first, in case you wish to "
"try other recovery strategies afterward."
msgstr ""
"B<ATTENTION !!!!> L'option B<-I> ne devrait être utilisée que dans les cas "
"désespérés où toutes les autres alternatives ont échoué. Si le système de "
"fichiers a été modifié depuis la création du fichier image, des données "
"B<seront> perdues. En général, il est prudent de faire une image de "
"sauvegarde complète du système de fichiers, dans le cas où vous souhaiteriez "
"essayer une autre méthode de récupération par la suite."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-n>"
msgstr "B<-n>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Cause all image writes to be skipped, and instead only print the block "
"numbers that would have been written."
msgstr ""
"Faire que toutes les écritures d’image soient omises et, à la place, "
"afficher les numéros de bloc qui seraient écrits."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-o>I< src_offset>"
msgstr "B<-o> I<src_offset>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Specify offset of the image to be read from the start of the source "
"I<device> in bytes.  See B<OFFSETS> for more details."
msgstr ""
"Indiquer le décalage de l’image à lire à partir du début du périphérique "
"source en octet. Consulter B<OFFSETS> pour davantage de détails."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-O>I< tgt_offset>"
msgstr "B<-O> I<tgt_offset>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Specify offset of the image to be written from the start of the target "
"I<image-file> in bytes.  See B<OFFSETS> for more details."
msgstr ""
"Indiquer le décalage de l’image à écrire à partir du début du "
"I<fichier_image> cible en octet. Consulter B<OFFSETS> pour davantage de "
"détails."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-p>"
msgstr "B<-p>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Show progress of image-file creation."
msgstr "Afficher la progression de la création du fichier image."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-Q>"
msgstr "B<-Q>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Create a QCOW2-format image file instead of a normal image file, suitable "
"for use by virtual machine images, and other tools that can use the B<.qcow> "
"image format. See B<QCOW2 IMAGE FILES> below for details."
msgstr ""
"Créer un fichier image au format QCOW2 au lieu d’un fichier image ordinaire "
"adapté pour une utilisation dans des images de machine virtuelle et pour "
"d’autres outils pouvant utiliser le format B<.qcow>. Consulter B<FICHIERS "
"IMAGE QCOW2> ci-dessous pour davantage de détails."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-r>"
msgstr "B<-r>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Create a raw image file instead of a normal image file.  See B<RAW IMAGE "
"FILES> below for details."
msgstr ""
"Créer un fichier image brut au lieu d’un fichier image ordinaire. Consulter "
"B<FICHIERS IMAGE BRUTS> pour davantage de détails."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>"
msgstr "B<-s>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Scramble directory entries and zero out unused portions of the directory "
"blocks in the written image file to avoid revealing information about the "
"contents of the file system.  However, this will prevent analysis of "
"problems related to hash-tree indexed directories."
msgstr ""
"Brouiller les entrées de répertoire et remplir de zéros les portions non "
"utilisées des blocs de répertoires dans le fichier écrit pour éviter de "
"révéler des informations sur le contenu du système de fichiers. Toutefois, "
"cela empêchera l’analyse de problèmes concernant les répertoires indexés par "
"l’arbre de hachages."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RAW IMAGE FILES"
msgstr "FICHIERS IMAGE BRUTS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<-r> option will create a raw image file, which differs from a normal "
"image file in two ways.  First, the file system metadata is placed in the "
"same relative offset within I<image-file> as it is in the I<device> so that "
"B<debugfs>(8), B<dumpe2fs>(8), B<e2fsck>(8), B<losetup>(8), etc. and can be "
"run directly on the raw image file.  In order to minimize the amount of disk "
"space consumed by the raw image file, it is created as a sparse file.  "
"(Beware of copying or compressing/decompressing this file with utilities "
"that don't understand how to create sparse files; the file will become as "
"large as the file system itself!)  Secondly, the raw image file also "
"includes indirect blocks and directory blocks, which the standard image file "
"does not have."
msgstr ""
"L'option B<-r> permet de créer un fichier image brut qui diffère d’un "
"fichier image ordinaire de deux façons. Premièrement, les métadonnées du "
"système de fichiers sont placées aux mêmes positions relatives dans "
"I<fichier_image> que celles dans I<périphérique> de telle sorte que "
"B<debugfs>(8), B<dumpe2fs>(8), B<e2fsck>(8), B<losetup>(8), etc., peuvent "
"fonctionner directement sur le fichier image brut. Afin de minimiser "
"l'espace disque utilisé par le fichier image brut, le fichier est créé comme "
"un fichier creux. Faites attention lors des copies, compression ou "
"décompression de ce fichier avec des outils qui ne sauraient pas créer des "
"fichiers creux ; le fichier prendrait autant de place que le système de "
"fichiers lui-même ! Deuxièmement, le fichier image brut inclut également les "
"blocs indirects ou les blocs de répertoires, ce que les fichiers images "
"normaux n'ont pas."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Raw image files are sometimes used when sending file systems to the "
"maintainer as part of bug reports to e2fsprogs.  When used in this capacity, "
"the recommended command is as follows (replace B<hda1> with the appropriate "
"device for your system):"
msgstr ""
"Les images brutes sont parfois utilisées pour l'envoi de systèmes de "
"fichiers en accompagnement d'un rapport de bogue pour e2fsprogs. Pour cela, "
"il est recommandé de procéder de la façon suivante (remplacez B<hda1> par le "
"périphérique approprié pour votre système) :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "  B<e2image -r /dev/hda1 - | bzip2 E<gt> hda1.e2i.bz2>"
msgstr "  B<e2image -r /dev/hda1 - | bzip2 E<gt> hda1.e2i.bz2>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This will only send the metadata information, without any data blocks.  "
"However, the filenames in the directory blocks can still reveal information "
"about the contents of the file system that the bug reporter may wish to keep "
"confidential.  To address this concern, the B<-s> option can be specified to "
"scramble the filenames in the image."
msgstr ""
"Cela n'enverra que les informations sur les métadonnées, sans les blocs de "
"données. Cependant les noms de fichiers dans les blocs des répertoires "
"peuvent toujours révéler des informations sur le contenu du système de "
"fichiers que l'auteur du rapport de bogue peut vouloir garder "
"confidentielles. Pour éviter ce problème, l'option B<-s> peut être utilisée "
"pour brouiller les noms de fichier dans l’image."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note that this will work even if you substitute B</dev/hda1> for another raw "
"disk image, or QCOW2 image previously created by B<e2image>."
msgstr ""
"Remarquez que cela fonctionnera également si vous remplacez B</dev/hda1> par "
"une autre image disque brute, ou une image QCOW2 créée au préalable par "
"B<e2image>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "QCOW2 IMAGE FILES"
msgstr "FICHIERS IMAGES QCOW2"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<-Q> option will create a QCOW2 image file instead of a normal, or raw "
"image file.  A QCOW2 image contains all the information the raw image does, "
"however unlike the raw image it is not sparse. The QCOW2 image minimize the "
"amount of space used by the image by storing it in special format which "
"packs data closely together, hence avoiding holes while still minimizing "
"size."
msgstr ""
"L'option B<-Q> va créer un fichier image QCOW2 au lieu d'un fichier image "
"normal ou brut. Une image QCOW2 contient autant d'informations qu'une image "
"brute, mais contrairement à cette dernière, elle n'est pas creuse. L'image "
"QCOW2 minimise l'espace disque utilisé en stockant les données dans un "
"format spécial, en tassant les paquets de données, afin d'éviter les trous "
"tout en minimisant la taille."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In order to send file system to the maintainer as a part of bug report to "
"e2fsprogs, use following commands (replace B<hda1> with the appropriate "
"device for your system):"
msgstr ""
"Afin d'envoyer un système de fichiers au responsable en accompagnement d'un "
"rapport de bogue pour e2fsprogs, veuillez procéder de la façon suivante "
"(remplacez B<hda1> par le périphérique approprié pour votre système) :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "\\\tB<e2image -Q /dev/hda1 hda1.qcow2>"
msgstr "\\\tB<e2image -Q /dev/hda1 hda1.qcow2>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "\\\tB<bzip2 -z hda1.qcow2>"
msgstr "\\\tB<bzip2 -z hda1.qcow2>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This will only send the metadata information, without any data blocks.  As "
"described for B<RAW IMAGE FILES> the B<-s> option can be specified to "
"scramble the file system names in the image."
msgstr ""
"Cela n'enverra que les informations sur les métadonnées, sans les blocs de "
"données. Comme décrit pour B<RAW IMAGE FILES>, l’option B<-s> peut être "
"indiquée pour brouiller les noms de système de fichiers dans l’image."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note that the QCOW2 image created by B<e2image> is a regular QCOW2 image and "
"can be processed by tools aware of QCOW2 format such as for example B<qemu-"
"img>."
msgstr ""
"Remarquez qu'une image QCOW2 créée par B<e2image> est une image QCOW2 "
"normale, qui peut donc être traitée par des outils pouvant manipuler le "
"format QCOW2, tels que B<qemu-img>, par exemple."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "You can convert a .qcow2 image into a raw image with:"
msgstr "Vous pouvez convertir une image .qcow2 en une image brute avec :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "\\\tB<e2image -r hda1.qcow2 hda1.raw>"
msgstr "\\\tB<e2image -r hda1.qcow2 hda1.raw>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This can be useful to write a QCOW2 image containing all data to a sparse "
"image file where it can be loop mounted, or to a disk partition.  Note that "
"this may not work with QCOW2 images not generated by e2image."
msgstr ""
"Il peut être utile d'écrire une image QCOW2 contenant toutes les données sur "
"un fichier image creux qui pourra être monté comme un périphérique boucle "
"(« loop ») ou sur une partition de disque. Veuillez noter que cela peut ne "
"pas fonctionner avec les images QCOW2 créées avec un autre outil que "
"B<e2image>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "OFFSETS"
msgstr "DÉCALAGES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Normally a file system starts at the beginning of a partition, and "
"B<e2image> is run on the partition.  When working with image files, you "
"don't have the option of using the partition device, so you can specify the "
"offset where the file system starts directly with the B<-o> option.  "
"Similarly the B<-O> option specifies the offset that should be seeked to in "
"the destination before writing the file system."
msgstr ""
"Normalement, un système de fichiers démarre au début de la partition, et "
"B<e2image> est exécuté sur la partition. Lorsqu'on travaille avec des "
"fichiers images, il n'est pas possible d'utiliser de partition. Vous pouvez "
"préciser le décalage à partir duquel le système de fichiers commence "
"directement avec l'option B<-o>. De même, l'option B<-O> précise le décalage "
"qui devrait être laissé sur la destination avant d'écrire le système de "
"fichiers."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For example, if you have a B<dd> image of a whole hard drive that contains "
"an ext2 fs in a partition starting at 1 MiB, you can clone that image to a "
"block device with:"
msgstr ""
"Par exemple, si vous avez une image B<dd> sur un disque dur entier qui "
"contient un système de fichiers ext2 dans une partition qui commence à 1 Mo, "
"vous pouvez cloner cette image sur un périphérique en mode bloc avec :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "\\\tB<e2image -aro 1048576 img /dev/sda1>"
msgstr "\\\tB<e2image -aro 1048576 img /dev/sda1>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Or you can clone a file system from a block device into an image file, "
"leaving room in the first MiB for a partition table with:"
msgstr ""
"Ou vous pouvez cloner un système de fichiers d’un périphérique en mode bloc "
"dans un fichier image, en laissant le premier Mo disponible pour une table "
"de partitions avec :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "\\\tB<e2image -arO 1048576 /dev/sda1 img>"
msgstr "\\\tB<e2image -arO 1048576 /dev/sda1 img>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If you specify at least one offset, and only one file, an in-place move will "
"be performed, allowing you to safely move the file system from one offset to "
"another."
msgstr ""
"Si vous précisez au moins un décalage et seulement un fichier, un mouvement "
"in situ sera effectué, ce qui vous permet de déplacer le système de fichiers "
"d'un décalage à l'autre."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<e2image> was written by Theodore Ts'o (tytso@mit.edu)."
msgstr "B<e2image> a été écrit par Theodore Ts'o (tytso@mit.edu)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AVAILABILITY"
msgstr "DISPONIBILITÉ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<e2image> is part of the e2fsprogs package and is available from http://"
"e2fsprogs.sourceforge.net."
msgstr ""
"B<e2image> fait partie du paquet e2fsprogs et est disponible sur http://"
"e2fsprogs.sourceforge.net."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<dumpe2fs>(8), B<debugfs>(8)  B<e2fsck>(8)"
msgstr "B<dumpe2fs>(8), B<debugfs>(8), B<e2fsck>(8)"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "April 2024"
msgstr "Avril 2024"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "E2fsprogs version 1.47.1-rc1"
msgstr "E2fsprogs version 1.47.1-rc1"

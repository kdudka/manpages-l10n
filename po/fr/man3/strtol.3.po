# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012-2014.
# Grégoire Scano <gregoire.scano@malloc.fr>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2024-05-01 15:54+0200\n"
"PO-Revision-Date: 2023-04-22 13:24+0200\n"
"Last-Translator: Grégoire Scano <gregoire.scano@malloc.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "strtol"
msgstr "strtol"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-12-19"
msgstr "19 décembre 2023"

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr "Pages du manuel de Linux 6.7"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "strtol, strtoll, strtoq - convert a string to a long integer"
msgstr "strtol, strtoll, strtoq - Convertir une chaîne en un entier long"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr "B<#include E<lt>stdlib.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<long strtol(const char *restrict >I<nptr>B<,>\n"
"B<            char **restrict >I<endptr>B<, int >I<base>B<);>\n"
"B<long long strtoll(const char *restrict >I<nptr>B<,>\n"
"B<            char **restrict >I<endptr>B<, int >I<base>B<);>\n"
msgstr ""
"B<long strtol(const char *restrict >I<nptr>B<,>\n"
"B<            char **restrict >I<endptr>B<, int >I<base>B<);>\n"
"B<long long strtoll(const char *restrict >I<nptr>B<,>\n"
"B<            char **restrict >I<endptr>B<, int >I<base>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consulter "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<strtoll>():"
msgstr "B<strtoll>() :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"    _ISOC99_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"
msgstr ""
"    _ISOC99_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<strtol>()  function converts the initial part of the string in I<nptr> "
"to a long integer value according to the given I<base>, which must be "
"between 2 and 36 inclusive, or be the special value 0."
msgstr ""
"La fonction B<strtol>() convertit la partie initiale de la chaîne I<nptr> en "
"un entier long en fonction de l'argument I<base>, qui doit être dans "
"l'intervalle B<2> à B<36> (bornes comprises), ou avoir la valeur spéciale "
"B<0>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The string may begin with an arbitrary amount of white space (as determined "
"by B<isspace>(3))  followed by a single optional \\[aq]+\\[aq] or \\[aq]-"
"\\[aq] sign.  If I<base> is zero or 16, the string may then include a \"0x\" "
"or \"0X\" prefix, and the number will be read in base 16; otherwise, a zero "
"I<base> is taken as 10 (decimal) unless the next character is \\[aq]0\\[aq], "
"in which case it is taken as 8 (octal)."
msgstr ""
"La chaîne peut commencer par un nombre quelconque d'espaces (tels que "
"définis par B<isspace>(3)) suivi d'un éventuel signe «\\ +\\ » ou «\\ -\\ ». "
"Si I<base> vaut B<0> ou B<16>, la chaîne peut inclure un préfixe «\\ 0x\\ » "
"ou « 0X » et le nombre sera interprété en base B<16>. Sinon, une I<base> "
"valant zéro est interprétée comme B<10> (base décimale) sauf si le caractère "
"suivant est «\\ 0\\ », auquel cas la base est B<8> (base octale)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The remainder of the string is converted to a I<long> value in the obvious "
"manner, stopping at the first character which is not a valid digit in the "
"given base.  (In bases above 10, the letter \\[aq]A\\[aq] in either "
"uppercase or lowercase represents 10, \\[aq]B\\[aq] represents 11, and so "
"forth, with \\[aq]Z\\[aq] representing 35.)"
msgstr ""
"Le reste de la chaîne est converti en une valeur I<long>, en s'arrêtant au "
"premier caractère qui ne soit pas un chiffre autorisé dans cette base. Dans "
"les bases supérieures à B<10>, la lettre «\\ A\\ » (majuscule ou minuscule) "
"représente B<10>, «\\ B\\ » représente B<11>, et ainsi de suite jusqu'à «\\ "
"Z\\ » représentant B<35>."

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If I<endptr> is not NULL, B<strtol>()  stores the address of the first "
#| "invalid character in I<*endptr>.  If there were no digits at all, "
#| "B<strtol>()  stores the original value of I<nptr> in I<*endptr> (and "
#| "returns 0).  In particular, if I<*nptr> is not \\[aq]\\e0\\[aq] but "
#| "I<**endptr> is \\[aq]\\e0\\[aq] on return, the entire string is valid."
msgid ""
"If I<endptr> is not NULL, and the I<base> is supported, B<strtol>()  stores "
"the address of the first invalid character in I<*endptr>.  If there were no "
"digits at all, B<strtol>()  stores the original value of I<nptr> in "
"I<*endptr> (and returns 0).  In particular, if I<*nptr> is not "
"\\[aq]\\e0\\[aq] but I<**endptr> is \\[aq]\\e0\\[aq] on return, the entire "
"string is valid."
msgstr ""
"Si I<endptr> n'est pas NULL, B<strtol>() stocke l'adresse du premier "
"caractère non valable dans I<*endptr>. S'il n'y avait aucun chiffre, "
"B<strtol>() stocke la valeur originale de I<nptr> dans I<*endptr> (et "
"renvoie 0). En particulier, si I<*nptr> n'est pas «\\ \\e0\\ » et si "
"I<**endptr> vaut «\\ \\e0\\ » en retour, la chaîne entière est valable."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<strtoll>()  function works just like the B<strtol>()  function but "
"returns a I<long long> integer value."
msgstr ""
"La fonction B<strtoll>() fonctionne comme B<strtol>() mais renvoie une "
"valeur entière de type I<long long>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<strtol>()  function returns the result of the conversion, unless the "
"value would underflow or overflow.  If an underflow occurs, B<strtol>()  "
"returns B<LONG_MIN>.  If an overflow occurs, B<strtol>()  returns "
"B<LONG_MAX>.  In both cases, I<errno> is set to B<ERANGE>.  Precisely the "
"same holds for B<strtoll>()  (with B<LLONG_MIN> and B<LLONG_MAX> instead of "
"B<LONG_MIN> and B<LONG_MAX>)."
msgstr ""
"La fonction B<strtol>() renvoie le résultat de la conversion, à moins qu'un "
"débordement supérieur (overflow) ou inférieur (underflow) se produise. Si un "
"dépassement inférieur se produit, B<strtol>() renvoie B<LONG_MIN>. Si un "
"dépassement supérieur se produit, B<strtol>() renvoie B<LONG_MAX>. Dans les "
"deux cas, I<errno> contient le code d'erreur B<ERANGE>. La même chose est "
"vraie pour B<strtoll>() avec B<LLONG_MIN> et B<LLONG_MAX> à la place de "
"B<LONG_MIN> et B<LONG_MAX>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "This function does not modify I<errno> on success."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "(not in C99)  The given I<base> contains an unsupported value."
msgstr "(pas dans C99) La I<base> indiquée n'est pas prise en charge."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ERANGE>"
msgstr "B<ERANGE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The resulting value was out of range."
msgstr "La valeur retournée est hors limites."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The implementation may also set I<errno> to B<EINVAL> in case no conversion "
"was performed (no digits seen, and 0 returned)."
msgstr ""
"L'implémentation peut aussi mettre I<errno> à B<EINVAL> si aucune conversion "
"n'a été réalisée (pas de chiffres trouvés, et B<0> renvoyé)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<strtol>(),\n"
"B<strtoll>(),\n"
"B<strtoq>()"
msgstr ""
"B<strtol>(),\n"
"B<strtoll>(),\n"
"B<strtoq>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe locale"
msgstr "MT-Safe locale"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<strtoll>():"
msgid "B<strtol>()"
msgstr "B<strtoll>() :"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2001, C89, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, C89, SVr4, 4.3BSD."

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<strtoll>():"
msgid "B<strtoll>()"
msgstr "B<strtoll>() :"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2001, C99."
msgstr "POSIX.1-2001, C99."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Since B<strtol>()  can legitimately return 0, B<LONG_MAX>, or B<LONG_MIN> "
#| "(B<LLONG_MAX> or B<LLONG_MIN> for B<strtoll>())  on both success and "
#| "failure, the calling program should set I<errno> to 0 before the call, "
#| "and then determine if an error occurred by checking whether I<errno> has "
#| "a nonzero value after the call."
msgid ""
"Since B<strtol>()  can legitimately return 0, B<LONG_MAX>, or B<LONG_MIN> "
"(B<LLONG_MAX> or B<LLONG_MIN> for B<strtoll>())  on both success and "
"failure, the calling program should set I<errno> to 0 before the call, and "
"then determine if an error occurred by checking whether I<errno == ERANGE> "
"after the call."
msgstr ""
"Comme B<strtol>() peut légitimement renvoyer 0, B<LONG_MAX> ou B<LONG_MIN> "
"(B<LLONG_MAX> ou B<LLONG_MIN> pour B<strtoll>()) à la fois en cas de succès "
"et d'échec, le programme appelant doit positionner I<errno> à 0 avant "
"l'appel, et déterminer si une erreur s'est produite en vérifiant si I<errno> "
"a une valeur non nulle après l'appel."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"According to POSIX.1, in locales other than \"C\" and \"POSIX\", these "
"functions may accept other, implementation-defined numeric strings."
msgstr ""
"POSIX.1 spécifie que dans le cas de locales autres que « C » et « POSIX », "
"ces fonctions peuvent accepter des chaînes numériques propres à "
"l'implémentation."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "BSD also has"
msgstr "BSD a aussi"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<quad_t strtoq(const char *>I<nptr>B<, char **>I<endptr>B<, int >I<base>B<);>\n"
msgstr "B<quad_t strtoq(const char *>I<nptr>B<, char **>I<endptr>B<, int >I<base>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"with completely analogous definition.  Depending on the wordsize of the "
"current architecture, this may be equivalent to B<strtoll>()  or to "
"B<strtol>()."
msgstr ""
"avec une définition exactement analogue. Suivant l'architecture, cela peut "
"être équivalent à B<strtoll>() ou B<strtol>()."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "CAVEATS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"If the I<base> needs to be tested, it should be tested in a call where the "
"string is known to succeed.  Otherwise, it's impossible to portably "
"differentiate the errors."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"errno = 0;\n"
"strtol(\"0\", NULL, base);\n"
"if (errno == EINVAL)\n"
"    goto unsupported_base;\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The program shown below demonstrates the use of B<strtol>().  The first "
"command-line argument specifies a string from which B<strtol>()  should "
"parse a number.  The second (optional) argument specifies the base to be "
"used for the conversion.  (This argument is converted to numeric form using "
"B<atoi>(3), a function that performs no error checking and has a simpler "
"interface than B<strtol>().)  Some examples of the results produced by this "
"program are the following:"
msgstr ""
"Le programme suivant montre l'utilisation de B<strtol>(). Le premier "
"argument de la ligne de commande spécifie une chaîne dans laquelle "
"B<strtol>() analysera un nombre. Le second argument, optionnel, spécifie la "
"base à utiliser pour la conversion. (Cet argument est converti sous forme "
"numérique avec B<atoi>(3), une fonction qui n'effectue aucune vérification "
"d'erreur et qui a une interface plus simple que B<strtol>()). Voici "
"plusieurs exemples de résultats produits par ce programme\\ :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"$B< ./a.out 123>\n"
"strtol() returned 123\n"
"$B< ./a.out \\[aq]    123\\[aq]>\n"
"strtol() returned 123\n"
"$B< ./a.out 123abc>\n"
"strtol() returned 123\n"
"Further characters after number: \"abc\"\n"
"$B< ./a.out 123abc 55>\n"
"strtol: Invalid argument\n"
"$B< ./a.out \\[aq]\\[aq]>\n"
"No digits were found\n"
"$B< ./a.out 4000000000>\n"
"strtol: Numerical result out of range\n"
msgstr ""
"$B< ./a.out 123>\n"
"strtol() a renvoyé 123\n"
"$B< ./a.out \\[aq]    123\\[aq]>\n"
"strtol() a renvoyé 123\n"
"$B< ./a.out 123abc>\n"
"strtol() a renvoyé 123\n"
"Caractères trouvés après le nombre : abc\n"
"$B< ./a.out 123abc 55>\n"
"strtol: Argument non valable\n"
"$B< ./a.out \\[aq]\\[aq]>\n"
"Pas de chiffre trouvé\n"
"$B< ./a.out 4000000000>\n"
"strtol: Résultat numérique hors intervalle\n"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr "Source du programme"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int base;\n"
"    char *endptr, *str;\n"
"    long val;\n"
"\\&\n"
"    if (argc E<lt> 2) {\n"
"        fprintf(stderr, \"Usage: %s str [base]\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    str = argv[1];\n"
"    base = (argc E<gt> 2) ? atoi(argv[2]) : 0;\n"
"\\&\n"
"    errno = 0;    /* To distinguish success/failure after call */\n"
"    strtol(\"0\", NULL, base);\n"
"    if (errno == EINVAL) {\n"
"        perror(\"strtol\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    errno = 0;    /* To distinguish success/failure after call */\n"
"    val = strtol(str, &endptr, base);\n"
"\\&\n"
"    /* Check for various possible errors. */\n"
"\\&\n"
"    if (errno == ERANGE) {\n"
"        perror(\"strtol\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    if (endptr == str) {\n"
"        fprintf(stderr, \"No digits were found\\en\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    /* If we got here, strtol() successfully parsed a number. */\n"
"\\&\n"
"    printf(\"strtol() returned %ld\\en\", val);\n"
"\\&\n"
"    if (*endptr != \\[aq]\\e0\\[aq])        /* Not necessarily an error... */\n"
"        printf(\"Further characters after number: \\e\"%s\\e\"\\en\", endptr);\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<atof>(3), B<atoi>(3), B<atol>(3), B<strtod>(3), B<strtoimax>(3), "
"B<strtoul>(3)"
msgstr ""
"B<atof>(3), B<atoi>(3), B<atol>(3), B<strtod>(3), B<strtoimax>(3), "
"B<strtoul>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"If I<endptr> is not NULL, B<strtol>()  stores the address of the first "
"invalid character in I<*endptr>.  If there were no digits at all, "
"B<strtol>()  stores the original value of I<nptr> in I<*endptr> (and returns "
"0).  In particular, if I<*nptr> is not \\[aq]\\e0\\[aq] but I<**endptr> is "
"\\[aq]\\e0\\[aq] on return, the entire string is valid."
msgstr ""
"Si I<endptr> n'est pas NULL, B<strtol>() stocke l'adresse du premier "
"caractère non valable dans I<*endptr>. S'il n'y avait aucun chiffre, "
"B<strtol>() stocke la valeur originale de I<nptr> dans I<*endptr> (et "
"renvoie 0). En particulier, si I<*nptr> n'est pas «\\ \\e0\\ » et si "
"I<**endptr> vaut «\\ \\e0\\ » en retour, la chaîne entière est valable."

#. type: Plain text
#: debian-bookworm
msgid "B<strtol>(): POSIX.1-2001, POSIX.1-2008, C99, SVr4, 4.3BSD."
msgstr "B<strtol>() : POSIX.1-2001, POSIX.1-2008, C99 SVr4, 4.3BSD."

#. type: Plain text
#: debian-bookworm
msgid "B<strtoll>(): POSIX.1-2001, POSIX.1-2008, C99."
msgstr "B<strtoll>() : POSIX.1-2001, POSIX.1-2008, C99."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Since B<strtol>()  can legitimately return 0, B<LONG_MAX>, or B<LONG_MIN> "
"(B<LLONG_MAX> or B<LLONG_MIN> for B<strtoll>())  on both success and "
"failure, the calling program should set I<errno> to 0 before the call, and "
"then determine if an error occurred by checking whether I<errno> has a "
"nonzero value after the call."
msgstr ""
"Comme B<strtol>() peut légitimement renvoyer 0, B<LONG_MAX> ou B<LONG_MIN> "
"(B<LLONG_MAX> ou B<LLONG_MIN> pour B<strtoll>()) à la fois en cas de succès "
"et d'échec, le programme appelant doit positionner I<errno> à 0 avant "
"l'appel, et déterminer si une erreur s'est produite en vérifiant si I<errno> "
"a une valeur non nulle après l'appel."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>limits.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
msgstr ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>limits.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int base;\n"
"    char *endptr, *str;\n"
"    long val;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int base;\n"
"    char *endptr, *str;\n"
"    long val;\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    if (argc E<lt> 2) {\n"
"        fprintf(stderr, \"Usage: %s str [base]\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (argc E<lt> 2) {\n"
"        fprintf(stderr, \"Usage : %s str [base]\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    str = argv[1];\n"
"    base = (argc E<gt> 2) ? atoi(argv[2]) : 0;\n"
msgstr ""
"    str = argv[1];\n"
"    base = (argc E<gt> 2) ? atoi(argv[2]) : 0;\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    errno = 0;    /* To distinguish success/failure after call */\n"
"    val = strtol(str, &endptr, base);\n"
msgstr ""
"    errno = 0;    /* Pour distinguer la réussite/échec après l'appel */\n"
"    val = strtol(str, &endptr, base);\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "    /* Check for various possible errors. */\n"
msgstr "    /* Vérification de certaines erreurs possibles. */\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    if (errno != 0) {\n"
"        perror(\"strtol\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (errno != 0) {\n"
"        perror(\"strtol\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    if (endptr == str) {\n"
"        fprintf(stderr, \"No digits were found\\en\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (endptr == str) {\n"
"        fprintf(stderr, \"Pas de chiffre trouvé\\en\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "    /* If we got here, strtol() successfully parsed a number. */\n"
msgstr "    /* Si nous sommes ici, strtol() a analysé un nombre avec succès. */\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "    printf(\"strtol() returned %ld\\en\", val);\n"
msgstr "    printf(\"strtol() a renvoyé %ld\\en\", val);\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    if (*endptr != \\[aq]\\e0\\[aq])        /* Not necessarily an error... */\n"
"        printf(\"Further characters after number: \\e\"%s\\e\"\\en\", endptr);\n"
msgstr ""
"    if (*endptr != \\[aq]\\e0\\[aq])        /* Pas nécessairement une erreur... */\n"
"        printf(\"Caractères trouvés après le nombre : \\e\"%s\\e\"\\en\", endptr);\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pages du manuel de Linux 6.04"

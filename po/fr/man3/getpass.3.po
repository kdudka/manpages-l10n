# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013.
# Denis Barbier <barbier@debian.org>, 2006, 2010, 2011.
# David Prévot <david@tilapin.org>, 2010, 2012-2014.
# Lucien Gentis <lucien.gentis@waika9.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2024-05-01 15:40+0200\n"
"PO-Revision-Date: 2023-11-03 13:19+0100\n"
"Last-Translator: Lucien Gentis <lucien.gentis@waika9.com>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: vim-gtk3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "getpass"
msgstr "getpass"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr "Pages du manuel de Linux 6.7"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "getpass - get a password"
msgstr "getpass - Obtenir un mot de passe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<[[deprecated]] char *getpass(const char *>I<prompt>B<);>\n"
msgstr "B<[[obsolète]] char *getpass(const char *>I<invite>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consulter "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<getpass>():"
msgstr "B<getpass>() :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.2.2:\n"
"        _XOPEN_SOURCE && ! (_POSIX_C_SOURCE E<gt>= 200112L)\n"
"            || /* glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"            || /* glibc E<lt>= 2.19: */ _BSD_SOURCE\n"
"    Before glibc 2.2.2:\n"
"        none\n"
msgstr ""
"    Depuis la glibc 2.2.2 :\n"
"        _XOPEN_SOURCE && ! (_POSIX_C_SOURCE E<gt>= 200112L)\n"
"            || /* Depuis la glibc 2.19 : */ _DEFAULT_SOURCE\n"
"            || /* glibc 2.19 et précédentes : */ _BSD_SOURCE\n"
"    Avant la glibc 2.2.2 :\n"
"        aucune\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This function is obsolete.  Do not use it.  See NOTES.  If you want to read "
"input without terminal echoing enabled, see the description of the I<ECHO> "
"flag in B<termios>(3)."
msgstr ""
"Cette fonction est obsolète. Ne l'utilisez pas. Voir les NOTES. Si vous "
"voulez lire une saisie de terminal sans écho local, consultez la description "
"du drapeau I<ECHO> dans B<termios>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<getpass>()  function opens I</dev/tty> (the controlling terminal of "
"the process), outputs the string I<prompt>, turns off echoing, reads one "
"line (the \"password\"), restores the terminal state and closes I</dev/tty> "
"again."
msgstr ""
"La fonction B<getpass>() ouvre I</dev/tty> (le terminal de contrôle du "
"processus), affiche l'I<invite>, désactive l'écho local, lit une ligne de "
"saisie (le mot de passe), puis restaure l'état du terminal et referme I</dev/"
"tty>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The function B<getpass>()  returns a pointer to a static buffer containing "
"(the first B<PASS_MAX> bytes of) the password without the trailing newline, "
"terminated by a null byte (\\[aq]\\e0\\[aq]).  This buffer may be "
"overwritten by a following call.  On error, the terminal state is restored, "
"I<errno> is set to indicate the error, and NULL is returned."
msgstr ""
"La fonction B<getpass>() renvoie un pointeur sur un tampon alloué "
"statiquement contenant les B<PASS_MAX> premiers caractères du mot de passe "
"sans le retour chariot final et terminé par un caractère NULL (« \\e0 »). Ce "
"tampon peut être écrasé par un autre appel. En cas d'erreur, l'état du "
"terminal est restauré, I<errno> est renseignée et la fonction renvoie NULL."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENXIO>"
msgstr "B<ENXIO>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The process does not have a controlling terminal."
msgstr "Le processus n'a pas de terminal de contrôle."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FICHIERS"

#. #-#-#-#-#  archlinux: getpass.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: getpass.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH HISTORY
#.  A
#.  .BR getpass ()
#.  function appeared in Version 7 AT&T UNIX.
#. type: Plain text
#. #-#-#-#-#  debian-unstable: getpass.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-40: getpass.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: getpass.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: getpass.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-6: getpass.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: getpass.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I</dev/tty>"
msgstr "I</dev/tty>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<getpass>()"
msgstr "B<getpass>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe term"
msgstr "MT-Unsafe term"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "None."
msgstr "Aucun."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Version 7 AT&T UNIX.  Present in SUSv2, but marked LEGACY.  Removed in "
"POSIX.1-2001."
msgstr ""
"Version 7 AT&T UNIX. Présente dans SUSv2, mais marquée «\\ LEGACY\\ ». "
"Supprimée de POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#.  For libc4 and libc5, the prompt is not written to
#.  .I /dev/tty
#.  but to
#.  .IR stderr .
#.  Moreover, if
#.  .I /dev/tty
#.  cannot be opened, the password is read from
#.  .IR stdin .
#.  The static buffer has length 128 so that only the first 127
#.  bytes of the password are returned.
#.  While reading the password, signal generation
#.  .RB ( SIGINT ,
#.  .BR SIGQUIT ,
#.  .BR SIGSTOP ,
#.  .BR SIGTSTP )
#.  is disabled and the corresponding characters
#.  (usually control-C, control-\e, control-Z and control-Y)
#.  are transmitted as part of the password.
#.  Since libc 5.4.19 also line editing is disabled, so that also
#.  backspace and the like will be seen as part of the password.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "You should use instead B<readpassphrase>(3bsd), provided by I<libbsd>."
msgstr ""
"Vous devriez plutôt utiliser la fonction B<readpassphrase>(3bsd) fournie par "
"la bibliothèque I<libbsd>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In the GNU C library implementation, if I</dev/tty> cannot be opened, the "
"prompt is written to I<stderr> and the password is read from I<stdin>.  "
"There is no limit on the length of the password.  Line editing is not "
"disabled."
msgstr ""
"Dans l'implémentation de la bibliothèque GNU C, si I</dev/tty> ne peut pas "
"être ouvert, l'invite est envoyée sur I<stderr> et le mot de passe est lu "
"depuis I<stdin>. Il n'y a pas de limite à la longueur du mot de passe. "
"L'édition de ligne n'est pas désactivée."

#.  Libc4 and libc5 have never supported
#.  .B PASS_MAX
#.  or
#.  .BR _SC_PASS_MAX .
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"According to SUSv2, the value of B<PASS_MAX> must be defined in "
"I<E<lt>limits.hE<gt>> in case it is smaller than 8, and can in any case be "
"obtained using I<sysconf(_SC_PASS_MAX)>.  However, POSIX.2 withdraws the "
"constants B<PASS_MAX> and B<_SC_PASS_MAX>, and the function B<getpass>().  "
"The glibc version accepts B<_SC_PASS_MAX> and returns B<BUFSIZ> (e.g., 8192)."
msgstr ""
"D'après SUSv2, la valeur de B<PASS_MAX> doit être définie dans I<E<lt>limits."
"hE<gt>> dans le cas où elle est inférieure à B<8>, et peut toujours être "
"accessible en utilisant I<sysconf(_SC_PASS_MAX)>. Quoiqu'il en soit, POSIX.2 "
"retire les constantes B<PASS_MAX> et B<_SC_PASS_MAX>, ainsi que la fonction "
"B<getpass>(). La version de la glibc accepte B<_SC_PASS_MAX> et renvoie "
"B<BUFSIZ> (par exemple, 8192)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The calling process should zero the password as soon as possible to avoid "
"leaving the cleartext password visible in the process's address space."
msgstr ""
"Le processus appelant doit effacer le mot de passe saisi aussi vite que "
"possible, afin d'éviter d'en conserver une copie en texte clair dans "
"l'espace d'adressage du processus."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<crypt>(3)"
msgstr "B<crypt>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm
msgid "Present in SUSv2, but marked LEGACY.  Removed in POSIX.1-2001."
msgstr ""
"Présente dans SUSv2, mais marquée «\\ LEGACY\\ ». Supprimée de POSIX.1-2001."

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pages du manuel de Linux 6.04"

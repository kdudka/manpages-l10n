# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012.
# Frédéric Hantrais <fhantrais@gmail.com>, 2013, 2014.
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2023-2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.22.0\n"
"POT-Creation-Date: 2024-05-01 15:49+0200\n"
"PO-Revision-Date: 2024-04-22 12:19+0200\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "pthread_sigmask"
msgstr "pthread_sigmask"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr "Pages du manuel de Linux 6.7"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "pthread_sigmask - examine and change mask of blocked signals"
msgstr "pthread_sigmask - Examiner et modifier le masque des signaux bloqués"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX threads library (I<libpthread>, I<-lpthread>)"
msgstr "Bibliothèque de threads POSIX (I<libpthread>, I<-lpthread>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>signal.hE<gt>>\n"
msgstr "B<#include E<lt>signal.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int pthread_sigmask(int >I<how>B<, const sigset_t *>I<set>B<, sigset_t *>I<oldset>B<);>\n"
msgstr "B<int pthread_sigmask(int >I<how>B<, const sigset_t *>I<set>B<, sigset_t *>I<oldset>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consulter "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<pthread_sigmask>():"
msgstr "B<pthread_sigmask>() :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "    _POSIX_C_SOURCE E<gt>= 199506L || _XOPEN_SOURCE E<gt>= 500\n"
msgstr "    _POSIX_C_SOURCE E<gt>= 199506L || _XOPEN_SOURCE E<gt>= 500\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<pthread_sigmask>()  function is just like B<sigprocmask>(2), with the "
"difference that its use in multithreaded programs is explicitly specified by "
"POSIX.1.  Other differences are noted in this page."
msgstr ""
"La fonction B<pthread_sigmask>() est identique à B<sigprocmask>(2), à la "
"différence près que son utilisation dans des programmes multithread est "
"explicitement spécifié dans POSIX.1. D'autres différences sont indiquées "
"dans cette page."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For a description of the arguments and operation of this function, see "
"B<sigprocmask>(2)."
msgstr ""
"Pour une description des arguments et du mode d'opération de cette fonction, "
"consultez B<sigprocmask>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On success, B<pthread_sigmask>()  returns 0; on error, it returns an error "
"number."
msgstr ""
"En cas de réussite, B<pthread_sigmask>() renvoie 0 ; en cas d'erreur, elle "
"renvoie un numéro d'erreur."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "See B<sigprocmask>(2)."
msgstr "Consultez B<sigprocmask>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<pthread_sigmask>()"
msgstr "B<pthread_sigmask>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr "POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "A new thread inherits a copy of its creator's signal mask."
msgstr ""
"Un nouveau thread hérite d'une copie du masque de signaux de son créateur."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The glibc B<pthread_sigmask>()  function silently ignores attempts to block "
"the two real-time signals that are used internally by the NPTL threading "
"implementation.  See B<nptl>(7)  for details."
msgstr ""
"La fonction B<pthread_sigmask>() de la glibc ignore silencieusement les "
"tentatives de blocage des deux signaux en temps réel qui sont utilisés en "
"interne par l'implémentation de threading de NPTL. Consultez B<nptl>(7) pour "
"des détails."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The program below blocks some signals in the main thread, and then creates a "
"dedicated thread to fetch those signals via B<sigwait>(3).  The following "
"shell session demonstrates its use:"
msgstr ""
"Le programme ci-dessous bloque certains signaux dans le thread principal, "
"puis crée un thread dédié pour récupérer ces signaux avec B<sigwait>(3). La "
"session d'interpréteur de commande ci-dessous démontre l'utilisation du "
"programme."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"$B< ./a.out &>\n"
"[1] 5423\n"
"$B< kill -QUIT %1>\n"
"Signal handling thread got signal 3\n"
"$B< kill -USR1 %1>\n"
"Signal handling thread got signal 10\n"
"$B< kill -TERM %1>\n"
"[1]+  Terminated              ./a.out\n"
msgstr ""
"$B< ./a.out &>\n"
"[1] 5423\n"
"$B< kill -QUIT %1>\n"
"Signal handling thread got signal 3\n"
"$B< kill -USR1 %1>\n"
"Signal handling thread got signal 10\n"
"$B< kill -TERM %1>\n"
"[1]+  Terminated              ./a.out\n"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr "Source du programme"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>signal.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"/* Simple error handling functions */\n"
"\\&\n"
"#define handle_error_en(en, msg) \\e\n"
"        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)\n"
"\\&\n"
"static void *\n"
"sig_thread(void *arg)\n"
"{\n"
"    sigset_t *set = arg;\n"
"    int s, sig;\n"
"\\&\n"
"    for (;;) {\n"
"        s = sigwait(set, &sig);\n"
"        if (s != 0)\n"
"            handle_error_en(s, \"sigwait\");\n"
"        printf(\"Signal handling thread got signal %d\\en\", sig);\n"
"    }\n"
"}\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    pthread_t thread;\n"
"    sigset_t set;\n"
"    int s;\n"
"\\&\n"
"    /* Block SIGQUIT and SIGUSR1; other threads created by main()\n"
"       will inherit a copy of the signal mask. */\n"
"\\&\n"
"    sigemptyset(&set);\n"
"    sigaddset(&set, SIGQUIT);\n"
"    sigaddset(&set, SIGUSR1);\n"
"    s = pthread_sigmask(SIG_BLOCK, &set, NULL);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_sigmask\");\n"
"\\&\n"
"    s = pthread_create(&thread, NULL, &sig_thread, &set);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_create\");\n"
"\\&\n"
"    /* Main thread carries on to create other threads and/or do\n"
"       other work. */\n"
"\\&\n"
"    pause();            /* Dummy pause so we can test program */\n"
"}\n"
msgstr ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>signal.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"/* Simple error handling functions */\n"
"\\&\n"
"#define handle_error_en(en, msg) \\e\n"
"        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)\n"
"\\&\n"
"static void *\n"
"sig_thread(void *arg)\n"
"{\n"
"    sigset_t *set = arg;\n"
"    int s, sig;\n"
"\\&\n"
"    for (;;) {\n"
"        s = sigwait(set, &sig);\n"
"        if (s != 0)\n"
"            handle_error_en(s, \"sigwait\");\n"
"        printf(\"Signal handling thread got signal %d\\en\", sig);\n"
"    }\n"
"}\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    pthread_t thread;\n"
"    sigset_t set;\n"
"    int s;\n"
"\\&\n"
"    /* Block SIGQUIT and SIGUSR1; other threads created by main()\n"
"       will inherit a copy of the signal mask. */\n"
"\\&\n"
"    sigemptyset(&set);\n"
"    sigaddset(&set, SIGQUIT);\n"
"    sigaddset(&set, SIGUSR1);\n"
"    s = pthread_sigmask(SIG_BLOCK, &set, NULL);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_sigmask\");\n"
"\\&\n"
"    s = pthread_create(&thread, NULL, &sig_thread, &set);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_create\");\n"
"\\&\n"
"    /* Main thread carries on to create other threads and/or do\n"
"       other work. */\n"
"\\&\n"
"    pause();            /* Dummy pause so we can test program */\n"
"}\n"

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<sigaction>(2), B<sigpending>(2), B<sigprocmask>(2), "
"B<pthread_attr_setsigmask_np>(3), B<pthread_create>(3), B<pthread_kill>(3), "
"B<sigsetops>(3), B<pthreads>(7), B<signal>(7)"
msgstr ""
"B<sigaction>(2), B<sigpending>(2), B<sigprocmask>(2), "
"B<pthread_attr_setsigmask_np>(3), B<pthread_create>(3), B<pthread_kill>(3), "
"B<sigsetops>(3), B<pthreads>(7), B<signal>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr "15 décembre 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>signal.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>signal.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "/* Simple error handling functions */\n"
msgstr "/* Simple error handling functions */\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"#define handle_error_en(en, msg) \\e\n"
"        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)\n"
msgstr ""
"#define handle_error_en(en, msg) \\e\n"
"        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"static void *\n"
"sig_thread(void *arg)\n"
"{\n"
"    sigset_t *set = arg;\n"
"    int s, sig;\n"
msgstr ""
"static void *\n"
"sig_thread(void *arg)\n"
"{\n"
"    sigset_t *set = arg;\n"
"    int s, sig;\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    for (;;) {\n"
"        s = sigwait(set, &sig);\n"
"        if (s != 0)\n"
"            handle_error_en(s, \"sigwait\");\n"
"        printf(\"Signal handling thread got signal %d\\en\", sig);\n"
"    }\n"
"}\n"
msgstr ""
"    for (;;) {\n"
"        s = sigwait(set, &sig);\n"
"        if (s != 0)\n"
"            handle_error_en(s, \"sigwait\");\n"
"        printf(\"Signal handling thread got signal %d\\en\", sig);\n"
"    }\n"
"}\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    pthread_t thread;\n"
"    sigset_t set;\n"
"    int s;\n"
msgstr ""
"int\n"
"main(void)\n"
"{\n"
"    pthread_t thread;\n"
"    sigset_t set;\n"
"    int s;\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    /* Block SIGQUIT and SIGUSR1; other threads created by main()\n"
"       will inherit a copy of the signal mask. */\n"
msgstr ""
"    /* Block SIGQUIT and SIGUSR1; other threads created by main()\n"
"       will inherit a copy of the signal mask. */\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    sigemptyset(&set);\n"
"    sigaddset(&set, SIGQUIT);\n"
"    sigaddset(&set, SIGUSR1);\n"
"    s = pthread_sigmask(SIG_BLOCK, &set, NULL);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_sigmask\");\n"
msgstr ""
"    sigemptyset(&set);\n"
"    sigaddset(&set, SIGQUIT);\n"
"    sigaddset(&set, SIGUSR1);\n"
"    s = pthread_sigmask(SIG_BLOCK, &set, NULL);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_sigmask\");\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    s = pthread_create(&thread, NULL, &sig_thread, &set);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_create\");\n"
msgstr ""
"    s = pthread_create(&thread, NULL, &sig_thread, &set);\n"
"    if (s != 0)\n"
"        handle_error_en(s, \"pthread_create\");\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    /* Main thread carries on to create other threads and/or do\n"
"       other work. */\n"
msgstr ""
"    /* Main thread carries on to create other threads and/or do\n"
"       other work. */\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    pause();            /* Dummy pause so we can test program */\n"
"}\n"
msgstr ""
"    pause();            /* Dummy pause so we can test program */\n"
"}\n"

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pages du manuel de Linux 6.04"

# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010, 2012.
# David Prévot <david@tilapin.org>, 2010, 2012.
# Cédric Boutillier <cedric.boutillier@gmail.com>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2024-05-01 15:38+0200\n"
"PO-Revision-Date: 2023-03-15 00:19+0100\n"
"Last-Translator: Weblate Admin <jean-baptiste@holcroft.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 22.12.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "erfc"
msgstr "erfc"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr "Pages du manuel de Linux 6.7"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "erfc, erfcf, erfcl - complementary error function"
msgstr "erfc, erfcf, erfcl - Fonctions d'erreur complémentaires"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Math library (I<libm>, I<-lm>)"
msgstr "Bibliothèque de math (I<libm>, I<-lm>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr "B<#include E<lt>math.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<double erfc(double >I<x>B<);>\n"
"B<float erfcf(float >I<x>B<);>\n"
"B<long double erfcl(long double >I<x>B<);>\n"
msgstr ""
"B<double erfc(double >I<x>B<);>\n"
"B<float erfcf(float >I<x>B<);>\n"
"B<long double erfcl(long double >I<x>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consulter "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<erfc>():"
msgstr "B<erfc>() :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L || _XOPEN_SOURCE\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L || _XOPEN_SOURCE\n"
"        || /* Depuis la glibc 2.19 : */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19 : */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<erfcf>(), B<erfcl>():"
msgstr "B<erfcf>(), B<erfcl>() :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"        || /* Depuis la glibc 2.19 : */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19 : */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"These functions return the complementary error function of I<x>, that is, "
"1.0 - erf(x)."
msgstr ""
"Ces fonctions renvoient la fonction d'erreur complémentaire de I<x>, qui "
"vaut 1,0 - erf(x)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On success, these functions return the complementary error function of I<x>, "
"a value in the range [0,2]."
msgstr ""
"En cas de réussite, ces fonctions renvoient la fonction d'erreur "
"complémentaire de I<x>, une valeur comprise dans l'intervalle [0,2]."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If I<x> is a NaN, a NaN is returned."
msgstr "Si I<x> n’est pas un nombre, un B<NaN> est renvoyé."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If I<x> is +0 or -0, 1 is returned."
msgstr "Si I<x> vaut +0 ou -0, la valeur renvoyée est B<1>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If I<x> is positive infinity, +0 is returned."
msgstr "Si I<x> est une valeur infinie, la valeur renvoyée est +0."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If I<x> is negative infinity, +2 is returned."
msgstr "Si I<x> est une valeur infinie négative, la valeur renvoyée est +2."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the function result underflows and produces an unrepresentable value, the "
"return value is 0.0."
msgstr ""
"En cas de soupassement (« underflow ») du résultat de la fonction qui "
"produit une valeur non représentable, la valeur de retour est 0,0."

#.  e.g., erfc(27) on x86-32
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the function result underflows but produces a representable (i.e., "
"subnormal) value, that value is returned, and a range error occurs."
msgstr ""
"Si le résultat de la fonction est en soupassement mais produit une valeur "
"représentable (par exemple, dénormalisée), cette valeur est renvoyée et une "
"erreur d'intervalle est déclenchée. "

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"See B<math_error>(7)  for information on how to determine whether an error "
"has occurred when calling these functions."
msgstr ""
"Voir B<math_error>(7) pour savoir comment déterminer si une erreur s'est "
"produite lors de l'appel d'une de ces fonctions."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The following errors can occur:"
msgstr "Les erreurs suivantes peuvent se produire :"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Range error: result underflow (result is subnormal)"
msgstr "Erreur d'intervalle\\ : résultat en soupassement (le résultat est dénormalisé)."

#.  .I errno
#.  is set to
#.  .BR ERANGE .
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "An underflow floating-point exception (B<FE_UNDERFLOW>)  is raised."
msgstr ""
"Une exception en virgule flottante de dépassement par le bas "
"(B<FE_UNDERFLOW>) est levée."

#. #-#-#-#-#  archlinux: erfc.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  It is intentional that these functions do not set errno for this case
#.  see https://www.sourceware.org/bugzilla/show_bug.cgi?id=6785
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: erfc.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  It is intentional that these functions do not set errno for this case
#.  see http://sources.redhat.com/bugzilla/show_bug.cgi?id=6785
#. type: Plain text
#. #-#-#-#-#  debian-unstable: erfc.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  It is intentional that these functions do not set errno for this case
#.  see https://www.sourceware.org/bugzilla/show_bug.cgi?id=6785
#. type: Plain text
#. #-#-#-#-#  fedora-40: erfc.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  It is intentional that these functions do not set errno for this case
#.  see https://www.sourceware.org/bugzilla/show_bug.cgi?id=6785
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: erfc.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  It is intentional that these functions do not set errno for this case
#.  see https://www.sourceware.org/bugzilla/show_bug.cgi?id=6785
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: erfc.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  It is intentional that these functions do not set errno for this case
#.  see https://www.sourceware.org/bugzilla/show_bug.cgi?id=6785
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-6: erfc.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  It is intentional that these functions do not set errno for this case
#.  see https://www.sourceware.org/bugzilla/show_bug.cgi?id=6785
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: erfc.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  It is intentional that these functions do not set errno for this case
#.  see https://www.sourceware.org/bugzilla/show_bug.cgi?id=6785
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "These functions do not set I<errno>."
msgstr "Ces fonctions n'affectent pas de valeur à I<errno>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<erfc>(),\n"
"B<erfcf>(),\n"
"B<erfcl>()"
msgstr ""
"B<erfc>(),\n"
"B<erfcf>(),\n"
"B<erfcl>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "C99, POSIX.1-2001."
msgstr "C99, POSIX.1-2001."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The variant returning I<double> also conforms to SVr4, 4.3BSD."
msgstr "La variante renvoyant I<double> est également conforme à SVr4, 4.3BSD."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<erfc>(), B<erfcf>(), and B<erfcl>()  functions are provided to avoid "
"the loss accuracy that would occur for the calculation 1-erf(x) for large "
"values of I<x> (for which the value of erf(x) approaches 1)."
msgstr ""
"Les fonctions B<erfc>(), B<erfcf>() et B<erfcl>() sont fournies pour éviter "
"la perte de précision qui se produirait avec le calcul de 1-erf(x) pour des "
"valeurs de I<x> élevées (pour lesquelles la valeur de erf(x) est proche de "
"1)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<cerf>(3), B<erf>(3), B<exp>(3)"
msgstr "B<cerf>(3), B<erf>(3), B<exp>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm
msgid "C99, POSIX.1-2001, POSIX.1-2008."
msgstr "C99, POSIX.1-2001, POSIX.1-2008."

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pages du manuel de Linux 6.04"

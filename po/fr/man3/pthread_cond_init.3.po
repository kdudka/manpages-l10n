# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Gérard Delafond <gerard@delafond.org>
# Christophe Blaess <ccb@club-internet.fr>, 1999, 2001, 2003, 2005, 2006.
# Thierry Vignaud <tvignaud@mandriva.com>, 2000, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003.
# Denis Barbier <barbier@debian.org>, 2004.
# Nicolas François <nicolas.francois@centraliens.net>, 2007, 2009.
# Florentin Duneau <fduneau@gmail.com>, 2006, 2007, 2008, 2009.
# Thomas Blein <tblein@tblein.eu>, 2011, 2014.
# David Prévot <david@tilapin.org>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: glibc\n"
"POT-Creation-Date: 2024-05-01 15:49+0200\n"
"PO-Revision-Date: 2024-04-24 18:12+0200\n"
"Last-Translator: Thomas Blein <tblein@tblein.eu>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.3\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "pthread_cond_init"
msgstr "pthread_cond_init"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2024-02-26"
msgstr "26 février 2024"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr "Pages du manuel de Linux 6.7"

#. type: SH
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"pthread_cond_init, pthread_cond_signal, pthread_cond_broadcast, "
"pthread_cond_wait, pthread_cond_timedwait, pthread_cond_destroy - operations "
"on conditions"
msgstr ""
"pthread_cond_init, pthread_cond_signal, pthread_cond_broadcast, "
"pthread_cond_wait, pthread_cond_timedwait, pthread_cond_destroy - Opérations "
"sur les conditions"

#. type: SH
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<#include E<lt>pthread.hE<gt>>"
msgstr "B<#include E<lt>pthread.hE<gt>>"

#. type: Plain text
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<pthread_cond_t >I<cond>B< = PTHREAD_COND_INITIALIZER;>"
msgstr "B<pthread_cond_t >I<cond>B< = PTHREAD_COND_INITIALIZER;>"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"B<int pthread_cond_init(pthread_cond_t *>I<cond>B<, pthread_condattr_t "
"*>I<cond_attr>B<);> B<int pthread_cond_signal(pthread_cond_t *>I<cond>B<);> "
"B<int pthread_cond_broadcast(pthread_cond_t *>I<cond>B<);> B<int "
"pthread_cond_wait(pthread_cond_t *>I<cond>B<, pthread_mutex_t *>I<mutex>B<);"
"> B<int pthread_cond_timedwait(pthread_cond_t *>I<cond>B<, pthread_mutex_t "
"*>I<mutex>B<, const struct timespec *>I<abstime>B<);> B<int "
"pthread_cond_destroy(pthread_cond_t *>I<cond>B<);>"
msgstr ""
"B<int pthread_cond_init(pthread_cond_t *>I<cond>B<, pthread_condattr_t "
"*>I<cond_attr>B<);> B<int pthread_cond_signal(pthread_cond_t *>I<cond>B<);> "
"B<int pthread_cond_broadcast(pthread_cond_t *>I<cond>B<);> B<int "
"pthread_cond_wait(pthread_cond_t *>I<cond>B<, pthread_mutex_t *>I<mutex>B<);"
"> B<int pthread_cond_timedwait(pthread_cond_t *>I<cond>B<, pthread_mutex_t "
"*>I<mutex>B<, const struct timespec *>I<abstime>B<);> B<int "
"pthread_cond_destroy(pthread_cond_t *>I<cond>B<);>"

#. type: SH
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"A condition (short for ``condition variable'')  is a synchronization device "
"that allows threads to suspend execution and relinquish the processors until "
"some predicate on shared data is satisfied.  The basic operations on "
"conditions are: signal the condition (when the predicate becomes true), and "
"wait for the condition, suspending the thread execution until another thread "
"signals the condition."
msgstr ""
"Une condition (abréviation pour «\\ variable condition\\ ») est un mécanisme "
"de synchronisation permettant à un thread de suspendre son exécution jusqu'à "
"ce qu'une certaine condition (un prédicat) sur des données partagées soit "
"vérifiée. Les opérations fondamentales sur les conditions sont\\ : signaler "
"la condition (quand le prédicat devient vrai) et attendre la condition en "
"suspendant l'exécution du thread jusqu'à ce qu'un autre thread signale la "
"condition."

#. type: Plain text
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"A condition variable must always be associated with a mutex, to avoid the "
"race condition where a thread prepares to wait on a condition variable and "
"another thread signals the condition just before the first thread actually "
"waits on it."
msgstr ""
"Une variable condition doit toujours être associée à un mutex, pour éviter "
"une condition concurrente où un thread se prépare à attendre une condition "
"et un autre thread signale la condition juste avant que le premier n'attende "
"réellement."

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"B<pthread_cond_init> initializes the condition variable I<cond>, using the "
"condition attributes specified in I<cond_attr>, or default attributes if "
"I<cond_attr> is B<NULL>.  The LinuxThreads implementation supports no "
"attributes for conditions, hence the I<cond_attr> parameter is actually "
"ignored."
msgstr ""
"B<pthread_cond_init>() initialise la variable condition I<cond>, en "
"utilisant les attributs de condition spécifiés par I<cond_attr>, ou les "
"attributs par défaut si I<cond_attr> vaut NULL. L'implémentation "
"LinuxThreads ne supporte aucun attribut de condition, aussi le paramètre "
"I<cond_attr> est pour l'instant ignoré."

#. type: Plain text
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Variables of type B<pthread_cond_t> can also be initialized statically, "
"using the constant B<PTHREAD_COND_INITIALIZER>."
msgstr ""
"Les variables de type B<pthread_cond_t> peuvent également être statiquement "
"initialisées, en utilisant la constante B<PTHREAD_COND_INITIALIZER>."

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"B<pthread_cond_signal> restarts one of the threads that are waiting on the "
"condition variable I<cond>.  If no threads are waiting on I<cond>, nothing "
"happens.  If several threads are waiting on I<cond>, exactly one is "
"restarted, but it is not specified which."
msgstr ""
"B<pthread_cond_signal>() relance l'un des threads attendant la variable "
"condition I<cond>. S'il n'existe aucun thread répondant à ce critère, rien "
"ne se produit. Si plusieurs threads attendent sur I<cond>, seul l'un d'entre "
"eux sera relancé, mais il est impossible de savoir lequel."

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"B<pthread_cond_broadcast> restarts all the threads that are waiting on the "
"condition variable I<cond>.  Nothing happens if no threads are waiting on "
"I<cond>."
msgstr ""
"B<pthread_cond_broadcast>() relance tous les threads attendant sur la "
"variable condition I<cond>. Rien ne se passe s'il n'y a aucun thread "
"attendant sur I<cond>."

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"B<pthread_cond_wait> atomically unlocks the I<mutex> (as per "
"B<pthread_unlock_mutex>)  and waits for the condition variable I<cond> to be "
"signaled.  The thread execution is suspended and does not consume any CPU "
"time until the condition variable is signaled.  The I<mutex> must be locked "
"by the calling thread on entrance to B<pthread_cond_wait>.  Before returning "
"to the calling thread, B<pthread_cond_wait> re-acquires I<mutex> (as per "
"B<pthread_lock_mutex>)."
msgstr ""
"B<pthread_cond_wait>() déverrouille atomiquement le I<mutex> (comme "
"B<pthread_unlock_mutex>()) et attend que la variable condition I<cond> soit "
"signalée. L'exécution du thread est suspendue et ne consomme pas de temps "
"CPU jusqu'à ce que la variable condition soit signalée. Le I<mutex> doit "
"être verrouillé par le thread appelant à l'entrée de B<pthread_cond_wait>(). "
"Avant de rendre la main au thread appelant, B<pthread_cond_wait>() "
"reverrouille I<mutex> (comme B<pthread_lock_mutex>())."

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Unlocking the mutex and suspending on the condition variable is done "
"atomically.  Thus, if all threads always acquire the mutex before signaling "
"the condition, this guarantees that the condition cannot be signaled (and "
"thus ignored)  between the time a thread locks the mutex and the time it "
"waits on the condition variable."
msgstr ""
"Le déverrouillage du mutex et la suspension de l'exécution sur la variable "
"condition sont liés atomiquement. Donc, si tous les threads verrouillent le "
"mutex avant de signaler la condition, il est garanti que la condition ne "
"peut être signalée (et donc ignorée) entre le moment où un thread verrouille "
"le mutex et le moment où il attend sur la variable condition."

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"B<pthread_cond_timedwait> atomically unlocks I<mutex> and waits on I<cond>, "
"as B<pthread_cond_wait> does, but it also bounds the duration of the wait.  "
"If I<cond> has not been signaled within the amount of time specified by "
"I<abstime>, the mutex I<mutex> is re-acquired and B<pthread_cond_timedwait> "
"returns the error B<ETIMEDOUT>.  The I<abstime> parameter specifies an "
"absolute time, with the same origin as B<time>(2) and B<gettimeofday>(2): an "
"I<abstime> of 0 corresponds to 00:00:00 GMT, January 1, 1970."
msgstr ""
"B<pthread_cond_timedwait>() déverrouille le I<mutex> et attend sur I<cond>, "
"en liant atomiquement ces deux étapes, comme le fait B<pthread_cond_wait>(), "
"cependant l'attente est bornée temporellement. Si I<cond> n'a pas été "
"signalée après la période spécifiée par I<abstime>, le mutex I<mutex> est "
"reverrouillé et B<pthread_cond_timedwait>() rend la main avec l'erreur "
"B<ETIMEDOUT>. Le paramètre I<abstime> spécifie un temps absolu, avec la même "
"origine que B<time>(2) et B<gettimeofday>(2)\\ : un I<abstime> de 0 "
"correspond à 00:00:00 GMT, le 1er Janvier 1970."

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"B<pthread_cond_destroy> destroys a condition variable, freeing the resources "
"it might hold.  No threads must be waiting on the condition variable on "
"entrance to B<pthread_cond_destroy>.  In the LinuxThreads implementation, no "
"resources are associated with condition variables, thus "
"B<pthread_cond_destroy> actually does nothing except checking that the "
"condition has no waiting threads."
msgstr ""
"B<pthread_cond_destroy>() détruit une variable condition, libérant les "
"ressources qu'elle possède. Aucun thread ne doit attendre sur la condition à "
"l'entrée de B<pthread_cond_destroy>(). Dans l'implémentation LinuxThreads, "
"aucune ressource ne peut être associée à une variable condition, aussi "
"B<pthread_cond_destroy>() ne fait en fait rien d'autre que vérifier qu'aucun "
"thread n'attend la condition."

#. type: SH
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "CANCELLATION"
msgstr "ANNULATION"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"B<pthread_cond_wait> and B<pthread_cond_timedwait> are cancelation points.  "
"If a thread is cancelled while suspended in one of these functions, the "
"thread immediately resumes execution, then locks again the I<mutex> argument "
"to B<pthread_cond_wait> and B<pthread_cond_timedwait>, and finally executes "
"the cancelation.  Consequently, cleanup handlers are assured that I<mutex> "
"is locked when they are called."
msgstr ""
"B<pthread_cond_wait>() et B<pthread_cond_timedwait>() sont des points "
"d'annulation. Si un thread est annulé alors qu'il est suspendu dans l'une de "
"ces fonctions, son exécution reprend immédiatement, reverrouillant le "
"paramètre I<mutex> à B<pthread_cond_wait>() et B<pthread_cond_timedwait>(), "
"et exécute finalement l'annulation. Aussi, les gestionnaires d'annulation "
"sont assurés que I<mutex> est verrouillé lorsqu'ils sont exécutés."

#. type: SH
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "ASYNC-SIGNAL SAFETY"
msgstr "ASYNC-SIGNAL SAFETY"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"The condition functions are not async-signal safe, and should not be called "
"from a signal handler.  In particular, calling B<pthread_cond_signal> or "
"B<pthread_cond_broadcast> from a signal handler may deadlock the calling "
"thread."
msgstr ""
"Ces fonctions ne sont pas fiables par rapport aux signaux asynchrones et ne "
"doivent donc pas être utilisées dans des gestionnaires de signaux [NdT\\ : "
"sous peine de perdre leur propriété d'atomicité]. En particulier, appeler "
"B<pthread_cond_signal>() ou B<pthread_cond_broadcast>() dans un gestionnaire "
"de signal peut placer le thread appelant dans une situation de blocage "
"définitif."

#. type: SH
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"All condition variable functions return 0 on success and a non-zero error "
"code on error."
msgstr ""
"Toutes ces fonctions renvoient 0 en cas de succès et un code d'erreur non "
"nul en cas de problème."

#. type: SH
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: Plain text
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<pthread_cond_init>, B<pthread_cond_signal>, B<pthread_cond_broadcast>, and "
"B<pthread_cond_wait> never return an error code."
msgstr ""
"B<pthread_cond_init>(), B<pthread_cond_signal>(), "
"B<pthread_cond_broadcast>() et B<pthread_cond_wait>() ne renvoient jamais de "
"code d'erreur."

#. type: Plain text
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The B<pthread_cond_timedwait> function returns the following error codes on "
"error:"
msgstr ""
"La fonction B<pthread_cond_timedwait>() renvoie l'un des codes d'erreur "
"suivants en cas de problème\\ :"

#. type: TP
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<ETIMEDOUT>"
msgstr "B<ETIMEDOUT>"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"The condition variable was not signaled until the timeout specified by "
"I<abstime>."
msgstr ""
"La variable condition n'a pas reçu de signal avant le délai spécifié par "
"I<abstime>."

#. type: TP
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<EINTR>"
msgstr "B<EINTR>"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<pthread_cond_timedwait> was interrupted by a signal."
msgstr "B<pthread_cond_timedwait>() a été interrompu par un signal."

#. type: Plain text
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The B<pthread_cond_destroy> function returns the following error code on "
"error:"
msgstr ""
"La fonction B<pthread_cond_destroy>() renvoie le code d'erreur suivant en "
"cas de problème\\ :"

#. type: TP
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<EBUSY>"
msgstr "B<EBUSY>"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "Some threads are currently waiting on I<cond>."
msgstr "Il existe des threads attendant I<cond>."

#. type: SH
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<pthread_condattr_init>(3), B<pthread_mutex_lock>(3), "
"B<pthread_mutex_unlock>(3), B<gettimeofday>(2), B<nanosleep>(2)."
msgstr ""
"B<pthread_condattr_init>(3), B<pthread_mutex_lock>(3), "
"B<pthread_mutex_unlock>(3), B<gettimeofday>(2), B<nanosleep>(2)"

#. type: SH
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLE"
msgstr "EXEMPLE"

#. type: Plain text
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Consider two shared variables I<x> and I<y>, protected by the mutex I<mut>, "
"and a condition variable I<cond> that is to be signaled whenever I<x> "
"becomes greater than I<y>."
msgstr ""
"Considérons deux variables globales partagées I<x> et I<y>, protégées par le "
"mutex I<mut>, et une variable condition I<cond> pour signaler que I<x> "
"devient plus grand que I<y>."

#. type: Plain text
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int x,y;\n"
"pthread_mutex_t mut = PTHREAD_MUTEX_INITIALIZER;\n"
"pthread_cond_t cond = PTHREAD_COND_INITIALIZER;>\n"
msgstr ""
"B<int x,y;\n"
"pthread_mutex_t mut = PTHREAD_MUTEX_INITIALIZER;\n"
"pthread_cond_t cond = PTHREAD_COND_INITIALIZER;>\n"

#. type: Plain text
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Waiting until I<x> is greater than I<y> is performed as follows:"
msgstr ""
"Attendre que I<x> devienne plus grand que I<y> se réalise de la manière "
"suivante\\ :"

#. type: Plain text
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<pthread_mutex_lock(&mut);\n"
"while (x E<lt>= y) {\n"
"        pthread_cond_wait(&cond, &mut);\n"
"}\n"
"/* operate on x and y */\n"
"pthread_mutex_unlock(&mut);>\n"
msgstr ""
"B<pthread_mutex_lock(&mut);\n"
"while (x E<lt>= y) {\n"
"        pthread_cond_wait(&cond, &mut);\n"
"}\n"
"/* operate on x and y */\n"
"pthread_mutex_unlock(&mut);>\n"

#. type: Plain text
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Modifications on I<x> and I<y> that may cause I<x> to become greater than "
"I<y> should signal the condition if needed:"
msgstr ""
"Les modifications de I<x> et I<y> qui peuvent rendre I<x> plus grand que "
"I<y> doivent signaler la condition si nécessaire\\ :"

#. type: Plain text
#: archlinux debian-bookworm fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<pthread_mutex_lock(&mut);\n"
"/* modify x and y */\n"
"if (x E<gt> y) pthread_cond_broadcast(&cond);\n"
"pthread_mutex_unlock(&mut);>\n"
msgstr ""
"B<pthread_mutex_lock(&mut);\n"
"/* modify x and y */\n"
"if (x E<gt> y) pthread_cond_broadcast(&cond);\n"
"pthread_mutex_unlock(&mut);>\n"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"If it can be proved that at most one waiting thread needs to be waken up "
"(for instance, if there are only two threads communicating through I<x> and "
"I<y>), B<pthread_cond_signal> can be used as a slightly more efficient "
"alternative to B<pthread_cond_broadcast>.  In doubt, use "
"B<pthread_cond_broadcast>."
msgstr ""
"S'il peut être prouvé qu'au plus un thread en attente nécessite d'être "
"réveillé (par exemple, s'il n'y a que deux threads communiquant via I<x> et "
"I<y>), B<pthread_cond_signal>() peut être utilisé en tant qu'alternative "
"efficace à B<pthread_cond_broadcast>(). En cas de doute, utilisez "
"B<pthread_cond_broadcast>()."

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"To wait for I<x> to become greater than I<y> with a timeout of 5 seconds, do:"
msgstr ""
"Pour attendre que I<x> devienne plus grand que I<y> avec un délai de 5 "
"secondes, faîtes\\ :"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<struct timeval now;\n"
"struct timespec timeout;\n"
"int retcode;\n"
"\\&\n"
"pthread_mutex_lock(&mut);\n"
"gettimeofday(&now);\n"
"timeout.tv_sec = now.tv_sec + 5;\n"
"timeout.tv_nsec = now.tv_usec * 1000;\n"
"retcode = 0;\n"
"while (x E<lt>= y && retcode != ETIMEDOUT) {\n"
"        retcode = pthread_cond_timedwait(&cond, &mut, &timeout);\n"
"}\n"
"if (retcode == ETIMEDOUT) {\n"
"        /* timeout occurred */\n"
"} else {\n"
"        /* operate on x and y */\n"
"}\n"
"pthread_mutex_unlock(&mut);>\n"
msgstr ""
"B<struct timeval now;\n"
"struct timespec timeout;\n"
"int retcode;\n"
"\\&\n"
"pthread_mutex_lock(&mut);\n"
"gettimeofday(&now);\n"
"timeout.tv_sec = now.tv_sec + 5;\n"
"timeout.tv_nsec = now.tv_usec * 1000;\n"
"retcode = 0;\n"
"while (x E<lt>= y && retcode != ETIMEDOUT) {\n"
"        retcode = pthread_cond_timedwait(&cond, &mut, &timeout);\n"
"}\n"
"if (retcode == ETIMEDOUT) {\n"
"        /* timeout occurred */\n"
"} else {\n"
"        /* operate on x and y */\n"
"}\n"
"pthread_mutex_unlock(&mut);>\n"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "PTHREAD_COND"
msgstr "PTHREAD_COND"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "LinuxThreads"
msgstr "LinuxThreads"

#. type: Plain text
#: debian-bookworm
msgid ""
"pthread_cond_init, pthread_cond_destroy, pthread_cond_signal, "
"pthread_cond_broadcast, pthread_cond_wait, pthread_cond_timedwait - "
"operations on conditions"
msgstr ""
"pthread_cond_init, pthread_cond_destroy, pthread_cond_signal, "
"pthread_cond_broadcast, pthread_cond_wait, pthread_cond_timedwait - "
"Opérations sur les conditions"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<int pthread_cond_init(pthread_cond_t *>I<cond>B<, pthread_condattr_t "
"*>I<cond_attr>B<);>"
msgstr ""
"B<int pthread_cond_init(pthread_cond_t *>I<cond>B<, pthread_condattr_t "
"*>I<cond_attr>B<);>"

#. type: Plain text
#: debian-bookworm
msgid "B<int pthread_cond_signal(pthread_cond_t *>I<cond>B<);>"
msgstr "B<int pthread_cond_signal(pthread_cond_t *>I<cond>B<);>"

#. type: Plain text
#: debian-bookworm
msgid "B<int pthread_cond_broadcast(pthread_cond_t *>I<cond>B<);>"
msgstr "B<int pthread_cond_broadcast(pthread_cond_t *>I<cond>B<);>"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<int pthread_cond_wait(pthread_cond_t *>I<cond>B<, pthread_mutex_t "
"*>I<mutex>B<);>"
msgstr ""
"B<int pthread_cond_wait(pthread_cond_t *>I<cond>B<, pthread_mutex_t "
"*>I<mutex>B<);>"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<int pthread_cond_timedwait(pthread_cond_t *>I<cond>B<, pthread_mutex_t "
"*>I<mutex>B<, const struct timespec *>I<abstime>B<);>"
msgstr ""
"B<int pthread_cond_timedwait(pthread_cond_t *>I<cond>B<, pthread_mutex_t "
"*>I<mutex>B<, const struct timespec *>I<abstime>B<);>"

#. type: Plain text
#: debian-bookworm
msgid "B<int pthread_cond_destroy(pthread_cond_t *>I<cond>B<);>"
msgstr "B<int pthread_cond_destroy(pthread_cond_t *>I<cond>B<);>"

#. type: Plain text
#: debian-bookworm
msgid ""
"A condition (short for ``condition variable'') is a synchronization device "
"that allows threads to suspend execution and relinquish the processors until "
"some predicate on shared data is satisfied. The basic operations on "
"conditions are: signal the condition (when the predicate becomes true), and "
"wait for the condition, suspending the thread execution until another thread "
"signals the condition."
msgstr ""
"Une condition (abréviation pour «\\ variable condition\\ ») est un mécanisme "
"de synchronisation permettant à un thread de suspendre son exécution jusqu'à "
"ce qu'une certaine condition (un prédicat) sur des données partagées soit "
"vérifiée. Les opérations fondamentales sur les conditions sont\\ : signaler "
"la condition (quand le prédicat devient vrai) et attendre la condition en "
"suspendant l'exécution du thread jusqu'à ce qu'un autre thread signale la "
"condition."

#. type: Plain text
#: debian-bookworm
msgid ""
"B<pthread_cond_init> initializes the condition variable I<cond>, using the "
"condition attributes specified in I<cond_attr>, or default attributes if "
"I<cond_attr> is B<NULL>. The LinuxThreads implementation supports no "
"attributes for conditions, hence the I<cond_attr> parameter is actually "
"ignored."
msgstr ""
"B<pthread_cond_init>() initialise la variable condition I<cond>, en "
"utilisant les attributs de condition spécifiés par I<cond_attr>, ou les "
"attributs par défaut si I<cond_attr> vaut NULL. L'implémentation "
"LinuxThreads ne supporte aucun attribut de condition, aussi le paramètre "
"I<cond_attr> est pour l'instant ignoré."

#. type: Plain text
#: debian-bookworm
msgid ""
"B<pthread_cond_signal> restarts one of the threads that are waiting on the "
"condition variable I<cond>. If no threads are waiting on I<cond>, nothing "
"happens. If several threads are waiting on I<cond>, exactly one is "
"restarted, but it is not specified which."
msgstr ""
"B<pthread_cond_signal>() relance l'un des threads attendant la variable "
"condition I<cond>. S'il n'existe aucun thread répondant à ce critère, rien "
"ne se produit. Si plusieurs threads attendent sur I<cond>, seul l'un d'entre "
"eux sera relancé, mais il est impossible de savoir lequel."

#. type: Plain text
#: debian-bookworm
msgid ""
"B<pthread_cond_broadcast> restarts all the threads that are waiting on the "
"condition variable I<cond>. Nothing happens if no threads are waiting on "
"I<cond>."
msgstr ""
"B<pthread_cond_broadcast>() relance tous les threads attendant sur la "
"variable condition I<cond>. Rien ne se passe s'il n'y a aucun thread "
"attendant sur I<cond>."

#. type: Plain text
#: debian-bookworm
msgid ""
"B<pthread_cond_wait> atomically unlocks the I<mutex> (as per "
"B<pthread_unlock_mutex>) and waits for the condition variable I<cond> to be "
"signaled. The thread execution is suspended and does not consume any CPU "
"time until the condition variable is signaled. The I<mutex> must be locked "
"by the calling thread on entrance to B<pthread_cond_wait>. Before returning "
"to the calling thread, B<pthread_cond_wait> re-acquires I<mutex> (as per "
"B<pthread_lock_mutex>)."
msgstr ""
"B<pthread_cond_wait>() déverrouille atomiquement le I<mutex> (comme "
"B<pthread_unlock_mutex>()) et attend que la variable condition I<cond> soit "
"signalée. L'exécution du thread est suspendue et ne consomme pas de temps "
"CPU jusqu'à ce que la variable condition soit signalée. Le I<mutex> doit "
"être verrouillé par le thread appelant à l'entrée de B<pthread_cond_wait>(). "
"Avant de rendre la main au thread appelant, B<pthread_cond_wait>() "
"reverrouille I<mutex> (comme B<pthread_lock_mutex>())."

#. type: Plain text
#: debian-bookworm
msgid ""
"Unlocking the mutex and suspending on the condition variable is done "
"atomically. Thus, if all threads always acquire the mutex before signaling "
"the condition, this guarantees that the condition cannot be signaled (and "
"thus ignored) between the time a thread locks the mutex and the time it "
"waits on the condition variable."
msgstr ""
"Le déverrouillage du mutex et la suspension de l'exécution sur la variable "
"condition sont liés atomiquement. Donc, si tous les threads verrouillent le "
"mutex avant de signaler la condition, il est garanti que la condition ne "
"peut être signalée (et donc ignorée) entre le moment où un thread verrouille "
"le mutex et le moment où il attend sur la variable condition."

#. type: Plain text
#: debian-bookworm
msgid ""
"B<pthread_cond_timedwait> atomically unlocks I<mutex> and waits on I<cond>, "
"as B<pthread_cond_wait> does, but it also bounds the duration of the wait. "
"If I<cond> has not been signaled within the amount of time specified by "
"I<abstime>, the mutex I<mutex> is re-acquired and B<pthread_cond_timedwait> "
"returns the error B<ETIMEDOUT>.  The I<abstime> parameter specifies an "
"absolute time, with the same origin as B<time>(2) and B<gettimeofday>(2): an "
"I<abstime> of 0 corresponds to 00:00:00 GMT, January 1, 1970."
msgstr ""
"B<pthread_cond_timedwait>() déverrouille le I<mutex> et attend sur I<cond>, "
"en liant atomiquement ces deux étapes, comme le fait B<pthread_cond_wait>(), "
"cependant l'attente est bornée temporellement. Si I<cond> n'a pas été "
"signalée après la période spécifiée par I<abstime>, le mutex I<mutex> est "
"reverrouillé et B<pthread_cond_timedwait>() rend la main avec l'erreur "
"B<ETIMEDOUT>. Le paramètre I<abstime> spécifie un temps absolu, avec la même "
"origine que B<time>(2) et B<gettimeofday>(2)\\ : un I<abstime> de 0 "
"correspond à 00:00:00 GMT, le 1er Janvier 1970."

#. type: Plain text
#: debian-bookworm
msgid ""
"B<pthread_cond_destroy> destroys a condition variable, freeing the resources "
"it might hold. No threads must be waiting on the condition variable on "
"entrance to B<pthread_cond_destroy>. In the LinuxThreads implementation, no "
"resources are associated with condition variables, thus "
"B<pthread_cond_destroy> actually does nothing except checking that the "
"condition has no waiting threads."
msgstr ""
"B<pthread_cond_destroy>() détruit une variable condition, libérant les "
"ressources qu'elle possède. Aucun thread ne doit attendre sur la condition à "
"l'entrée de B<pthread_cond_destroy>(). Dans l'implémentation LinuxThreads, "
"aucune ressource ne peut être associée à une variable condition, aussi "
"B<pthread_cond_destroy>() ne fait en fait rien d'autre que vérifier qu'aucun "
"thread n'attend la condition."

#. type: Plain text
#: debian-bookworm
msgid ""
"B<pthread_cond_wait> and B<pthread_cond_timedwait> are cancellation points. "
"If a thread is cancelled while suspended in one of these functions, the "
"thread immediately resumes execution, then locks again the I<mutex> argument "
"to B<pthread_cond_wait> and B<pthread_cond_timedwait>, and finally executes "
"the cancellation.  Consequently, cleanup handlers are assured that I<mutex> "
"is locked when they are called."
msgstr ""
"B<pthread_cond_wait>() et B<pthread_cond_timedwait>() sont des points "
"d'annulation. Si un thread est annulé alors qu'il est suspendu dans l'une de "
"ces fonctions, son exécution reprend immédiatement, reverrouillant le "
"paramètre I<mutex> à B<pthread_cond_wait>() et B<pthread_cond_timedwait>(), "
"et exécute finalement l'annulation. Aussi, les gestionnaires d'annulation "
"sont assurés que I<mutex> est verrouillé lorsqu'ils sont exécutés."

#. type: Plain text
#: debian-bookworm
msgid ""
"The condition functions are not async-signal safe, and should not be called "
"from a signal handler. In particular, calling B<pthread_cond_signal> or "
"B<pthread_cond_broadcast> from a signal handler may deadlock the calling "
"thread."
msgstr ""
"Ces fonctions ne sont pas fiables par rapport aux signaux asynchrones et ne "
"doivent donc pas être utilisées dans des gestionnaires de signaux [NdT\\ : "
"sous peine de perdre leur propriété d'atomicité]. En particulier, appeler "
"B<pthread_cond_signal>() ou B<pthread_cond_broadcast>() dans un gestionnaire "
"de signal peut placer le thread appelant dans une situation de blocage "
"définitif."

#. type: Plain text
#: debian-bookworm
msgid ""
"the condition variable was not signaled until the timeout specified by "
"I<abstime>"
msgstr ""
"La variable condition n'a pas reçu de signal avant le délai spécifié par "
"I<abstime>."

#. type: Plain text
#: debian-bookworm
msgid "B<pthread_cond_timedwait> was interrupted by a signal"
msgstr "B<pthread_cond_timedwait>() a été interrompu par un signal."

#. type: Plain text
#: debian-bookworm
msgid "some threads are currently waiting on I<cond>."
msgstr "Il existe des threads attendant I<cond>."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: debian-bookworm
msgid "Xavier Leroy E<lt>Xavier.Leroy@inria.frE<gt>"
msgstr "Xavier Leroy E<lt>Xavier.Leroy@inria.frE<gt>"

#. type: Plain text
#: debian-bookworm
msgid ""
"If it can be proved that at most one waiting thread needs to be waken up "
"(for instance, if there are only two threads communicating through I<x> and "
"I<y>), B<pthread_cond_signal> can be used as a slightly more efficient "
"alternative to B<pthread_cond_broadcast>. In doubt, use "
"B<pthread_cond_broadcast>."
msgstr ""
"S'il peut être prouvé qu'au plus un thread en attente nécessite d'être "
"réveillé (par exemple, s'il n'y a que deux threads communiquant via I<x> et "
"I<y>), B<pthread_cond_signal>() peut être utilisé en tant qu'alternative "
"efficace à B<pthread_cond_broadcast>(). En cas de doute, utilisez "
"B<pthread_cond_broadcast>()."

#. type: Plain text
#: debian-bookworm
msgid ""
"To wait for I<x> to becomes greater than I<y> with a timeout of 5 seconds, "
"do:"
msgstr ""
"Pour attendre que I<x> devienne plus grand que I<y> avec un délai de 5 "
"secondes, faîtes\\ :"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"B<struct timeval now;\n"
"struct timespec timeout;\n"
"int retcode;>\n"
msgstr ""
"B<struct timeval now;\n"
"struct timespec timeout;\n"
"int retcode;>\n"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"B<pthread_mutex_lock(&mut);\n"
"gettimeofday(&now);\n"
"timeout.tv_sec = now.tv_sec + 5;\n"
"timeout.tv_nsec = now.tv_usec * 1000;\n"
"retcode = 0;\n"
"while (x E<lt>= y && retcode != ETIMEDOUT) {\n"
"        retcode = pthread_cond_timedwait(&cond, &mut, &timeout);\n"
"}\n"
"if (retcode == ETIMEDOUT) {\n"
"        /* timeout occurred */\n"
"} else {\n"
"        /* operate on x and y */\n"
"}\n"
"pthread_mutex_unlock(&mut);>\n"
msgstr ""
"B<pthread_mutex_lock(&mut);\n"
"gettimeofday(&now);\n"
"timeout.tv_sec = now.tv_sec + 5;\n"
"timeout.tv_nsec = now.tv_usec * 1000;\n"
"retcode = 0;\n"
"while (x E<lt>= y && retcode != ETIMEDOUT) {\n"
"        retcode = pthread_cond_timedwait(&cond, &mut, &timeout);\n"
"}\n"
"if (retcode == ETIMEDOUT) {\n"
"        /* timeout occurred */\n"
"} else {\n"
"        /* operate on x and y */\n"
"}\n"
"pthread_mutex_unlock(&mut);>\n"

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: Plain text
#: fedora-40 mageia-cauldron
msgid ""
"B<pthread_cond_wait> and B<pthread_cond_timedwait> are cancellation points.  "
"If a thread is cancelled while suspended in one of these functions, the "
"thread immediately resumes execution, then locks again the I<mutex> argument "
"to B<pthread_cond_wait> and B<pthread_cond_timedwait>, and finally executes "
"the cancellation.  Consequently, cleanup handlers are assured that I<mutex> "
"is locked when they are called."
msgstr ""
"B<pthread_cond_wait>() et B<pthread_cond_timedwait>() sont des points "
"d'annulation. Si un thread est annulé alors qu'il est suspendu dans l'une de "
"ces fonctions, son exécution reprend immédiatement, reverrouillant le "
"paramètre I<mutex> à B<pthread_cond_wait>() et B<pthread_cond_timedwait>(), "
"et exécute finalement l'annulation. Aussi, les gestionnaires d'annulation "
"sont assurés que I<mutex> est verrouillé lorsqu'ils sont exécutés."

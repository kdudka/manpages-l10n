# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013, 2014.
# Denis Barbier <barbier@debian.org>, 2006, 2010, 2011.
# David Prévot <david@tilapin.org>, 2010, 2012-2014.
# Lucien Gentis <lucien.gentis@waika9.com>, 2022
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-06-27 19:23+0200\n"
"PO-Revision-Date: 2023-03-13 19:12+0100\n"
"Last-Translator: Lucien Gentis <lucien.gentis@waika9.com>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Poedit 2.4.2\n"
"X-Poedit-Bookmarks: 26,-1,-1,-1,-1,-1,-1,-1,-1,-1\n"

#. type: Dd
#: debian-bookworm debian-unstable
#, no-wrap
msgid "October 11, 2017"
msgstr "11 Octobre 2017"

#. type: Dt
#: debian-bookworm debian-unstable
#, no-wrap
msgid "CRYPT 3"
msgstr "CRYPT 3"

#. type: Os
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Openwall Project"
msgstr "Projet Openwall"

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "E<.Nm crypt , crypt_r , crypt_rn , crypt_ra>"
msgstr "E<.Nm crypt , crypt_r , crypt_rn , crypt_ra>"

#. type: Nd
#: debian-bookworm debian-unstable
#, no-wrap
msgid "passphrase hashing"
msgstr "hachage des mot de passe"

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "E<.Lb libcrypt>"
msgstr "E<.Lb libcrypt>"

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: In
#: debian-bookworm debian-unstable
#, no-wrap
msgid "crypt.h"
msgstr "crypt.h"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"E<.Ft \"char *\"> E<.Fo crypt> E<.Fa \"const char *phrase\"> E<.Fa \"const "
"char *setting\"> E<.Fc> E<.Ft \"char *\"> E<.Fo crypt_r> E<.Fa \"const char "
"*phrase\"> E<.Fa \"const char *setting\"> E<.Fa \"struct crypt_data *data\"> "
"E<.Fc> E<.Ft \"char *\"> E<.Fo crypt_rn> E<.Fa \"const char *phrase\"> E<.Fa "
"\"const char *setting\"> E<.Fa \"struct crypt_data *data\"> E<.Fa \"int "
"size\"> E<.Fc> E<.Ft \"char *\"> E<.Fo crypt_ra> E<.Fa \"const char "
"*phrase\"> E<.Fa \"const char *setting\"> E<.Fa \"void **data\"> E<.Fa \"int "
"*size\"> E<.Fc>"
msgstr ""
"E<.Ft \"char *\"> E<.Fo crypt> E<.Fa \"const char *motdepasse\"> E<.Fa "
"\"const char *paramètres\"> E<.Fc> E<.Ft \"char *\"> E<.Fo crypt_r> E<.Fa "
"\"const char *motdepasse\"> E<.Fa \"const char *paramètres\"> E<.Fa \"struct "
"crypt_data *données\"> E<.Fc> E<.Ft \"char *\"> E<.Fo crypt_rn> E<.Fa "
"\"const char *motdepasse\"> E<.Fa \"const char *paramètres\"> E<.Fa \"struct "
"crypt_data *données\"> E<.Fa \"int taille\"> E<.Fc> E<.Ft \"char *\"> E<.Fo "
"crypt_ra> E<.Fa \"const char *motdepasse\"> E<.Fa \"const char "
"*paramètres\"> E<.Fa \"void **données\"> E<.Fa \"int *taille\"> E<.Fc>"

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The E<.Nm crypt>, E<.Nm crypt_r>, E<.Nm crypt_rn>, and E<.Nm crypt_ra> "
"functions irreversibly E<.Dq hash> E<.Fa phrase> for storage in the system "
"password database E<.Pq Xr shadow 5> using a cryptographic E<.Dq hashing "
"method.> The result of this operation is called a E<.Dq hashed passphrase> "
"or just a E<.Dq hash.> Hashing methods are described in E<.Xr crypt 5>."
msgstr ""
"Les fonctions E<.Nm crypt>, E<.Nm crypt_r>, E<.Nm crypt_rn> et E<.Nm "
"crypt_ra> E<.Dq hachent> de manière irréversible E<.Fa motdepasse> avant de "
"le stocker dans la base de données E<.Pq Xr shadow 5> des mots de passe du "
"système en utilisant une E<.Dq méthode de hachage> cryptographique. Le "
"résultat de cette opération se nomme E<.Dq mot de passe condensé> ou "
"simplement E<.Dq condensé.> Les méthodes de hachage sont décrites dans E<.Xr "
"crypt 5>."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"E<.Fa setting> controls which hashing method to use, and also supplies "
"various parameters to the chosen method, most importantly a random E<.Dq "
"salt> which ensures that no two stored hashes are the same, even if the E<."
"Fa phrase> strings are the same."
msgstr ""
"E<.Fa paramètres> permet de spécifier la méthode de hachage à utiliser, et "
"fournit aussi différents paramètres à cette dernière, en particulier un E<."
"Dq salage> (salt) aléatoire qui permet de s'assurer que deux condensés "
"stockés seront toujours différents, même si leurs chaînes E<.Fa motdepasse> "
"sont identiques."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The E<.Fa data> argument to E<.Nm crypt_r> is a structure of type E<.Vt "
"\"struct crypt_data\">.  It has at least these fields:"
msgstr ""
"L'argument E<.Fa données> de E<.Nm crypt_r> est une structure de type E<.Vt "
"\"struct crypt_data\">. Elle contient au minimum ces champs :"

#. type: Plain text
#: debian-bookworm debian-unstable
#, no-wrap
msgid ""
"struct crypt_data {\n"
"    char output[CRYPT_OUTPUT_SIZE];\n"
"    char setting[CRYPT_OUTPUT_SIZE];\n"
"    char input[CRYPT_MAX_PASSPHRASE_SIZE];\n"
"    char initialized;\n"
"};\n"
msgstr ""
"struct crypt_data {\n"
"    char output[CRYPT_OUTPUT_SIZE];\n"
"    char setting[CRYPT_OUTPUT_SIZE];\n"
"    char input[CRYPT_MAX_PASSPHRASE_SIZE];\n"
"    char initialized;\n"
"};\n"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Upon a successful return from E<.Nm crypt_r>, the hashed passphrase will be "
"stored in E<.Fa output>.  Applications are encouraged, but not required, to "
"use the E<.Fa input> and E<.Fa setting> fields to store the strings that "
"they will pass as E<.Fa input phrase> and E<.Fa setting> to E<.Nm crypt_r>.  "
"This will make it easier to erase all sensitive data after it is no longer "
"needed."
msgstr ""
"Si E<.Nm crypt_r> s'exécute avec succès, le mot de passe condensé sera "
"stocké dans E<.Fa output>. Même si ce n'est pas obligatoire, il est "
"recommandé dans les applications d'utiliser les champs E<.Fa motdepasse> et "
"E<.Fa paramètres> pour stocker les chaînes qu'elles passeront à E<.Nm "
"crypt_r> à l'aide des arguments E<.Fa motdepasse> et E<.Fa paramètres>. Cela "
"facilitera la suppression des données sensibles lorsqu'elles ne seront plus "
"utilisées."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The E<.Fa initialized> field must be set to zero before the first time a E<."
"Vt \"struct crypt_data\"> object is first used in a call to E<.Fn crypt_r>.  "
"We recommend zeroing the entire object, not just E<.Fa initialized> and not "
"just the documented fields, before the first use.  (Of course, do this "
"before storing anything in E<.Fa setting> and E<.Fa input>.)"
msgstr ""
"Le champ E<.Fa initialized> doit être défini à zéro avant la première "
"utilisation d'un objet de type E<.Vt \"struct crypt_data\"> dans un appel à "
"E<.Fn crypt_r>. Nous recommandons de définir à zéro l'objet dans son "
"ensemble avant sa première utilisation, et non pas seulement E<.Fa "
"initialized> et les champs documentés. (Bien entendu, il faut effectuer "
"cette opération avant de stocker quoi que ce soit dans E<.Fa paramètres> et "
"E<.Fa entrée>.)"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The E<.Fa data> argument to E<.Nm crypt_rn> should also point to a E<.Vt "
"\"struct crypt_data\"> object, and E<.Fa size> should be the size of that "
"object, cast to E<.Vt int>.  When used with E<.Nm crypt_rn>, the entire E<."
"Fa data> object (except for the E<.Fa input> and E<.Fa setting> fields) must "
"be zeroed before its first use; this is not just a recommendation, as it is "
"for E<.Nm crypt_r>.  Otherwise, the fields of the object have the same uses "
"that they do for E<.Nm crypt_r>."
msgstr ""
"L'argument E<.Fa données> de E<.Nm crypt_rn> doit aussi pointer vers un "
"objet de type E<.Vt \"struct crypt_data\">, et E<.Fa taille> doit contenir "
"la taille de ce dernier, convertie en E<.Vt int>. Lorsqu'il est utilisé avec "
"E<.Nm crypt_rn>, l'objet E<.Fa data> dans son ensemble (à l'exception des "
"champs E<.Fa entrée> et E<.Fa paramètres>) doit être défini à zéro avant sa "
"première utilisation, et cela n'est pas une simple recommandation, comme "
"avec E<.Nm crypt_r>. Cela mis à part, les champs de l'objet s'utilisent de "
"la même façon qu'avec E<.Nm crypt_r>."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"On the first call to E<.Nm crypt_ra>, E<.Fa data> should be the address of a "
"E<.Vt \"void *\"> variable set to NULL, and E<.Fa size> should be the "
"address of an E<.Vt int> variable set to zero.  E<.Nm crypt_ra> will "
"allocate and initialize a E<.Vt \"struct crypt_data\"> object, using E<.Xr "
"malloc 3>, and write its address and size into the variables pointed to by "
"E<.Fa data> and E<.Fa size>.  These can be reused in subsequent calls.  "
"After the application is done hashing passphrases, it should deallocate the "
"E<.Vt \"struct crypt_data\"> object using E<.Xr free 3>."
msgstr ""
"Au premier appel à E<.Nm crypt_ra>, E<.Fa données> doit contenir l'adresse "
"d'une variable de type E<.Vt \"void *\"> initialisée à NULL, et E<.Fa "
"taille> l'adresse d'une variable de type E<.Vt int> initialisée à zéro. E<."
"Nm crypt_ra> alloue et initialise un objet de type E<.Vt \"struct "
"crypt_data\"> en utilisant E<.Xr malloc 3>, et écrit son adresse et sa "
"taille dans les variables vers lesquelles pointent respectivement E<.Fa "
"data> et E<.Fa taille>. Ces dernières peuvent être réutilisées lors d'appels "
"ultérieurs à E<.Nm crypt_ra>. Lorsque l'application a terminé son hachage de "
"mots de passe, elle doit désallouer l'objet E<.Vt \"struct crypt_data\"> à "
"l'aide de E<.Xr free 3>."

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "RETURN VALUES"
msgstr "VALEURS DE RENVOI"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Upon successful completion, E<.Nm crypt>, E<.Nm crypt_r>, E<.Nm crypt_rn>, "
"and E<.Nm crypt_ra> return a pointer to a string which encodes both the "
"hashed passphrase, and the settings that were used to encode it.  This "
"string is directly usable as E<.Fa setting> in other calls to E<.Nm crypt>, "
"E<.Nm crypt_r>, E<.Nm crypt_rn>, and E<.Nm crypt_ra>, and as E<.Fa prefix> "
"in calls to E<.Nm crypt_gensalt>, E<.Nm crypt_gensalt_rn>, and E<.Nm "
"crypt_gensalt_ra>.  It will be entirely printable ASCII, and will not "
"contain whitespace or the characters E<.Sq Li \\&:>, E<.Sq Li \\&;>, E<.Sq "
"Li \\&*>, E<.Sq Li \\&!>, or E<.Sq Li \\&\\e>.  See E<.Xr crypt 5> for more "
"detail on the format of hashed passphrases."
msgstr ""
"Si elles s'exécutent avec succès, E<.Nm crypt>, E<.Nm crypt_r>, E<.Nm "
"crypt_rn> et E<.Nm crypt_ra> renvoient un pointeur vers une chaîne qui "
"contiendra le mot de passe condensé et les paramètres qui ont été utilisés "
"pour le hacher. Cette chaîne est directement utilisable comme valeur de E<."
"Fa paramètres> lors d'appels ultérieurs à E<.Nm crypt>, E<.Nm crypt_r>, E<."
"Nm crypt_rn> et E<.Nm crypt_ra>, et comme valeur de E<.Fa prefix> lors "
"d'appels ultérieurs à E<.Nm crypt_gensalt>, E<.Nm crypt_gensalt_rn> et E<.Nm "
"crypt_gensalt_ra>. Elle ne contiendra que des caractères ASCII imprimables "
"et ne contiendra ni espaces, ni aucun des caractères E<.Sq Li \\&:>, E<.Sq "
"Li \\&;>, E<.Sq Li \\&*>, E<.Sq Li \\&!> ou E<.Sq Li \\&\\e>. Voir E<.Xr "
"crypt 5> pour plus de détails sur le format des mots de passe condensés."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"E<.Nm crypt> places its result in a static storage area, which will be "
"overwritten by subsequent calls to E<.Nm crypt>.  It is not safe to call E<."
"Nm crypt> from multiple threads simultaneously."
msgstr ""
"E<.Nm crypt> place son résultat dans une zone de mémoire statique qui sera "
"écrasée lors d'appels ultérieurs à E<.Nm crypt>. Il n'est pas sans danger "
"d'appeler E<.Nm crypt> depuis plusieurs threads simultanément."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"E<.Nm crypt_r>, E<.Nm crypt_rn>, and E<.Nm crypt_ra> place their result in "
"the E<.Fa output> field of their E<.Fa data> argument.  It is safe to call "
"them from multiple threads simultaneously, as long as a separate E<.Fa data> "
"object is used for each thread."
msgstr ""
"E<.Nm crypt_r>, E<.Nm crypt_rn> et E<.Nm crypt_ra> placent leur résultat "
"dans le champ E<.Fa output> de leur argument E<.Fa données>. On peut sans "
"danger les appeler depuis plusieurs threads simultanément, sous réserve "
"qu'un objet E<.Fa données> séparé soit utilisé pour chaque thread."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Upon error, E<.Nm crypt_r>, E<.Nm crypt_rn>, and E<.Nm crypt_ra> write an E<."
"Em invalid> hashed passphrase to the E<.Fa output> field of their E<.Fa "
"data> argument, and E<.Nm crypt> writes an invalid hash to its static "
"storage area.  This string will be shorter than 13 characters, will begin "
"with a E<.Sq Li \\&*>, and will not compare equal to E<.Fa setting>."
msgstr ""
"En cas d'erreur, E<.Nm crypt_r>, E<.Nm crypt_rn> et E<.Nm crypt_ra> écrivent "
"un mot de passe condensé E<.Em non valable> dans le champ E<.Fa output> de "
"leur argument E<.Fa données>, et E<.Nm crypt> écrit un condensé non valable "
"dans sa zone de mémoire statique. La chaîne contiendra moins de 13 "
"caractères, commencera par un E<.Sq Li \\&*> et sera différente de E<.Fa "
"paramètres>."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Upon error, E<.Nm crypt_rn> and E<.Nm crypt_ra> return a null pointer.  E<."
"Nm crypt_r> and E<.Nm crypt> may also return a null pointer, or they may "
"return a pointer to the invalid hash, depending on how libcrypt was "
"configured.  (The option to return the invalid hash is for compatibility "
"with old applications that assume that E<.Nm crypt> cannot return a null "
"pointer.  See E<.Sx PORTABILITY NOTES> below.)"
msgstr ""
"En cas d'erreur, E<.Nm crypt_rn> et E<.Nm crypt_ra> renvoient un pointeur "
"NULL. E<.Nm crypt_r> et E<.Nm crypt>, quant à elles, renverront aussi un "
"pointeur NULL ou un pointeur vers le condensé non valable, selon la manière "
"dont aura été configurée libcrypt. Cette possibilité de renvoyer le condensé "
"non valable est offerte à titre de compatibilité avec les anciennes "
"applications qui partent du principe que E<.Nm crypt> ne peut pas renvoyer "
"de pointeur NULL (voir E<.Sx NOTES DE PORTABILITÉ> ci-dessous)."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "All four functions set E<.Va errno> when they fail."
msgstr "Les quatre fonctions définissent E<.Va errno> en cas d'échec."

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Er EINVAL"
msgstr "Er EINVAL"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"E<.Fa setting> is invalid, or requests a hashing method that is not "
"supported."
msgstr ""
"E<.Fa paramètres> est non valable ou spécifie une méthode de hachage non "
"prise en charge."

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Er ERANGE"
msgstr "Er ERANGE"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"E<.Fa phrase> is too long (more than E<.Dv CRYPT_MAX_PASSPHRASE_SIZE> "
"characters; some hashing methods may have lower limits)."
msgstr ""
"E<.Fa motdepasse> est trop long (nombre de caractères supérieur à E<.Dv "
"CRYPT_MAX_PASSPHRASE_SIZE> ; certaines méthodes de hachage imposeront peut-"
"être des limites plus basses)."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"E<.Nm crypt_rn> only: E<.Fa size> is too small for the hashing method "
"requested by E<.Fa setting>."
msgstr ""
"Pour E<.Nm crypt_rn> seulement : E<.Fa taille> est trop petite pour la "
"méthode de hachage spécifiée par E<.Fa paramètres>."

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Er ENOMEM"
msgstr "Er ENOMEM"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Failed to allocate internal scratch memory."
msgstr "L'allocation de mémoire de travail interne a échoué."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "E<.Nm crypt_ra> only: failed to allocate memory for E<.Fa data>."
msgstr ""
"Pour E<.Nm crypt_ra> seulement : l'allocation de mémoire pour E<.Fa données> "
"a échoué."

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Er ENOSYS No or Er EOPNOTSUPP"
msgstr "Er ENOSYS No ou Er EOPNOTSUPP"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Hashing passphrases is not supported at all on this installation, or the "
"hashing method requested by E<.Fa setting> is not supported.  These error "
"codes are not used by this version of libcrypt, but may be encountered on "
"other systems."
msgstr ""
"Le hachage de mots de passe ou la méthode de hachage spécifiée par E<.Fa "
"paramètres> ne sont pas pris en charge par cette installation. Ces codes "
"d'erreur ne sont pas utilisés par cette version de libcrypt, mais ils "
"peuvent l'être sur d'autres systèmes."

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "PORTABILITY NOTES"
msgstr "NOTES DE PORTABILITÉ"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"E<.Nm crypt> is included in POSIX, but E<.Nm crypt_r>, E<.Nm crypt_rn>, and "
"E<.Nm crypt_ra> are not part of any standard."
msgstr ""
"E<.Nm crypt> est incluse dans POSIX, mais E<.Nm crypt_r>, E<.Nm crypt_rn> et "
"E<.Nm crypt_ra> n'appartiennent à aucune norme."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"POSIX does not specify any hashing methods, and does not require hashed "
"passphrases to be portable between systems.  In practice, hashed passphrases "
"are portable as long as both systems support the hashing method that was "
"used.  However, the set of supported hashing methods varies considerably "
"from system to system."
msgstr ""
"POSIX ne spécifie aucune méthode de hachage et ne requiert pas la "
"portabilité des mots de passe condensés entre les différents systèmes. En "
"pratique, les mots de passe condensés sont portables entre deux systèmes à "
"partir du moment où ces derniers prennent en charge la méthode de hachage "
"qui a été utilisée. Cependant, le jeu de méthodes de hachage prises en "
"charge varie considérablement d'un système à l'autre."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The behavior of E<.Nm crypt> on errors isn't well standardized.  Some "
"implementations simply can't fail (except by crashing the program), others "
"return a null pointer or a fixed string.  Most implementations don't set E<."
"Va errno>, but some do.  POSIX specifies returning a null pointer and "
"setting E<.Va errno>, but it defines only one possible error, E<.Er ENOSYS>, "
"in the case where E<.Nm crypt> is not supported at all.  Some older "
"applications are not prepared to handle null pointers returned by E<.Nm "
"crypt>.  The behavior described above for this implementation, setting E<.Va "
"errno> and returning an invalid hashed passphrase different from E<.Fa "
"setting>, is chosen to make these applications fail closed when an error "
"occurs."
msgstr ""
"Le comportement de E<.Nm crypt> en cas d'erreur n'est pas bien normalisé. "
"Certaines implémentations n'ont tout simplement pas prévu d'échouer (hormis "
"en plantant le programme), alors que d'autres renvoient un pointeur NULL ou "
"une chaîne prédéfinie. Certaines implémentations définissent E<.Va errno>, "
"mais la plupart ne le font pas. POSIX préconise de renvoyer un pointeur NULL "
"et de définir E<.Va errno>, mais il ne définit qu'une erreur possible, E<.Er "
"ENOSYS>, dans le cas où E<.Nm crypt> n'est pas du tout pris en charge. "
"Certaines applications plus anciennes n'ont pas été conçues pour gérer les "
"pointeurs NULL renvoyés par E<.Nm crypt>. On choisit alors le comportement "
"décrit plus haut pour cette implémentation, à savoir définir E<.Va errno> et "
"renvoyer un mot de passe condensé non valable et différent de E<.Fa "
"paramètres>, de façon à ce que ces applications échouent en se terminant "
"lorsqu'une erreur survient."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Due to historical restrictions on the export of cryptographic software from "
"the USA, E<.Nm crypt> is an optional POSIX component.  Applications should "
"therefore be prepared for E<.Nm crypt> not to be available, or to always "
"fail (setting E<.Va errno> to E<.Er ENOSYS>)  at runtime."
msgstr ""
"Suite aux restrictions historiques à l'exportation des logiciels "
"cryptographiques depuis les USA, E<.Nm crypt> est un composant POSIX "
"optionnel. Les applications doivent donc prévoir l'éventualité que E<.Nm "
"crypt> ne soit pas disponible ou échoue systématiquement à l'exécution (en "
"définissant E<.Va errno> à E<.Er ENOSYS>)."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "POSIX specifies that E<.Nm crypt> is declared in"
msgstr "POSIX spécifie que E<.Nm crypt> est déclaré dans"

#. type: In
#: debian-bookworm debian-unstable
#, no-wrap
msgid "unistd.h ,"
msgstr "unistd.h,"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"but only if the macro E<.Dv _XOPEN_CRYPT> is defined and has a value greater "
"than or equal to zero.  Since libcrypt does not provide"
msgstr ""
"mais seulement si la macro E<.Dv _XOPEN_CRYPT> est définie et si sa valeur "
"est supérieure ou égale à zéro. Comme libcrypt ne fournit pas"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"it declares E<.Nm crypt>, E<.Nm crypt_r>, E<.Nm crypt_rn>, and E<.Nm "
"crypt_ra> in"
msgstr ""
"elle déclare E<.Nm crypt>, E<.Nm crypt_r>, E<.Nm crypt_rn> et E<.Nm "
"crypt_ra> dans"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "instead."
msgstr "à la place."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"On a minority of systems (notably recent versions of Solaris), E<.Nm crypt> "
"uses a thread-specific static storage buffer, which makes it safe to call "
"from multiple threads simultaneously, but does not prevent each call within "
"a thread from overwriting the results of the previous one."
msgstr ""
"Sur une minorité de systèmes (en particulier les versions récentes de "
"Solaris), E<.Nm crypt> utilise un tampon mémoire statique spécifique aux "
"threads qui lui permet d'être appelée sans danger depuis plusieurs threads "
"simultanément, mais n'empêche pas chaque appel depuis un thread d'écraser "
"les résultats de l'appel précédent."

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Some implementations of E<.Nm crypt>, upon error, return an invalid hash "
"that is stored in a read-only location or only initialized once, which means "
"that it is only safe to erase the buffer pointed to by the E<.Nm crypt> "
"return value if an error did not occur."
msgstr ""
"En cas d'erreur, certaines implémentations de E<.Nm crypt> renvoient un "
"condensé non valable qui est stocké dans une zone en lecture seule ou "
"seulement initialisé une fois, ce qui signifie que l'on ne peut supprimer "
"sans danger le tampon pointé par la valeur de retour de E<.Nm crypt> que si "
"aucune erreur n'est survenue."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"E<.Vt \"struct crypt_data\"> may be quite large (32kB in this implementation "
"of libcrypt; over 128kB in some other implementations).  This is large "
"enough that it may be unwise to allocate it on the stack."
msgstr ""
"E<.Vt \"struct crypt_data\"> peut avoir une taille assez importante (32ko "
"dans cette implémentation de libcrypt ; plus de 128ko dans certaines autres "
"implémentations). Cette taille est suffisamment importante pour qu'il soit "
"malavisé de l'allouer dans la pile."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Some recently designed hashing methods need even more scratch memory, but "
"the E<.Nm crypt_r> interface makes it impossible to change the size of E<.Vt "
"\"struct crypt_data\"> without breaking binary compatibility.  The E<.Nm "
"crypt_rn> interface could accommodate larger allocations for specific "
"hashing methods, but the caller of E<.Nm crypt_rn> has no way of knowing how "
"much memory to allocate.  E<.Nm crypt_ra> does the allocation itself, but "
"can only make a single call to E<.Xr malloc 3>."
msgstr ""
"Certaines méthodes de hachage récentes nécessitent encore plus de mémoire de "
"travail, mais l'interface E<.Nm crypt_r> rend impossible de modifier la "
"taille de E<.Vt \"struct crypt_data\"> sans casser la compatibilité binaire. "
"L'interface E<.Nm crypt_rn> pourrait accorder plus de mémoire pour certaines "
"méthodes de hachage spécifiques, mais l'appelant de E<.Nm crypt_rn> n'a "
"aucun moyen de connaître la quantité de mémoire à allouer. E<.Nm crypt_ra> "
"effectue l'allocation de mémoire elle-même, mais ne peut effectuer qu'un "
"seul appel à E<.Xr malloc 3>."

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"For an explanation of the terms used in this section, see E<.Xr attributes "
"7>."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter E<.Xr "
"attributes 7>."

#. type: tbl table
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: debian-bookworm debian-unstable
#, no-wrap
msgid ".Nm crypt\n"
msgstr ".Nm crypt\n"

#. type: tbl table
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: debian-bookworm debian-unstable
#, no-wrap
msgid "MT-Unsafe race:crypt"
msgstr "MT-Unsafe race:crypt"

#. type: tbl table
#: debian-bookworm debian-unstable
#, no-wrap
msgid ".Nm crypt_r ,\n"
msgstr ".Nm crypt_r ,\n"

#. type: tbl table
#: debian-bookworm debian-unstable
#, no-wrap
msgid ".Nm crypt_rn ,\n"
msgstr ".Nm crypt_rn ,\n"

#. type: tbl table
#: debian-bookworm debian-unstable
#, no-wrap
msgid ".Nm crypt_ra\n"
msgstr ".Nm crypt_ra\n"

#. type: tbl table
#: debian-bookworm debian-unstable
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"A rotor-based E<.Nm crypt> function appeared in E<.At v6>.  The E<.Dq "
"traditional> DES-based E<.Nm crypt> first appeared in E<.At v7>."
msgstr ""
"Une fonction E<.Nm crypt> s'inspirant des machines à rotor est apparue dans "
"E<.At v6>. La fonction E<.Nm crypt> E<.Dq traditionnelle> basée sur DES est "
"quant à elle apparue dans E<.At v7>."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"E<.Nm crypt_r> originates with the GNU C Library.  There's also a E<.Nm "
"crypt_r> function on HP-UX and MKS Toolkit, but the prototypes and semantics "
"differ."
msgstr ""
"E<.Nm crypt_r> trouve ses origines dans la bibliothèque GNU C. Il existe "
"aussi une fonction E<.Nm crypt_r> sur HP-UX et dans la boîte à outils MKS, "
"mais leurs prototype et sémantique diffèrent."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"E<.Nm crypt_rn> and E<.Nm crypt_ra> originate with the Openwall project."
msgstr ""
"E<.Nm crypt_rn> et E<.Nm crypt_ra> trouvent leur origine dans le projet "
"Openwall."

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"E<.Xr crypt_gensalt 3>, E<.Xr getpass 3>, E<.Xr getpwent 3>, E<.Xr shadow "
"3>, E<.Xr login 1>, E<.Xr passwd 1>, E<.Xr crypt 5>, E<.Xr passwd 5>, E<.Xr "
"shadow 5>, E<.Xr pam 8>"
msgstr ""
"E<.Xr crypt_gensalt 3>, E<.Xr getpass 3>, E<.Xr getpwent 3>, E<.Xr shadow "
"3>, E<.Xr login 1>, E<.Xr passwd 1>, E<.Xr crypt 5>, E<.Xr passwd 5>, E<.Xr "
"shadow 5>, E<.Xr pam 8>"

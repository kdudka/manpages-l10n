# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013, 2014.
# Denis Barbier <barbier@debian.org>, 2006, 2010, 2011.
# David Prévot <david@tilapin.org>, 2010, 2012-2014.
# Jean-Philippe MENGUAL <jpmengual@debian.org>, 2020-2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.22.0\n"
"POT-Creation-Date: 2024-05-01 15:42+0200\n"
"PO-Revision-Date: 2024-04-17 17:24+0200\n"
"Last-Translator: Jean-Philippe MENGUAL <jpmengual@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ioctl_ns"
msgstr "ioctl_ns"

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2024-03-03"
msgstr "3 mars 2024"

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr "Pages du manuel de Linux 6.7"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "ioctl_ns - ioctl() operations for Linux namespaces"
msgstr "ioctl_ns - opérations ioctl() pour les espaces de noms Linux"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#.  ============================================================
#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Discovering namespace relationships"
msgstr "Recherche des relations entre les espaces de noms"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The following B<ioctl>(2)  operations are provided to allow discovery of "
"namespace relationships (see B<user_namespaces>(7)  and "
"B<pid_namespaces>(7)).  The form of the calls is:"
msgstr ""
"Les opérations B<ioctl>(2) suivantes sont fournies pour permettre la "
"recherche de relations entre espaces de noms (voir B<user_namespaces>(7) et "
"B<pid_namespaces>(7)). La forme des appels est :"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "new_fd = ioctl(fd, op);\n"
msgstr "new_fd = ioctl(fd, op);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In each case, I<fd> refers to a I</proc/>pidI</ns/*> file.  Both operations "
"return a new file descriptor on success."
msgstr ""
"Dans tous les cas, I<fd> renvoie au fichier I</proc/>pidI</ns/*>. Les deux "
"opérations renvoient un nouveau descripteur de fichier en cas de succès."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<NS_GET_USERNS> (since Linux 4.9)"
msgstr "B<NS_GET_USERNS> (depuis Linux 4.9)"

#.  commit bcac25a58bfc6bd79191ac5d7afb49bea96da8c9
#.  commit 6786741dbf99e44fb0c0ed85a37582b8a26f1c3b
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Returns a file descriptor that refers to the owning user namespace for the "
"namespace referred to by I<fd>."
msgstr ""
"Renvoie un descripteur de fichier qui se rapporte à l'espace de noms de "
"l'utilisateur propriétaire pour l'espace de noms auquel renvoie I<fd>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<NS_GET_PARENT> (since Linux 4.9)"
msgstr "B<NS_GET_PARENT> (depuis Linux 4.9)"

#.  commit a7306ed8d94af729ecef8b6e37506a1c6fc14788
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Returns a file descriptor that refers to the parent namespace of the "
"namespace referred to by I<fd>.  This operation is valid only for "
"hierarchical namespaces (i.e., PID and user namespaces).  For user "
"namespaces, B<NS_GET_PARENT> is synonymous with B<NS_GET_USERNS>."
msgstr ""
"Renvoie un descripteur de fichier qui se rapporte à l'espace de noms parent "
"de l'espace auquel renvoie I<fd>. Cette opération n'est valable que pour des "
"espaces de noms hiérarchiques (à savoir les espaces de noms de PID et "
"d'utilisateur). Pour les espaces de noms utilisateur, B<NS_GET_PARENT> est "
"synonyme de B<NS_GET_USERNS>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The new file descriptor returned by these operations is opened with the "
"B<O_RDONLY> and B<O_CLOEXEC> (close-on-exec; see B<fcntl>(2))  flags."
msgstr ""
"Le nouveau descripteur de fichier renvoyé par ces opérations est ouvert avec "
"les attributs B<O_RDONLY> et B<O_CLOEXEC> (close-on-exec ; voir B<fcntl>(2))."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"By applying B<fstat>(2)  to the returned file descriptor, one obtains a "
"I<stat> structure whose I<st_dev> (resident device) and I<st_ino> (inode "
"number) fields together identify the owning/parent namespace.  This inode "
"number can be matched with the inode number of another I</proc/>pidI</ns/"
">{I<pid>,I<user>} file to determine whether that is the owning/parent "
"namespace."
msgstr ""
"En appliquant B<fstat>(2) au descripteur de fichier renvoyé, vous obtenez "
"une structure I<stat> où les champs I<st_dev> (le périphérique résident) et "
"I<st_ino> (le numéro d'inœud) identifient à la fois l'espace de noms du "
"propriétaire et du parent. Ce numéro d'inœud peut être mis en correspondance "
"avec le numéro d'inœud d'un autre fichier I</proc/>pidI</ns/>{I<pid>,"
"I<user>} pour savoir s'il s'agit de l'espace de noms du propriétaire ou du "
"parent."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Either of these B<ioctl>(2)  operations can fail with the following errors:"
msgstr ""
"Chacune de ces opérations B<ioctl>(2) peut échouer avec les erreurs "
"suivantes :"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The requested namespace is outside of the caller's namespace scope.  This "
"error can occur if, for example, the owning user namespace is an ancestor of "
"the caller's current user namespace.  It can also occur on attempts to "
"obtain the parent of the initial user or PID namespace."
msgstr ""
"L'espace de noms demandé va au-delà de la cible de l'espace de noms de "
"l'appelant. Cette erreur peut arriver si, par exemple, l'espace de noms du "
"propriétaire est un ancêtre de l'espace de noms de l'utilisateur actuel de "
"l'appelant. Elle peut aussi survenir en cas de tentative d'obtenir le parent "
"de l'espace de noms de l'utilisateur initial ou du PID."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTTY>"
msgstr "B<ENOTTY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The operation is not supported by this kernel version."
msgstr "L'opération n'est pas prise en charge par cette version du noyau."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Additionally, the B<NS_GET_PARENT> operation can fail with the following "
"error:"
msgstr ""
"De plus, l'opération B<NS_GET_PARENT> peut échouer avec l'erreur suivante :"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<fd> refers to a nonhierarchical namespace."
msgstr "I<fd> se rapporte à un espace de noms non hiérarchique."

#.  ============================================================
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "See the EXAMPLES section for an example of the use of these operations."
msgstr ""
"Voir la section EXEMPLE pour un exemple d'utilisation de ces opérations."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Discovering the namespace type"
msgstr "Recherche du type d'espace de noms"

#.  commit e5ff5ce6e20ee22511398bb31fb912466cf82a36
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<NS_GET_NSTYPE> operation (available since Linux 4.11) can be used to "
"discover the type of namespace referred to by the file descriptor I<fd>:"
msgstr ""
"L'opération B<NS_GET_NSTYPE> (disponible depuis Linux 4.11) peut être "
"utilisée pour rechercher le type d'espace de noms auquel se rapporte le "
"descripteur de fichier I<fd> :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "nstype = ioctl(fd, NS_GET_NSTYPE);\n"
msgstr "nstype = ioctl(fd, NS_GET_NSTYPE);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<fd> refers to a I</proc/>pidI</ns/*> file."
msgstr "I<fd> renvoie à un fichier I</proc/>pidI</ns/*>."

#.  ============================================================
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The return value is one of the B<CLONE_NEW*> values that can be specified to "
"B<clone>(2)  or B<unshare>(2)  in order to create a namespace."
msgstr ""
"La valeur de renvoi est une des valeurs de B<CLONE_NEW*>, qui peut être "
"indiquée à B<clone>(2) ou à B<unshare>(2) afin de créer un espace de noms."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Discovering the owner of a user namespace"
msgstr "Rechercher le propriétaire d'un espace de noms utilisateur"

#.  commit 015bb305b8ebe8d601a238ab70ebdc394c7a19ba
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<NS_GET_OWNER_UID> operation (available since Linux 4.11) can be used "
"to discover the owner user ID of a user namespace (i.e., the effective user "
"ID of the process that created the user namespace).  The form of the call is:"
msgstr ""
"L'opération B<NS_GET_OWNER_UID> (disponible depuis Linux 4.11) peut être "
"utilisée pour rechercher l'identifiant de l'utilisateur propriétaire d'un "
"espace de noms utilisateur (c'est-à-dire l'identifiant de l'utilisateur réel "
"du processus qui a créé l'espace de noms utilisateur). La forme de l'appel "
"est :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"uid_t uid;\n"
"ioctl(fd, NS_GET_OWNER_UID, &uid);\n"
msgstr ""
"uid_t uid;\n"
"ioctl(fd, NS_GET_OWNER_UID, &uid);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<fd> refers to a I</proc/>pidI</ns/user> file."
msgstr "I<fd> renvoie à un fichier I</proc/>pidI</ns/user>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The owner user ID is returned in the I<uid_t> pointed to by the third "
"argument."
msgstr ""
"L'identifiant de l'utilisateur propriétaire est renvoyé dans l'I<uid_t> vers "
"lequel pointe le troisième paramètre."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "This operation can fail with the following error:"
msgstr "Cette opération peut échouer avec l'erreur suivante :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<fd> does not refer to a user namespace."
msgstr "I<fd> ne se rapporte pas à un espace de noms utilisateur."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Any of the above B<ioctl>()  operations can return the following errors:"
msgstr ""
"Chacune des opérations B<ioctl>() ci-dessus peut renvoyer les erreurs "
"suivantes :"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<fd> does not refer to a I</proc/>pidI</ns/>* file."
msgstr "I<fd> ne renvoie pas à un fichier I</proc/>pidI</ns/>*."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The example shown below uses the B<ioctl>(2)  operations described above to "
"perform simple discovery of namespace relationships.  The following shell "
"sessions show various examples of the use of this program."
msgstr ""
"L'exemple présenté ci-dessous utilise les opérations B<ioctl>(2) décrites ci-"
"dessus pour effectuer une simple recherche de relations entre espaces de "
"noms. Les sessions d'interpréteur de commandes suivantes montrent divers "
"exemples de l'utilisation de ce programme."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Trying to get the parent of the initial user namespace fails, since it has "
"no parent:"
msgstr ""
"L'essai pour obtenir le parent de l'espace de noms utilisateur initial "
"échoue, car il n'a pas de parent :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<./ns_show /proc/self/ns/user p>\n"
"The parent namespace is outside your namespace scope\n"
msgstr ""
"$ B<./ns_show /proc/self/ns/user p>\n"
"L'espace de noms du parent dépasse la cible de votre espace utilisateur\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Create a process running B<sleep>(1)  that resides in new user and UTS "
"namespaces, and show that the new UTS namespace is associated with the new "
"user namespace:"
msgstr ""
"Créer un processus qui exécute B<sleep>(1) dans les nouveaux espaces de noms "
"de l'utilisateur et UTS, et indiquer que le nouvel espace de noms UTS est "
"associé au nouvel espace de noms de l'utilisateur :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<unshare -Uu sleep 1000 &>\n"
"[1] 23235\n"
"$ B<./ns_show /proc/23235/ns/uts u>\n"
"Device/Inode of owning user namespace is: [0,3] / 4026532448\n"
"$ B<readlink /proc/23235/ns/user>\n"
"user:[4026532448]\n"
msgstr ""
"$ B<unshare -Uu sleep 1000 &>\n"
"[1] 23235\n"
"$ B<./ns_show /proc/23235/ns/uts u>\n"
"Le périphérique/inœud de l'espace de noms de l'utilisateur propriétaire\n"
"est : [0,3] / 4026532448\n"
"$ B<readlink /proc/23235/ns/user>\n"
"utilisateur :[4026532448]\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Then show that the parent of the new user namespace in the preceding example "
"is the initial user namespace:"
msgstr ""
"Puis indiquer que le parent du nouvel espace de noms utilisateur de "
"l'exemple précédent est dans l'espace de noms de l'utilisateur initial :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<readlink /proc/self/ns/user>\n"
"user:[4026531837]\n"
"$ B<./ns_show /proc/23235/ns/user p>\n"
"Device/Inode of parent namespace is: [0,3] / 4026531837\n"
msgstr ""
"$ B<readlink /proc/self/ns/user>\n"
"utilisateur:[4026531837]\n"
"$ B<./ns_show /proc/23235/ns/user p>\n"
"Le périphérique/inœud de l'espace de noms parent est : [0,3] / 4026531837\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Start a shell in a new user namespace, and show that from within this shell, "
"the parent user namespace can't be discovered.  Similarly, the UTS namespace "
"(which is associated with the initial user namespace)  can't be discovered."
msgstr ""
"Démarrer un interpréteur de commandes dans un nouvel espace de noms "
"utilisateur et indiquer que depuis cet interpréteur, l'espace de noms parent "
"de l'utilisateur n'a pas pu être trouvé. De même, l'espace de nom UTS (qui "
"est associé à l'espace de noms utilisateur initial) n'a pas pu être trouvé."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<PS1=\"sh2$ \" unshare -U bash>\n"
"sh2$ B<./ns_show /proc/self/ns/user p>\n"
"The parent namespace is outside your namespace scope\n"
"sh2$ B<./ns_show /proc/self/ns/uts u>\n"
"The owning user namespace is outside your namespace scope\n"
msgstr ""
"$ B<PS1=\"sh2$ \" unshare -U bash>\n"
"sh2$ B<./ns_show /proc/self/ns/user p>\n"
"L'espace de noms parent dépasse la cible de votre espace de noms\n"
"sh2$ B<./ns_show /proc/self/ns/uts u>\n"
"L'espace de noms de l'utilisateur propriétaire dépasse la cible de votre espace de noms\n"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr "Source du programme"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"/* ns_show.c\n"
"\\&\n"
"   Licensed under the GNU General Public License v2 or later.\n"
"*/\n"
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>linux/nsfs.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"#include E<lt>sys/ioctl.hE<gt>\n"
"#include E<lt>sys/stat.hE<gt>\n"
"#include E<lt>sys/sysmacros.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int          fd, userns_fd, parent_fd;\n"
"    struct stat  sb;\n"
"\\&\n"
"    if (argc E<lt> 2) {\n"
"        fprintf(stderr, \"Usage: %s /proc/[pid]/ns/[file] [p|u]\\en\",\n"
"                argv[0]);\n"
"        fprintf(stderr, \"\\enDisplay the result of one or both \"\n"
"                \"of NS_GET_USERNS (u) or NS_GET_PARENT (p)\\en\"\n"
"                \"for the specified /proc/[pid]/ns/[file]. If neither \"\n"
"                \"\\[aq]p\\[aq] nor \\[aq]u\\[aq] is specified,\\en\"\n"
"                \"NS_GET_USERNS is the default.\\en\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    /* Obtain a file descriptor for the \\[aq]ns\\[aq] file specified\n"
"       in argv[1]. */\n"
"\\&\n"
"    fd = open(argv[1], O_RDONLY);\n"
"    if (fd == -1) {\n"
"        perror(\"open\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    /* Obtain a file descriptor for the owning user namespace and\n"
"       then obtain and display the inode number of that namespace. */\n"
"\\&\n"
"    if (argc E<lt> 3 || strchr(argv[2], \\[aq]u\\[aq])) {\n"
"        userns_fd = ioctl(fd, NS_GET_USERNS);\n"
"\\&\n"
"        if (userns_fd == -1) {\n"
"            if (errno == EPERM)\n"
"                printf(\"The owning user namespace is outside \"\n"
"                       \"your namespace scope\\en\");\n"
"            else\n"
"               perror(\"ioctl-NS_GET_USERNS\");\n"
"            exit(EXIT_FAILURE);\n"
"         }\n"
"\\&\n"
"        if (fstat(userns_fd, &sb) == -1) {\n"
"            perror(\"fstat-userns\");\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
"        printf(\"Device/Inode of owning user namespace is: \"\n"
"               \"[%x,%x] / %ju\\en\",\n"
"               major(sb.st_dev),\n"
"               minor(sb.st_dev),\n"
"               (uintmax_t) sb.st_ino);\n"
"\\&\n"
"        close(userns_fd);\n"
"    }\n"
"\\&\n"
"    /* Obtain a file descriptor for the parent namespace and\n"
"       then obtain and display the inode number of that namespace. */\n"
"\\&\n"
"    if (argc E<gt> 2 && strchr(argv[2], \\[aq]p\\[aq])) {\n"
"        parent_fd = ioctl(fd, NS_GET_PARENT);\n"
"\\&\n"
"        if (parent_fd == -1) {\n"
"            if (errno == EINVAL)\n"
"                printf(\"Can\\[aq] get parent namespace of a \"\n"
"                       \"nonhierarchical namespace\\en\");\n"
"            else if (errno == EPERM)\n"
"                printf(\"The parent namespace is outside \"\n"
"                       \"your namespace scope\\en\");\n"
"            else\n"
"                perror(\"ioctl-NS_GET_PARENT\");\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
"\\&\n"
"        if (fstat(parent_fd, &sb) == -1) {\n"
"            perror(\"fstat-parentns\");\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
"        printf(\"Device/Inode of parent namespace is: [%x,%x] / %ju\\en\",\n"
"               major(sb.st_dev),\n"
"               minor(sb.st_dev),\n"
"               (uintmax_t) sb.st_ino);\n"
"\\&\n"
"        close(parent_fd);\n"
"    }\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"/* ns_show.c\n"
"\\&\n"
"   Sous licence GNU General Public v2 ou supérieure.\n"
"*/\n"
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>linux/nsfs.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"#include E<lt>sys/ioctl.hE<gt>\n"
"#include E<lt>sys/stat.hE<gt>\n"
"#include E<lt>sys/sysmacros.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int          fd, userns_fd, parent_fd;\n"
"    struct stat  sb;\n"
"\\&\n"
"    if (argc E<lt> 2) {\n"
"        fprintf(stderr, \"Usage: %s /proc/[pid]/ns/[file] [p|u]\\en\",\n"
"                argv[0]);\n"
"        fprintf(stderr, \"\\enAfficher le résultat d'un ou plusieurs des \"\n"
"                \"NS_GET_USERNS (u) ou NS_GET_PARENT (p)\\en\"\n"
"                \"pour le /proc/[pid]/ns/[file] indiqué. Si ni \"\n"
"                \"\\[aq]p\\[aq] ni \\[aq]u\\[aq] ne sont indiqués,\\en\"\n"
"                \"NS_GET_USERNS l'est par défaut.\\en\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    /* Obtenir un descripteur de fichier pour le fichier \\[aq]ns\\[aq] indiqué\n"
"       dans argv[1]. */\n"
"       in argv[1]. */\n"
"\\&\n"
"    fd = open(argv[1], O_RDONLY);\n"
"    if (fd == -1) {\n"
"        perror(\"open\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    /* Obtenir un descripteur de fichier pour l'espace de noms de l'utilisateur\n"
"       propriétaire et obtenir et afficher le numéro d'inœud de cet espace de\n"
"       noms. */\n"
"\\&\n"
"    if (argc E<lt> 3 || strchr(argv[2], \\[aq]u\\[aq])) {\n"
"        userns_fd = ioctl(fd, NS_GET_USERNS);\n"
"\\&\n"
"        if (userns_fd == -1) {\n"
"            if (errno == EPERM)\n"
"                printf(\"L'espace de noms de l'utilisateur propriétaire dépasse \"\n"
"                        \"la cible de votre espace de noms\\en\");\n"
"            else\n"
"               perror(\"ioctl-NS_GET_USERNS\");\n"
"            exit(EXIT_FAILURE);\n"
"         }\n"
"\\&\n"
"        if (fstat(userns_fd, &sb) == -1) {\n"
"            perror(\"fstat-userns\");\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
"        printf(\"Le périphérique/inœud de l'espace de noms de l'utilisateur propriétaire est : \"\n"
"               \"[%x,%x] / %ju\\en\",\n"
"               major(sb.st_dev),\n"
"               minor(sb.st_dev),\n"
"               (uintmax_t) sb.st_ino);\n"
"\\&\n"
"        close(userns_fd);\n"
"    }\n"
"\\&\n"
"    /* Obtenir un descripteur de fichier pour l'espace de noms parent et\n"
"       obtenir et afficher le numéro d'inœud de cet espace de noms. */\n"
"\\&\n"
"    if (argc E<gt> 2 && strchr(argv[2], \\[aq]p\\[aq])) {\n"
"        parent_fd = ioctl(fd, NS_GET_PARENT);\n"
"\\&\n"
"        if (parent_fd == -1) {\n"
"            if (errno == EINVAL)\n"
"                printf(\"Ne peut pas obtenir l'espace de noms parent d'un \"\n"
"                        \"espace de noms non hiérarchique\\en\");\n"
"            else if (errno == EPERM)\n"
"                printf(\"L'espace de noms parent dépasse \"\n"
"                        \"la cible de votre espace de noms\\en\");\n"
"            else\n"
"                perror(\"ioctl-NS_GET_PARENT\");\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
"\\&\n"
"        if (fstat(parent_fd, &sb) == -1) {\n"
"            perror(\"fstat-parentns\");\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
"        printf(\"Le périphérique/inœud de l'espace de noms parent est : [%x,%x] / %ju\\en\",\n"
"               major(sb.st_dev),\n"
"               minor(sb.st_dev),\n"
"               (uintmax_t) sb.st_ino);\n"
"\\&\n"
"        close(parent_fd);\n"
"    }\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<fstat>(2), B<ioctl>(2), B<proc>(5), B<namespaces>(7)"
msgstr "B<fstat>(2), B<ioctl>(2), B<proc>(5), B<namespaces>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm fedora-40 mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "new_fd = ioctl(fd, request);\n"
msgstr "new_fd = ioctl(fd, request);\n"

#. type: Plain text
#: debian-bookworm
msgid "I<fd> does not refer to a I</proc/[pid]/ns/*> file."
msgstr "I<fd> ne renvoie pas à un fichier I</proc/[pid]/ns/*>."

#. type: Plain text
#: debian-bookworm
msgid ""
"Namespaces and the operations described on this page are a Linux-specific."
msgstr ""
"Les espaces de noms et les opérations décrits sur cette page sont "
"spécifiques à Linux."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "/* ns_show.c\n"
msgstr "/* ns_show.c\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"   Licensed under the GNU General Public License v2 or later.\n"
"*/\n"
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>linux/nsfs.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"#include E<lt>sys/ioctl.hE<gt>\n"
"#include E<lt>sys/stat.hE<gt>\n"
"#include E<lt>sys/sysmacros.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""
"   Sous licence GNU General Public v2 ou supérieure.\n"
"*/\n"
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>linux/nsfs.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"#include E<lt>sys/ioctl.hE<gt>\n"
"#include E<lt>sys/stat.hE<gt>\n"
"#include E<lt>sys/sysmacros.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int          fd, userns_fd, parent_fd;\n"
"    struct stat  sb;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int          fd, userns_fd, parent_fd;\n"
"    struct stat  sb;\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    if (argc E<lt> 2) {\n"
"        fprintf(stderr, \"Usage: %s /proc/[pid]/ns/[file] [p|u]\\en\",\n"
"                argv[0]);\n"
"        fprintf(stderr, \"\\enDisplay the result of one or both \"\n"
"                \"of NS_GET_USERNS (u) or NS_GET_PARENT (p)\\en\"\n"
"                \"for the specified /proc/[pid]/ns/[file]. If neither \"\n"
"                \"\\[aq]p\\[aq] nor \\[aq]u\\[aq] is specified,\\en\"\n"
"                \"NS_GET_USERNS is the default.\\en\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (argc E<lt> 2) {\n"
"        fprintf(stderr, \"Utilisation : %s /proc/[pid]/ns/[file] [p|u]\\en\",\n"
"                argv[0]);\n"
"        fprintf(stderr, \"\\enAfficher le résultat d'un ou plusieurs des \"\n"
"                \"NS_GET_USERNS (u) ou NS_GET_PARENT (p)\\en\"\n"
"                \"pour le /proc/[pid]/ns/[file] indiqué. Si ni \"\n"
"                \"\\[aq]p\\[aq] ni \\[aq]u\\[aq] ne sont indiqués,\\en\"\n"
"                \"NS_GET_USERNS l'est par défaut.\\en\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    /* Obtain a file descriptor for the \\[aq]ns\\[aq] file specified\n"
"       in argv[1]. */\n"
msgstr ""
"    /* Obtenir un descripteur de fichier pour le fichier \\[aq]ns\\[aq] indiqué\n"
"       dans argv[1]. */\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    fd = open(argv[1], O_RDONLY);\n"
"    if (fd == -1) {\n"
"        perror(\"open\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    fd = open(argv[1], O_RDONLY);\n"
"    if (fd == -1) {\n"
"        perror(\"open\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    /* Obtain a file descriptor for the owning user namespace and\n"
"       then obtain and display the inode number of that namespace. */\n"
msgstr ""
"    /* Obtenir un descripteur de fichier pour l'espace de noms de l'utilisateur\n"
"       propriétaire et obtenir et afficher le numéro d'inœud de cet espace de\n"
"       noms. */\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    if (argc E<lt> 3 || strchr(argv[2], \\[aq]u\\[aq])) {\n"
"        userns_fd = ioctl(fd, NS_GET_USERNS);\n"
msgstr ""
"    if (argc E<lt> 3 || strchr(argv[2], \\[aq]u\\[aq])) {\n"
"        userns_fd = ioctl(fd, NS_GET_USERNS);\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"        if (userns_fd == -1) {\n"
"            if (errno == EPERM)\n"
"                printf(\"The owning user namespace is outside \"\n"
"                       \"your namespace scope\\en\");\n"
"            else\n"
"               perror(\"ioctl-NS_GET_USERNS\");\n"
"            exit(EXIT_FAILURE);\n"
"         }\n"
msgstr ""
"        if (userns_fd == -1) {\n"
"            if (errno == EPERM)\n"
"                printf(\"L'espace de noms de l'utilisateur propriétaire dépasse \"\n"
"                        \"la cible de votre espace de noms\\en\");\n"
"            else\n"
"               perror(\"ioctl-NS_GET_USERNS\");\n"
"            exit(EXIT_FAILURE);\n"
"         }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"        if (fstat(userns_fd, &sb) == -1) {\n"
"            perror(\"fstat-userns\");\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
"        printf(\"Device/Inode of owning user namespace is: \"\n"
"               \"[%x,%x] / %ju\\en\",\n"
"               major(sb.st_dev),\n"
"               minor(sb.st_dev),\n"
"               (uintmax_t) sb.st_ino);\n"
msgstr ""
"        if (fstat(userns_fd, &sb) == -1) {\n"
"            perror(\"fstat-userns\");\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
"        printf(\"Le périphérique/inœud de l'espace de noms de l'utilisateur propriétaire est : \"\n"
"               \"[%x,%x] / %ju\\en\",\n"
"               major(sb.st_dev),\n"
"               minor(sb.st_dev),\n"
"               (uintmax_t) sb.st_ino);\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"        close(userns_fd);\n"
"    }\n"
msgstr ""
"        close(userns_fd);\n"
"     }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    /* Obtain a file descriptor for the parent namespace and\n"
"       then obtain and display the inode number of that namespace. */\n"
msgstr ""
"    /* Obtenir un descripteur de fichier pour l'espace de noms parent et\n"
"       obtenir et afficher le numéro d'inœud de cet espace de noms. */\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    if (argc E<gt> 2 && strchr(argv[2], \\[aq]p\\[aq])) {\n"
"        parent_fd = ioctl(fd, NS_GET_PARENT);\n"
msgstr ""
"    if (argc E<gt> 2 && strchr(argv[2], \\[aq]p\\[aq])) {\n"
"        parent_fd = ioctl(fd, NS_GET_PARENT);\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"        if (parent_fd == -1) {\n"
"            if (errno == EINVAL)\n"
"                printf(\"Can\\[aq] get parent namespace of a \"\n"
"                       \"nonhierarchical namespace\\en\");\n"
"            else if (errno == EPERM)\n"
"                printf(\"The parent namespace is outside \"\n"
"                       \"your namespace scope\\en\");\n"
"            else\n"
"                perror(\"ioctl-NS_GET_PARENT\");\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
msgstr ""
"        if (parent_fd == -1) {\n"
"            if (errno == EINVAL)\n"
"                printf(\"Ne peut pas obtenir l'espace de noms parent d'un \"\n"
"                        \"espace de noms non hiérarchique\\en\");\n"
"            else if (errno == EPERM)\n"
"                printf(\"L'espace de noms parent dépasse \"\n"
"                        \"la cible de votre espace de noms\\en\");\n"
"            else\n"
"                perror(\"ioctl-NS_GET_PARENT\");\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"        if (fstat(parent_fd, &sb) == -1) {\n"
"            perror(\"fstat-parentns\");\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
"        printf(\"Device/Inode of parent namespace is: [%x,%x] / %ju\\en\",\n"
"               major(sb.st_dev),\n"
"               minor(sb.st_dev),\n"
"               (uintmax_t) sb.st_ino);\n"
msgstr ""
"        if (fstat(parent_fd, &sb) == -1) {\n"
"            perror(\"fstat-parentns\");\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
"        printf(\"Le périphérique/inœud de l'espace de noms parent est : [%x,%x] / %ju\\en\",\n"
"               major(sb.st_dev),\n"
"               minor(sb.st_dev),\n"
"               (uintmax_t) sb.st_ino);\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"        close(parent_fd);\n"
"    }\n"
msgstr ""
"        close(parent_fd);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-04-03"
msgstr "3 avril 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pages du manuel de Linux 6.04"

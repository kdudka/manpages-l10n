# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013, 2014.
# Denis Barbier <barbier@debian.org>, 2006, 2010, 2011.
# David Prévot <david@tilapin.org>, 2010, 2012-2014.
# Jean-Philippe MENGUAL <jpmengual@debian.org>, 2020-2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-05-01 15:36+0200\n"
"PO-Revision-Date: 2024-01-232 08:55+0100\n"
"Last-Translator: Jean-Philippe MENGUAL <jpmengual@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "chroot"
msgstr "chroot"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr "Pages du manuel de Linux 6.7"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "chroot - change root directory"
msgstr "chroot - Modifier le répertoire racine"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int chroot(const char *>I<path>B<);>\n"
msgstr "B<int chroot(const char *>I<chemin>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consulter "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<chroot>():"
msgstr "B<chroot>()\\ :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.2.2:\n"
"        _XOPEN_SOURCE && ! (_POSIX_C_SOURCE E<gt>= 200112L)\n"
"            || /* Since glibc 2.20: */ _DEFAULT_SOURCE\n"
"            || /* glibc E<lt>= 2.19: */ _BSD_SOURCE\n"
"    Before glibc 2.2.2:\n"
"        none\n"
msgstr ""
"    Depuis la glibc 2.2.2 :\n"
"        _XOPEN_SOURCE && ! (_POSIX_C_SOURCE E<gt>= 200112L)\n"
"            || /* Depuis la glibc 2.20 : */ _DEFAULT_SOURCE\n"
"            || /* glibc E<lt>= 2.19 : */ _BSD_SOURCE\n"
"    Avant la glibc 2.2.2 :\n"
"        none\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<chroot>()  changes the root directory of the calling process to that "
"specified in I<path>.  This directory will be used for pathnames beginning "
"with I</>.  The root directory is inherited by all children of the calling "
"process."
msgstr ""
"B<chroot>() remplace le répertoire racine du processus appelant par celui "
"indiqué par le chemin I<chemin>. Ce répertoire sera utilisé comme origine "
"des chemins commençant par I</>. Tous les enfants du processus appelant "
"héritent du répertoire racine."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Only a privileged process (Linux: one with the B<CAP_SYS_CHROOT> capability "
"in its user namespace) may call B<chroot>()."
msgstr ""
"Seul un processus privilégié (sous Linux\\ : un processus ayant la capacité "
"B<CAP_SYS_CHROOT> dans son espace de noms d'utilisateur) peut appeler "
"B<chroot>()."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This call changes an ingredient in the pathname resolution process and does "
"nothing else.  In particular, it is not intended to be used for any kind of "
"security purpose, neither to fully sandbox a process nor to restrict "
"filesystem system calls.  In the past, B<chroot>()  has been used by daemons "
"to restrict themselves prior to passing paths supplied by untrusted users to "
"system calls such as B<open>(2).  However, if a folder is moved out of the "
"chroot directory, an attacker can exploit that to get out of the chroot "
"directory as well.  The easiest way to do that is to B<chdir>(2)  to the to-"
"be-moved directory, wait for it to be moved out, then open a path "
"like ../../../etc/passwd."
msgstr ""
"Cet appel modifie un élément du processus de résolution des chemins et ne "
"fait rien d'autre. En particulier, ce n'est pas conçu pour être utilisé à "
"des fins de sécurité, ou pour enfermer totalement un processus ou "
"restreindre les appels système du système de fichiers. Autrefois, "
"B<chroot>() était utilisé par des démons pour se restreindre avant de passer "
"des chemins fournis par des utilisateurs non fiables à des appels système "
"tels qu'B<open>(2). Toutefois, si un dossier est déplacé en dehors du "
"nouveau répertoire racine, un attaquant peut l'exploiter pour sortir lui "
"aussi du nouveau répertoire racine. La manière la plus facile de le faire "
"est de B<chdir>(2) vers le répertoire à déplacer, d'attendre qu'il soit "
"déplacé et d'ouvrir un chemin comme I<../../../etc/passwd>."

#.  This is how the "slightly trickier variation" works:
#.  https://github.com/QubesOS/qubes-secpack/blob/master/QSBs/qsb-014-2015.txt#L142
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A slightly trickier variation also works under some circumstances if "
"B<chdir>(2)  is not permitted.  If a daemon allows a \"chroot directory\" to "
"be specified, that usually means that if you want to prevent remote users "
"from accessing files outside the chroot directory, you must ensure that "
"folders are never moved out of it."
msgstr ""
"Une variante légèrement plus délicate fonctionne aussi dans certaines "
"circonstances si B<chdir>(2) n'a pas les droits. Si un démon permet de "
"spécifier un « chroot directory » (nouveau répertoire racine), cela veut "
"souvent dire que si vous voulez empêcher des utilisateurs distants d'accéder "
"à des fichiers à l'extérieur du nouveau répertoire racine, vous devez vous "
"assurer que les dossiers n'en sortent jamais."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This call does not change the current working directory, so that after the "
"call \\[aq]I<.>\\[aq] can be outside the tree rooted at \\[aq]I</>\\[aq].  "
"In particular, the superuser can escape from a \"chroot jail\" by doing:"
msgstr ""
"Notez que cet appel système ne modifie pas le répertoire de travail, aussi "
"«\\ I<.>\\ » peut se retrouver en dehors de l'arbre dont la racine est «\\ "
"I</>\\ ». En particulier, le superutilisateur peut s'évader d'un «\\ piège "
"chroot\\ » en faisant\\ :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "mkdir foo; chroot foo; cd ..\n"
msgstr "mkdir foo; chroot foo; cd ..\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This call does not close open file descriptors, and such file descriptors "
"may allow access to files outside the chroot tree."
msgstr ""
"Cet appel ne ferme aucun descripteur de fichier, et de tels descripteurs "
"peuvent permettre un accès à des fichiers hors de l'arbre dont la racine est "
"le nouveau «\\ /\\ »."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"En cas de succès, zéro est renvoyé. En cas d'erreur, B<-1> est renvoyé et "
"I<errno> est définie pour préciser l'erreur."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Depending on the filesystem, other errors can be returned.  The more general "
"errors are listed below:"
msgstr ""
"Suivant le type de système de fichiers, plusieurs erreurs peuvent être "
"renvoyées. Les plus courantes sont les suivantes\\ :"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EACCES>"
msgstr "B<EACCES>"

#.  Also search permission is required on the final component,
#.  maybe just to guarantee that it is a directory?
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Search permission is denied on a component of the path prefix.  (See also "
"B<path_resolution>(7).)"
msgstr ""
"L'accès à un élément du chemin est interdit. (Voir aussi "
"B<path_resolution>(7).)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<path> points outside your accessible address space."
msgstr "I<path> pointe en dehors de l'espace d'adressage accessible."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EIO>"
msgstr "B<EIO>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "An I/O error occurred."
msgstr "Une erreur d'entrée-sortie s'est produite."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ELOOP>"
msgstr "B<ELOOP>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Too many symbolic links were encountered in resolving I<path>."
msgstr ""
"I<path> contient une référence circulaire (à travers un lien symbolique)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENAMETOOLONG>"
msgstr "B<ENAMETOOLONG>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<path> is too long."
msgstr "I<path> est trop long."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The file does not exist."
msgstr "Le fichier n'existe pas."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Insufficient kernel memory was available."
msgstr "La mémoire disponible du noyau n'était pas suffisante."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTDIR>"
msgstr "B<ENOTDIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "A component of I<path> is not a directory."
msgstr "Un élément du chemin d'accès I<path> n'est pas un répertoire."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The caller has insufficient privilege."
msgstr "L'appelant n'a pas les privilèges suffisants."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "None."
msgstr "Aucun"

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#.  SVr4 documents additional EINTR, ENOLINK and EMULTIHOP error conditions.
#.  X/OPEN does not document EIO, ENOMEM or EFAULT error conditions.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"SVr4, 4.4BSD, SUSv2 (marked LEGACY).  This function is not part of "
"POSIX.1-2001."
msgstr ""
"SVr4, BSD\\ 4.4, SUSv2 (considéré comme historique). Cette fonction n'est "
"pas décrite dans POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A child process created via B<fork>(2)  inherits its parent's root "
"directory.  The root directory is left unchanged by B<execve>(2)."
msgstr ""
"Un processus enfant créé avec B<fork>(2) hérite du répertoire racine de son "
"parent. Le répertoire racine n'est pas modifié par un B<execve>(2)."

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The magic symbolic link, I</proc/>pidI</root>, can be used to discover a "
"process's root directory; see B<proc>(5)  for details."
msgstr ""
"Le lien symbolique magique, I</proc/>pidI</root>, peut être utilisé pour "
"trouver le répertoire racine d'un processus ; voir B<proc>(5) pour des "
"détails."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "FreeBSD has a stronger B<jail>()  system call."
msgstr "FreeBSD a un appel système B<jail>() plus solide."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<chroot>(1), B<chdir>(2), B<pivot_root>(2), B<path_resolution>(7), "
"B<switch_root>(8)"
msgstr ""
"B<chroot>(1), B<chdir>(2), B<pivot_root>(2), B<path_resolution>(7), "
"B<symlink>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm
msgid ""
"The magic symbolic link, I</proc/[pid]/root>, can be used to discover a "
"process's root directory; see B<proc>(5)  for details."
msgstr ""
"Le lien symbolique magique, I</proc/[pid]/root>, peut être utilisé pour "
"trouver le répertoire racine d'un processus ; voir B<proc>(5) pour des "
"détails."

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-04-03"
msgstr "3 avril 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pages du manuel de Linux 6.04"

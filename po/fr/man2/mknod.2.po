# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999,2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013.
# Denis Barbier <barbier@debian.org>, 2006,2010.
# David Prévot <david@tilapin.org>, 2010, 2013.
# Cédric Boutillier <cedric.boutillier@gmail.com>, 2011, 2012, 2013.
# Frédéric Hantrais <fhantrais@gmail.com>, 2013, 2014.
# Jean-Philippe MENGUAL <jpmengual@debian.org>, 2020-2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-fr\n"
"POT-Creation-Date: 2024-05-01 15:45+0200\n"
"PO-Revision-Date: 2024-03-09 16:13+0100\n"
"Last-Translator: Jean-Philippe MENGUAL <jpmengual@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: vim\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "mknod"
msgstr "mknod"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr "Pages du manuel de Linux 6.7"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "mknod, mknodat - create a special or ordinary file"
msgstr "mknod, mknodat - Créer un fichier spécial ou ordinaire"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/stat.hE<gt>>\n"
msgstr "B<#include E<lt>sys/stat.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int mknod(const char *>I<pathname>B<, mode_t >I<mode>B<, dev_t >I<dev>B<);>\n"
msgstr "B<int mknod(const char *>I<nom_chemin>B<, mode_t >I<mode>B<, dev_t >I<dev>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>fcntl.hE<gt>           >/* Definition of AT_* constants */\n"
"B<#include E<lt>sys/stat.hE<gt>>\n"
msgstr ""
"B<#include E<lt>fcntl.hE<gt>           >/* Définition des constantes AT_* */\n"
"B<#include E<lt>sys/stat.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int mknodat(int >I<dirfd>B<, const char *>I<pathname>B<, mode_t >I<mode>B<, dev_t >I<dev>B<);>\n"
msgstr "B<int mknodat(int >I<dirfd>B<, const char *>I<nom_chemin>B<, mode_t >I<mode>B<, dev_t >I<dev>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consulter "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<mknod>():"
msgstr "B<mknod>() :"

#.     || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"    _XOPEN_SOURCE E<gt>= 500\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    _XOPEN_SOURCE E<gt>= 500\n"
"        || /* Depuis la glibc 2.19 : */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19 : */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The system call B<mknod>()  creates a filesystem node (file, device special "
"file, or named pipe) named I<pathname>, with attributes specified by I<mode> "
"and I<dev>."
msgstr ""
"B<mknod>() crée un nœud du système de fichiers (fichier, fichier spécial de "
"périphérique ou tube nommé) appelé I<nom_chemin>, avec les attributs I<mode> "
"et I<dev>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<mode> argument specifies both the file mode to use and the type of "
"node to be created.  It should be a combination (using bitwise OR) of one of "
"the file types listed below and zero or more of the file mode bits listed in "
"B<inode>(7)."
msgstr ""
"L'argument I<mode> définit à la fois les permissions d'utilisation, et le "
"type de nœud à créer. C'est une combinaison (en utilisant une opération "
"I<OU> bit à bit) entre l'un des types de fichier ci-dessous et aucun ou "
"plusieurs bits de mode de fichier listés dans B<inode>(7)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The file mode is modified by the process's I<umask> in the usual way: in the "
"absence of a default ACL, the permissions of the created node are (I<mode> & "
"\\[ti]I<umask>)."
msgstr ""
"Le mode du fichier est modifié par le I<umask> du processus de manière "
"habituelle : en l'absence de liste de contrôle d'accès (ACL) par défaut, les "
"permissions du nœud créé sont (I<mode> & \\[ti]I<umask>)."

#.  (S_IFSOCK since Linux 1.2.4)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The file type must be one of B<S_IFREG>, B<S_IFCHR>, B<S_IFBLK>, B<S_IFIFO>, "
"or B<S_IFSOCK> to specify a regular file (which will be created empty), "
"character special file, block special file, FIFO (named pipe), or UNIX "
"domain socket, respectively.  (Zero file type is equivalent to type "
"B<S_IFREG>.)"
msgstr ""
"Le type de nœud doit être l'un des suivants B<S_IFREG>, B<S_IFCHR>, "
"B<S_IFBLK>, B<S_IFIFO> ou B<S_IFSOCK> pour indiquer respectivement un "
"fichier régulier (vide à la création), un fichier spécial mode caractère, un "
"fichier spécial mode bloc, un tube nommé (FIFO) ou un socket du domaine "
"UNIX. Un type de fichier égal à zéro est équivalent à B<S_IFREG>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the file type is B<S_IFCHR> or B<S_IFBLK>, then I<dev> specifies the "
"major and minor numbers of the newly created device special file "
"(B<makedev>(3)  may be useful to build the value for I<dev>); otherwise it "
"is ignored."
msgstr ""
"Si le type de fichier est B<S_IFCHR> ou B<S_IFBLK> alors I<dev> doit "
"indiquer les numéros majeur et mineur du fichier spécial nouvellement créé "
"(B<makedev>(3) peut être utile pour construire la valeur de I<dev>). Pour "
"les autres types de fichier, I<dev> est ignoré."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<pathname> already exists, or is a symbolic link, this call fails with "
"an B<EEXIST> error."
msgstr ""
"Si I<nom_chemin> existe déjà, ou est un lien symbolique, l'appel échoue avec "
"l'erreur B<EEXIST>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The newly created node will be owned by the effective user ID of the "
"process.  If the directory containing the node has the set-group-ID bit set, "
"or if the filesystem is mounted with BSD group semantics, the new node will "
"inherit the group ownership from its parent directory; otherwise it will be "
"owned by the effective group ID of the process."
msgstr ""
"Le nœud nouvellement créé aura pour propriétaire l'UID effectif du "
"processus. Si le répertoire contenant ce nœud a son bit Set-GID défini, ou "
"si le système de fichiers est monté avec une sémantique de groupe BSD, le "
"nouveau nœud héritera de la propriété de groupe de son répertoire parent. "
"Sinon il appartiendra au GID effectif du processus."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "mknodat()"
msgstr "mknodat()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<mknodat>()  system call operates in exactly the same way as "
"B<mknod>(), except for the differences described here."
msgstr ""
"L'appel système B<mknodat>() agit exactement de la même façon que "
"B<mknod>(2), aux différences suivantes près."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the pathname given in I<pathname> is relative, then it is interpreted "
"relative to the directory referred to by the file descriptor I<dirfd> "
"(rather than relative to the current working directory of the calling "
"process, as is done by B<mknod>()  for a relative pathname)."
msgstr ""
"Si le chemin donné dans I<nom_chemin> est relatif, il est interprété par "
"rapport au répertoire référencé par le descripteur de fichier I<dirfd> "
"(plutôt que par rapport au répertoire courant de travail du processus "
"appelant, comme le fait B<mknod>() pour un chemin relatif)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<pathname> is relative and I<dirfd> is the special value B<AT_FDCWD>, "
"then I<pathname> is interpreted relative to the current working directory of "
"the calling process (like B<mknod>())."
msgstr ""
"Si I<nom_chemin> est un chemin relatif et si I<dirfd> est la valeur spéciale "
"B<AT_FDCWD>, I<nom_chemin> est interprété comme étant relatif au répertoire "
"courant du processus appelant, comme B<mknod>()."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If I<pathname> is absolute, then I<dirfd> is ignored."
msgstr "Si I<pathname> est absolu, alors I<dirfd> est ignoré."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "See B<openat>(2)  for an explanation of the need for B<mknodat>()."
msgstr ""
"Consultez B<openat>(2) pour une explication de la nécessité de B<mknodat>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<mknod>()  and B<mknodat>()  return zero on success.  On error, -1 is "
"returned and I<errno> is set to indicate the error."
msgstr ""
"B<mknod>() et B<mknodat>() renvoient B<0> s’ils réussissent. En cas "
"d'erreur, la valeur de retour est B<-1> et I<errno> est défini pour préciser "
"l'erreur."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EACCES>"
msgstr "B<EACCES>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The parent directory does not allow write permission to the process, or one "
"of the directories in the path prefix of I<pathname> did not allow search "
"permission.  (See also B<path_resolution>(7).)"
msgstr ""
"Le répertoire parent n'autorise pas l'écriture au processus, ou l'un des "
"répertoires dans le chemin d’accès à I<nom_chemin> n'autorise pas la "
"consultation de son contenu. (Consultez aussi B<path_resolution>(7).)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"(B<mknodat>())  I<pathname> is relative but I<dirfd> is neither B<AT_FDCWD> "
"nor a valid file descriptor."
msgstr ""
"(B<(>mknodatB<())>) I<pathname> est relatif mais I<dirfd> n'est ni "
"B<AT_FDCWD> ni un descripteur de fichier valable."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EDQUOT>"
msgstr "B<EDQUOT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The user's quota of disk blocks or inodes on the filesystem has been "
"exhausted."
msgstr ""
"Le quota de blocs de disque ou d'inœuds de l'utilisateur sur le système de "
"fichiers a été atteint."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EEXIST>"
msgstr "B<EEXIST>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<pathname> already exists.  This includes the case where I<pathname> is a "
"symbolic link, dangling or not."
msgstr ""
"I<nom_chemin> existe déjà. Cela inclut le cas où I<nom_chemin> est un lien "
"symbolique, pouvant pointer nulle part."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<pathname> points outside your accessible address space."
msgstr "I<nom_chemin> pointe en dehors de l'espace d'adressage accessible."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<mode> requested creation of something other than a regular file, device "
"special file, FIFO or socket."
msgstr ""
"I<mode> demande la création d'autre chose qu'un fichier régulier, fichier "
"spécial de périphérique, FIFO ou socket."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ELOOP>"
msgstr "B<ELOOP>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Too many symbolic links were encountered in resolving I<pathname>."
msgstr ""
"Trop de liens symboliques ont été rencontrés en parcourant I<nom_chemin>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENAMETOOLONG>"
msgstr "B<ENAMETOOLONG>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<pathname> was too long."
msgstr "I<nom_chemin> est trop long."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A directory component in I<pathname> does not exist or is a dangling "
"symbolic link."
msgstr ""
"Un des répertoires du chemin d'accès I<nom_chemin> n'existe pas ou est un "
"lien symbolique pointant nulle part."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Insufficient kernel memory was available."
msgstr "La mémoire disponible du noyau n'était pas suffisante."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSPC>"
msgstr "B<ENOSPC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The device containing I<pathname> has no room for the new node."
msgstr ""
"Le périphérique contenant I<nom_chemin> n'a pas assez de place pour le "
"nouveau nœud."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTDIR>"
msgstr "B<ENOTDIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A component used as a directory in I<pathname> is not, in fact, a directory."
msgstr ""
"Un élément, utilisé comme répertoire, du chemin d'accès I<nom_chemin> n'est "
"pas en fait un répertoire."

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"(B<mknodat>())  I<pathname> is relative and I<dirfd> is a file descriptor "
"referring to a file other than a directory."
msgstr ""
"(B<mknodat>()) I<pathname> est relatif et I<dirfd> est un descripteur de "
"fichier faisant référence à un fichier qui n'est pas un répertoire."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#.  For UNIX domain sockets and regular files, EPERM is returned only in
#.  Linux 2.2 and earlier; in Linux 2.4 and later, unprivileged can
#.  use mknod() to make these files.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<mode> requested creation of something other than a regular file, FIFO "
"(named pipe), or UNIX domain socket, and the caller is not privileged "
"(Linux: does not have the B<CAP_MKNOD> capability); also returned if the "
"filesystem containing I<pathname> does not support the type of node "
"requested."
msgstr ""
"I<mode> demande la création de quelque chose d’autre qu'un fichier régulier, "
"une FIFO (tube nommé) ou un socket du domaine UNIX, alors que le processus "
"appelant n'est pas privilégié (sous Linux\\ : n'a pas la capacité "
"B<CAP_MKNOD>). Cette erreur se produit également si le système de fichiers "
"contenant I<nom_chemin> ne gère pas les nœuds du type demandé."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EROFS>"
msgstr "B<EROFS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<pathname> refers to a file on a read-only filesystem."
msgstr "I<pathname> est placé sur un système de fichiers en lecture seule."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"POSIX.1-2001 says: \"The only portable use of B<mknod>()  is to create a "
"FIFO-special file.  If I<mode> is not B<S_IFIFO> or I<dev> is not 0, the "
"behavior of B<mknod>()  is unspecified.\" However, nowadays one should never "
"use B<mknod>()  for this purpose; one should use B<mkfifo>(3), a function "
"especially defined for this purpose."
msgstr ""
"POSIX.1-2001 dit\\ : «\\ Le seul usage portable de B<mknod>() est réservé à "
"la création de fichiers spéciaux FIFO. Si le I<mode> n'est pas B<S_IFIFO> ou "
"si I<dev> n'est pas 0, alors le comportement de B<mknod>() est indéterminé\\ "
"». Toutefois, aujourd'hui, on ne devrait jamais utiliser B<mknod>() pour "
"cela ; on devrait utiliser B<mkfifo>(3), une fonction spécialement conçue "
"pour cela."

#.  and one should make UNIX domain sockets with socket(2) and bind(2).
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Under Linux, B<mknod>()  cannot be used to create directories.  One should "
"make directories with B<mkdir>(2)."
msgstr ""
"Sous Linux, B<mknod>() ne peut pas être utilisé pour créer des répertoires. "
"Il faut créer les répertoires avec B<mkdir>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<mknod>()"
msgstr "B<mknod>()"

#.  The Linux version differs from the SVr4 version in that it
#.  does not require root permission to create pipes, also in that no
#.  EMULTIHOP, ENOLINK, or EINTR error is documented.
#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "SVr4, 4.4BSD, POSIX.1-2001 (but see VERSIONS)."
msgstr "SVr4, 4.4BSD, POSIX.1-2001 (mais voir VERSIONS)."

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<mknodat>()"
msgstr "B<mknodat>()"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Linux 2.6.16, glibc 2.4.  POSIX.1-2008."
msgstr "Linux 2.6.16, glibc 2.4. POSIX.1-2008."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"There are many infelicities in the protocol underlying NFS.  Some of these "
"affect B<mknod>()  and B<mknodat>()."
msgstr ""
"Il y a de nombreux problèmes avec le protocole sous\\(hyjacent à NFS, "
"certains d'entre eux pouvant affecter B<mknod>() et B<mknodat>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<mknod>(1), B<chmod>(2), B<chown>(2), B<fcntl>(2), B<mkdir>(2), "
"B<mount>(2), B<socket>(2), B<stat>(2), B<umask>(2), B<unlink>(2), "
"B<makedev>(3), B<mkfifo>(3), B<acl>(5), B<path_resolution>(7)"
msgstr ""
"B<mknod>(1), B<chmod>(2), B<chown>(2), B<fcntl>(2), B<mkdir>(2), "
"B<mount>(2), B<socket>(2), B<stat>(2), B<umask>(2), B<unlink>(2), "
"B<makedev>(3), B<mkfifo>(3), B<acl>(5), B<path_resolution>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<(>mknodatB<())> I<pathname> is relative but I<dirfd> is neither "
"B<AT_FDCWD> nor a valid file descriptor."
msgstr ""
"B<(>mknodatB<())> I<pathname> est relatif mais I<dirfd> n'est ni B<AT_FDCWD> "
"ni un descripteur de fichier valable."

#. type: Plain text
#: debian-bookworm
msgid ""
"B<(>mknodatB<())> I<pathname> is relative and I<dirfd> is a file descriptor "
"referring to a file other than a directory."
msgstr ""
"B<(>mknodatB<())> I<pathname> est relatif et I<dirfd> est un descripteur de "
"fichier faisant référence à un fichier qui n'est pas un dossier."

#. type: Plain text
#: debian-bookworm
msgid ""
"B<mknodat>()  was added in Linux 2.6.16; library support was added in glibc "
"2.4."
msgstr ""
"B<mknodat>() a été ajouté à Linux 2.6.16 ; la prise en charge de la "
"bibliothèque a été ajoutée dans la glibc 2.4."

#.  The Linux version differs from the SVr4 version in that it
#.  does not require root permission to create pipes, also in that no
#.  EMULTIHOP, ENOLINK, or EINTR error is documented.
#. type: Plain text
#: debian-bookworm
msgid "B<mknod>(): SVr4, 4.4BSD, POSIX.1-2001 (but see below), POSIX.1-2008."
msgstr ""
"B<mknod>() : SVr4, 4.4BSD, POSIX.1-2001 (mais voir plus loin), POSIX.1-2008."

#. type: Plain text
#: debian-bookworm
msgid "B<mknodat>(): POSIX.1-2008."
msgstr "B<mknodat>() : POSIX.1-2008."

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-31"
msgstr "31 mars 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pages du manuel de Linux 6.04"

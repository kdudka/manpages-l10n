# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jos Boersema <joshb@xs4all.nl>, 2001.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
# Luc Castermans <luc.castermans@gmail.com>, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 2.15\n"
"POT-Creation-Date: 2024-05-01 15:59+0200\n"
"PO-Revision-Date: 2023-01-15 20:23+0100\n"
"Last-Translator: Luc Castermans <luc.castermans@gmail.com>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "write"
msgstr "write"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-10-31"
msgstr "31 oktober 2023"

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr "Linux man-pages 6.7"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAAM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "write - write to a file descriptor"
msgstr "write - schrijf naar een bestandsindicator"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEEK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard C bibliotheek  (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SAMENVATTING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ssize_t write(int >I<fd>B<, const void >I<buf>B<[.>I<count>B<], size_t >I<count>B<);>\n"
msgstr "B<ssize_t write(int >I<fd>B<, const void >I<buf>B<[.>I<count>B<], size_t >I<tel>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHRIJVING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<write>()  writes up to I<count> bytes from the buffer starting at I<buf> "
"to the file referred to by the file descriptor I<fd>."
msgstr ""
"B<write>() schrijft tot I<tel> bytes naar het bestand waar I<bi> naar wijst, "
"van de buffer die begint op I<buf>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The number of bytes written may be less than I<count> if, for example, there "
"is insufficient space on the underlying physical medium, or the "
"B<RLIMIT_FSIZE> resource limit is encountered (see B<setrlimit>(2)), or the "
"call was interrupted by a signal handler after having written less than "
"I<count> bytes.  (See also B<pipe>(7).)"
msgstr ""
"Het aantal geschreven bytes kan kleiner zijn dan I<tel> als bijvoorbeeld er "
"onvoldoende ruimte was op het onderliggende fysieke medium, of als de "
"B<RLIMIT_FSIZE> hulpbron limiet werd bereikt (zie B<setrlimit>(2)), of als "
"de aanroep werd geïnterrumpeerd door een signaal nadat het minder dan I<tel> "
"bytes geschreven had. (Zie ook B<pipe>(7).)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For a seekable file (i.e., one to which B<lseek>(2)  may be applied, for "
"example, a regular file)  writing takes place at the file offset, and the "
"file offset is incremented by the number of bytes actually written.  If the "
"file was B<open>(2)ed with B<O_APPEND>, the file offset is first set to the "
"end of the file before writing.  The adjustment of the file offset and the "
"write operation are performed as an atomic step."
msgstr ""
"In een doorzoekbaar bestand (m.a.w. een op welk B<lseek>(2) toegepast kan "
"worden, bijvoorbeeld een regulier bestand) wordt geschreven aan de "
"bestandspositie, en de positie wordt verhoogd met het daadwerkelijk "
"geschreven aantal bytes. Als het bestand geB<open>(2)d werd met B<O_APPEND>. "
"dan wordt de bestandspositie eerst gezet op het einde van het bestand voor "
"schrijven. De aanpassing van de bestandspositie en de schrijf operatie "
"worden in een ondeelbare stap gedaan."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"POSIX requires that a B<read>(2)  that can be proved to occur after a "
"B<write>()  has returned will return the new data.  Note that not all "
"filesystems are POSIX conforming."
msgstr ""
"POSIX eist dat een B<read>(2) die bewijsbaar na een B<write>() plaatsvindt "
"de nieuwe gegevens oplevert. Merk op dat niet alle bestandsystemen voldoen "
"aan POSIX."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"According to POSIX.1, if I<count> is greater than B<SSIZE_MAX>, the result "
"is implementation-defined; see NOTES for the upper limit on Linux."
msgstr ""
"Volgens POSIX.1 als I<tel> groter is dan B<SSIZE_MAX> dan is het resultaat "
"implementatie afhankelijk; zie OPMERKINGEN voor de boven grens op Linux."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "EIND WAARDE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On success, the number of bytes written is returned.  On error, -1 is "
"returned, and I<errno> is set to indicate the error."
msgstr ""
"Bij success wordt het aantal geschreven bytes teruggegeven. Bij falen wordt "
"-1 teruggegeven en I<errno> wordt overeenkomstig gezet."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note that a successful B<write>()  may transfer fewer than I<count> bytes.  "
"Such partial writes can occur for various reasons; for example, because "
"there was insufficient space on the disk device to write all of the "
"requested bytes, or because a blocked B<write>()  to a socket, pipe, or "
"similar was interrupted by a signal handler after it had transferred some, "
"but before it had transferred all of the requested bytes.  In the event of a "
"partial write, the caller can make another B<write>()  call to transfer the "
"remaining bytes.  The subsequent call will either transfer further bytes or "
"may result in an error (e.g., if the disk is now full)."
msgstr ""
"Merk op dat een succesvolle B<write>() ook minder dan I<tel> bytes kan "
"overdragen. Zulke partiële schrijf resultaten kunnen optreden door diverse "
"redenen; bij voorbeeld, omdat er niet genoeg ruimte op het schijf apparaat "
"was om alle gevraagde bytes te schrijven, of omdat een geblokkeerde "
"B<write>() naar een socket, pijp, of vergelijkbaar werd onderbroken door een "
"signaal nadat het enkele, maar voor dat het alle gevraagde bytes had "
"geschreven. Als zulk een  partieel schrijf resultaat optreedt dan kan de "
"aanroeper een andere B<write>() aanroepen om de resterende bytes over te "
"dragen. Deze aanroep kan de resterende bytes overdragen of resulteren in een "
"fout (b.v. de schijf is nu vol)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<count> is zero and I<fd> refers to a regular file, then B<write>()  may "
"return a failure status if one of the errors below is detected.  If no "
"errors are detected, or error detection is not performed, 0 is returned "
"without causing any other effect.  If I<count> is zero and I<fd> refers to a "
"file other than a regular file, the results are not specified."
msgstr ""
"Als I<tel> nul is en I<bi> wijst naar een normaal bestand dan kan B<write>() "
"een fout status teruggeven als een van de hieronder gegeven fouten werd "
"gedetecteerd. Als een fouten worden gedetecteerd of fout detectie is niet "
"gedaan, dan wordt 0 teruggegeven zonder enig andere effect te veroorzaken. "
"Als I<tel> nul is en I<bi> wijst naar een bestand  anders dan een regulier "
"bestand, dan zijn de resultaten onbepaald."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "FOUTEN"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EAGAIN>"
msgstr "B<EAGAIN>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The file descriptor I<fd> refers to a file other than a socket and has been "
"marked nonblocking (B<O_NONBLOCK>), and the write would block.  See "
"B<open>(2)  for further details on the B<O_NONBLOCK> flag."
msgstr ""
"De file beschrijving I<bi> wijst naar een ander bestand dan een socket en is "
"gemarkeerd als niet-blokkerend (B<O_NONBLOCK>), en de schrijf aanroep zou "
"blokkeren. Zie B<open>(2) voor meer details over de B<O_NONBLOCK> vlag. "

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EAGAIN> or B<EWOULDBLOCK>"
msgstr "B<EAGAIN> of B<EWOULDBLOCK>"

#.  Actually EAGAIN on Linux
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The file descriptor I<fd> refers to a socket and has been marked nonblocking "
"(B<O_NONBLOCK>), and the write would block.  POSIX.1-2001 allows either "
"error to be returned for this case, and does not require these constants to "
"have the same value, so a portable application should check for both "
"possibilities."
msgstr ""
"De file beschrijving I<bi> wijst naar een ander bestand dan een socket en is "
"gemarkeerd als niet-blokkerend (B<O_NONBLOCK>), en de schrijf aanroep zou "
"blokkeren. Zie B<open>(2) voor meer details over de B<O_NONBLOCK> vlag.  "
"POSIX.1-2001 staat toe in dit geval een fout terug te geven, en vereist niet "
"dat deze constanten dezelfde waarde hebben, daarom moet een overdraagbare "
"applicatie op beide mogelijkheden controleren."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<fd> is not a valid file descriptor or is not open for writing."
msgstr "I<bi> is geen geldige bestandsindicator, of is niet open voor lezen."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EDESTADDRREQ>"
msgstr "B<EDESTADDRREQ>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<fd> refers to a datagram socket for which a peer address has not been set "
"using B<connect>(2)."
msgstr ""
"I<bi> wijst naar een datagram socket waarvoor een gelijk adres niet "
"ingesteld werd gebruikmakend van B<connect>(2)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EDQUOT>"
msgstr "B<EDQUOT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The user's quota of disk blocks on the filesystem containing the file "
"referred to by I<fd> has been exhausted."
msgstr ""
"De quota van de gebruiker van schijf blokken op het bestandssysteem "
"bevattende het bestand aangewezen door I<bi> is opgebruikt."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<buf> is outside your accessible address space."
msgstr "I<buf> ligt buiten de door u toegankelijke adres ruimte."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFBIG>"
msgstr "B<EFBIG>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"An attempt was made to write a file that exceeds the implementation-defined "
"maximum file size or the process's file size limit, or to write at a "
"position past the maximum allowed offset."
msgstr ""
"Een poging werd ondernomen om een bestand te schrijven dat de implementatie-"
"bepaalde maximum bestand grootte of een proces bestand grootte limiet "
"overschrijd, of naar een schrijf actie bij een positie voorbij de maximaal "
"toegestane positie. "

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINTR>"
msgstr "B<EINTR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The call was interrupted by a signal before any data was written; see "
"B<signal>(7)."
msgstr ""
"De aanroep werd onderbroken door een signaal voordat gegevens werden "
"geschreven; zie B<signal>(7)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<fd> is attached to an object which is unsuitable for writing; or the file "
"was opened with the B<O_DIRECT> flag, and either the address specified in "
"I<buf>, the value specified in I<count>, or the file offset is not suitably "
"aligned."
msgstr ""
"I<bi> is gekoppeld aan een object dat ongeschikt is om naar te schrijven; of "
"het bestand werd geopend met de B<O_DIRECT> vlag, en ofwel het opgegeven "
"adres in I<buf>, de waarde opgegeven in I<count>, of de bestandspositie is "
"niet goed opgelijnd. "

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EIO>"
msgstr "B<EIO>"

#.  commit 088737f44bbf6378745f5b57b035e57ee3dc4750
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A low-level I/O error occurred while modifying the inode.  This error may "
"relate to the write-back of data written by an earlier B<write>(), which may "
"have been issued to a different file descriptor on the same file.  Since "
"Linux 4.13, errors from write-back come with a promise that they I<may> be "
"reported by subsequent.  B<write>()  requests, and I<will> be reported by a "
"subsequent B<fsync>(2)  (whether or not they were also reported by "
"B<write>()).  An alternate cause of B<EIO> on networked filesystems is when "
"an advisory lock had been taken out on the file descriptor and this lock has "
"been lost.  See the I<Lost locks> section of B<fcntl>(2)  for further "
"details."
msgstr ""
"Een laag-niveau Invoer/Uitvoer fout trad op terwijl de inode veranderd werd. "
"Deze fout kan gerelateerd zijn aan een schrijf-terug van gegevens geschreven "
"door een eerdere B<write>(), die kan zijn gedaan via een andere "
"bestandsbeschrijving op hetzelfde bestand. Vanaf Linux 4.13, komen fouten "
"van een schrijf-terug terug met de belofte dat ze gerapporteerd I<kunnen> "
"worden door volgende B<write>() aanroepen en worden gerapporteerd door een "
"volgende B<fsync>(2) (al dan niet ook gerapporteerd door B<write>()). Een "
"alternatief geval van B<EIO> op netwerk bestandssystemen is wanneer een "
"geadviseerde lock werd verwijderd op de bestandsbeschrijving en als die lock "
"verloren werd. Zie de I<Verloren Locks> sectie van B<fcntl>(2) voor meer "
"details. "

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSPC>"
msgstr "B<ENOSPC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The device containing the file referred to by I<fd> has no room for the data."
msgstr ""
"Het apparaat dat het bestand bevat waar I<bi> naar wijst heeft geen ruimte "
"voor de gegevens."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The operation was prevented by a file seal; see B<fcntl>(2)."
msgstr "De operatie werd voorkomen door een bestandszegel; zie B<fcntl>(2)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPIPE>"
msgstr "B<EPIPE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<fd> is connected to a pipe or socket whose reading end is closed.  When "
"this happens the writing process will also receive a B<SIGPIPE> signal.  "
"(Thus, the write return value is seen only if the program catches, blocks or "
"ignores this signal.)"
msgstr ""
"I<bi> is verbonden met een pijp of socket waarvan de lees-uitgang gesloten "
"is. Wanneer dit gebeurd ontvangt het schrijvende proces een B<SIGPIPE> "
"signaal; (Dus, de uitvoer waarde van de write wordt alleen gezien als het "
"programma het signaal ontvangt, blokkeert of negeert.)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Other errors may occur, depending on the object connected to I<fd>."
msgstr ""
"Andere fouten kunnen optreden afhankelijk van dat wat verbonden is met I<bi>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "VOLDOET AAN"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHIEDENIS"

#.  SVr4 documents additional error
#.  conditions EDEADLK, ENOLCK, ENOLNK, ENOSR, ENXIO, or ERANGE.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "SVr4, 4.3BSD, POSIX.1-2001."
msgstr "SVr4, 4.3BSD, POSIX.1-2001."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Under SVr4 a write may be interrupted and return B<EINTR> at any point, not "
"just before any data is written."
msgstr ""
"Under SVr4 kan een write op elk punt onderbroken worden en B<EINTR> "
"teruggeven, niet alleen voordat enige gegevens zijn geschreven. "

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "OPMERKINGEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A successful return from B<write>()  does not make any guarantee that data "
"has been committed to disk.  On some filesystems, including NFS, it does not "
"even guarantee that space has successfully been reserved for the data.  In "
"this case, some errors might be delayed until a future B<write>(), "
"B<fsync>(2), or even B<close>(2).  The only way to be sure is to call "
"B<fsync>(2)  after you are done writing all your data."
msgstr ""
"Een succesvolle terugkeer uit B<write>() is geen garantie dat de gegevens "
"zijn toegekend aan de schijf. Op sommige bestandssystemen, inclusief NFS, is "
"het zelfs geen garantie dat de ruimte succesvol werd gereserveerd voor de "
"gegevens. In dit geval, kunnen sommige fouten worden vertraagd tot een "
"toekomstige B<write>(), B<fsync>(2), of zelfs B<close>(2). De enige manier "
"om zeker te zijn is door B<fsync>(2) aan te roepen, nadat u klaar bent met "
"het schrijven van alle gegevens."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If a B<write>()  is interrupted by a signal handler before any bytes are "
"written, then the call fails with the error B<EINTR>; if it is interrupted "
"after at least one byte has been written, the call succeeds, and returns the "
"number of bytes written."
msgstr ""
"Als een B<write>() werd onderbroken door een signaal voordat enige bytes "
"werden geschreven, dan zal de aanroep falen met de fout B<EINTR>; als hij "
"werd onderbroken nadat op zijn minst een byte werd geschreven, dan is de "
"aanroep succesvol, en retourneert het aantal geschreven bytes."

#.  commit e28cc71572da38a5a12c1cfe4d7032017adccf69
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On Linux, B<write>()  (and similar system calls) will transfer at most "
"0x7ffff000 (2,147,479,552) bytes, returning the number of bytes actually "
"transferred.  (This is true on both 32-bit and 64-bit systems.)"
msgstr ""
"Op Linux zal B<write>() (en vergelijkbare systeem aanroepen) maximaal "
"0x7ffff000 (2,147,479,552) bytes overdragen, en het daadwerkelijk aantal "
"geschreven bytes retourneren. (Dit is waar voor zowel 32-bit als 64-but "
"systemen.)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"An error return value while performing B<write>()  using direct I/O does not "
"mean the entire write has failed.  Partial data may be written and the data "
"at the file offset on which the B<write>()  was attempted should be "
"considered inconsistent."
msgstr ""
"Een foutmelding gedurende het uitvoeren van B<write>() onder gebruik van "
"directe Invoer/Uitvoer betekent niet dat de volledige schrijf actie faalde. "
"Een deel van de gegevens kan zijn geschreven en de gegevens op de "
"bestandspositie waar de B<write>() werd geprobeerd dienen beschouwd te "
"worden als inconsistent."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BUGS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"According to POSIX.1-2008/SUSv4 Section XSI 2.9.7 (\"Thread Interactions "
"with Regular File Operations\"):"
msgstr ""
"Volgens POSIX.1-2008/SUSv4 Sectie XSI 2.9.7 (\"Thread interacties met "
"Reguliere Bestand Operaties\"):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"All of the following functions shall be atomic with respect to each other in "
"the effects specified in POSIX.1-2008 when they operate on regular files or "
"symbolic links: ..."
msgstr ""
"Alle volgende functie zullen onderling atomair zijn  voor wat betreft de "
"effecten die in POSIX.1-2008 gespecificeerd zijn indien ze werken op "
"reguliere bestanden of symbolische koppelingen: ..."

#.  http://thread.gmane.org/gmane.linux.kernel/1649458
#.     From: Michael Kerrisk (man-pages <mtk.manpages <at> gmail.com>
#.     Subject: Update of file offset on write() etc. is non-atomic with I/O
#.     Date: 2014-02-17 15:41:37 GMT
#.     Newsgroups: gmane.linux.kernel, gmane.linux.file-systems
#.  commit 9c225f2655e36a470c4f58dbbc99244c5fc7f2d4
#.     Author: Linus Torvalds <torvalds@linux-foundation.org>
#.     Date:   Mon Mar 3 09:36:58 2014 -0800
#.         vfs: atomic f_pos accesses as per POSIX
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Among the APIs subsequently listed are B<write>()  and B<writev>(2).  And "
"among the effects that should be atomic across threads (and processes)  are "
"updates of the file offset.  However, before Linux 3.14, this was not the "
"case: if two processes that share an open file description (see B<open>(2))  "
"perform a B<write>()  (or B<writev>(2))  at the same time, then the I/O "
"operations were not atomic with respect to updating the file offset, with "
"the result that the blocks of data output by the two processes might "
"(incorrectly) overlap.  This problem was fixed in Linux 3.14."
msgstr ""
"Tussen de opeenvolgende API´s staan  B<write>()  en B<writev>(2). En tussen "
"de effecten die atomair zouden moeten zijn langs threads (en processen) zijn "
"de veranderingen in de bestandspositie. Echter op Linux voor versie 3.14 was "
"dit niet het geval: als twee processen die een open bestandsindicator "
"deelden (zie B<open>(2))  een B<write>()  (or B<writev>(2)) tegelijkertijd "
"uitvoeren, dan zijn de Invoer/Uitvoer operaties niet atomair met betrekking "
"tot het aanpassen van de bestandspositie, hetgeen kan resulteren in het "
"kunnen overlappen van de gegevens blokken zoals geschreven door beide "
"processen. Dit probleem werd opgelost in Linux 3.14. "

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZIE OOK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<close>(2), B<fcntl>(2), B<fsync>(2), B<ioctl>(2), B<lseek>(2), B<open>(2), "
"B<pwrite>(2), B<read>(2), B<select>(2), B<writev>(2), B<fwrite>(3)"
msgstr ""
"B<close>(2), B<fcntl>(2), B<fsync>(2), B<ioctl>(2), B<lseek>(2), B<open>(2), "
"B<pwrite>(2), B<read>(2), B<select>(2), B<writev>(2), B<fwrite>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-04"
msgstr "4 december 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pagina's 6.03"

#. type: Plain text
#: debian-bookworm
msgid ""
"The types I<size_t> and I<ssize_t> are, respectively, unsigned and signed "
"integer data types specified by POSIX.1."
msgstr ""
"De typen I<size_t> en I<ssize_t> zijn, respectively, gehele getal typen met "
"of zonder teken zoals gespecificeerd door POSIX.1."

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-04-03"
msgstr "3 april 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
#
# Jos Boersema <joshb@xs4all.nl>, 2001.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
# Luc Castermans <luc.castermans@gmail.com>, 2021-2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 2.15\n"
"POT-Creation-Date: 2024-05-01 15:40+0200\n"
"PO-Revision-Date: 2023-05-21 21:14+0200\n"
"Last-Translator: Luc Castermans <luc.castermans@gmail.com>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl_NL\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "getpriority"
msgstr "getpriority"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-10-31"
msgstr "31 oktober 2023"

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr "Linux man-pages 6.7"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAAM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "getpriority, setpriority - get/set program scheduling priority"
msgstr "getpriority, setpriority - krijg/zet programma in-rooster prioriteit"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEEK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard C bibliotheek  (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SAMENVATTING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/resource.hE<gt>>\n"
msgstr "B<#include E<lt>sys/resource.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int getpriority(int >I<which>B<, id_t >I<who>B<);>\n"
"B<int setpriority(int >I<which>B<, id_t >I<who>B<, int >I<prio>B<);>\n"
msgstr ""
"B<int getpriority(int >I<welk>B<, id_t >I<wie>B<);>\n"
"B<int setpriority(int >I<welk>B<, id_t >I<wie>B<, int >I<prio>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHRIJVING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The scheduling priority of the process, process group, or user, as indicated "
"by I<which> and I<who> is obtained with the B<getpriority>()  call and set "
"with the B<setpriority>()  call.  The process attribute dealt with by these "
"system calls is the same attribute (also known as the \"nice\" value) that "
"is dealt with by B<nice>(2)."
msgstr ""
"De in-rooster prioriteit van het proces, proces groep of gebruiker aangeduid "
"door I<welk> en I<wie> wordt verkregen met de B<getpriority> aanroep en "
"wordt gezet met de B<setpriority> aanroep."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The value I<which> is one of B<PRIO_PROCESS>, B<PRIO_PGRP>, or B<PRIO_USER>, "
"and I<who> is interpreted relative to I<which> (a process identifier for "
"B<PRIO_PROCESS>, process group identifier for B<PRIO_PGRP>, and a user ID "
"for B<PRIO_USER>).  A zero value for I<who> denotes (respectively) the "
"calling process, the process group of the calling process, or the real user "
"ID of the calling process."
msgstr ""
"I<Welk> is één van B<PRIO_PROCESS>, B<PRIO_PGRP> of B<PRIO_USER> en I<who> "
"wordt geïnterpreteerd afhankelijk van I<welk> (een proces identificeerder "
"voor B<PRIO_PROCESS>, proces groep identificeerder voor B<PRIO_PGRP>, en een "
"gebruiker ID voor B<PRIO_USER>). Een nul waarde van I<wie> duidt het huidige "
"proces, proces groep of gebruiker aan."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<prio> argument is a value in the range -20 to 19 (but see NOTES "
"below), with -20 being the highest priority and 19 being the lowest "
"priority.  Attempts to set a priority outside this range are silently "
"clamped to the range.  The default priority is 0; lower values give a "
"process a higher scheduling priority."
msgstr ""
"Het I<prio> argument is een waarde in het interval van -20 tot 19 (zie "
"OPMERKINGEN hieronder), met -20 zijnde de hoogste prioriteit en 19 de "
"laagste prioriteit. Pogingen om een prioriteit buiten het interval in te "
"stellen worden stilzwijgend beperkt tot het interval. De standaard "
"prioriteit is 0;  lagere waarden geven een proces een hogere inroostering "
"prioriteit."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<getpriority>()  call returns the highest priority (lowest numerical "
"value)  enjoyed by any of the specified processes.  The B<setpriority>()  "
"call sets the priorities of all of the specified processes to the specified "
"value."
msgstr ""
"De B<getpriority>() aanroep geeft de hoogste prioriteit (laagste genummerde "
"waarde) die één van de opgegeven processen geniet. De B<setpriority> aanroep "
"zet de prioriteiten van alle opgegeven processen naar de opgegeven waarde."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Traditionally, only a privileged process could lower the nice value (i.e., "
"set a higher priority).  However, since Linux 2.6.12, an unprivileged "
"process can decrease the nice value of a target process that has a suitable "
"B<RLIMIT_NICE> soft limit; see B<getrlimit>(2)  for details."
msgstr ""
"Traditioneel kon alleen een geprivilegieerd proces de prioriteit waarde "
"verlagen (m.a.w. een hogere prioriteit instellen). Echter vanaf Linux 2.6.12 "
"kan een niet-geprivilegieerd  proces de prioriteit van een doel proces "
"verlagen als dat een bruikbare B<RLIMIT_NICE> zachte limiet heeft; zie "
"B<getrlimit>(2) voor details."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "EIND WAARDE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On success, B<getpriority>()  returns the calling thread's nice value, which "
"may be a negative number.  On error, it returns -1 and sets I<errno> to "
"indicate the error."
msgstr ""
"Bij succes geeft B<setpriority> de prioriteit van de aanroepende thread "
"terug, en dat kan een negatief getal zijn. Bij een fout geeft het -1 terug "
"en zet I<errno> op de fout aan te geven."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Since a successful call to B<getpriority>()  can legitimately return the "
"value -1, it is necessary to clear I<errno> prior to the call, then check "
"I<errno> afterward to determine if -1 is an error or a legitimate value."
msgstr ""
"Omdat een succesvolle aanroep van B<getpriority>() legitiem de waarde -1 kan "
"teruggeven, is het nodig om I<errno> te wissen vóór de aanroep, vervolgens "
"nadien I<errno> te controleren en bepalen of -1 een fout is of een legitieme "
"waarde."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<setpriority>()  returns 0 on success.  On failure, it returns -1 and sets "
"I<errno> to indicate the error."
msgstr ""
"B<setpriority> geeft 0 terug bij succes.  Bij een fout geeft het -1 terug en "
"zet I<errno> om de fout aan te geven."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "FOUTEN"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EACCES>"
msgstr "B<EACCES>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The caller attempted to set a lower nice value (i.e., a higher process "
"priority), but did not have the required privilege (on Linux: did not have "
"the B<CAP_SYS_NICE> capability)."
msgstr ""
"De aanroeper probeerde een lagere prioriteit in te stellen (m.a.w. een "
"hogere proces prioriteit), maar had niet het vereiste privilege (op Linux: "
"had niet de B<CAP_SYS_NICE> capaciteit)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<which> was not one of B<PRIO_PROCESS>, B<PRIO_PGRP>, or B<PRIO_USER>."
msgstr ""
"I<which> was niet een van B<PRIO_PROCESS>, B<PRIO_PGRP> of B<PRIO_USER>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A process was located, but its effective user ID did not match either the "
"effective or the real user ID of the caller, and was not privileged (on "
"Linux: did not have the B<CAP_SYS_NICE> capability).  But see NOTES below."
msgstr ""
"Een proces werd gevonden, maar nog zijn effectieve gebruiker ID kwam niet "
"overeen met ofwel de effectieve of echte gebruiker ID van de aanroeper, en "
"was niet gerechtigd (op Linux: had niet de B<CAP_SYS_NICE> capaciteit). Maar "
"zie OPMERKINGEN hieronder."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ESRCH>"
msgstr "B<ESRCH>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "No process was located using the I<which> and I<who> values specified."
msgstr ""
"Geen proces gevonden dat de I<welk> en I<who> opgegeven waardes gebruikt."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "VOLDOET AAN"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHIEDENIS"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2001, SVr4, 4.4BSD (these interfaces first appeared in 4.2BSD)."
msgstr ""
"POSIX.1-2001, SVr4, 4.4BSD (deze functie aanroepen verschenen voor het eerst "
"in 4.2BSD)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "OPMERKINGEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "For further details on the nice value, see B<sched>(7)."
msgstr "Voor verdere  details over de prioriteit waarde, zie B<sched>(7)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<Note>: the addition of the \"autogroup\" feature in Linux 2.6.38 means "
"that the nice value no longer has its traditional effect in many "
"circumstances.  For details, see B<sched>(7)."
msgstr ""
"I<Opmerkinge>: de toevoeging van het \"autogroup\" kenmerk in Linux 2.6.38 "
"betekent dat de prioriteit waarde niet langer zijn traditioneel effect in "
"veel omstandigheden heeft. Voor details, zie B<sched>(7)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A child created by B<fork>(2)  inherits its parent's nice value.  The nice "
"value is preserved across B<execve>(2)."
msgstr ""
"Een kind aangemaakt door B<fork>(2) erft de prioriteit van zijn ouder.  De "
"prioriteit wordt doorgegeven langs B<execve>(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The details on the condition for B<EPERM> depend on the system.  The above "
"description is what POSIX.1-2001 says, and seems to be followed on all "
"System\\ V-like systems.  Linux kernels before Linux 2.6.12 required the "
"real or effective user ID of the caller to match the real user of the "
"process I<who> (instead of its effective user ID).  Linux 2.6.12 and later "
"require the effective user ID of the caller to match the real or effective "
"user ID of the process I<who>.  All BSD-like systems (SunOS 4.1.3, Ultrix "
"4.2, 4.3BSD, FreeBSD 4.3, OpenBSD-2.5, ...) behave in the same manner as "
"Linux 2.6.12 and later."
msgstr ""
"De details van de conditie voor B<EPERM> zijn afhankelijk van het systeem. "
"Bovenstaande beschrijving is wat POSIX.1-2001 zegt en lijkt gevolgd te "
"worden op alle System\\ V-achtige systemen. Linux kernels voor Linux 2.6.12 "
"vereisten dat het echte of effectieve gebruiker ID van de aanroeper overeen "
"kwamen met de echte gebruiker van het proces I<wie> (in plaats van het "
"effectieve gebruiker ID). Linux 2.6.12 en later vereiste dat het effectieve "
"gebruiker ID van de aanroeper overeen kwam met het echte of effectieve "
"gebruiker ID van het proces I<wie>.  Alle BSD-achtige systemen (SunOS 4.1.3, "
"Ultrix 4.2, 4.3BSD, FreeBSD 4.3, OpenBSD-2.5,...) gedragen zich op dezelfde "
"manier als Linux 2.6.12 en later."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "C library/kernel differences"
msgstr "C library/kernel verschillen"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The getpriority system call returns nice values translated to the range "
"40..1, since a negative return value would be interpreted as an error.  The "
"glibc wrapper function for B<getpriority>()  translates the value back "
"according to the formula I<unice\\~=\\~20\\~-\\~knice> (thus, the 40..1 "
"range returned by the kernel corresponds to the range -20..19 as seen by "
"user space)."
msgstr ""
"De getpriority systeem aanroep geeft nice waarden terug in het interval "
"40...1, omdat negatieve waarden worden geinterpreteerd als een fout. De "
"glibc omwikkel functie voor B<getpriority>()  vertaalt de waarde volgens de "
"formule I<unice\\ =\\ 20\\ -\\ knice>. (Daarom komt het kernel 40...1 "
"interval overeen met het bereik -20...19 zoals gezien door de gebruiker.)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BUGS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"According to POSIX, the nice value is a per-process setting.  However, under "
"the current Linux/NPTL implementation of POSIX threads, the nice value is a "
"per-thread attribute: different threads in the same process can have "
"different nice values.  Portable applications should avoid relying on the "
"Linux behavior, which may be made standards conformant in the future."
msgstr ""
"Volgens POSIX, is de prioriteit waarde een per-proces instelling. Echter "
"onder de huidige Linux/NPTL implementatie van de POSIX threads, is de "
"prioriteit waarde een per-thread attribute: verschillende threads in "
"hetzelfde proces kunnen verschillende prioriteiten hebben. Overdraagbare "
"applicaties moeten vermijden te vertrouwen op Linux gedrag, omdat dit in de "
"toekomst aangepast kan worden aan de standaarden."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZIE OOK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<nice>(1), B<renice>(1), B<fork>(2), B<capabilities>(7), B<sched>(7)"
msgstr "B<nice>(1), B<renice>(1), B<fork>(2), B<capabilities>(7), B<sched>(7)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<Documentation/scheduler/sched-nice-design.txt> in the Linux kernel source "
"tree (since Linux 2.6.23)"
msgstr ""
"I<Documentation/scheduler/sched-nice-design.txt> in de Linux kernel bron "
"code  (vanaf Linux 2.6.23)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-04"
msgstr "4 december 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pagina's 6.03"

#. type: Plain text
#: debian-bookworm
msgid ""
"POSIX.1-2001, POSIX.1-2008, SVr4, 4.4BSD (these interfaces first appeared in "
"4.2BSD)."
msgstr ""
"POSIX.1-2001, POSIX.1-2008, SVr4, 4.4BSD (deze functie aanroepen verschenen "
"voor het eerst in 4.2BSD)."

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30 maart 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

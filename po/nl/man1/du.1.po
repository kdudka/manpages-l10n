# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
# Luc Castermans <luc.castermans@gmail.com>, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 2.15\n"
"POT-Creation-Date: 2024-05-01 15:38+0200\n"
"PO-Revision-Date: 2023-10-23 12:32+0200\n"
"Last-Translator: Luc Castermans <luc.castermans@gmail.com>\n"
"Language-Team: Dutch <>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DU"
msgstr "DU"

#. type: TH
#: archlinux debian-unstable
#, no-wrap
msgid "March 2024"
msgstr "Maart 2024"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "GNU coreutils 9.4"
msgid "GNU coreutils 9.5"
msgstr "GNU coreutils 9.4"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Opdrachten voor gebruikers"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAAM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "du - estimate file space usage"
msgstr "du - schat schijfruimte gebruik"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SAMENVATTING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<du> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]..."
msgstr "B<du> [I<\\,OPTIE\\/>]... [I<\\,BESTAND\\/>]..."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<du> [I<\\,OPTION\\/>]... I<\\,--files0-from=F\\/>"
msgstr "B<du> [I<\\,OPTIE\\/>]... I<\\,--files0-from=LST\\/>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHRIJVING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Summarize device usage of the set of FILEs, recursively for directories."
msgstr "Vat het schijfgebruik samen van de BESTAND´en, recursief voor mappen."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Een verplicht argument bij een lange optie is ook verplicht voor de korte "
"optie."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-0>, B<--null>"
msgstr "B<-0>, B<--null>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "end each output line with NUL, not newline"
msgstr "elke regel afsluiten met NUL-byte, niet met nieuwe regel"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-a>, B<--all>"
msgstr "B<-a>, B<--all>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "write counts for all files, not just directories"
msgstr "waardes tonen voor alle bestanden, niet alleen mappen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--apparent-size>"
msgstr "B<--apparent-size>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"print apparent sizes rather than device usage; although the apparent size is "
"usually smaller, it may be larger due to holes in ('sparse') files, internal "
"fragmentation, indirect blocks, and the like"
msgstr ""
"werkelijke groottes tonen in plaats van schijfbeslag; hoewel meestal "
"kleiner, kan de werkelijke grootte ook groter zijn dan het schijfbeslag, "
"bijvoorbeeld door gaten in ('ijle') bestanden, interne fragmentatie, "
"indirecte blokken en dergelijke"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-B>, B<--block-size>=I<\\,SIZE\\/>"
msgstr "B<-B>, B<--block-size>=I<\\,GROOTTE\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"scale sizes by SIZE before printing them; e.g., \\&'-BM' prints sizes in "
"units of 1,048,576 bytes; see SIZE format below"
msgstr ""
"de te tonen groottes schalen naar deze waarde; \\&'-BM' bijvoorbeeld toont "
"de groottes in eenheden van 1.048.576 bytes; zie hieronder voor meer info"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-b>, B<--bytes>"
msgstr "B<-b>, B<--bytes>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "equivalent to '--apparent-size B<--block-size>=I<\\,1\\/>'"
msgstr "hetzelfde als '--apparent-size B<--block-size>=I<\\,1\\/>'"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--total>"
msgstr "B<-c>, B<--total>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "produce a grand total"
msgstr "een eindtotaal tonen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-D>, B<--dereference-args>"
msgstr "B<-D>, B<--dereference-args>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "dereference only symlinks that are listed on the command line"
msgstr "alleen direct gegeven symbolische koppelingen volgen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--max-depth>=I<\\,N\\/>"
msgstr "B<-d>, B<--max-depth>=I<\\,N\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"print the total for a directory (or file, with B<--all>)  only if it is N or "
"fewer levels below the command line argument; B<--max-depth>=I<\\,0\\/> is "
"the same as B<--summarize>"
msgstr ""
"het totaal voor een map (of een bestand, met B<--all>) alleen tonen als deze "
"N of minder niveaus onder het opdrachtregelargument ligt; B<--max-"
"depth>=I<\\,0\\/> is hetzelfde als B<--summarize>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--files0-from>=I<\\,F\\/>"
msgstr "B<--files0-from>=I<\\,LST\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"summarize device usage of the NUL-terminated file names specified in file F; "
"if F is -, then read names from standard input"
msgstr ""
"het schijfgebruik tonen van de op NUL eindigende bestandsnamen die opgesomd "
"staan in bestand LST, als LST '-' is, dan de namen van standaardinvoer lezen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-H>"
msgstr "B<-H>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "equivalent to B<--dereference-args> (B<-D>)"
msgstr "hetzelfde als B<--dereference-args> (B<-D>)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--human-readable>"
msgstr "B<-h>, B<--human-readable>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "print sizes in human readable format (e.g., 1K 234M 2G)"
msgstr "groottes in leesbare vorm tonen (bijv. 15K, 234M, 2G)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--inodes>"
msgstr "B<--inodes>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "list inode usage information instead of block usage"
msgstr "informatie over inode-gebruik i.p.v. blokgebruik tonen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-k>"
msgstr "B<-k>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "like B<--block-size>=I<\\,1K\\/>"
msgstr "hetzelfde als B<--block-size>=I<\\,1K\\/>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-L>, B<--dereference>"
msgstr "B<-L>, B<--dereference>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "dereference all symbolic links"
msgstr "alle symbolische koppelingen volgen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-l>, B<--count-links>"
msgstr "B<-l>, B<--count-links>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "count sizes many times if hard linked"
msgstr "de grootte tellen voor elke harde koppeling"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-m>"
msgstr "B<-m>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "like B<--block-size>=I<\\,1M\\/>"
msgstr "hetzelfde als B<--block-size>=I<\\,1M\\/>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-P>, B<--no-dereference>"
msgstr "B<-P>, B<--no-dereference>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "don't follow any symbolic links (this is the default)"
msgstr "symbolische koppelingen niet volgen (standaard)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-S>, B<--separate-dirs>"
msgstr "B<-S>, B<--separate-dirs>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "for directories do not include size of subdirectories"
msgstr "bij mappen de grootte van submappen niet meetellen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--si>"
msgstr "B<--si>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "like B<-h>, but use powers of 1000 not 1024"
msgstr "als B<-h>, maar machten van 1000 i.p.v. 1024 gebruiken"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--summarize>"
msgstr "B<-s>, B<--summarize>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display only a total for each argument"
msgstr "voor elk gegeven argument alleen een totaal tonen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--threshold>=I<\\,SIZE\\/>"
msgstr "B<-t>, B<--threshold>=I<\\,GROOTTE\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"exclude entries smaller than SIZE if positive, or entries greater than SIZE "
"if negative"
msgstr ""
"items weglaten die kleiner zijn dan GROOTTE indien positief, of groter dan "
"GROOTTE indien negatief"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--time>"
msgstr "B<--time>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"show time of the last modification of any file in the directory, or any of "
"its subdirectories"
msgstr ""
"tijd van laatste wijziging tonen van elk bestand in de map, of van alle "
"submappen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--time>=I<\\,WORD\\/>"
msgstr "B<--time>=I<\\,SOORT\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"show time as WORD instead of modification time: atime, access, use, ctime or "
"status"
msgstr ""
"toon tijd als een WOORD in plaats van wijzigingstijd; 'atime', 'access', "
"'use', 'ctime' of 'status'"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--time-style>=I<\\,STYLE\\/>"
msgstr "B<--time-style>=I<\\,STIJL\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"show times using STYLE, which can be: full-iso, long-iso, iso, or +FORMAT; "
"FORMAT is interpreted like in 'date'"
msgstr ""
"te gebruiken opmaak voor tijdsstempels; mogelijke waarden zijn 'iso', 'long-"
"iso', 'full-iso', of '+OPMAAK' (zie bij 'date' voor de mogelijkheden)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-X>, B<--exclude-from>=I<\\,FILE\\/>"
msgstr "B<-X>, B<--exclude-from>=I<\\,BESTAND\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "exclude files that match any pattern in FILE"
msgstr "bestanden weglaten die overeenkomen met een patroon in BESTAND"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--exclude>=I<\\,PATTERN\\/>"
msgstr "B<--exclude>=I<\\,PATROON\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "exclude files that match PATTERN"
msgstr "bestanden weglaten die overeenkomen met PATROON"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-x>, B<--one-file-system>"
msgstr "B<-x>, B<--one-file-system>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "skip directories on different file systems"
msgstr "mappen op andere bestandssystemen overslaan"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "toon de helptekst en stop"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "toon programmaversie en stop"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Display values are in units of the first available SIZE from B<--block-"
"size>, and the DU_BLOCK_SIZE, BLOCK_SIZE and BLOCKSIZE environment "
"variables.  Otherwise, units default to 1024 bytes (or 512 if "
"POSIXLY_CORRECT is set)."
msgstr ""
"Getoonde waarden zijn in eenheden van de eerst beschikbare GROOTTE uit B<--"
"block-size> en de omgevingsvariabelen DU_BLOCK_SIZE, BLOCK_SIZE en "
"BLOCKSIZE.  Anders is de eenheid standaard 1024 bytes (of 512 als "
"POSIXLY_CORRECT gezet is)."

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The SIZE argument is an integer and optional unit (example: 10K is "
"10*1024).  Units are K,M,G,T,P,E,Z,Y,R,Q (powers of 1024) or KB,MB,... "
"(powers of 1000).  Binary prefixes can be used, too: KiB=K, MiB=M, and so on."
msgstr ""
"GROOTTE is een geheel getal met een optionele eenheid (bijv. 10K is "
"10*1024). Mogelijke eenheden zijn: K, M, G, T, P, E, Z, Y, R, Q (machten van "
"1024) of KB, MB, ... (machten van 1000).  Binaire prefixen mogen ook "
"gebruikt worden: KiB=K, MiB=M, etc."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "PATTERNS"
msgstr "PATRONEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"PATTERN is a shell pattern (not a regular expression).  The pattern B<?\\&> "
"matches any one character, whereas B<*> matches any string (composed of "
"zero, one or multiple characters).  For example, B<*.o> will match any files "
"whose names end in B<.o>.  Therefore, the command"
msgstr ""
"PATROON is een shell patroon (geen reguliere expressie). Het patroon B<?\\&> "
"past elk enkel teken, terwijl B<*> past elke tekenreeks (van nul, een of "
"meer tekens). Bijvoorbeeld, B<*.o> past op alle bestanden waarvan de naam op "
"B<.o> eindigt.  Daarom, zal het commando:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<du --exclude=\\(aq*.o\\(aq>"
msgstr "B<du --exclude=\\(aq*.o\\(aq>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"will skip all files and subdirectories ending in B<.o> (including the file "
"B<.o> itself)."
msgstr ""
"alle bestanden en submappen overslaan die eindigen op B<.o> (inclusief het "
"bestand B<.o> zelf)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Written by Torbjorn Granlund, David MacKenzie, Paul Eggert, and Jim Meyering."
msgstr ""
"Geschreven door Torbjorn Granlund, David MacKenzie, Paul Eggert en Jim "
"Meyering."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTEREN VAN BUGS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Online hulp bij GNU coreutils: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Meld alle vertaalfouten op E<lt>https://translationproject.org/team/nl."
"htmlE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU "
#| "GPL version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgid ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  Licentie GPLv3+: GNU "
"GPL versie 3 of later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Dit is vrije software: u mag het vrijelijk wijzigen en verder verspreiden. "
"Deze software kent GEEN GARANTIE, voor zover de wet dit toestaat."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZIE OOK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Full documentation E<lt>https://www.gnu.org/software/coreutils/duE<gt>"
msgstr ""
"Volledige documentatie op: E<lt>https://www.gnu.org/software/coreutils/"
"duE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) du invocation\\(aq"
msgstr "of lokaal via: info \\(aq(coreutils) du invocation\\(aq"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "September 2022"
msgstr "September 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The SIZE argument is an integer and optional unit (example: 10K is "
"10*1024).  Units are K,M,G,T,P,E,Z,Y (powers of 1024) or KB,MB,... (powers "
"of 1000).  Binary prefixes can be used, too: KiB=K, MiB=M, and so on."
msgstr ""
"GROOTTE is een geheel getal met een optionele eenheid (bijv. 10K is "
"10*1024). Mogelijke eenheden zijn: K, M, G, T, P, E, Z, Y (machten van 1024) "
"of KB, MB, ... (machten van 1000).  Binaire prefixen mogen ook gebruikt "
"worden: KiB=K, MiB=M, etc."

#. type: Plain text
#: debian-bookworm
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  Licentie GPLv3+: GNU "
"GPL versie 3 of later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: debian-unstable fedora-40 mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: Plain text
#: debian-unstable fedora-40 mageia-cauldron
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  Licentie GPLv3+: GNU "
"GPL versie 3 of later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: fedora-40 opensuse-leap-15-6
#, no-wrap
msgid "January 2024"
msgstr "Januari 2024"

#. type: TH
#: fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "April 2022"
msgid "April 2024"
msgstr "April 2022"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "August 2023"
msgstr "Augustus 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Summarize disk usage of the set of FILEs, recursively for directories."
msgstr ""
"Vat het schijfgebruik samen van elk gegeven BESTAND, recursief voor mappen."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"print apparent sizes, rather than disk usage; although the apparent size is "
"usually smaller, it may be larger due to holes in ('sparse') files, internal "
"fragmentation, indirect blocks, and the like"
msgstr ""
"werkelijke groottes tonen in plaats van schijfbeslag; hoewel meestal "
"kleiner, kan de werkelijke grootte ook groter zijn dan het schijfbeslag, "
"bijvoorbeeld bij luchtige bestanden (met onopgeslagen gaten), interne "
"fragmentatie, indirecte blokken en dergelijke"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"summarize disk usage of the NUL-terminated file names specified in file F; "
"if F is -, then read names from standard input"
msgstr ""
"het schijfgebruik tonen van de bestanden die opgesomd staan in bestand LST, "
"waar elke naam eindigt op NUL; als LST '-' is, dan de namen van "
"standaardinvoer lezen"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  Licentie GPLv3+: GNU "
"GPL versie 3 of later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

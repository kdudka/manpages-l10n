# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.20.0\n"
"POT-Creation-Date: 2024-02-15 18:15+0100\n"
"PO-Revision-Date: 2023-12-16 00:35+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "UNIFY"
msgstr "UNIFY"

#. type: TH
#: fedora-40 fedora-rawhide
#, no-wrap
msgid "January 2024"
msgstr "ianuarie 2024"

#. type: TH
#: fedora-40 fedora-rawhide
#, no-wrap
msgid "GNU wdiff 1.2.2"
msgstr "GNU wdiff 1.2.2"

#. type: TH
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Comenzi utilizator"

#. type: SH
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
msgid "unify - manual page for unify 1.2.2"
msgstr "unify - pagina de manual pentru unify 1.2.2"

#. type: SH
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<unify> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]"
msgstr "B<unify> [I<\\,OPȚIUNE\\/>]... [I<\\,FIȘIER\\/>]"

#. type: SH
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
msgid "unify - Transforms context diffs into unidiffs, or vice-versa."
msgstr ""
"unify - transformă diferențele de context în diferențe unidirecționale sau "
"invers."

#. type: TP
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--context-diffs>"
msgstr "B<-c>, B<--context-diffs>"

#. type: Plain text
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
msgid "force output to context diffs"
msgstr "forțează ieșirea la diferențele de context"

#. type: TP
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-e>, B<--echo-comments>"
msgstr "B<-e>, B<--echo-comments>"

#. type: Plain text
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
msgid "echo comments to standard error"
msgstr "trimite comentariile la ieșirea de eroare standard"

#. type: TP
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-o>, B<--old-diffs>"
msgstr "B<-o>, B<--old-diffs>"

#. type: Plain text
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
msgid "output old-style diffs, no matter what"
msgstr "afișează diferențele în stilul vechi, indiferent de situație"

#. type: TP
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-p>, B<--patch-format>"
msgstr "B<-p>, B<--patch-format>"

#. type: Plain text
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
msgid "generate patch format"
msgstr "generează ieșirea în format de plasture(patch)"

#. type: TP
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-P>"
msgstr "B<-P>"

#. type: Plain text
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
msgid "same as B<-p>"
msgstr "la fel ca B<-p>"

#. type: TP
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--strip-comments>"
msgstr "B<-s>, B<--strip-comments>"

#. type: Plain text
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
msgid "strip comment lines"
msgstr "elimină liniile de comentarii"

#. type: TP
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-u>, B<--unidiffs>"
msgstr "B<-u>, B<--unidiffs>"

#. type: Plain text
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
msgid "force output to unidiffs"
msgstr "forțează generarea rezultatului ca diferențe unificate"

#. type: TP
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-U>"
msgstr "B<-U>"

#. type: Plain text
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
msgid "same as B<-p> and B<-u>"
msgstr "la fel ca B<-p> și B<-u>."

#. type: TP
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-=>, B<--use-equals>"
msgstr "B<-=>, B<--use-equals>"

#. type: Plain text
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
msgid "replace spaces by equal signs in unidiffs"
msgstr "reamplasează spațiile, cu seme de egalitate în diferențele unificate"

#. type: TP
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
msgid "display this help then exit"
msgstr "afișează acest ajutor și iese"

#. type: TP
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
msgid "display program version then exit"
msgstr "afișează versiunea programului, apoi iese"

#. type: Plain text
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
msgid "If FILE is not specified, read standard input."
msgstr "Dacă FIȘIERul nu este specificat, citește de la intrarea standard."

#. type: SH
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
msgid "Written by Wayne Davison E<lt>davison@borland.comE<gt>."
msgstr "Scris de Wayne Davison E<lt>davison@borland.comE<gt>."

#. type: SH
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPORTAREA ERORILOR"

#. type: Plain text
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
msgid "Report bugs to E<lt>wdiff-bugs@gnu.orgE<gt>."
msgstr "Raportați erorile la E<lt>wdiff-bugs@gnu.orgE<gt>."

#. type: SH
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "DREPTURI DE AUTOR"

#. type: Plain text
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
msgid "Copyright \\(co 1994, 1997 Free Software Foundation, Inc."
msgstr "Drepturi de autor © 1994, 1997 Free Software Foundation, Inc."

#. type: Plain text
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is free software; see the source for copying conditions.  There is NO "
"warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE."
msgstr ""
"Acesta este software liber; consultați sursa pentru condițiile de copiere. "
"NU există NICIO garanție; nici măcar pentru COMERCIALIZARE sau POTRIVIRE "
"PENTRU UN ANUMIT SCOP."

#. type: SH
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The full documentation for B<unify> is maintained as a Texinfo manual.  If "
"the B<info> and B<unify> programs are properly installed at your site, the "
"command"
msgstr ""
"Documentația completă pentru B<unify> este menținută ca un manual Texinfo. "
"Dacă programele B<info> și B<unify> sunt instalate corect pe sistemul dvs., "
"comanda"

#. type: Plain text
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<info unify>"
msgstr "B<info unify>"

#. type: Plain text
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "ar trebui să vă permită accesul la manualul complet."

#. type: TH
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "April 2014"
msgstr "aprilie 2014"

#. type: TH
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "unify 1.2.2"
msgstr "unify 1.2.2"

# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2024-03-09 15:52+0100\n"
"PO-Revision-Date: 2024-03-10 11:46+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Ybmtopbm User Manual"
msgstr "Manualul utilizatorului ybmtopbm"

#. type: TH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "06 March 1990"
msgstr "6 martie 1990"

#. type: TH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "netpbm documentation"
msgstr "documentația netpbm"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "ybmtopbm - convert a Bennet Yee \"face\" file to PBM"
msgstr "ybmtopbm - convertește un fișier Bennet Yee chip „face” în PBM"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<ybmtopbm>"
msgstr "B<ybmtopbm>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "[I<facefile>]"
msgstr "[I<fișier-chip>]"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "This program is part of B<Netpbm>(1)  \\&."
msgstr "Acest program face parte din B<Netpbm>(1)\\&."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<ymtopbm> reads a file acceptable to the B<face> and B<xbm> programs by "
"Bennet Yee (I<bsy+@cs.cmu.edu>).  and writes a PBM image as output."
msgstr ""
"B<ymtopbm> citește un fișier acceptabil pentru programele B<face> și B<xbm> "
"de Bennet Yee (I<bsy+@cs.cmu.edu>). și scrie o imagine PBM ca ieșire."

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPȚIUNI"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"There are no command line options defined specifically\n"
"for B<ybmtopbm>, but it recognizes the options common to all\n"
"programs based on libnetpbm (See \n"
"E<.UR index.html#commonoptions>\n"
" Common Options\n"
"E<.UE>\n"
"\\&.)\n"
msgstr ""
"Nu există opțiuni de linie de comandă definite în mod specific\n"
"pentru B<ybmtopbm>, dar recunoaște opțiunile comune tuturor\n"
"programele bazate pe „libnetpbm” (a se vedea\n"
"E<.UR index.html#commonoptions>\n"
" Common Options\n"
"E<.UE>\n"
")\\&.\n"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
msgid "B<pbmtoybm>(1)  \\&, B<pbm>(1)  \\&"
msgstr "B<pbmtoybm>(1)  \\&, B<pbm>(1)  \\&"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Copyright (C) 1991 by Jamie Zawinski and Jef Poskanzer."
msgstr "Drepturi de autor © 1991 pentru Jamie Zawinski și Jef Poskanzer."

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DOCUMENT SOURCE"
msgstr "SURSA DOCUMENTULUI"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This manual page was generated by the Netpbm tool 'makeman' from HTML "
"source.  The master documentation is at"
msgstr ""
"Această pagină de manual a fost generată de instrumentul Netpbm «makeman» "
"din sursa HTML. Documentația principală este la"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<http://netpbm.sourceforge.net/doc/ybmtopbm.html>"
msgstr "B<http://netpbm.sourceforge.net/doc/ybmtopbm.html>"

#. type: Plain text
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<pbmtoybm>(1)  \\&, B<pbm>(5)  \\&"
msgstr "B<pbmtoybm>(1)  \\&, B<pbm>(5)  \\&"

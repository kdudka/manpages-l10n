# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.s
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2024-02-15 18:04+0100\n"
"PO-Revision-Date: 2023-09-09 23:38+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "mpartition"
msgstr "mpartition"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "21Mar23"
msgstr "21 martie 2023"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "mtools-4.0.43"
msgstr "mtools-4.0.43"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Name"
msgstr "Nume"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "mpartition - partition an MSDOS hard disk"
msgstr "mpartition - partiționează un disc dur MSDOS"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Note\\ of\\ warning"
msgstr "Notă\\ de\\ avertisment"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This manpage has been automatically generated from mtools's texinfo "
"documentation, and may not be entirely accurate or complete.  See the end of "
"this man page for details."
msgstr ""
"Această pagină de manual a fost generată automat din documentația „texinfo” "
"a «mtools» și este posibil să nu fie în întregime exactă sau completă. "
"Pentru detalii, consultați sfârșitul acestei pagini de manual."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Description"
msgstr "Descriere"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The \\&CW<mpartition> command is used to create MS-DOS file systems as "
"partitions.  This is intended to be used on non-Linux systems, i.e. systems "
"where fdisk and easy access to SCSI devices are not available.  This command "
"only works on drives whose partition variable is set."
msgstr ""
"Comanda \\&CW<mpartition> este utilizată pentru a crea sisteme de fișiere MS-"
"DOS ca partiții.  Aceasta este destinată utilizării pe sisteme non-Linux, "
"adică pe sisteme în care nu sunt disponibile fdisk și accesul ușor la "
"dispozitive SCSI.  Această comandă funcționează numai pe unități a căror "
"variabilă de partiție este definită."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"I<\\&>\\&CW<mpartition> \\&CW<-p> I<drive>\n"
"\\&\\&CW<mpartition> \\&CW<-r> I<drive>\n"
"\\&\\&CW<mpartition> \\&CW<-I> [\\&CW<-B> I<bootSector>] I<drive> \n"
"\\&\\&CW<mpartition> \\&CW<-a> I<drive>\n"
"\\&\\&CW<mpartition> \\&CW<-d> I<drive>\n"
"\\&\\&CW<mpartition> \\&CW<-c> [\\&CW<-s> I<sectors>] [\\&CW<-h> I<heads>]\n"
"[\\&CW<-t> I<cylinders>] [\\&CW<-v> [\\&CW<-T> I<type>] [\\&CW<-b>\n"
"\\&I<begin>] [\\&CW<-l> length] [\\&CW<-f>]\n"
"\\&\\&\n"
msgstr ""
"I<\\&>\\&CW<mpartition> \\&CW<-p> I<unitatea>\n"
"\\&\\&CW<mpartition> \\&CW<-r> I<unitatea>\n"
"\\&\\&CW<mpartition> \\&CW<-I> [\\&CW<-B> I<Sector-pornire>] I<unitatea> \n"
"\\&\\&CW<mpartition> \\&CW<-a> I<unitatea>\n"
"\\&\\&CW<mpartition> \\&CW<-d> I<unitatea>\n"
"\\&\\&CW<mpartition> \\&CW<-c> [\\&CW<-s> I<sectoare>] [\\&CW<-h> I<capete>]\n"
"[\\&CW<-t> I<cilindrii>] [\\&CW<-v> [\\&CW<-T> I<tip>] [\\&CW<-b>\n"
"\\&I<începe>] [\\&CW<-l> lungime] [\\&CW<-f>]\n"
"\\&\\&\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Mpartition supports the following operations:"
msgstr "mpartition acceptă următoarele operații:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\&\\&CW<p>\\ "
msgstr "\\&\\&CW<p>\\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Prints a command line to recreate the partition for the drive.  Nothing is "
"printed if the partition for the drive is not defined, or an inconsistency "
"has been detected.  If verbose (\\&CW<-v>) is also set, prints the current "
"partition table."
msgstr ""
"Lansează o linie de comandă pentru a recrea partiția pentru unitate.  Nu se "
"afișează nimic dacă partiția pentru unitate nu este definită sau dacă a fost "
"detectată o inconsecvență.  Dacă este activat de asemenea modul descriptiv "
"(\\&CW<-v>), se afișează și tabelul de partiții curent."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\&\\&CW<r>\\ "
msgstr "\\&\\&CW<r>\\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Removes the partition described by I<drive>."
msgstr "Elimină partiția descrisă de I<unitatea>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\&\\&CW<I>\\ "
msgstr "\\&\\&CW<I>\\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Initializes the partition table, and removes all partitions."
msgstr "Inițializează tabelul de partiții și elimină toate partițiile."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\&\\&CW<c>\\ "
msgstr "\\&\\&CW<c>\\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Creates the partition described by I<drive>."
msgstr "Creează partiția descrisă de I<unitatea>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\&\\&CW<a>\\ "
msgstr "\\&\\&CW<a>\\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"\"Activates\" the partition, i.e. makes it bootable.  Only one partition can "
"be bootable at a time."
msgstr ""
"„Activează” partiția, adică o face capabilă să poată fi pornită.  Doar o "
"singură partiție poate fi capabilă să poată fi pornită la un moment dat."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\&\\&CW<d>\\ "
msgstr "\\&\\&CW<d>\\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "\"Deactivates\" the partition, i.e. makes it unbootable."
msgstr "„Dezactivează” partiția, adică o face să nu poată fi pornită."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If no operation is given, the current settings are printed."
msgstr "Dacă nu se indică nicio operație, se afișează configurărrile curente."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "For partition creations, the following options are available:"
msgstr "Pentru crearea de partiții, sunt disponibile următoarele opțiuni:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\&\\&CW<s\\ >I<sectors>\\&\\ "
msgstr "\\&\\&CW<s\\ >I<sectoare>\\&\\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The number of sectors per track of the partition (which is also the number "
"of sectors per track for the whole drive)."
msgstr ""
"Numărul de sectoare pe pistă al partiției (care este, de asemenea, numărul "
"de sectoare pe pistă pentru întreaga unitate)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\&\\&CW<h\\ >I<heads>\\&\\ "
msgstr "\\&\\&CW<h\\ >I<capete>\\&\\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The number of heads of the partition (which is also the number of heads for "
"the whole drive).  By default, the geometry information (number of sectors "
"and heads) is figured out from neighboring partition table entries, or "
"guessed from the size."
msgstr ""
"Numărul de capete ale partiției (care este, de asemenea, numărul de capete "
"pentru întreaga unitate).  În mod implicit, informațiile despre geometrie "
"(numărul de sectoare și de capete) sunt calculate din intrările vecine ale "
"tabelei de partiții sau se ghicesc după dimensiune."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\&\\&CW<t\\ >I<cylinders>\\&\\ "
msgstr "\\&\\&CW<t\\ >I<clindrii>\\&\\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The number of cylinders of the partition (not the number of cylinders of the "
"whole drive."
msgstr ""
"Numărul de cilindri al partiției (nu numărul de cilindri al întregii "
"unități)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\&\\&CW<b\\ >I<begin>\\&\\ "
msgstr "\\&\\&CW<b\\ >I<începe>\\&\\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The starting offset of the partition, expressed in sectors. If begin is not "
"given, \\&CW<mpartition> lets the partition begin at the start of the disk "
"(partition number 1), or immediately after the end of the previous partition."
msgstr ""
"Decalajul inițial al partiției, exprimat în sectoare. Dacă nu se indică "
"„începe”, \\&CW<mpartition> permite ca partiția să înceapă la începutul "
"discului (partiția numărul 1) sau imediat după sfârșitul partiției "
"anterioare."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\&\\&CW<l\\ >I<length>\\&\\ "
msgstr "\\&\\&CW<l\\ >I<lungimea>\\&\\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The size (length) of the partition, expressed in sectors.  If end is not "
"given, \\&CW<mpartition> figures out the size from the number of sectors, "
"heads and cylinders.  If these are not given either, it gives the partition "
"the biggest possible size, considering disk size and start of the next "
"partition."
msgstr ""
"Dimensiunea (lungimea) partiției, exprimată în sectoare.  Dacă nu se indică "
"sfârșitul, \\&CW<mpartition> calculează dimensiunea din numărul de sectoare, "
"capete și cilindri.  În cazul în care nici acestea nu sunt date, se dă "
"partiției cea mai mare dimensiune posibilă, luând în considerare dimensiunea "
"discului și începutul partiției următoare."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The following option is available for all operation which modify the "
"partition table:"
msgstr ""
"Următoarea opțiune este disponibilă pentru toate operațiile care modifică "
"tabelul de partiții:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\&\\&CW<f>\\ "
msgstr "\\&\\&CW<f>\\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Usually, before writing back any changes to the partition, mpartition "
"performs certain consistency checks, such as checking for overlaps and "
"proper alignment of the partitions.  If any of these checks fails, the "
"partition table is not changed.  The \\&CW<-f> allows you to override these "
"safeguards."
msgstr ""
"De obicei, înainte de a scrie înapoi orice modificare a partiției, "
"B<mpartition> efectuează anumite verificări de coerență, cum ar fi "
"verificarea suprapunerilor și alinierea corectă a partițiilor.  Dacă una "
"dintre aceste verificări eșuează, tabelul de partiții nu este modificat.  "
"Funcția \\&CW<-f> vă permite să anulați aceste măsuri de siguranță."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The following options are available for all operations:"
msgstr "Următoarele opțiuni sunt disponibile pentru toate operațiile:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\&\\&CW<v>\\ "
msgstr "\\&\\&CW<v>\\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Together with \\&CW<-p> prints the partition table as it is now (no change "
"operation), or as it is after it is modified."
msgstr ""
"Împreună cu \\&CW<-p> afișează tabelul de partiții așa cum este acum (fără "
"operația de modificare), sau așa cum este după ce este modificat."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\&\\&CW<vv>\\ "
msgstr "\\&\\&CW<vv>\\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the verbosity flag is given twice, \\&CW<mpartition> will print out a "
"hexdump of the partition table when reading it from and writing it to the "
"device."
msgstr ""
"Dacă opțiunea de afișare detaliată a informațiilor este dată de două ori, "
"\\&CW<mpartition> va afișa un „hexdump” (rezultat în hexazecimal) al tabelei "
"de partiții atunci când o citește și o scrie pe dispozitiv."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The following option is available for partition table initialization:"
msgstr ""
"Următoarea opțiune este disponibilă pentru inițializarea tabelei de partiții:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\&\\&CW<B\\ >I<bootSector>\\&\\ "
msgstr "\\&\\&CW<B\\ >I<Sector-pornire>\\&\\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Reads the template master boot record from file I<bootSector>."
msgstr ""
"Citește înregistrarea principală de pornire a șablonului din fișierul "
"I<Sector-pornire> (bootSector)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Choice\\ of\\ partition\\ type"
msgstr "Alegerea tipului de partiție"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Mpartition proceeds as follows to pick a type for the partition:"
msgstr ""
"B<mpartition> procedează după cum urmează pentru a alege un tip de partiție:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "-\\ \\ "
msgstr "-\\ \\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "FAT32 partitions are assigned type 0x0C (``\\&CW<Win95 FAT32, LBA>'')"
msgstr ""
"Partițiilor FAT32 li se atribuie tipul 0x0C (``\\&CW<Win95 FAT32, LBA>'')."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For all others, if the partition fits entirely within the first 65536 "
"sectors of the disk, assign 0x01 (``\\&CW<DOS FAT12, CHS>'') for FAT12 "
"partition and 0x04 (``\\&CW<DOS FAT16, CHS>'') for FAT16 partitions"
msgstr ""
"Pentru toate celelalte, dacă partiția se încadrează în întregime în primele "
"65536 sectoare ale discului, atribuie 0x01 („\\&CW<DOS FAT12, CHS>'”) pentru "
"partiția FAT12 și 0x04 („\\&CW<DOS FAT16, CHS>”) pentru partițiile FAT16."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If not covered by the above, assign 0x06 (``\\&CW<DOS BIG FAT16 CHS>'') if "
"partition fits entirely within the first 1024 cylinders (CHS mode)"
msgstr ""
"Dacă nu se încadrează de cele de mai sus, atribuie 0x06 („\\&CW<DOS BIG "
"FAT16 CHS>”) dacă partiția se încadrează în întregime în primii 1024 de "
"cilindri (modul CHS)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "All remaining cases get 0x0E (``\\&CW<Win95 BIG FAT16, LBA>'')"
msgstr "Toate cazurile rămase primesc 0x0E („\\&CW<Win95 BIG FAT16, LBA>”)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If number of fat bits is not known (not specified in drive's definition), "
"then FAT12 is assumed for all drives with less than 4096 sectors, and FAT16 "
"for those with more than 4096 sectors."
msgstr ""
"Dacă numărul de biți de FAT nu este cunoscut (nu este specificat în "
"definiția unității), atunci se presupune FAT12 pentru toate unitățile cu mai "
"puțin de 4096 sectoare și FAT16 pentru cele cu mai mult de 4096 sectoare."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This corresponds more or less to the definitions outlined at \\&CW<https://"
"en.wikipedia.org/wiki/Partition_type#List_of_partition_IDs> and "
"\\&\\&CW<https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/"
"windows-2000-server/cc977219(v=technet.10)>, with two notable differences:"
msgstr ""
"Aceasta corespunde mai mult sau mai puțin definițiilor prezentate în "
"paginile \\&CW<https://en.wikipedia.org/wiki/"
"Partition_type#List_of_partition_IDs> și \\&\\&CW<https://docs.microsoft.com/"
"en-us/previous-versions/windows/it-pro/windows-2000-server/"
"cc977219(v=technet.10)>, cu două diferențe notabile:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If fat bits are unknown, the reference documents consider drives with less "
"than 32680 sectors to be FAT12. Mtools uses 4096 sectors as the cutoff "
"point, as older versions of DOS only support FAT12 on disks with less than "
"4096 sectors (and these older versions are the ones which would be most "
"likely to use FAT12 in the first place)."
msgstr ""
"În cazul în care biții de FAT sunt necunoscuți, documentele de referință "
"consideră că unitățile cu mai puțin de 32680 de sectoare sunt FAT12. Mtools "
"folosește 4096 de sectoare ca punct de separare, deoarece versiunile mai "
"vechi de DOS acceptă FAT12 numai pe discuri cu mai puțin de 4096 de sectoare "
"(iar aceste versiuni mai vechi sunt cele care ar fi cel mai probabil să "
"folosească FAT12 în primul rând)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The reference documents use a 8GB (wikipedia) or a 4GB (Microsoft)  cutoff "
"between 0x06 (\\&CW<DOS BIG FAT16 CHS>) and 0x0E. Mtools uses 1024 "
"cylinders. This is because any partition beyond 1024 cylinders must be LBA "
"and cannot be CHS. 8GB works out to be the biggest capacity which can be "
"represented as CHS (63 sectors, 255 heads and 1024 cylinders). 4GB is the "
"capacity limit for windows 2000, so it makes sense that a documentation for "
"windows 2000 would specify this as the upper limit for any partition type."
msgstr ""
"Documentele de referință folosesc o limită de 8GB (wikipedia) sau de 4GB "
"(Microsoft) între 0x06 (\\&CW<DOS BIG FAT16 CHS>) și 0x0E. Mtools utilizează "
"1024 de cilindri. Acest lucru se datorează faptului că orice partiție care "
"depășește 1024 de cilindri trebuie să fie LBA și nu poate fi CHS. 8GB este "
"cea mai mare capacitate care poate fi reprezentată ca CHS (63 de sectoare, "
"255 de capete și 1024 de cilindri). 4GB este limita de capacitate pentru "
"Windows 2000, astfel încât este logic ca o documentație pentru Windows 2000 "
"să specifice această limită superioară pentru orice tip de partiție."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "See\\ Also"
msgstr "Consultați\\ și"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Mtools' texinfo doc"
msgstr "Documentația texinfo de Mtools"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Viewing\\ the\\ texi\\ doc"
msgstr "Vizualizarea\\ documentului\\ texi"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This manpage has been automatically generated from mtools's texinfo "
"documentation. However, this process is only approximative, and some items, "
"such as crossreferences, footnotes and indices are lost in this translation "
"process.  Indeed, these items have no appropriate representation in the "
"manpage format.  Moreover, not all information has been translated into the "
"manpage version.  Thus I strongly advise you to use the original texinfo "
"doc.  See the end of this manpage for instructions how to view the texinfo "
"doc."
msgstr ""
"Această pagină de manual a fost generată automat din documentația texinfo a "
"„mtools”. Cu toate acestea, acest proces este doar aproximativ, iar unele "
"elemente, cum ar fi referințele încrucișate, notele de subsol și indicii, se "
"pierd în acest proces de conversie. Într-adevăr, aceste elemente nu au o "
"reprezentare adecvată în formatul paginii de manual. În plus, nu toate "
"informațiile au fost convertite în versiunea pentru pagina de manual. Prin "
"urmare, vă sfătuiesc cu tărie să folosiți documentul texinfo original. "
"Consultați sfârșitul acestei pagini de manual pentru instrucțiuni privind "
"modul de vizualizare a documentului texinfo."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "* \\ \\ "
msgstr "* \\ \\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"To generate a printable copy from the texinfo doc, run the following "
"commands:"
msgstr ""
"Pentru a genera o copie imprimabilă din documentul texinfo, executați "
"următoarele comenzi:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<    ./configure; make dvi; dvips mtools.dvi>\n"
msgstr "B<    ./configure; make dvi; dvips mtools.dvi>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "To generate a html copy, run:"
msgstr "Pentru a genera o copie html, executați:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<    ./configure; make html>\n"
msgstr "B<    ./configure; make html>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"\\&A premade html can be found at \\&\\&CW<\\(ifhttp://www.gnu.org/software/"
"mtools/manual/mtools.html\\(is>"
msgstr ""
"\\&Un fișier html preconstruit poate fi găsit la adresa \\&\\&CW<\\(ifhttp://"
"www.gnu.org/software/mtools/manual/mtools.html\\(is>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "To generate an info copy (browsable using emacs' info mode), run:"
msgstr ""
"Pentru a genera o copie info (care poate fi răsfoită folosind modul info al "
"lui emacs), executați:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<    ./configure; make info>\n"
msgstr "B<    ./configure; make info>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The texinfo doc looks most pretty when printed or as html.  Indeed, in the "
"info version certain examples are difficult to read due to the quoting "
"conventions used in info."
msgstr ""
"Documentul texinfo arată cel mai bine atunci când este imprimat sau în "
"format html. Într-adevăr, în versiunea info, anumite exemple sunt greu de "
"citit din cauza convențiilor de folosire a ghilimelelor utilizate în «info»."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "10Jul21"
msgstr "10 iulie 2021"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "mtools-4.0.32"
msgstr "mtools-4.0.32"

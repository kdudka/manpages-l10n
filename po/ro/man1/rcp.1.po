# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2024-02-09 17:06+0100\n"
"PO-Revision-Date: 2023-07-12 08:52+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "RCP"
msgstr "RCP"

#. type: TH
#: archlinux
#, no-wrap
msgid "December 2023"
msgstr "decembrie 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "GNU inetutils 2.5"
msgstr "GNU inetutils 2.5"

#. type: TH
#: archlinux
#, no-wrap
msgid "User Commands"
msgstr "Comenzi utilizator"

#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux
msgid "rcp - Remote copy"
msgstr "rcp - Copiază (de) la distanță"

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: archlinux
msgid "B<rcp> [I<\\,OPTION\\/>...] I<\\,SOURCE DEST\\/>"
msgstr "B<rcp> [I<\\,OPȚIUNE\\/>...] I<\\,SURSĂ DESTINAȚIE\\/>"

#. type: Plain text
#: archlinux
msgid "B<rcp> [I<\\,OPTION\\/>...] I<\\,SOURCE\\/>... I<\\,DIRECTORY\\/>"
msgstr "B<rcp> [I<\\,OPȚIUNE\\/>...] I<\\,SURSĂ\\/>... I<\\,DIRECTOR\\/>"

#. type: Plain text
#: archlinux
msgid ""
"B<rcp> [I<\\,OPTION\\/>...] I<\\,--target-directory=DIRECTORY SOURCE\\/>..."
msgstr ""
"B<rcp> [I<\\,OPȚIUNE\\/>...] I<\\,--target-directory=DIRECTOR SURSĂ\\/>..."

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux
msgid "Remote copy SOURCE to DEST, or multiple SOURCE(s) to DIRECTORY."
msgstr ""
"Copiază de la distanță SURSA în DESTINAȚIE sau mai multe SURSE în DIRECTOR."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-4>, B<--ipv4>"
msgstr "B<-4>, B<--ipv4>"

#. type: Plain text
#: archlinux
msgid "use only IPv4"
msgstr "utilizează doar ipv4"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-6>, B<--ipv6>"
msgstr "B<-6>, B<--ipv6>"

#. type: Plain text
#: archlinux
msgid "use only IPv6"
msgstr "utilizează doar ipv6"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-d>, B<--target-directory>[=I<\\,DIRECTORY\\/>]"
msgstr "B<-d>, B<--target-directory>[=I<\\,DIRECTOR\\/>]"

#. type: Plain text
#: archlinux
msgid "copy all SOURCE arguments into DIRECTORY"
msgstr "copiază toate argumentele SURSĂ în DIRECTOR"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-f>, B<--from>"
msgstr "B<-f>, B<--from>"

#. type: Plain text
#: archlinux
msgid "copying from remote host (server use only)"
msgstr "copierea de la o gazdă la distanță (numai pentru server)"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-p>, B<--preserve>"
msgstr "B<-p>, B<--preserve>"

#. type: Plain text
#: archlinux
msgid ""
"attempt to preserve (duplicate) in its copies the modification times and "
"modes of the source files"
msgstr ""
"încercarea de a păstra (duplica) în copiile sale data+ora și modurile de "
"modificare a fișierelor sursă."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-r>, B<--recursive>"
msgstr "B<-r>, B<--recursive>"

#. type: Plain text
#: archlinux
msgid ""
"if any of the source files are directories, copies each subtree rooted at "
"that name; in this case the destination must be a directory"
msgstr ""
"în cazul în care oricare dintre fișierele sursă sunt directoare, copiază "
"fiecare sub-arbore care își are rădăcina în acel nume; în acest caz, "
"destinația trebuie să fie un director"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-t>, B<--to>"
msgstr "B<-t>, B<--to>"

#. type: Plain text
#: archlinux
msgid "copying to remote host (server use only)"
msgstr "copierea la o gazdă la distanță (numai pentru server)"

#. type: TP
#: archlinux
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux
msgid "give this help list"
msgstr "oferă această listă de ajutor"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux
msgid "give a short usage message"
msgstr "oferă un mesaj de utilizare scurt"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux
msgid "print program version"
msgstr "afișează versiunea programului"

#. type: Plain text
#: archlinux
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Argumentele obligatorii sau opționale pentru opțiunile lungi sunt "
"obligatorii sau opționale și pentru opțiunile corespunzătoare scurte."

#. type: SH
#: archlinux
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux
msgid "Written by many authors."
msgstr "Scris de mai mulți autori."

#. type: SH
#: archlinux
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPORTAREA ERORILOR"

#. type: Plain text
#: archlinux
msgid "Report bugs to E<lt>bug-inetutils@gnu.orgE<gt>."
msgstr "Raportați erorile la: E<lt>bug-inetutils@gnu.orgE<gt>."

#. type: SH
#: archlinux
#, no-wrap
msgid "COPYRIGHT"
msgstr "DREPTURI DE AUTOR"

#. type: Plain text
#: archlinux
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Drepturi de autor \\(co 2023 Free Software Foundation, Inc. Licența GPLv3+: "
"GNU GPL versiunea 3 sau ulterioară E<lt>https://gnu.org/licenses/gpl."
"htmlE<gt>."

#. type: Plain text
#: archlinux
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Acesta este software liber: sunteți liber să-l modificați și să-l "
"redistribuiți. Nu există NICIO GARANȚIE, în limitele prevăzute de lege."

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux
msgid ""
"The full documentation for B<rcp> is maintained as a Texinfo manual.  If the "
"B<info> and B<rcp> programs are properly installed at your site, the command"
msgstr ""
"Documentația completă pentru B<rcp> este menținută ca un manual Texinfo.  "
"Dacă programele B<info> și B<rcp> sunt instalate corect în sistemul dvs., "
"comanda"

#. type: Plain text
#: archlinux
msgid "B<info rcp>"
msgstr "B<info rcp>"

#. type: Plain text
#: archlinux
msgid "should give you access to the complete manual."
msgstr "ar trebui să vă permită accesul la manualul complet."

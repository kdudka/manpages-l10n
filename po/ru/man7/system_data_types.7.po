# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alexander Golubev <fatzer2@gmail.com>, 2018.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2011, 2014-2016.
# Hotellook, 2014.
# Nikita <zxcvbnm3230@mail.ru>, 2014.
# Spiros Georgaras <sng@hellug.gr>, 2016.
# Vladislav <ivladislavefimov@gmail.com>, 2015.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-05-01 15:55+0200\n"
"PO-Revision-Date: 2019-10-15 18:55+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "system_data_types"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr "Linux man-pages 6.7"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "system_data_types - overview of system data types"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I<siginfo_t>"
msgstr "I<siginfo_t>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "B<#include E<lt>sys/vfs.hE<gt> >/* or E<lt>sys/statfs.hE<gt> */"
msgid ""
"I<Include>: I<E<lt>signal.hE<gt>>.  Alternatively, I<E<lt>sys/wait.hE<gt>>."
msgstr "B<#include E<lt>sys/vfs.hE<gt> >/* или E<lt>sys/statfs.hE<gt> */"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"typedef struct {\n"
"    int      si_signo;  /* Signal number */\n"
"    int      si_code;   /* Signal code */\n"
"    pid_t    si_pid;    /* Sending process ID */\n"
"    uid_t    si_uid;    /* Real user ID of sending process */\n"
"    void    *si_addr;   /* Memory location which caused fault */\n"
"    int      si_status; /* Exit value or signal */\n"
"    union sigval si_value;  /* Signal value */\n"
"} siginfo_t;\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Information associated with a signal.  For further details on this structure "
"(including additional, Linux-specific fields), see B<sigaction>(2)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<Conforming to>: POSIX.1-2001 and later."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<sigaction>(2), B<signalfd>(2), B<sigpending>(2), B<sigsuspend>(2), "
#| "B<sigwaitinfo>(2), B<sigsetops>(3), B<signal>(7)"
msgid ""
"I<See also>: B<pidfd_send_signal>(2), B<rt_sigqueueinfo>(2), "
"B<sigaction>(2), B<sigwaitinfo>(2), B<psiginfo>(3)"
msgstr ""
"B<sigaction>(2), B<signalfd>(2), B<sigpending>(2), B<sigsuspend>(2), "
"B<sigwaitinfo>(2), B<sigsetops>(3), B<signal>(7)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I<sigset_t>"
msgstr "I<sigset_t>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<#include E<lt>sys/types.hE<gt>>\n"
#| "B<#include E<lt>sys/ipc.hE<gt>>\n"
#| "B<#include E<lt>sys/sem.hE<gt>>\n"
msgid ""
"I<Include>: I<E<lt>signal.hE<gt>>.  Alternatively, I<E<lt>spawn.hE<gt>>, or "
"I<E<lt>sys/select.hE<gt>>."
msgstr ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>sys/ipc.hE<gt>>\n"
"B<#include E<lt>sys/sem.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is a type that represents a set of signals.  According to POSIX, this "
"shall be an integer or structure type."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<kill>(2), B<sigaction>(2), B<signal>(2), B<signalfd>(2), "
#| "B<sigpending>(2), B<sigprocmask>(2), B<sigqueue>(3), B<sigsetops>(3), "
#| "B<sigwait>(3), B<signal>(7), B<time>(7)"
msgid ""
"I<See also>: B<epoll_pwait>(2), B<ppoll>(2), B<pselect>(2), B<sigaction>(2), "
"B<signalfd>(2), B<sigpending>(2), B<sigprocmask>(2), B<sigsuspend>(2), "
"B<sigwaitinfo>(2), B<signal>(7)"
msgstr ""
"B<kill>(2), B<sigaction>(2), B<signal>(2), B<signalfd>(2), B<sigpending>(2), "
"B<sigprocmask>(2), B<sigqueue>(3), B<sigsetops>(3), B<sigwait>(3), "
"B<signal>(7), B<time>(7)"

#. #-#-#-#-#  archlinux: system_data_types.7.pot (PACKAGE VERSION)  #-#-#-#-#
#. ------------------------------------- sigval -----------------------/
#. ------------------------------------- size_t -----------------------/
#. ------------------------------------- sockaddr ---------------------/
#. ------------------------------------- socklen_t --------------------/
#. ------------------------------------- ssize_t ----------------------/
#. ------------------------------------- stat -------------------------/
#. ------------------------------------- suseconds_t ------------------/
#. ------------------------------------- time_t -----------------------/
#. ------------------------------------- timer_t ----------------------/
#. ------------------------------------- timespec ---------------------/
#. ------------------------------------- timeval ----------------------/
#. ------------------------------------- uid_t ----------------------/
#. ------------------------------------- uintmax_t --------------------/
#. ------------------------------------- uintN_t ----------------------/
#. ------------------------------------- uintptr_t --------------------/
#. ------------------------------------- useconds_t -------------------/
#. ------------------------------------- va_list ----------------------/
#. ------------------------------------- void * -----------------------/
#. --------------------------------------------------------------------/
#. type: SH
#. #-#-#-#-#  debian-bookworm: system_data_types.7.pot (PACKAGE VERSION)  #-#-#-#-#
#. ------------------------------------- size_t -----------------------/
#. ------------------------------------- sockaddr ---------------------/
#. ------------------------------------- socklen_t --------------------/
#. ------------------------------------- ssize_t ----------------------/
#. ------------------------------------- stat -------------------------/
#. ------------------------------------- suseconds_t ------------------/
#. ------------------------------------- time_t -----------------------/
#. ------------------------------------- timer_t ----------------------/
#. ------------------------------------- timespec ---------------------/
#. ------------------------------------- timeval ----------------------/
#. ------------------------------------- uid_t ----------------------/
#. ------------------------------------- uintmax_t --------------------/
#. ------------------------------------- uintN_t ----------------------/
#. ------------------------------------- uintptr_t --------------------/
#. ------------------------------------- useconds_t -------------------/
#. ------------------------------------- va_list ----------------------/
#. ------------------------------------- void * -----------------------/
#. --------------------------------------------------------------------/
#. type: SH
#. #-#-#-#-#  debian-unstable: system_data_types.7.pot (PACKAGE VERSION)  #-#-#-#-#
#. ------------------------------------- sigval -----------------------/
#. ------------------------------------- size_t -----------------------/
#. ------------------------------------- sockaddr ---------------------/
#. ------------------------------------- socklen_t --------------------/
#. ------------------------------------- ssize_t ----------------------/
#. ------------------------------------- stat -------------------------/
#. ------------------------------------- suseconds_t ------------------/
#. ------------------------------------- time_t -----------------------/
#. ------------------------------------- timer_t ----------------------/
#. ------------------------------------- timespec ---------------------/
#. ------------------------------------- timeval ----------------------/
#. ------------------------------------- uid_t ----------------------/
#. ------------------------------------- uintmax_t --------------------/
#. ------------------------------------- uintN_t ----------------------/
#. ------------------------------------- uintptr_t --------------------/
#. ------------------------------------- useconds_t -------------------/
#. ------------------------------------- va_list ----------------------/
#. ------------------------------------- void * -----------------------/
#. --------------------------------------------------------------------/
#. type: SH
#. #-#-#-#-#  fedora-40: system_data_types.7.pot (PACKAGE VERSION)  #-#-#-#-#
#. ------------------------------------- sigval -----------------------/
#. ------------------------------------- size_t -----------------------/
#. ------------------------------------- sockaddr ---------------------/
#. ------------------------------------- socklen_t --------------------/
#. ------------------------------------- ssize_t ----------------------/
#. ------------------------------------- stat -------------------------/
#. ------------------------------------- suseconds_t ------------------/
#. ------------------------------------- time_t -----------------------/
#. ------------------------------------- timer_t ----------------------/
#. ------------------------------------- timespec ---------------------/
#. ------------------------------------- timeval ----------------------/
#. ------------------------------------- uid_t ----------------------/
#. ------------------------------------- uintmax_t --------------------/
#. ------------------------------------- uintN_t ----------------------/
#. ------------------------------------- uintptr_t --------------------/
#. ------------------------------------- useconds_t -------------------/
#. ------------------------------------- va_list ----------------------/
#. ------------------------------------- void * -----------------------/
#. --------------------------------------------------------------------/
#. type: SH
#. #-#-#-#-#  fedora-rawhide: system_data_types.7.pot (PACKAGE VERSION)  #-#-#-#-#
#. ------------------------------------- sigval -----------------------/
#. ------------------------------------- size_t -----------------------/
#. ------------------------------------- sockaddr ---------------------/
#. ------------------------------------- socklen_t --------------------/
#. ------------------------------------- ssize_t ----------------------/
#. ------------------------------------- stat -------------------------/
#. ------------------------------------- suseconds_t ------------------/
#. ------------------------------------- time_t -----------------------/
#. ------------------------------------- timer_t ----------------------/
#. ------------------------------------- timespec ---------------------/
#. ------------------------------------- timeval ----------------------/
#. ------------------------------------- uid_t ----------------------/
#. ------------------------------------- uintmax_t --------------------/
#. ------------------------------------- uintN_t ----------------------/
#. ------------------------------------- uintptr_t --------------------/
#. ------------------------------------- useconds_t -------------------/
#. ------------------------------------- va_list ----------------------/
#. ------------------------------------- void * -----------------------/
#. --------------------------------------------------------------------/
#. type: SH
#. #-#-#-#-#  mageia-cauldron: system_data_types.7.pot (PACKAGE VERSION)  #-#-#-#-#
#. ------------------------------------- sigval -----------------------/
#. ------------------------------------- size_t -----------------------/
#. ------------------------------------- sockaddr ---------------------/
#. ------------------------------------- socklen_t --------------------/
#. ------------------------------------- ssize_t ----------------------/
#. ------------------------------------- stat -------------------------/
#. ------------------------------------- suseconds_t ------------------/
#. ------------------------------------- time_t -----------------------/
#. ------------------------------------- timer_t ----------------------/
#. ------------------------------------- timespec ---------------------/
#. ------------------------------------- timeval ----------------------/
#. ------------------------------------- uid_t ----------------------/
#. ------------------------------------- uintmax_t --------------------/
#. ------------------------------------- uintN_t ----------------------/
#. ------------------------------------- uintptr_t --------------------/
#. ------------------------------------- useconds_t -------------------/
#. ------------------------------------- va_list ----------------------/
#. ------------------------------------- void * -----------------------/
#. --------------------------------------------------------------------/
#. type: SH
#. #-#-#-#-#  opensuse-leap-15-6: system_data_types.7.pot (PACKAGE VERSION)  #-#-#-#-#
#. ------------------------------------- size_t -----------------------/
#. ------------------------------------- sockaddr ---------------------/
#. ------------------------------------- socklen_t --------------------/
#. ------------------------------------- ssize_t ----------------------/
#. ------------------------------------- stat -------------------------/
#. ------------------------------------- suseconds_t ------------------/
#. ------------------------------------- time_t -----------------------/
#. ------------------------------------- timer_t ----------------------/
#. ------------------------------------- timespec ---------------------/
#. ------------------------------------- timeval ----------------------/
#. ------------------------------------- uid_t ----------------------/
#. ------------------------------------- uintmax_t --------------------/
#. ------------------------------------- uintN_t ----------------------/
#. ------------------------------------- uintptr_t --------------------/
#. ------------------------------------- useconds_t -------------------/
#. ------------------------------------- va_list ----------------------/
#. ------------------------------------- void * -----------------------/
#. --------------------------------------------------------------------/
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: system_data_types.7.pot (PACKAGE VERSION)  #-#-#-#-#
#. ------------------------------------- sigval -----------------------/
#. ------------------------------------- size_t -----------------------/
#. ------------------------------------- sockaddr ---------------------/
#. ------------------------------------- socklen_t --------------------/
#. ------------------------------------- ssize_t ----------------------/
#. ------------------------------------- stat -------------------------/
#. ------------------------------------- suseconds_t ------------------/
#. ------------------------------------- time_t -----------------------/
#. ------------------------------------- timer_t ----------------------/
#. ------------------------------------- timespec ---------------------/
#. ------------------------------------- timeval ----------------------/
#. ------------------------------------- uid_t ----------------------/
#. ------------------------------------- uintmax_t --------------------/
#. ------------------------------------- uintN_t ----------------------/
#. ------------------------------------- uintptr_t --------------------/
#. ------------------------------------- useconds_t -------------------/
#. ------------------------------------- va_list ----------------------/
#. ------------------------------------- void * -----------------------/
#. --------------------------------------------------------------------/
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The structures described in this manual page shall contain, at least, the "
"members shown in their definition, in no particular order."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Most of the integer types described in this page don't have a corresponding "
"length modifier for the B<printf>(3)  and the B<scanf>(3)  families of "
"functions.  To print a value of an integer type that doesn't have a length "
"modifier, it should be converted to I<intmax_t> or I<uintmax_t> by an "
"explicit cast.  To scan into a variable of an integer type that doesn't have "
"a length modifier, an intermediate temporary variable of type I<intmax_t> or "
"I<uintmax_t> should be used.  When copying from the temporary variable to "
"the destination variable, the value could overflow.  If the type has upper "
"and lower limits, the user should check that the value is within those "
"limits, before actually copying the value.  The example below shows how "
"these conversions should be done."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Conventions used in this page"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In \"Conforming to\" we only concern ourselves with C99 and later and "
"POSIX.1-2001 and later.  Some types may be specified in earlier versions of "
"one of these standards, but in the interests of simplicity we omit details "
"from earlier standards."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In \"Include\", we first note the \"primary\" header(s) that define the type "
"according to either the C or POSIX.1 standards.  Under \"Alternatively\", we "
"note additional headers that the standards specify shall define the type."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "ПРИМЕРЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The program shown below scans from a string and prints a value stored in a "
"variable of an integer type that doesn't have a length modifier.  The "
"appropriate conversions from and to I<intmax_t>, and the appropriate range "
"checks, are used as explained in the notes section above."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/types.hE<gt>\n"
"\\&\n"
"int\n"
"main (void)\n"
"{\n"
"    static const char *const str = \"500000 us in half a second\";\n"
"    suseconds_t us;\n"
"    intmax_t    tmp;\n"
"\\&\n"
"    /* Scan the number from the string into the temporary variable. */\n"
"\\&\n"
"    sscanf(str, \"%jd\", &tmp);\n"
"\\&\n"
"    /* Check that the value is within the valid range of suseconds_t. */\n"
"\\&\n"
"    if (tmp E<lt> -1 || tmp E<gt> 1000000) {\n"
"        fprintf(stderr, \"Scanned value outside valid range!\\en\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    /* Copy the value to the suseconds_t variable \\[aq]us\\[aq]. */\n"
"\\&\n"
"    us = tmp;\n"
"\\&\n"
"    /* Even though suseconds_t can hold the value -1, this isn\\[aq]t\n"
"       a sensible number of microseconds. */\n"
"\\&\n"
"    if (us E<lt> 0) {\n"
"        fprintf(stderr, \"Scanned value shouldn\\[aq]t be negative!\\en\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    /* Print the value. */\n"
"\\&\n"
"    printf(\"There are %jd microseconds in half a second.\\en\",\n"
"            (intmax_t) us);\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<feature_test_macros>(7), B<standards>(7)"
msgstr "B<feature_test_macros>(7), B<standards>(7)"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TP
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "I<sigevent>"
msgstr "I<sigevent>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"I<Include>: I<E<lt>signal.hE<gt>>.  Alternatively, I<E<lt>aio.hE<gt>>, "
"I<E<lt>mqueue.hE<gt>>, or I<E<lt>time.hE<gt>>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid ""
#| "struct sigevent {\n"
#| "    int          sigev_notify; /* Notification method */\n"
#| "    int          sigev_signo;  /* Notification signal */\n"
#| "    union sigval sigev_value;  /* Data passed with\n"
#| "                                  notification */\n"
#| "    void       (*sigev_notify_function) (union sigval);\n"
#| "                     /* Function used for thread\n"
#| "                        notification (SIGEV_THREAD) */\n"
#| "    void        *sigev_notify_attributes;\n"
#| "                     /* Attributes for notification thread\n"
#| "                        (SIGEV_THREAD) */\n"
#| "    pid_t        sigev_notify_thread_id;\n"
#| "                     /* ID of thread to signal (SIGEV_THREAD_ID) */\n"
#| "};\n"
msgid ""
"struct sigevent {\n"
"    int             sigev_notify; /* Notification type */\n"
"    int             sigev_signo;  /* Signal number */\n"
"    union sigval    sigev_value;  /* Signal value */\n"
"    void          (*sigev_notify_function)(union sigval);\n"
"                                  /* Notification function */\n"
"    pthread_attr_t *sigev_notify_attributes;\n"
"                                  /* Notification attributes */\n"
"};\n"
msgstr ""
"struct sigevent {\n"
"    int          sigev_notify; /* метод уведомления */\n"
"    int          sigev_signo;  /* сигнал уведомления */\n"
"    union sigval sigev_value;  /* данные, передаваемые\n"
"                                  с уведомлением */\n"
"    void       (*sigev_notify_function) (union sigval);\n"
"                     /* функция, используемая для нити\n"
"                        notification (SIGEV_THREAD) */\n"
"    void        *sigev_notify_attributes;\n"
"                     /* атрибуты для уведомления нити\n"
"                        (SIGEV_THREAD) */\n"
"    pid_t        sigev_notify_thread_id;\n"
"                     /* ID нити для уведомления (SIGEV_THREAD_ID) */\n"
"};\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy
#| msgid "For further details on the nice value, see B<sched>(7)."
msgid "For further details about this type, see B<sigevent>(7)."
msgstr ""
"Дополнительную информацию о значении уступчивости смотрите в B<sched>(7)."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"I<Versions>: I<E<lt>aio.hE<gt>> and I<E<lt>time.hE<gt>> define I<sigevent> "
"since POSIX.1-2008."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "B<timer_create>(2), B<aio_fsync>(3), B<aio_read>(3), B<aio_write>(3), "
#| "B<getaddrinfo_a>(3), B<lio_listio>(3), B<mq_notify>(3), B<aio>(7), "
#| "B<pthreads>(7)"
msgid ""
"I<See also>: B<timer_create>(2), B<getaddrinfo_a>(3), B<lio_listio>(3), "
"B<mq_notify>(3)"
msgstr ""
"B<timer_create>(2), B<aio_fsync>(3), B<aio_read>(3), B<aio_write>(3), "
"B<getaddrinfo_a>(3), B<lio_listio>(3), B<mq_notify>(3), B<aio>(7), "
"B<pthreads>(7)"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "See also the I<aiocb> structure in this page."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"typedef struct {\n"
"    int      si_signo;  /* Signal number */\n"
"    int      si_code;   /* Signal code */\n"
"    pid_t    si_pid;    /* Sending process ID */\n"
"    uid_t    si_uid;    /* Real user ID of sending process */\n"
"    void    *si_addr;   /* Address of faulting instruction */\n"
"    int      si_status; /* Exit value or signal */\n"
"    union sigval si_value;  /* Signal value */\n"
"} siginfo_t;\n"
msgstr ""

#. type: TP
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "I<sigval>"
msgstr "I<sigval>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy
#| msgid "B<#include E<lt>signal.hE<gt>>"
msgid "I<Include>: I<E<lt>signal.hE<gt>>."
msgstr "B<#include E<lt>signal.hE<gt>>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid ""
#| "union sigval {          /* Data passed with notification */\n"
#| "    int     sival_int;         /* Integer value */\n"
#| "    void   *sival_ptr;         /* Pointer value */\n"
#| "};\n"
msgid ""
"union sigval {\n"
"    int     sigval_int; /* Integer value */\n"
"    void   *sigval_ptr; /* Pointer value */\n"
"};\n"
msgstr ""
"union sigval {          /* Данные, передаваемые с уведомлением */\n"
"    int     sival_int;         /* целое */\n"
"    void   *sival_ptr;         /* указатель */\n"
"};\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy
#| msgid "sigwait - wait for a signal"
msgid "Data passed with a signal."
msgstr "sigwait - ожидание сигнала"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "I<See also>: B<pthread_sigqueue>(3), B<sigqueue>(3), B<sigevent>(7)"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"See also the I<sigevent> structure and the I<siginfo_t> type in this page."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/types.hE<gt>\n"
msgstr ""
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/types.hE<gt>\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"int\n"
"main (void)\n"
"{\n"
"    static const char *const str = \"500000 us in half a second\";\n"
"    suseconds_t us;\n"
"    intmax_t    tmp;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "    /* Scan the number from the string into the temporary variable. */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "    sscanf(str, \"%jd\", &tmp);\n"
msgstr "    sscanf(str, \"%jd\", &tmp);\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "    /* Check that the value is within the valid range of suseconds_t. */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid ""
#| "    if (argc E<lt> 2) {\n"
#| "        fprintf(stderr, \"Usage: %s str [base]\\en\", argv[0]);\n"
#| "        exit(EXIT_FAILURE);\n"
#| "    }\n"
msgid ""
"    if (tmp E<lt> -1 || tmp E<gt> 1000000) {\n"
"        fprintf(stderr, \"Scanned value outside valid range!\\en\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (argc E<lt> 2) {\n"
"        fprintf(stderr, \"Использование: %s строка [система_счисления]\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "    /* Copy the value to the suseconds_t variable \\[aq]us\\[aq]. */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "    us = tmp;\n"
msgstr "    us = tmp;\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    /* Even though suseconds_t can hold the value -1, this isn\\[aq]t\n"
"       a sensible number of microseconds. */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid ""
#| "    if (argc E<lt> 2) {\n"
#| "        fprintf(stderr, \"Usage: %s str [base]\\en\", argv[0]);\n"
#| "        exit(EXIT_FAILURE);\n"
#| "    }\n"
msgid ""
"    if (us E<lt> 0) {\n"
"        fprintf(stderr, \"Scanned value shouldn\\[aq]t be negative!\\en\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (argc E<lt> 2) {\n"
"        fprintf(stderr, \"Использование: %s строка [система_счисления]\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid "    /* Wait up to five seconds. */\n"
msgid "    /* Print the value. */\n"
msgstr "    /* Ждать не больше пяти секунд. */\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    printf(\"There are %jd microseconds in half a second.\\en\",\n"
"            (intmax_t) us);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Yuri Kozlov <yuray@komyakino.ru>, 2012-2019.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-05-01 15:41+0200\n"
"PO-Revision-Date: 2019-09-05 21:56+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "I<search>"
msgid "hsearch"
msgstr "I<поиск>"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr "Linux man-pages 6.7"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"hcreate, hdestroy, hsearch, hcreate_r, hdestroy_r, hsearch_r - hash table "
"management"
msgstr ""
"hcreate, hdestroy, hsearch, hcreate_r, hdestroy_r, hsearch_r - операции над "
"ассоциативными массивами"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>search.hE<gt>>\n"
msgstr "B<#include E<lt>search.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int hcreate(size_t >I<nel>B<);>\n"
msgid ""
"B<int hcreate(size_t >I<nel>B<);>\n"
"B<void hdestroy(void);>\n"
msgstr "B<int hcreate(size_t >I<nel>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENTRY *hsearch(ENTRY >I<item>B<, ACTION >I<action>B<);>\n"
msgstr "B<ENTRY *hsearch(ENTRY >I<item>B<, ACTION >I<action>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#define _GNU_SOURCE>         /* See feature_test_macros(7) */\n"
"B<#include E<lt>search.hE<gt>>\n"
msgstr ""
"B<#define _GNU_SOURCE>         /* смотрите feature_test_macros(7) */\n"
"B<#include E<lt>search.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int hcreate_r(size_t >I<nel>B<, struct hsearch_data *>I<htab>B<);>\n"
msgid ""
"B<int hcreate_r(size_t >I<nel>B<, struct hsearch_data *>I<htab>B<);>\n"
"B<void hdestroy_r(struct hsearch_data *>I<htab>B<);>\n"
msgstr "B<int hcreate_r(size_t >I<nel>B<, struct hsearch_data *>I<htab>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int hsearch_r(ENTRY >I<item>B<, ACTION >I<action>B<, ENTRY **>I<retval>B<,>\n"
"B<              struct hsearch_data *>I<htab>B<);>\n"
msgstr ""
"B<int hsearch_r(ENTRY >I<item>B<, ACTION >I<action>B<, ENTRY **>I<retval>B<,>\n"
"B<              struct hsearch_data *>I<htab>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The three functions B<hcreate>(), B<hsearch>(), and B<hdestroy>()  allow the "
"caller to create and manage a hash search table containing entries "
"consisting of a key (a string) and associated data.  Using these functions, "
"only one hash table can be used at a time."
msgstr ""
"Функции B<hcreate>(), B<hsearch>() и B<hdestroy>() позволяют создать и "
"работать с ассоциативным массивом поиска (хэшем), который содержит элементы, "
"состоящие из ключа (строки) и связанных с ним данных. Функции позволяют "
"работать одномоментно только с одним массивом."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The three functions B<hcreate_r>(), B<hsearch_r>(), B<hdestroy_r>()  are "
"reentrant versions that allow a program to use more than one hash search "
"table at the same time.  The last argument, I<htab>, points to a structure "
"that describes the table on which the function is to operate.  The "
"programmer should treat this structure as opaque (i.e., do not attempt to "
"directly access or modify the fields in this structure)."
msgstr ""
"Функции B<hcreate_r>(), B<hsearch_r>() и B<hdestroy_r>() являются "
"реентерабильными версиями, позволяющими программе создавать более одного "
"массива поиска одномоментно. Последний параметр, I<htab>, указывает на "
"структуру, описывающую массив, с которым работает функция. Программист "
"должен считать, что не знает её формат (т. е., не должен пытаться напрямую "
"читать и изменять её поля)."

#.  e.g., in glibc it is raised to the next higher prime number
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"First a hash table must be created using B<hcreate>().  The argument I<nel> "
"specifies the maximum number of entries in the table.  (This maximum cannot "
"be changed later, so choose it wisely.)  The implementation may adjust this "
"value upward to improve the performance of the resulting hash table."
msgstr ""
"Сначала, хэш должен быть создан с помощью функции B<hcreate>(). В аргументе "
"I<nel> указывается предполагаемое максимальное количество элементов в "
"массиве (данный максимум нельзя изменить позже, указывайте значение с "
"запасом). Реализация функции может изменить этот параметр для повышения "
"быстродействия массива."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<hcreate_r>()  function performs the same task as B<hcreate>(), but for "
"the table described by the structure I<*htab>.  The structure pointed to by "
"I<htab> must be zeroed before the first call to B<hcreate_r>()."
msgstr ""
"Функция B<hcreate_r>() выполняет тоже, что и B<hcreate>(), но для массива, "
"описанного в структуре I<*htab>. Перед вызовом B<hcreate_r>() структура, на "
"которую указывает I<htab>, должна быть обнулена."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The function B<hdestroy>()  frees the memory occupied by the hash table that "
"was created by B<hcreate>().  After calling B<hdestroy>(), a new hash table "
"can be created using B<hcreate>().  The B<hdestroy_r>()  function performs "
"the analogous task for a hash table described by I<*htab>, which was "
"previously created using B<hcreate_r>()."
msgstr ""
"Функция B<hdestroy>() освобождает память, занимаемую массивом, созданным "
"B<hcreate>(). После вызова B<hdestroy>() функцией B<hcreate>() можно создать "
"новый массив. Функция B<hdestroy_r>() выполняет аналогичную задачу для "
"массива, описанного в I<*htab>, который был ранее создан B<hcreate_r>()."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<hsearch>()  function searches the hash table for an item with the same "
"key as I<item> (where \"the same\" is determined using B<strcmp>(3)), and if "
"successful returns a pointer to it."
msgstr ""
"Функция B<hsearch>() ищет в массиве элемент с ключом, равным I<item> "
"(«равенство» определяется функцией B<strcmp>(3)), и, в случае удачного "
"завершения операции, возвращает указатель на него."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The argument I<item> is of type I<ENTRY>, which is defined in I<E<lt>search."
"hE<gt>> as follows:"
msgstr ""
"Аргумент I<item> имеет тип I<ENTRY>, который определён в I<E<lt>search."
"hE<gt>> следующим образом:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"typedef struct entry {\n"
"    char *key;\n"
"    void *data;\n"
"} ENTRY;\n"
msgstr ""
"typedef struct entry {\n"
"    char *key;\n"
"    void *data;\n"
"} ENTRY;\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The field I<key> points to a null-terminated string which is the search "
"key.  The field I<data> points to data that is associated with that key."
msgstr ""
"Поле I<key> указывает на оканчивающуюся null строку, используемую в качестве "
"ключа для поиска. Поле I<data> указывает на данные, связанные с ключом."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The argument I<action> determines what B<hsearch>()  does after an "
"unsuccessful search.  This argument must either have the value B<ENTER>, "
"meaning insert a copy of I<item> (and return a pointer to the new hash table "
"entry as the function result), or the value B<FIND>, meaning that NULL "
"should be returned.  (If I<action> is B<FIND>, then I<data> is ignored.)"
msgstr ""
"Аргумент I<action> определяет действие, выполняемое B<hsearch>() после "
"неудачного поиска. Его значением должно быть B<ENTER> (указывает, что нужно "
"вставить копию I<item> и вернуть указатель на новый элемент массива) или "
"B<FIND> (нужно вернуть NULL). Если значение I<action> равно B<FIND>, то поле "
"I<data> игнорируется."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<hsearch_r>()  function is like B<hsearch>()  but operates on the hash "
"table described by I<*htab>.  The B<hsearch_r>()  function differs from "
"B<hsearch>()  in that a pointer to the found item is returned in I<*retval>, "
"rather than as the function result."
msgstr ""
"Функция B<hsearch_r>() подобна B<hsearch>(), но работает с массивом, "
"указанным в I<*htab>. Функция B<hsearch_r>() отлична от B<hsearch>() в том, "
"что указатель найденного элемента возвращается в I<*retval>, а не как "
"результат работы функции."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<hcreate>()  and B<hcreate_r>()  return nonzero on success.  They return "
#| "0 on error, with I<errno> set to indicate the cause of the error."
msgid ""
"B<hcreate>()  and B<hcreate_r>()  return nonzero on success.  They return 0 "
"on error, with I<errno> set to indicate the error."
msgstr ""
"Функции B<hcreate>() и B<hcreate_r>() возвращают ненулевое значение при "
"успешном выполнении. При ошибке возвращается 0, а I<errno> устанавливается в "
"соответствующее значение."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, B<hsearch>()  returns a pointer to an entry in the hash "
#| "table.  B<hsearch>()  returns NULL on error, that is, if I<action> is "
#| "B<ENTER> and the hash table is full, or I<action> is B<FIND> and I<item> "
#| "cannot be found in the hash table.  B<hsearch_r>()  returns nonzero on "
#| "success, and 0 on error.  In the event of an error, these two functions "
#| "set I<errno> to indicate the cause of the error."
msgid ""
"On success, B<hsearch>()  returns a pointer to an entry in the hash table.  "
"B<hsearch>()  returns NULL on error, that is, if I<action> is B<ENTER> and "
"the hash table is full, or I<action> is B<FIND> and I<item> cannot be found "
"in the hash table.  B<hsearch_r>()  returns nonzero on success, and 0 on "
"error.  In the event of an error, these two functions set I<errno> to "
"indicate the error."
msgstr ""
"Функция B<hsearch>() при успешном выполнении возвращает указатель на элемент "
"массива. При ошибке возвращается NULL, то есть, если I<action> равно "
"B<ENTER> массив полон, или, если I<action> равно B<FIND> и I<item> не может "
"быть найден в массиве. Функция B<hsearch_r>() возвращает 0 в случае ошибки "
"или ненулевое значение при успешном выполнении. При ошибке данные функции "
"присваивают I<errno> код ошибки."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<hcreate_r>()  and B<hdestroy_r>()  can fail for the following reasons:"
msgstr ""
"Функции B<hcreate_r>() и B<hdestroy_r>() могут завершиться с ошибкой по "
"следующим причинам:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<htab> is NULL."
msgstr "Значение I<htab> равно NULL."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<hsearch>()  and B<hsearch_r>()  can fail for the following reasons:"
msgstr ""
"Функции B<hsearch>() и B<hsearch_r>() могут завершиться с ошибкой по "
"следующим причинам:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<action> was B<ENTER>, I<key> was not found in the table, and there was no "
"room in the table to add a new entry."
msgstr ""
"Значение I<action> равно B<ENTER>, значение I<key> не найдено в массиве и в "
"массиве нет места под новый элемент."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ESRCH>"
msgstr "B<ESRCH>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<action> was B<FIND>, and I<key> was not found in the table."
msgstr ""
"Значение I<action> равно B<FIND> и значение I<key> не найдено в массиве."

#.  PROX.1-2001, POSIX.1-2008
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1 specifies only the B<ENOMEM> error."
msgstr "В POSIX.1 определена только ошибка B<ENOMEM>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<hcreate>(),\n"
#| "B<hsearch>(),\n"
msgid ""
"B<hcreate>(),\n"
"B<hsearch>(),\n"
"B<hdestroy>()"
msgstr ""
"B<hcreate>(),\n"
"B<hsearch>(),\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:hsearch"
msgstr "MT-Unsafe race:hsearch"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<hcreate_r>(),\n"
#| "B<hsearch_r>(),\n"
msgid ""
"B<hcreate_r>(),\n"
"B<hsearch_r>(),\n"
"B<hdestroy_r>()"
msgstr ""
"B<hcreate_r>(),\n"
"B<hsearch_r>(),\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe race:htab"
msgstr "MT-Safe race:htab"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<creat>(2)"
msgid "B<hcreate>()"
msgstr "B<creat>(2)"

#. type: TQ
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "I<search>"
msgid "B<hsearch>()"
msgstr "I<поиск>"

#. type: TQ
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<hdestroy>()"
msgstr "B<hdestroy>()"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<creat>(2)"
msgid "B<hcreate_r>()"
msgstr "B<creat>(2)"

#. type: TQ
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "I<search>"
msgid "B<hsearch_r>()"
msgstr "I<поиск>"

#. type: TQ
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<hdestroy_r>()"
msgstr "B<hdestroy_r>()"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "GNU."
msgstr "GNU."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "SVr4, POSIX.1-2001."
msgstr "SVr4, POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Hash table implementations are usually more efficient when the table "
"contains enough free space to minimize collisions.  Typically, this means "
"that I<nel> should be at least 25% larger than the maximum number of "
"elements that the caller expects to store in the table."
msgstr ""
"Реализации ассоциативных массивов более эффективны, если массив содержит "
"достаточно свободного места, чтобы не искать незанятое пространство. Это "
"означает, что обычно, значение I<nel> нужно увеличить на, как минимум, 25% "
"от максимального количества элементов, ожидаемых в массиве."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<hdestroy>()  and B<hdestroy_r>()  functions do not free the buffers "
"pointed to by the I<key> and I<data> elements of the hash table entries.  "
"(It can't do this because it doesn't know whether these buffers were "
"allocated dynamically.)  If these buffers need to be freed (perhaps because "
"the program is repeatedly creating and destroying hash tables, rather than "
"creating a single table whose lifetime matches that of the program), then "
"the program must maintain bookkeeping data structures that allow it to free "
"them."
msgstr ""
"Функции B<hdestroy>() и B<hdestroy_r>() не освобождают буферы, на которые "
"указывают элементы I<key> и I<data> в массиве (это невозможно сделать, так "
"как неизвестно, были ли выделены эти буферы динамически). Если эти буферы "
"требуется освобождать (возможно, из-за того, что программа много раз создаёт "
"и удаляет массивы, а не создаёт один массив на всё время работы), то "
"программа должна предусмотреть хранилище под используемые структуры, которое "
"позволило бы их освободить."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "SVr4 and POSIX.1-2001 specify that I<action> is significant only for "
#| "unsuccessful searches, so that an B<ENTER> should not do anything for a "
#| "successful search.  In libc and glibc (before version 2.3), the "
#| "implementation violates the specification, updating the I<data> for the "
#| "given I<key> in this case."
msgid ""
"SVr4 and POSIX.1-2001 specify that I<action> is significant only for "
"unsuccessful searches, so that an B<ENTER> should not do anything for a "
"successful search.  In libc and glibc (before glibc 2.3), the implementation "
"violates the specification, updating the I<data> for the given I<key> in "
"this case."
msgstr ""
"В SVr4 и POSIX 1003.1-2001 указано, что I<action> имеет смысл только при "
"неудачном поиске, поэтому при B<ENTER> не нужно ничего делать, если что-то "
"найдено. В библиотеках libc и glibc (до версии 2.3) реализация функций "
"обновляет I<data> для указанного I<key> в этом случае."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Individual hash table entries can be added, but not deleted."
msgstr "Можно добавлять элементы массива, но не удалять."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "ПРИМЕРЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The following program inserts 24 items into a hash table, then prints some "
"of them."
msgstr ""
"Следующая программа вставляет в массив 24 элемента, а затем выводит на "
"печать некоторые из них:"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>search.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"\\&\n"
"static char *data[] = { \"alpha\", \"bravo\", \"charlie\", \"delta\",\n"
"     \"echo\", \"foxtrot\", \"golf\", \"hotel\", \"india\", \"juliet\",\n"
"     \"kilo\", \"lima\", \"mike\", \"november\", \"oscar\", \"papa\",\n"
"     \"quebec\", \"romeo\", \"sierra\", \"tango\", \"uniform\",\n"
"     \"victor\", \"whisky\", \"x-ray\", \"yankee\", \"zulu\"\n"
"};\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    ENTRY e;\n"
"    ENTRY *ep;\n"
"\\&\n"
"    hcreate(30);\n"
"\\&\n"
"    for (size_t i = 0; i E<lt> 24; i++) {\n"
"        e.key = data[i];\n"
"        /* data is just an integer, instead of a\n"
"           pointer to something */\n"
"        e.data = (void *) i;\n"
"        ep = hsearch(e, ENTER);\n"
"        /* there should be no failures */\n"
"        if (ep == NULL) {\n"
"            fprintf(stderr, \"entry failed\\en\");\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
"    }\n"
"\\&\n"
"    for (size_t i = 22; i E<lt> 26; i++) {\n"
"        /* print two entries from the table, and\n"
"           show that two are not in the table */\n"
"        e.key = data[i];\n"
"        ep = hsearch(e, FIND);\n"
"        printf(\"%9.9s -E<gt> %9.9s:%d\\en\", e.key,\n"
"               ep ? ep-E<gt>key : \"NULL\", ep ? (int)(ep-E<gt>data) : 0);\n"
"    }\n"
"    hdestroy();\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<bsearch>(3), B<lsearch>(3), B<malloc>(3), B<tsearch>(3)"
msgstr "B<bsearch>(3), B<lsearch>(3), B<malloc>(3), B<tsearch>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr "15 декабря 2022 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm
msgid ""
"The functions B<hcreate>(), B<hsearch>(), and B<hdestroy>()  are from SVr4, "
"and are described in POSIX.1-2001 and POSIX.1-2008."
msgstr ""
"Функции B<hcreate>(), B<hsearch>() и B<hdestroy>() взяты из SVr4 и описаны в "
"POSIX.1-2001 и POSIX.1-2008."

#. type: Plain text
#: debian-bookworm
msgid ""
"The functions B<hcreate_r>(), B<hsearch_r>(), and B<hdestroy_r>()  are GNU "
"extensions."
msgstr ""
"Функции B<hcreate_r>(), B<hsearch_r>() и B<hdestroy_r>() являются "
"расширениями GNU."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid ""
#| "#include E<lt>stdio.hE<gt>\n"
#| "#include E<lt>stdlib.hE<gt>\n"
#| "#include E<lt>unistd.hE<gt>\n"
msgid ""
"#include E<lt>search.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
msgstr ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"static char *data[] = { \"alpha\", \"bravo\", \"charlie\", \"delta\",\n"
"     \"echo\", \"foxtrot\", \"golf\", \"hotel\", \"india\", \"juliet\",\n"
"     \"kilo\", \"lima\", \"mike\", \"november\", \"oscar\", \"papa\",\n"
"     \"quebec\", \"romeo\", \"sierra\", \"tango\", \"uniform\",\n"
"     \"victor\", \"whisky\", \"x-ray\", \"yankee\", \"zulu\"\n"
"};\n"
msgstr ""
"static char *data[] = { \"alpha\", \"bravo\", \"charlie\", \"delta\",\n"
"     \"echo\", \"foxtrot\", \"golf\", \"hotel\", \"india\", \"juliet\",\n"
"     \"kilo\", \"lima\", \"mike\", \"november\", \"oscar\", \"papa\",\n"
"     \"quebec\", \"romeo\", \"sierra\", \"tango\", \"uniform\",\n"
"     \"victor\", \"whisky\", \"x-ray\", \"yankee\", \"zulu\"\n"
"};\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid ""
#| "int\n"
#| "main(void)\n"
#| "{\n"
#| "    ENTRY e, *ep;\n"
#| "    int i;\n"
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    ENTRY e;\n"
"    ENTRY *ep;\n"
msgstr ""
"int\n"
"main(void)\n"
"{\n"
"    ENTRY e, *ep;\n"
"    int i;\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "    hcreate(30);\n"
msgstr "    hcreate(30);\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid ""
#| "    for (i = 0; i E<lt> 24; i++) {\n"
#| "        e.key = data[i];\n"
#| "        /* data is just an integer, instead of a\n"
#| "           pointer to something */\n"
#| "        e.data = (void *) i;\n"
#| "        ep = hsearch(e, ENTER);\n"
#| "        /* there should be no failures */\n"
#| "        if (ep == NULL) {\n"
#| "            fprintf(stderr, \"entry failed\\en\");\n"
#| "            exit(EXIT_FAILURE);\n"
#| "        }\n"
#| "    }\n"
msgid ""
"    for (size_t i = 0; i E<lt> 24; i++) {\n"
"        e.key = data[i];\n"
"        /* data is just an integer, instead of a\n"
"           pointer to something */\n"
"        e.data = (void *) i;\n"
"        ep = hsearch(e, ENTER);\n"
"        /* there should be no failures */\n"
"        if (ep == NULL) {\n"
"            fprintf(stderr, \"entry failed\\en\");\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
"    }\n"
msgstr ""
"    for (i = 0; i E<lt> 24; i++) {\n"
"        e.key = data[i];\n"
"        /* data — это просто целое число, а не\n"
"           указатель на что-то */\n"
"        e.data = (void *) i;\n"
"        ep = hsearch(e, ENTER);\n"
"        /* здесь ошибки быть не должно */\n"
"        if (ep == NULL) {\n"
"            fprintf(stderr, \"ошибка элемента\\en\");\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
"    }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid ""
#| "    for (i = 22; i E<lt> 26; i++) {\n"
#| "        /* print two entries from the table, and\n"
#| "           show that two are not in the table */\n"
#| "        e.key = data[i];\n"
#| "        ep = hsearch(e, FIND);\n"
#| "        printf(\"%9.9s -E<gt> %9.9s:%d\\en\", e.key,\n"
#| "               ep ? ep-E<gt>key : \"NULL\", ep ? (int)(ep-E<gt>data) : 0);\n"
#| "    }\n"
#| "    hdestroy();\n"
#| "    exit(EXIT_SUCCESS);\n"
#| "}\n"
msgid ""
"    for (size_t i = 22; i E<lt> 26; i++) {\n"
"        /* print two entries from the table, and\n"
"           show that two are not in the table */\n"
"        e.key = data[i];\n"
"        ep = hsearch(e, FIND);\n"
"        printf(\"%9.9s -E<gt> %9.9s:%d\\en\", e.key,\n"
"               ep ? ep-E<gt>key : \"NULL\", ep ? (int)(ep-E<gt>data) : 0);\n"
"    }\n"
"    hdestroy();\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    for (i = 22; i E<lt> 26; i++) {\n"
"        /* показать два элемента массива и\n"
"           два не из массива */\n"
"        e.key = data[i];\n"
"        ep = hsearch(e, FIND);\n"
"        printf(\"%9.9s -E<gt> %9.9s:%d\\en\", e.key,\n"
"               ep ? ep-E<gt>key : \"NULL\", ep ? (int)(ep-E<gt>data) : 0);\n"
"    }\n"
"    hdestroy();\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Yuri Kozlov <yuray@komyakino.ru>, 2014-2017.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-05-01 16:00+0200\n"
"PO-Revision-Date: 2019-06-23 15:54+0000\n"
"Last-Translator: Иван Павлов <pavia00@gmail.com>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "y"
msgid "y0"
msgstr "и"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr "Linux man-pages 6.7"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"y0, y0f, y0l, y1, y1f, y1l, yn, ynf, ynl - Bessel functions of the second "
"kind"
msgstr ""
"y0, y0f, y0l, y1, y1f, y1l, yn, ynf, ynl - функции Бесселя второго рода"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Math library (I<libm>, I<-lm>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr "B<#include E<lt>math.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<double y0(double >I<x>B<);>\n"
"B<double y1(double >I<x>B<);>\n"
"B<double yn(int >I<n>B<, double >I<x>B<);>\n"
msgstr ""
"B<double y0(double >I<x>B<);>\n"
"B<double y1(double >I<x>B<);>\n"
"B<double yn(int >I<n>B<, double >I<x>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<float y0f(float >I<x>B<);>\n"
"B<float y1f(float >I<x>B<);>\n"
"B<float ynf(int >I<n>B<, float >I<x>B<);>\n"
msgstr ""
"B<float y0f(float >I<x>B<);>\n"
"B<float y1f(float >I<x>B<);>\n"
"B<float ynf(int >I<n>B<, float >I<x>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<long double y0l(long double >I<x>B<);>\n"
"B<long double y1l(long double >I<x>B<);>\n"
"B<long double ynl(int >I<n>B<, long double >I<x>B<);>\n"
msgstr ""
"B<long double y0l(long double >I<x>B<);>\n"
"B<long double y1l(long double >I<x>B<);>\n"
"B<long double ynl(int >I<n>B<, long double >I<x>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<y0>(), B<y1>(), B<yn>():"
msgstr "B<y0>(), B<y1>(), B<yn>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "_XOPEN_SOURCE\n"
#| "    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
#| "    || /* Glibc versions E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"
msgid ""
"    _XOPEN_SOURCE\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"
msgstr ""
"_XOPEN_SOURCE\n"
"    || /* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* версии glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<y0f>(), B<y0l>(), B<y1f>(), B<y1l>(), B<ynf>(), B<ynl>():"
msgstr "B<y0f>(), B<y0l>(), B<y1f>(), B<y1l>(), B<ynf>(), B<ynl>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "_XOPEN_SOURCE \\ E<gt>=\\ 600\n"
#| "    || (_ISOC99_SOURCE && _XOPEN_SOURCE)\n"
#| "    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
#| "    || /* Glibc versions E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"
msgid ""
"    _XOPEN_SOURCE E<gt>= 600\n"
"        || (_ISOC99_SOURCE && _XOPEN_SOURCE)\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"
msgstr ""
"_XOPEN_SOURCE \\ E<gt>=\\ 600\n"
"    || (_ISOC99_SOURCE && _XOPEN_SOURCE)\n"
"    || /* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* версии glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<y0>()  and B<y1>()  functions return Bessel functions of I<x> of the "
"second kind of orders 0 and 1, respectively.  The B<yn>()  function returns "
"the Bessel function of I<x> of the second kind of order I<n>."
msgstr ""
"Функции B<y0>() и B<y1>() возвращают функцию Бесселя второго рода от I<x> "
"для порядков 0 и 1 соответственно. Функция B<yn>() возвращает функцию "
"Бесселя второго рода от I<x> для порядка I<n>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The value of I<x> must be positive."
msgstr "Значение I<x> должно быть положительным."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<y0f>(), B<y1f>(), and B<ynf>()  functions are versions that take and "
"return I<float> values.  The B<y0l>(), B<y1l>(), and B<ynl>()  functions are "
"versions that take and return I<long double> values."
msgstr ""
"Функции B<y0f>(), B<y1f>() и B<ynf>() представляют собой версии, которые "
"принимают и возвращают значения типа I<float>. Функции B<y0l>(), B<y1l>() и "
"B<ynl>() представляют собой версии, которые принимают и возвращают значения "
"типа I<long double>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On success, these functions return the appropriate Bessel value of the "
"second kind for I<x>."
msgstr ""
"В случае успеха эти функции возвращают соответствующее значение функции "
"Бесселя второго рода от I<x>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If I<x> is a NaN, a NaN is returned."
msgstr "Если I<x> имеет значение NaN, будет возвращено NaN."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<x> is negative, a domain error occurs, and the functions return -"
"B<HUGE_VAL>, -B<HUGE_VALF>, or -B<HUGE_VALL>, respectively.  (POSIX.1-2001 "
"also allows a NaN return for this case.)"
msgstr ""
"Если I<x> отрицательное, то генерируется ошибка выхода за пределы области, а "
"функции возвращают -B<HUGE_VAL>, -B<HUGE_VALF> или -B<HUGE_VALL> "
"соответственно. (В POSIX.1-2001 также допускается в этом случае возврат NaN)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<x> is 0.0, a pole error occurs, and the functions return -B<HUGE_VAL>, -"
"B<HUGE_VALF>, or -B<HUGE_VALL>, respectively."
msgstr ""
"Если I<x> равно 0.0, генерируется ошибка особой точки, а функции возвращают -"
"B<HUGE_VAL>, -B<HUGE_VALF> или -B<HUGE_VALL> соответственно."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the result underflows, a range error occurs, and the functions return 0.0"
msgstr ""
"Если результат исчерпал степень, генерируется ошибка диапазона, а функции "
"возвращают 0.0."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the result overflows, a range error occurs, and the functions return -"
"B<HUGE_VAL>, -B<HUGE_VALF>, or -B<HUGE_VALL>, respectively.  (POSIX.1-2001 "
"also allows a 0.0 return for this case.)"
msgstr ""
"Если результат превышает разрядность, генерируется ошибка диапазона, а "
"функции возвращают -B<HUGE_VAL>, -B<HUGE_VALF> или -B<HUGE_VALL> "
"соответственно (В POSIX.1-2001 также допускается в этом случае возврат 0.0)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"See B<math_error>(7)  for information on how to determine whether an error "
"has occurred when calling these functions."
msgstr ""
"Смотрите B<math_error>(7), чтобы определить, какие ошибки могут возникать "
"при вызове этих функций."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The following errors can occur:"
msgstr "Могут возникать следующие ошибки:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Domain error: I<x> is negative"
msgstr "Ошибка области: I<x> является отрицательным"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<errno> is set to B<EDOM>.  An invalid floating-point exception "
"(B<FE_INVALID>)  is raised."
msgstr ""
"I<errno> устанавливается в B<EDOM>. Вызывается исключение неправильной "
"плавающей точки (B<FE_INVALID>)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Pole error: I<x> is 0.0"
msgstr "Ошибка особой точки: I<x> равно 0.0"

#.  Before POSIX.1-2001 TC2, this was (inconsistently) specified
#.  as a range error.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "I<errno> is set to B<ERANGE>.  No B<FE_UNDERFLOW> exception is returned "
#| "by B<fetestexcept>(3)  for this case."
msgid ""
"I<errno> is set to B<ERANGE> and an B<FE_DIVBYZERO> exception is raised (but "
"see BUGS)."
msgstr ""
"I<errno> устанавливается в B<ERANGE>. В данном случае от B<fetestexcept>(3) "
"исключение B<FE_DIVBYZERO> не возвращается."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Range error: result underflow"
msgstr "Ошибка диапазона: результат исчерпал степень"

#. #-#-#-#-#  archlinux: y0.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  e.g., y0(1e33) on glibc 2.8/x86-32
#.  This is intended behavior
#.  See https://www.sourceware.org/bugzilla/show_bug.cgi?id=6806
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: y0.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  e.g., y0(1e33) on glibc 2.8/x86-32
#.  This is intended behavior
#.  See http://sources.redhat.com/bugzilla/show_bug.cgi?id=6806
#. type: Plain text
#. #-#-#-#-#  debian-unstable: y0.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  e.g., y0(1e33) on glibc 2.8/x86-32
#.  This is intended behavior
#.  See https://www.sourceware.org/bugzilla/show_bug.cgi?id=6806
#. type: Plain text
#. #-#-#-#-#  fedora-40: y0.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  e.g., y0(1e33) on glibc 2.8/x86-32
#.  This is intended behavior
#.  See https://www.sourceware.org/bugzilla/show_bug.cgi?id=6806
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: y0.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  e.g., y0(1e33) on glibc 2.8/x86-32
#.  This is intended behavior
#.  See https://www.sourceware.org/bugzilla/show_bug.cgi?id=6806
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: y0.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  e.g., y0(1e33) on glibc 2.8/x86-32
#.  This is intended behavior
#.  See https://www.sourceware.org/bugzilla/show_bug.cgi?id=6806
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-6: y0.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  e.g., y0(1e33) on glibc 2.8/x86-32
#.  This is intended behavior
#.  See https://www.sourceware.org/bugzilla/show_bug.cgi?id=6806
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: y0.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  e.g., y0(1e33) on glibc 2.8/x86-32
#.  This is intended behavior
#.  See https://www.sourceware.org/bugzilla/show_bug.cgi?id=6806
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<errno> is set to B<ERANGE>.  No B<FE_UNDERFLOW> exception is returned by "
"B<fetestexcept>(3)  for this case."
msgstr ""
"I<errno> устанавливается в B<ERANGE>. В данном случае от B<fetestexcept>(3) "
"исключение B<FE_DIVBYZERO> не возвращается."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Range error: result overflow"
msgstr "Ошибка диапазона: результат превысил разрядность"

#.  e.g., yn(10, 1e-40) on glibc 2.8/x86-32
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<errno> is set to B<ERANGE> (but see BUGS).  An overflow floating-point "
"exception (B<FE_OVERFLOW>)  is raised."
msgstr ""
"Значение I<errno> устанавливается в B<ERANGE> (но см. ДЕФЕКТЫ). Возникает "
"исключение переполнения плавающей точки (B<FE_OVERFLOW>)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<y0>(),\n"
"B<y0f>(),\n"
"B<y0l>()"
msgstr ""
"B<y0>(),\n"
"B<y0f>(),\n"
"B<y0l>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<y1>(),\n"
"B<y1f>(),\n"
"B<y1l>()"
msgstr ""
"B<y1>(),\n"
"B<y1f>(),\n"
"B<y1l>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<yn>(),\n"
"B<ynf>(),\n"
"B<ynl>()"
msgstr ""
"B<yn>(),\n"
"B<ynf>(),\n"
"B<ynl>()"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<y0>()"
msgstr ""

#. type: TQ
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<y1>()"
msgstr ""

#. type: TQ
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<isnan>():"
msgid "B<yn>()"
msgstr "B<isnan>():"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<ethers>"
msgid "Others:"
msgstr "B<ethers>"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "BSD."
msgstr "BSD."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "SVr4, 4.3BSD, POSIX.1-2001."
msgstr "SVr4, 4.3BSD, POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ОШИБКИ"

#.  http://sourceware.org/bugzilla/show_bug.cgi?id=6807
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Before glibc 2.19, these functions misdiagnosed pole errors: I<errno> was "
"set to B<EDOM>, instead of B<ERANGE> and no B<FE_DIVBYZERO> exception was "
"raised."
msgstr ""

#. #-#-#-#-#  archlinux: y0.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6808
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: y0.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6808
#. type: Plain text
#. #-#-#-#-#  debian-unstable: y0.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6808
#. type: Plain text
#. #-#-#-#-#  fedora-40: y0.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6808
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: y0.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6808
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: y0.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6808
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-6: y0.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6808
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: y0.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6808
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Before glibc 2.17, did not set I<errno> for \"range error: result "
"underflow\"."
msgstr ""

#.  Actually, 2.3.2 is the earliest test result I have; so yet
#.  to confirm if this error occurs only in glibc 2.3.2.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "In glibc version 2.3.2 and earlier, these functions do not raise an "
#| "invalid floating-point exception (B<FE_INVALID>)  when a domain error "
#| "occurs."
msgid ""
"In glibc 2.3.2 and earlier, these functions do not raise an invalid floating-"
"point exception (B<FE_INVALID>)  when a domain error occurs."
msgstr ""
"В glibc-2.3.2 и младше данные функции не вызывают исключение неправильной "
"плавающей точки (B<FE_INVALID>) при возникновении ошибки выхода за пределы "
"области."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<j0>(3)"
msgstr "B<j0>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm
msgid ""
"The functions returning I<double> conform to SVr4, 4.3BSD, POSIX.1-2001, "
"POSIX.1-2008.  The others are nonstandard functions that also exist on the "
"BSDs."
msgstr ""
"Функции, возвращающие I<double>, соответствуют SVr4, 4.3BSD, POSIX.1-2001 и "
"POSIX.1-2008. Остальные являются не стандартизованными функциями, которые "
"также существуют в BSD."

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

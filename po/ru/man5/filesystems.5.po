# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2017.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-05-01 15:39+0200\n"
"PO-Revision-Date: 2019-09-27 19:31+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "filesystems"
msgstr "filesystems"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2024-01-28"
msgstr "28 января 2024 г."

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr "Linux man-pages 6.7"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"filesystems - Linux filesystem types: ext, ext2, ext3, ext4, hpfs, iso9660, "
"JFS, minix, msdos, ncpfs nfs, ntfs, proc, Reiserfs, smb, sysv, umsdos, vfat, "
"XFS, xiafs"
msgstr ""
"filesystems - типы файловых систем Linux: ext, ext2, ext3, ext4, hpfs, "
"iso9660, JFS, minix, msdos, ncpfs nfs, ntfs, proc, Reiserfs, smb, sysv, "
"umsdos, vfat, XFS, xiafs"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#.  commit: 6af9f7bf3c399e0ab1eee048e13572c6d4e15fe9
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When, as is customary, the B<proc> filesystem is mounted on I</proc>, you "
"can find in the file I</proc/filesystems> which filesystems your kernel "
"currently supports; see B<proc>(5)  for more details.  There is also a "
"legacy B<sysfs>(2)  system call (whose availability is controlled by the "
"B<CONFIG_SYSFS_SYSCALL> kernel build configuration option since Linux 3.15)  "
"that enables enumeration of the currently available filesystem types "
"regardless of I</proc> availability and/or sanity."
msgstr ""
"Если, как обычно, файловая система B<proc> смонтирована в I</proc>, то в "
"файле I</proc/filesystems> можно найти список типов файловых систем, которые "
"в текущий момент поддерживаются ядром; подробности смотрите в B<proc>(5). "
"Также есть устаревший системный вызов B<sysfs>(2) (доступен, если ядро "
"собрано с параметром настойки B<CONFIG_SYSFS_SYSCALL>, начиная с Linux "
"3.15), который содержи перечисление доступных типов файловых систем "
"независимо от того, смонтировано ли что-то в I</proc> или нет."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If you need a currently unsupported filesystem, insert the corresponding "
"kernel module or recompile the kernel."
msgstr ""
"Если требуется пока не поддерживаемая файловая система, то загрузите "
"соответствующий модуль или пересоберите ядро."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In order to use a filesystem, you have to I<mount> it; see B<mount>(2)  and "
"B<mount>(8)."
msgstr ""
"Чтобы использовать файловую систему, её нужно I<смонтировать>; смотрите "
"B<mount>(2) и B<mount>(8)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The following list provides a short description of the available or "
"historically available filesystems in the Linux kernel.  See the kernel "
"documentation for a comprehensive description of all options and limitations."
msgstr ""
"В списке далее приводится краткое описание доступных или доступных ранее "
"файловых систем в ядре Linux. Полное описание, все параметры и ограничения "
"описаны в документации к ядру."

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<erofs>"
msgstr ""

#.  commit 47e4937a4a7ca4184fd282791dfee76c6799966a moves it out of staging
#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"is the Enhanced Read-Only File System, stable since Linux 5.4.  See "
"B<erofs>(5)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ext>"
msgstr "B<ext>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "is an elaborate extension of the B<minix> filesystem.  It has been "
#| "completely superseded by the second version of the extended filesystem "
#| "(B<ext2>)  and has been removed from the kernel (in 2.1.21)."
msgid ""
"is an elaborate extension of the B<minix> filesystem.  It has been "
"completely superseded by the second version of the extended filesystem "
"(B<ext2>)  and has been removed from the kernel (in Linux 2.1.21)."
msgstr ""
"Доработанное расширение файловой системы B<minix>. Полностью вытеснена "
"второй версией расширенной файловой системой (B<ext2>) и была удалена из "
"ядра (в 2.1.21)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ext2>"
msgstr "B<ext2>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "is the high performance disk filesystem used by Linux for fixed disks as "
#| "well as removable media.  The second extended filesystem was designed as "
#| "an extension of the extended filesystem (B<ext>).  See B<ext2>(5)."
msgid ""
"is a disk filesystem that was used by Linux for fixed disks as well as "
"removable media.  The second extended filesystem was designed as an "
"extension of the extended filesystem (B<ext>).  See B<ext2>(5)."
msgstr ""
"Высокопроизводительная дисковая файловая система Linux для жёстких дисков, а "
"также сменных носителей. Вторая версия расширенной файловой системы "
"разрабатывалась как расширение расширенной файловой системы (B<ext>). "
"Смотрите B<ext2>(5)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ext3>"
msgstr "B<ext3>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"is a journaling version of the B<ext2> filesystem.  It is easy to switch "
"back and forth between B<ext2> and B<ext3>.  See B<ext3>(5)."
msgstr ""
"Журналируемая версия файловой системы B<ext2>. Очень легко переключаться с "
"B<ext2> на B<ext3> и обратно. Смотрите B<ext3>(5)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ext4>"
msgstr "B<ext4>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"is a set of upgrades to B<ext3> including substantial performance and "
"reliability enhancements, plus large increases in volume, file, and "
"directory size limits.  See B<ext4>(5)."
msgstr ""
"Обновлённая версия B<ext3>, включающая существенное увеличение "
"производительности и надёжности, размеров границ томов, файлов и каталогов. "
"Смотрите B<ext4>(5)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<hpfs>"
msgstr "B<hpfs>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"is the High Performance Filesystem, used in OS/2.  This filesystem is read-"
"only under Linux due to the lack of available documentation."
msgstr ""
"Высокопроизводительная файловая система (High Performance Filesystem), "
"используемая в OS/2. Данная файловая система доступна под Linux только для "
"чтения из-за отсутствия документации."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<iso9660>"
msgstr "B<iso9660>"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid "is a CD-ROM filesystem type conforming to the ISO 9660 standard."
msgid "is a CD-ROM filesystem type conforming to the ISO/IEC\\~9660 standard."
msgstr "Файловая система для CD-ROM, соответствующая стандарту ISO 9660."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<High Sierra>"
msgstr "B<High Sierra>"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Linux supports High Sierra, the precursor to the ISO 9660 standard for CD-"
#| "ROM filesystems.  It is automatically recognized within the B<iso9660> "
#| "filesystem support under Linux."
msgid ""
"Linux supports High Sierra, the precursor to the ISO/IEC\\~9660 standard for "
"CD-ROM filesystems.  It is automatically recognized within the B<iso9660> "
"filesystem support under Linux."
msgstr ""
"В Linux есть поддержка стандарта High Sierra, предшественника стандарта ISO "
"9660 для файловых систем CD-ROM. High Sierra автоматически распознается при "
"включении в Linux поддержки файловой системы B<iso9660>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<Rock Ridge>"
msgstr "B<Rock Ridge>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Linux also supports the System Use Sharing Protocol records specified by the "
"Rock Ridge Interchange Protocol.  They are used to further describe the "
"files in the B<iso9660> filesystem to a UNIX host, and provide information "
"such as long filenames, UID/GID, POSIX permissions, and devices.  It is "
"automatically recognized within the B<iso9660> filesystem support under "
"Linux."
msgstr ""
"В Linux также есть поддержка записей System Use Sharing Protocol, которые "
"определены в протоколе обмена Rock Ridge. Они используются для подробного "
"описания файлов в файловой системе B<iso9660> для машин UNIX и предоставляют "
"информацию о длинных именах файлов, UID/GID, правах доступа к файлам по "
"стандарту POSIX и файлах устройств. Rock Ridge автоматически распознаётся "
"при включении в Linux поддержки файловой системы B<iso9660>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<JFS>"
msgstr "B<JFS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "is a journaling filesystem, developed by IBM, that was integrated into "
#| "Linux in kernel 2.4.24."
msgid ""
"is a journaling filesystem, developed by IBM, that was integrated into Linux "
"2.4.24."
msgstr ""
"Журналируемая файловая система, разработанная IBM, была добавлена в Linux "
"начиная с ядра версии 2.4.24."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<minix>"
msgstr "B<minix>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"is the filesystem used in the Minix operating system, the first to run under "
"Linux.  It has a number of shortcomings, including a 64\\ MB partition size "
"limit, short filenames, and a single timestamp.  It remains useful for "
"floppies and RAM disks."
msgstr ""
"Файловая система, использующаяся в операционной системе Minix, первая, на "
"которой заработал Linux. Имеет несколько недостатков: максимальный размер "
"раздела 64\\ МБ, короткие имена файлов и одна временная метка. Осталась "
"полезной для дискет и RAM-дисков."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<msdos>"
msgstr "B<msdos>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"is the filesystem used by DOS, Windows, and some OS/2 computers.  B<msdos> "
"filenames can be no longer than 8 characters, followed by an optional period "
"and 3 character extension."
msgstr ""
"Файловая система, используемая в DOS, Windows и на некоторых компьютерах с "
"OS/2. Имена файлов в B<msdos> не могут быть более 8 символов с "
"необязательным расширением из 3 символов, отделённого точкой."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ncpfs>"
msgstr "B<ncpfs>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "is a network filesystem that supports the NCP protocol, used by Novell "
#| "NetWare."
msgid ""
"is a network filesystem that supports the NCP protocol, used by Novell "
"NetWare.  It was removed from the kernel in Linux 4.17."
msgstr ""
"Сетевая файловая система, которая поддерживает протокол NCP, используемый в "
"Novell NetWare."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"To use B<ncpfs>, you need special programs, which can be found at E<.UR "
"ftp://ftp.gwdg.de\\:/pub\\:/linux\\:/misc\\:/ncpfs> E<.UE .>"
msgstr ""
"Для использования B<ncpfs> необходимы специальные программы, которые можно "
"найти по адресу E<.UR ftp://ftp.gwdg.de\\:/pub\\:/linux\\:/misc\\:/ncpfs> E<."
"UE .>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<nfs>"
msgstr "B<nfs>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"is the network filesystem used to access disks located on remote computers."
msgstr ""
"Сетевая файловая система, используемая для доступа к дискам, расположенным "
"на других компьютерах в сети."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ntfs>"
msgstr "B<ntfs>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"is the filesystem native to Microsoft Windows NT, supporting features like "
"ACLs, journaling, encryption, and so on."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<proc>"
msgstr "B<proc>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"is a pseudo filesystem which is used as an interface to kernel data "
"structures rather than reading and interpreting I</dev/kmem>.  In "
"particular, its files do not take disk space.  See B<proc>(5)."
msgstr ""
"Ненастоящая файловая система, используется как интерфейс к структурам данных "
"ядра вместо прямого чтения из I</dev/kmem>. В частности, её файлы не "
"занимают пространство на диске. Смотрите B<proc>(5)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<Reiserfs>"
msgstr "B<Reiserfs>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "is a journaling filesystem, designed by Hans Reiser, that was integrated "
#| "into Linux in kernel 2.4.1."
msgid ""
"is a journaling filesystem, designed by Hans Reiser, that was integrated "
"into Linux 2.4.1."
msgstr ""
"Журналируемая файловая система, разработанная Гансом Рейзером (Hans Reiser), "
"была добавлена в Linux начиная с ядра версии 2.4.1."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<smb>"
msgstr "B<smb>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "is a network filesystem that supports the SMB protocol, used by Windows "
#| "for Workgroups, Windows NT, and Lan Manager.  See E<.UR https://www.samba."
#| "org\\:/samba\\:/smbfs/> E<.UE .>"
msgid ""
"is a network filesystem that supports the SMB protocol, used by Windows.  "
"See E<.UR https://www.samba.org\\:/samba\\:/smbfs/> E<.UE .>"
msgstr ""
"Сетевая файловая система, которая поддерживает протокол SMB, используемый в "
"Windows for Workgroups, Windows NT и Lan Manager. Смотрите E<.UR https://www."
"samba.org\\:/samba\\:/smbfs/> E<.UE .>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<sysv>"
msgstr "B<sysv>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"is an implementation of the System V/Coherent filesystem for Linux.  It "
"implements all of Xenix FS, System V/386 FS, and Coherent FS."
msgstr ""
"Реализация файловой системы System V/Coherent для Linux. Поддерживает "
"файловые системы из Xenix, System V/386 и Coherent."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<umsdos>"
msgstr "B<umsdos>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"is an extended DOS filesystem used by Linux.  It adds capability for long "
"filenames, UID/GID, POSIX permissions, and special files (devices, named "
"pipes, etc.) under the DOS filesystem, without sacrificing compatibility "
"with DOS."
msgstr ""
"Расширенная файловая система DOS, используемая в Linux. Была добавлена "
"поддержка длинных имён файлов, UID/GID, права доступа POSIX и специальные "
"файлы (устройства, именованные каналы и т.д.) без ухудшения совместимости с "
"DOS."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<tmpfs>"
msgstr "B<tmpfs>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"is a filesystem whose contents reside in virtual memory.  Since the files on "
"such filesystems typically reside in RAM, file access is extremely fast.  "
"See B<tmpfs>(5)."
msgstr ""
"Файловая система, чьё содержимое находится в виртуальной памяти. Так как "
"файлы в таких файловых системах, обычно, располагаются в оперативной памяти, "
"то доступ к файлах очень быстр. Смотрите B<tmpfs>(5)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<vfat>"
msgstr "B<vfat>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"is an extended FAT filesystem used by Microsoft Windows95 and Windows NT.  "
"B<vfat> adds the capability to use long filenames under the MSDOS filesystem."
msgstr ""
"Расширенная файловая система FAT, используемая в Microsoft Windows95 и "
"Windows NT. В B<vfat> добавлена поддержка длинных имён файлов из файловой "
"системы MSDOS."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<XFS>"
msgstr "B<XFS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "is a journaling filesystem, developed by SGI, that was integrated into "
#| "Linux in kernel 2.4.20."
msgid ""
"is a journaling filesystem, developed by SGI, that was integrated into Linux "
"2.4.20."
msgstr ""
"Журналируемая файловая система, разработанная SGI, была добавлена в Linux "
"начиная с ядра версии 2.4.20."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<xiafs>"
msgstr "B<xiafs>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "was designed and implemented to be a stable, safe filesystem by extending "
#| "the Minix filesystem code.  It provides the basic most requested features "
#| "without undue complexity.  The B<xiafs> filesystem is no longer actively "
#| "developed or maintained.  It was removed from the kernel in 2.1.21."
msgid ""
"was designed and implemented to be a stable, safe filesystem by extending "
"the Minix filesystem code.  It provides the basic most requested features "
"without undue complexity.  The B<xiafs> filesystem is no longer actively "
"developed or maintained.  It was removed from the kernel in Linux 2.1.21."
msgstr ""
"Разработана и реализована как стабильная надёжная файловая система "
"посредством расширения кода файловой системы Minix. Предоставляет основные "
"часто запрашиваемые возможности без неоправданной сложности. Файловая "
"система B<xiafs> больше активно не разрабатывается или сопровождается. Была "
"удалена из ядра 2.1.21."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<fuse>(4), B<btrfs>(5), B<ext2>(5), B<ext3>(5), B<ext4>(5), B<nfs>(5), "
"B<proc>(5), B<sysfs>(5), B<tmpfs>(5), B<xfs>(5), B<fsck>(8), B<mkfs>(8), "
"B<mount>(8)"
msgstr ""
"B<fuse>(4), B<btrfs>(5), B<ext2>(5), B<ext3>(5), B<ext4>(5), B<nfs>(5), "
"B<proc>(5), B<sysfs>(5), B<tmpfs>(5), B<xfs>(5), B<fsck>(8), B<mkfs>(8), "
"B<mount>(8)"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "2022-12-05"
msgstr "5 декабря 2022 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "is a CD-ROM filesystem type conforming to the ISO 9660 standard."
msgstr "Файловая система для CD-ROM, соответствующая стандарту ISO 9660."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Linux supports High Sierra, the precursor to the ISO 9660 standard for CD-"
"ROM filesystems.  It is automatically recognized within the B<iso9660> "
"filesystem support under Linux."
msgstr ""
"В Linux есть поддержка стандарта High Sierra, предшественника стандарта ISO "
"9660 для файловых систем CD-ROM. High Sierra автоматически распознается при "
"включении в Linux поддержки файловой системы B<iso9660>."

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alexey, 2016.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2014-2017.
# kogamatranslator49 <r.podarov@yandex.ru>, 2015.
# Darima Kogan <silverdk99@gmail.com>, 2014.
# Max Is <ismax799@gmail.com>, 2016.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-05-01 15:47+0200\n"
"PO-Revision-Date: 2019-10-12 08:58+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "pidfd_open"
msgstr "pidfd_open"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr "Linux man-pages 6.7"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "pidfd_open - obtain a file descriptor that refers to a process"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>sys/syscall.hE<gt>>      /* Definition of B<SYS_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>sys/syscall.hE<gt>>      /* определения констант B<SYS_*> */\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int syscall(SYS_pidfd_open, pid_t >I<pid>B<, unsigned int >I<flags>B<);>\n"
msgstr "B<int syscall(SYS_pidfd_open, pid_t >I<pid>B<, unsigned int >I<flags>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<Note>: glibc provides no wrapper for B<pidfd_open>(), necessitating the "
"use of B<syscall>(2)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<pidfd_open>()  system call creates a file descriptor that refers to "
"the process whose PID is specified in I<pid>.  The file descriptor is "
"returned as the function result; the close-on-exec flag is set on the file "
"descriptor."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The I<flags> argument is a bit mask that ORs together zero or more of the "
#| "following flags:"
msgid ""
"The I<flags> argument either has the value 0, or contains the following flag:"
msgstr ""
"Аргумент I<flags> представляет собой битовую маску из комбинации (OR) нуля "
"или более следующих флагов:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PIDFD_NONBLOCK> (since Linux 5.10)"
msgstr "B<PIDFD_NONBLOCK> (начиная с Linux 5.10)"

#.  commit 4da9af0014b51c8b015ed8c622440ef28912efe6
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Return a nonblocking file descriptor.  If the process referred to by the "
"file descriptor has not yet terminated, then an attempt to wait on the file "
"descriptor using B<waitid>(2)  will immediately return the error B<EAGAIN> "
"rather than blocking."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, B<posix_openpt>()  returns a nonnegative file descriptor "
#| "which is the lowest numbered unused file descriptor.  On failure, -1 is "
#| "returned, and I<errno> is set to indicate the error."
msgid ""
"On success, B<pidfd_open>()  returns a file descriptor (a nonnegative "
"integer).  On error, -1 is returned and I<errno> is set to indicate the "
"error."
msgstr ""
"При успешном выполнении B<posix_openpt>() возвращает неотрицательный "
"файловый дескриптор с наименьшим номером неиспользуемого файлового "
"дескриптора. При ошибке возвращается -1, и в I<errno> записывается номер "
"ошибки."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<flags> is not valid."
msgstr "Значение I<flags> неверно."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<pid> is not valid."
msgstr "I<pid> задан некорректно."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EMFILE>"
msgstr "B<EMFILE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The per-process limit on the number of open file descriptors has been "
"reached (see the description of B<RLIMIT_NOFILE> in B<getrlimit>(2))."
msgstr ""
"Было достигнуто ограничение по количеству открытых файловых дескрипторов на "
"процесс (смотрите описание B<RLIMIT_NOFILE> в B<getrlimit>(2))."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENFILE>"
msgstr "B<ENFILE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The system-wide limit on the total number of open files has been reached."
msgstr "Достигнуто максимальное количество открытых файлов в системе."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENODEV>"
msgstr "B<ENODEV>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The anonymous inode filesystem is not available in this kernel."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Insufficient kernel memory was available."
msgstr "Недостаточное количество памяти ядра."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ESRCH>"
msgstr "B<ESRCH>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "The named file does not exist."
msgid "The process specified by I<pid> does not exist."
msgstr "Указанный файл не существует."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "Linux"
msgid "Linux 5.3."
msgstr "Linux"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The following code sequence can be used to obtain a file descriptor for the "
"child of B<fork>(2):"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"pid = fork();\n"
"if (pid E<gt> 0) {     /* If parent */\n"
"    pidfd = pidfd_open(pid, 0);\n"
"    ...\n"
"}\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Even if the child has already terminated by the time of the B<pidfd_open>()  "
"call, its PID will not have been recycled and the returned file descriptor "
"will refer to the resulting zombie process.  Note, however, that this is "
"guaranteed only if the following conditions hold true:"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"the disposition of B<SIGCHLD> has not been explicitly set to B<SIG_IGN> (see "
"B<sigaction>(2));"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"the B<SA_NOCLDWAIT> flag was not specified while establishing a handler for "
"B<SIGCHLD> or while setting the disposition of that signal to B<SIG_DFL> "
"(see B<sigaction>(2)); and"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"the zombie process was not reaped elsewhere in the program (e.g., either by "
"an asynchronously executed signal handler or by B<wait>(2)  or similar in "
"another thread)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If any of these conditions does not hold, then the child process (along with "
"a PID file descriptor that refers to it)  should instead be created using "
"B<clone>(2)  with the B<CLONE_PIDFD> flag."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "open file descriptors"
msgid "Use cases for PID file descriptors"
msgstr "Открытые файловые дескрипторы"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A PID file descriptor returned by B<pidfd_open>()  (or by B<clone>(2)  with "
"the B<CLONE_PID> flag) can be used for the following purposes:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<pidfd_send_signal>(2)  system call can be used to send a signal to the "
"process referred to by a PID file descriptor."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A PID file descriptor can be monitored using B<poll>(2), B<select>(2), and "
"B<epoll>(7).  When the process that it refers to terminates, these "
"interfaces indicate the file descriptor as readable.  Note, however, that in "
"the current implementation, nothing can be read from the file descriptor "
"(B<read>(2)  on the file descriptor fails with the error B<EINVAL>)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the PID file descriptor refers to a child of the calling process, then it "
"can be waited on using B<waitid>(2)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<pidfd_getfd>(2)  system call can be used to obtain a duplicate of a "
"file descriptor of another process referred to by a PID file descriptor."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A PID file descriptor can be used as the argument of B<setns>(2)  in order "
"to move into one or more of the same namespaces as the process referred to "
"by the file descriptor."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A PID file descriptor can be used as the argument of B<process_madvise>(2)  "
"in order to provide advice on the memory usage patterns of the process "
"referred to by the file descriptor."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<pidfd_open>()  system call is the preferred way of obtaining a PID "
"file descriptor for an already existing process.  The alternative is to "
"obtain a file descriptor by opening a I</proc/>pid directory.  However, the "
"latter technique is possible only if the B<proc>(5)  filesystem is mounted; "
"furthermore, the file descriptor obtained in this way is I<not> pollable and "
"can't be waited on with B<waitid>(2)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "ПРИМЕРЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The program below opens a PID file descriptor for the process whose PID is "
"specified as its command-line argument.  It then uses B<poll>(2)  to monitor "
"the file descriptor for process exit, as indicated by an B<EPOLLIN> event."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr "Исходный код программы"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>poll.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/syscall.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"static int\n"
"pidfd_open(pid_t pid, unsigned int flags)\n"
"{\n"
"    return syscall(SYS_pidfd_open, pid, flags);\n"
"}\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int            pidfd, ready;\n"
"    struct pollfd  pollfd;\n"
"\\&\n"
"    if (argc != 2) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>pidE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_SUCCESS);\n"
"    }\n"
"\\&\n"
"    pidfd = pidfd_open(atoi(argv[1]), 0);\n"
"    if (pidfd == -1) {\n"
"        perror(\"pidfd_open\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    pollfd.fd = pidfd;\n"
"    pollfd.events = POLLIN;\n"
"\\&\n"
"    ready = poll(&pollfd, 1, -1);\n"
"    if (ready == -1) {\n"
"        perror(\"poll\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    printf(\"Events (%#x): POLLIN is %sset\\en\", pollfd.revents,\n"
"           (pollfd.revents & POLLIN) ? \"\" : \"not \");\n"
"\\&\n"
"    close(pidfd);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<clone>(2), B<kill>(2), B<pidfd_getfd>(2), B<pidfd_send_signal>(2), "
"B<poll>(2), B<process_madvise>(2), B<select>(2), B<setns>(2), B<waitid>(2), "
"B<epoll>(7)"
msgstr ""
"B<clone>(2), B<kill>(2), B<pidfd_getfd>(2), B<pidfd_send_signal>(2), "
"B<poll>(2), B<process_madvise>(2), B<select>(2), B<setns>(2), B<waitid>(2), "
"B<epoll>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid "This system call first appeared in Linux 2.6.10."
msgid "B<pidfd_open>()  first appeared in Linux 5.3."
msgstr "Этот системный вызов впервые появился в Linux 2.6.10."

#. type: Plain text
#: debian-bookworm
msgid "B<pidfd_open>()  is Linux specific."
msgstr "Вызов B<pidfd_open>() есть только в Linux."

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<pidfd_open>()  system call is the preferred way of obtaining a PID "
"file descriptor for an already existing process.  The alternative is to "
"obtain a file descriptor by opening a I</proc/[pid]> directory.  However, "
"the latter technique is possible only if the B<proc>(5)  filesystem is "
"mounted; furthermore, the file descriptor obtained in this way is I<not> "
"pollable and can't be waited on with B<waitid>(2)."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>poll.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/syscall.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""
"#define _GNU_SOURCE\n"
"#include E<lt>poll.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/syscall.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"static int\n"
"pidfd_open(pid_t pid, unsigned int flags)\n"
"{\n"
"    return syscall(SYS_pidfd_open, pid, flags);\n"
"}\n"
msgstr ""
"static int\n"
"pidfd_open(pid_t pid, unsigned int flags)\n"
"{\n"
"    return syscall(SYS_pidfd_open, pid, flags);\n"
"}\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int            pidfd, ready;\n"
"    struct pollfd  pollfd;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int            pidfd, ready;\n"
"    struct pollfd  pollfd;\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    if (argc != 2) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>pidE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_SUCCESS);\n"
"    }\n"
msgstr ""
"    if (argc != 2) {\n"
"        fprintf(stderr, \"Использование: %s E<lt>PIDE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_SUCCESS);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    pidfd = pidfd_open(atoi(argv[1]), 0);\n"
"    if (pidfd == -1) {\n"
"        perror(\"pidfd_open\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    pidfd = pidfd_open(atoi(argv[1]), 0);\n"
"    if (pidfd == -1) {\n"
"        perror(\"pidfd_open\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    pollfd.fd = pidfd;\n"
"    pollfd.events = POLLIN;\n"
msgstr ""
"    pollfd.fd = pidfd;\n"
"    pollfd.events = POLLIN;\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    ready = poll(&pollfd, 1, -1);\n"
"    if (ready == -1) {\n"
"        perror(\"poll\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    ready = poll(&pollfd, 1, -1);\n"
"    if (ready == -1) {\n"
"        perror(\"poll\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    printf(\"Events (%#x): POLLIN is %sset\\en\", pollfd.revents,\n"
"           (pollfd.revents & POLLIN) ? \"\" : \"not \");\n"
msgstr ""
"    printf(\"Events (%#x): POLLIN is %sset\\en\", pollfd.revents,\n"
"           (pollfd.revents & POLLIN) ? \"\" : \"not \");\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    close(pidfd);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    close(pidfd);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-04-03"
msgstr "3 апреля 2023 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

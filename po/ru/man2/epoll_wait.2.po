# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2012.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-05-01 15:38+0200\n"
"PO-Revision-Date: 2019-10-05 07:57+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "epoll_wait"
msgstr "epoll_wait"

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2024-03-03"
msgstr "3 марта 2024 г."

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr "Linux man-pages 6.7"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"epoll_wait, epoll_pwait, epoll_pwait2 - wait for an I/O event on an epoll "
"file descriptor"
msgstr ""
"epoll_wait, epoll_pwait, epoll_pwait2 - ждать события ввода/вывода на "
"файловом дескрипторе epoll"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/epoll.hE<gt>>\n"
msgstr "B<#include E<lt>sys/epoll.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<int epoll_wait(int >I<epfd>B<, struct epoll_event *>I<events>B<,>\n"
#| "B<               int >I<maxevents>B<, int >I<timeout>B<);>\n"
#| "B<int epoll_pwait(int >I<epfd>B<, struct epoll_event *>I<events>B<,>\n"
#| "B<               int >I<maxevents>B<, int >I<timeout>B<,>\n"
#| "B<               const sigset_t *>I<sigmask>B<);>\n"
#| "B<int epoll_pwait2(int >I<epfd>B<, struct epoll_event *>I<events>B<,>\n"
#| "B<               int >I<maxevents>B<, const struct timespec *>I<timeout>B<,>\n"
#| "B<               const sigset_t *>I<sigmask>B<);>\n"
msgid ""
"B<int epoll_wait(int >I<epfd>B<, struct epoll_event *>I<events>B<,>\n"
"B<               int >I<maxevents>B<, int >I<timeout>B<);>\n"
"B<int epoll_pwait(int >I<epfd>B<, struct epoll_event *>I<events>B<,>\n"
"B<               int >I<maxevents>B<, int >I<timeout>B<,>\n"
"B<               const sigset_t *_Nullable >I<sigmask>B<);>\n"
"B<int epoll_pwait2(int >I<epfd>B<, struct epoll_event *>I<events>B<,>\n"
"B<               int >I<maxevents>B<, const struct timespec *_Nullable >I<timeout>B<,>\n"
"B<               const sigset_t *_Nullable >I<sigmask>B<);>\n"
msgstr ""
"B<int epoll_wait(int >I<epfd>B<, struct epoll_event *>I<events>B<,>\n"
"B<               int >I<maxevents>B<, int >I<timeout>B<);>\n"
"B<int epoll_pwait(int >I<epfd>B<, struct epoll_event *>I<events>B<,>\n"
"B<               int >I<maxevents>B<, int >I<timeout>B<,>\n"
"B<               const sigset_t *>I<sigmask>B<);>\n"
"B<int epoll_pwait2(int >I<epfd>B<, struct epoll_event *>I<events>B<,>\n"
"B<               int >I<maxevents>B<, const struct timespec *>I<timeout>B<,>\n"
"B<               const sigset_t *>I<sigmask>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<epoll_wait>()  system call waits for events on the B<epoll>(7)  "
#| "instance referred to by the file descriptor I<epfd>.  The memory area "
#| "pointed to by I<events> will contain the events that will be available "
#| "for the caller.  Up to I<maxevents> are returned by B<epoll_wait>().  The "
#| "I<maxevents> argument must be greater than zero."
msgid ""
"The B<epoll_wait>()  system call waits for events on the B<epoll>(7)  "
"instance referred to by the file descriptor I<epfd>.  The buffer pointed to "
"by I<events> is used to return information from the ready list about file "
"descriptors in the interest list that have some events available.  Up to "
"I<maxevents> are returned by B<epoll_wait>().  The I<maxevents> argument "
"must be greater than zero."
msgstr ""
"Системный вызов B<epoll_wait>() ожидает события на экземпляре B<epoll>(7), "
"на который указывает файловый дескриптор I<epfd>. Область памяти, на которую "
"указывает I<events>, будет содержать события, доступные для вызываемого. "
"Вызов B<epoll_wait>() может вернуть до I<maxevents> событий. Параметр "
"I<maxevents> должен быть больше нуля."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<timeout> argument specifies the number of milliseconds that "
"B<epoll_wait>()  will block.  Time is measured against the "
"B<CLOCK_MONOTONIC> clock."
msgstr ""
"В аргументе I<timeout> указывается количество миллисекунд, на которые будет "
"заблокирован B<epoll_wait>(). Время отслеживается по часам "
"B<CLOCK_MONOTONIC>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "A call to B<epoll_wait>(2)  is done."
msgid "A call to B<epoll_wait>()  will block until either:"
msgstr "Вызов B<epoll_wait>(2) завершается."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "a file descriptor delivers an event;"
msgstr "событие не будет доставлено в файловый дескриптор;"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "the call is interrupted by a signal handler; or"
msgstr "вызов не прервётся обработчиком сигнала;"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "the timeout expires."
msgstr "не истечёт время ожидания."

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Note that the I<timeout> interval will be rounded up to the system clock "
#| "granularity, and kernel scheduling delays mean that the blocking interval "
#| "may overrun by a small amount.  Specifying a I<timeout> of -1 causes "
#| "B<epoll_wait>()  to block indefinitely, while specifying a I<timeout> "
#| "equal to zero cause B<epoll_wait>()  to return immediately, even if no "
#| "events are available."
msgid ""
"Note that the I<timeout> interval will be rounded up to the system clock "
"granularity, and kernel scheduling delays mean that the blocking interval "
"may overrun by a small amount.  Specifying a I<timeout> of -1 causes "
"B<epoll_wait>()  to block indefinitely, while specifying a I<timeout> equal "
"to zero causes B<epoll_wait>()  to return immediately, even if no events are "
"available."
msgstr ""
"Заметим, что интервал I<timeout> будет округлён в соответствии с точностью "
"системных часов, а задержки ядерного планирования приведут к тому, что "
"интервал блокировки может быть немного больше. Если присвоить I<timeout> "
"значение -1, то B<epoll_wait>() блокируется навсегда; если значение "
"I<timeout> равно 0, то B<epoll_wait>() сразу завершает работу, даже если "
"никаких событий не произошло."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "The I<struct epoll_event> is defined as:"
msgid "The I<struct epoll_event> is described in B<epoll_event>(3type)."
msgstr "Структура I<struct epoll_event> определена так:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The I<data> field of each returned structure contains the same data as "
#| "was specified in the most recent call to B<epoll_ctl>(2)  "
#| "(B<EPOLL_CTL_ADD>, B<EPOLL_CTL_MOD>)  for the corresponding open file "
#| "description.  The I<events> field contains the returned event bit field."
msgid ""
"The I<data> field of each returned I<epoll_event> structure contains the "
"same data as was specified in the most recent call to B<epoll_ctl>(2)  "
"(B<EPOLL_CTL_ADD>, B<EPOLL_CTL_MOD>)  for the corresponding open file "
"descriptor."
msgstr ""
"Поле I<data> в каждой возвращаемой структуре содержит те же данные, которые "
"были указаны в самом последнем вызове B<epoll_ctl>(2) (B<EPOLL_CTL_ADD>, "
"B<EPOLL_CTL_MOD>) для соответствующего открытого описания файла. В поле "
"I<events> содержится битовое поле возвращаемого события."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<events> field is a bit mask that indicates the events that have "
"occurred for the corresponding open file description.  See B<epoll_ctl>(2)  "
"for a list of the bits that may appear in this mask."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "epoll_pwait()"
msgstr "epoll_pwait()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The relationship between B<epoll_wait>()  and B<epoll_pwait>()  is analogous "
"to the relationship between B<select>(2)  and B<pselect>(2): like "
"B<pselect>(2), B<epoll_pwait>()  allows an application to safely wait until "
"either a file descriptor becomes ready or until a signal is caught."
msgstr ""
"Отношения между B<epoll_wait>() и B<epoll_pwait>() аналогичны родству "
"B<select>(2) и B<pselect>(2): как B<pselect>(2), B<epoll_pwait>() позволяет "
"приложению безопасно ждать, пока файловый дескриптор не станет готов или "
"пока не будет получен сигнал."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The following B<epoll_pwait>()  call:"
msgstr "Вызов B<epoll_pwait>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ready = epoll_pwait(epfd, &events, maxevents, timeout, &sigmask);\n"
msgstr "ready = epoll_pwait(epfd, &events, maxevents, timeout, &sigmask);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "is equivalent to I<atomically> executing the following calls:"
msgstr "эквивалентен I<атомарному> выполнению следующих вызовов:"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "pthread_sigmask(SIG_SETMASK, &sigmask, &origmask);\n"
#| "ready = epoll_wait(epfd, &events, maxevents, timeout);\n"
#| "pthread_sigmask(SIG_SETMASK, &origmask, NULL);\n"
msgid ""
"sigset_t origmask;\n"
"\\&\n"
"pthread_sigmask(SIG_SETMASK, &sigmask, &origmask);\n"
"ready = epoll_wait(epfd, &events, maxevents, timeout);\n"
"pthread_sigmask(SIG_SETMASK, &origmask, NULL);\n"
msgstr ""
"pthread_sigmask(SIG_SETMASK, &sigmask, &origmask);\n"
"ready = epoll_wait(epfd, &events, maxevents, timeout);\n"
"pthread_sigmask(SIG_SETMASK, &origmask, NULL);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<sigmask> argument may be specified as NULL, in which case "
"B<epoll_pwait>()  is equivalent to B<epoll_wait>()."
msgstr ""
"Аргумент I<sigmask> может быть равен NULL \\(em в этом случае "
"B<epoll_pwait>() эквивалентен B<epoll_wait>()."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "epoll_pwait2()"
msgstr "epoll_pwait2()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<epoll_pwait2>()  system call is equivalent to B<epoll_pwait>()  except "
"for the I<timeout> argument.  It takes an argument of type I<timespec> to be "
"able to specify nanosecond resolution timeout.  This argument functions the "
"same as in B<pselect>(2)  and B<ppoll>(2).  If I<timeout> is NULL, then "
"B<epoll_pwait2>()  can block indefinitely."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "When successful, B<epoll_wait>()  returns the number of file descriptors "
#| "ready for the requested I/O, or zero if no file descriptor became ready "
#| "during the requested I<timeout> milliseconds.  When an error occurs, "
#| "B<epoll_wait>()  returns -1 and I<errno> is set appropriately."
msgid ""
"On success, B<epoll_wait>()  returns the number of file descriptors ready "
"for the requested I/O operation, or zero if no file descriptor became ready "
"during the requested I<timeout> milliseconds.  On failure, B<epoll_wait>()  "
"returns -1 and I<errno> is set to indicate the error."
msgstr ""
"При нормальном выполнении B<epoll_wait>() возвращает количество файловых "
"дескрипторов, готовых для запросов ввода-вывода, или ноль, если ни один "
"файловый дескриптор не стал готов за отведённые I<timeout> миллисекунд. При "
"возникновении ошибки B<epoll_wait>() возвращает -1 и устанавливает I<errno> "
"в соответствующее значение."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<epfd> is not a valid file descriptor."
msgstr "Значение I<epfd> не является правильным файловым дескриптором."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The memory area pointed to by I<events> is not accessible with write "
"permissions."
msgstr "Память, указанная I<events>, недоступна на запись из-за прав доступа."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINTR>"
msgstr "B<EINTR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The call was interrupted by a signal handler before either (1) any of the "
"requested events occurred or (2) the I<timeout> expired; see B<signal>(7)."
msgstr ""
"Вызов был прерван обработчиком сигнала до возникновения любого из "
"запрошенных событий или истечения I<timeout>; см. B<signal>(7)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<epfd> is not an B<epoll> file descriptor, or I<maxevents> is less than or "
"equal to zero."
msgstr ""
"I<epfd> не является файловым дескриптором B<epoll>, или I<maxevents> меньше "
"или равно нулю."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "epoll_pwait()"
msgid "B<epoll_wait>()"
msgstr "epoll_pwait()"

#.  To be precise: Linux 2.5.44.
#.  The interface should be finalized by Linux 2.5.66.
#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "Since glibc 2.2.2:"
msgid "Linux 2.6, glibc 2.3.2."
msgstr "Начиная с glibc 2.2.2:"

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "epoll_pwait()"
msgid "B<epoll_pwait>()"
msgstr "epoll_pwait()"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Linux 2.6.19, glibc 2.6."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "epoll_pwait2()"
msgid "B<epoll_pwait2>()"
msgstr "epoll_pwait2()"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "Linux"
msgid "Linux 5.11."
msgstr "Linux"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"While one thread is blocked in a call to B<epoll_wait>(), it is possible for "
"another thread to add a file descriptor to the waited-upon B<epoll> "
"instance.  If the new file descriptor becomes ready, it will cause the "
"B<epoll_wait>()  call to unblock."
msgstr ""
"Пока одна нить блокирована в вызове B<epoll_wait>(), в другой нити возможно "
"добавить файловый дескриптор, который будет ожидаться экземпляром B<epoll>. "
"Как только новый файловый дескриптор станет готовым, это разблокирует вызов "
"B<epoll_wait>()."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If more than I<maxevents> file descriptors are ready when B<epoll_wait>()  "
"is called, then successive B<epoll_wait>()  calls will round robin through "
"the set of ready file descriptors.  This behavior helps avoid starvation "
"scenarios, where a process fails to notice that additional file descriptors "
"are ready because it focuses on a set of file descriptors that are already "
"known to be ready."
msgstr ""
"Если готово более I<maxevents> файловых дескрипторов при вызове "
"B<epoll_wait>(), то последующие вызовы B<epoll_wait>() циклически обработают "
"весь набор готовых файловых дескрипторов. Такое поведение помогает избежать "
"голодания — когда процесс не уведомляется, что дополнительные файловые "
"дескрипторы готовы, так как он нацелен на набор файловых дескрипторов, про "
"которые уже известно об их готовности."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note that it is possible to call B<epoll_wait>()  on an B<epoll> instance "
"whose interest list is currently empty (or whose interest list becomes empty "
"because file descriptors are closed or removed from the interest in another "
"thread).  The call will block until some file descriptor is later added to "
"the interest list (in another thread) and that file descriptor becomes ready."
msgstr ""
"Заметим, что возможно вызвать B<epoll_wait>() для экземпляра B<epoll>, чей "
"список interest ещё пуст (или чей список interest станет пустым, так как "
"файловые дескрипторы закрыты или удалены из interest в другой нити). Вызов "
"будет заблокирован до тех пор, пока какой-нибудь файловый дескриптор не "
"будет добавлен в список interest (в другой нити) и этот файлоый дескриптор "
"не станет готовым."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "C library/kernel differences"
msgstr "Отличия между библиотекой C и ядром"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The raw B<epoll_pwait>()  system call has a sixth argument, I<size_t "
#| "sigsetsize>, which specifies the size in bytes of the I<sigmask> "
#| "argument.  The glibc B<epoll_pwait>()  wrapper function specifies this "
#| "argument as a fixed value (equal to I<sizeof(sigset_t)>)."
msgid ""
"The raw B<epoll_pwait>()  and B<epoll_pwait2>()  system calls have a sixth "
"argument, I<size_t sigsetsize>, which specifies the size in bytes of the "
"I<sigmask> argument.  The glibc B<epoll_pwait>()  wrapper function specifies "
"this argument as a fixed value (equal to I<sizeof(sigset_t)>)."
msgstr ""
"Ядерный системный вызов B<epoll_pwait>() имеет шестой аргумент, I<size_t "
"sigsetsize>, в котором указывается размер аргумента I<sigmask> в байтах. В "
"обёрточной функции glibc B<epoll_pwait>() в этом аргументе передаётся "
"постоянная величина (равная I<sizeof(sigset_t)>)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "In kernels before 2.6.37, a I<timeout> value larger than approximately "
#| "I<LONG_MAX / HZ> milliseconds is treated as -1 (i.e., infinity).  Thus, "
#| "for example, on a system where I<sizeof(long)> is 4 and the kernel I<HZ> "
#| "value is 1000, this means that timeouts greater than 35.79 minutes are "
#| "treated as infinity."
msgid ""
"Before Linux 2.6.37, a I<timeout> value larger than approximately "
"I<LONG_MAX / HZ> milliseconds is treated as -1 (i.e., infinity).  Thus, for "
"example, on a system where I<sizeof(long)> is 4 and the kernel I<HZ> value "
"is 1000, this means that timeouts greater than 35.79 minutes are treated as "
"infinity."
msgstr ""
"В ядрах до версии 2.6.37, если значение I<timeout> больше чем "
"приблизительное I<LONG_MAX / HZ> секунд, то оно воспринимается как -1 (т.е., "
"бесконечность). То есть, например, в системе, где I<sizeof(long)> равно 4 и "
"значение ядра I<HZ> равно 1000, задержка более 35,79 минут считается "
"бесконечностью."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<epoll_create>(2), B<epoll_ctl>(2), B<epoll>(7)"
msgstr "B<epoll_create>(2), B<epoll_ctl>(2), B<epoll>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm fedora-40 mageia-cauldron opensuse-leap-15-6
msgid ""
"Note that the I<timeout> interval will be rounded up to the system clock "
"granularity, and kernel scheduling delays mean that the blocking interval "
"may overrun by a small amount.  Specifying a I<timeout> of -1 causes "
"B<epoll_wait>()  to block indefinitely, while specifying a I<timeout> equal "
"to zero cause B<epoll_wait>()  to return immediately, even if no events are "
"available."
msgstr ""
"Заметим, что интервал I<timeout> будет округлён в соответствии с точностью "
"системных часов, а задержки ядерного планирования приведут к тому, что "
"интервал блокировки может быть немного больше. Если присвоить I<timeout> "
"значение -1, то B<epoll_wait>() блокируется навсегда; если значение "
"I<timeout> равно 0, то B<epoll_wait>() сразу завершает работу, даже если "
"никаких событий не произошло."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "sigset_t origmask;\n"
msgstr "sigset_t origmask;\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"pthread_sigmask(SIG_SETMASK, &sigmask, &origmask);\n"
"ready = epoll_wait(epfd, &events, maxevents, timeout);\n"
"pthread_sigmask(SIG_SETMASK, &origmask, NULL);\n"
msgstr ""
"pthread_sigmask(SIG_SETMASK, &sigmask, &origmask);\n"
"ready = epoll_wait(epfd, &events, maxevents, timeout);\n"
"pthread_sigmask(SIG_SETMASK, &origmask, NULL);\n"

#. type: Plain text
#: debian-bookworm fedora-40 mageia-cauldron opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "When successful, B<epoll_wait>()  returns the number of file descriptors "
#| "ready for the requested I/O, or zero if no file descriptor became ready "
#| "during the requested I<timeout> milliseconds.  When an error occurs, "
#| "B<epoll_wait>()  returns -1 and I<errno> is set appropriately."
msgid ""
"On success, B<epoll_wait>()  returns the number of file descriptors ready "
"for the requested I/O, or zero if no file descriptor became ready during the "
"requested I<timeout> milliseconds.  On failure, B<epoll_wait>()  returns -1 "
"and I<errno> is set to indicate the error."
msgstr ""
"При нормальном выполнении B<epoll_wait>() возвращает количество файловых "
"дескрипторов, готовых для запросов ввода-вывода, или ноль, если ни один "
"файловый дескриптор не стал готов за отведённые I<timeout> миллисекунд. При "
"возникновении ошибки B<epoll_wait>() возвращает -1 и устанавливает I<errno> "
"в соответствующее значение."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#.  To be precise: kernel 2.5.44.
#.  The interface should be finalized by Linux 2.5.66.
#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "B<epoll_wait>()  was added to the kernel in version 2.6.  Library support "
#| "is provided in glibc starting with version 2.3.2."
msgid ""
"B<epoll_wait>()  was added in Linux 2.6.  Library support is provided in "
"glibc 2.3.2."
msgstr ""
"Вызов B<epoll_wait>() был добавлен в ядро версии 2.6. В glibc "
"соответствующая функция появилась в версии 2.3.2."

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "B<epoll_pwait>()  was added to Linux in kernel 2.6.19.  Library support "
#| "is provided in glibc starting with version 2.6."
msgid ""
"B<epoll_pwait>()  was added in Linux 2.6.19.  Library support is provided in "
"glibc 2.6."
msgstr ""
"Вызов B<epoll_pwait>() был добавлен в ядро Linux 2.6.19. В glibc "
"соответствующая функция появилась в версии 2.6."

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid "B<epoll_ctl>()  was added to the kernel in version 2.6."
msgid "B<epoll_pwait2>()  was added in Linux 5.11."
msgstr "Системный вызов B<epoll_ctl>() был добавлен в ядро версии 2.6."

#. type: Plain text
#: debian-bookworm
msgid ""
"B<epoll_wait>(), B<epoll_pwait>(), and B<epoll_pwait2>()  are Linux-specific."
msgstr ""
"Вызовы B<epoll_wait>(), B<epoll_pwait>() и B<epoll_pwait2>() есть только в "
"Linux."

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2014, 2017.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Vladislav <ivladislavefimov@gmail.com>, 2015.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-05-01 15:40+0200\n"
"PO-Revision-Date: 2019-10-05 08:06+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "getpeername"
msgstr "getpeername"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: archlinux debian-unstable fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.7"
msgstr "Linux man-pages 6.7"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "getpeername - get name of connected peer socket"
msgstr "getpeername - получает имя подключившегося сокета"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/socket.hE<gt>>\n"
msgstr "B<#include E<lt>sys/socket.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int getpeername(int >I<sockfd>B<, struct sockaddr *restrict >I<addr>B<,>\n"
"B<                socklen_t *restrict >I<addrlen>B<);>\n"
msgstr ""
"B<int getpeername(int >I<sockfd>B<, struct sockaddr *restrict >I<addr>B<,>\n"
"B<                socklen_t *restrict >I<addrlen>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<getpeername>()  returns the address of the peer connected to the socket "
"I<sockfd>, in the buffer pointed to by I<addr>.  The I<addrlen> argument "
"should be initialized to indicate the amount of space pointed to by "
"I<addr>.  On return it contains the actual size of the name returned (in "
"bytes).  The name is truncated if the buffer provided is too small."
msgstr ""
"B<getpeername>() возвращает адрес машины, подключившейся к сокету I<sockfd>, "
"в буфер, указанный I<addr>. В параметре I<addrlen> должно быть указано, "
"сколько места выделено, на которое указывает I<addr>. По возвращении он "
"содержит реальный размер памяти, занимаемый возвращаемым именем (в байтах). "
"Имя обрезается, если буфер окажется слишком мал."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The returned address is truncated if the buffer provided is too small; in "
"this case, I<addrlen> will return a value greater than was supplied to the "
"call."
msgstr ""
"Возвращаемый адрес урежется, если предоставленный буфер окажется слишком "
"маленьким; в этом случае в I<addrlen> будет возвращено значение большее чем "
"было в вызове."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, B<sethostid>()  returns 0; on error, -1 is returned, and "
#| "I<errno> is set to indicate the error."
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"При успешном выполнении B<sethostid>() возвращает 0; при ошибке — -1, а в "
"I<errno> задаётся код ошибки."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The argument I<sockfd> is not a valid file descriptor."
msgstr "Аргумент I<sockfd> не является допустимым файловым дескриптором."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<addr> argument points to memory not in a valid part of the process "
"address space."
msgstr ""
"Аргумент I<addr> указывает на память в недопустимой части адресуемого "
"пространства процесса."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<addrlen> is invalid (e.g., is negative)."
msgstr "Неправильное значение аргумента I<addrlen> (например, отрицательное)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOBUFS>"
msgstr "B<ENOBUFS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Insufficient resources were available in the system to perform the operation."
msgstr "Недостаточно ресурсов в системе для выполнения операции."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTCONN>"
msgstr "B<ENOTCONN>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The socket is not connected."
msgstr "Сокет не подключён."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTSOCK>"
msgstr "B<ENOTSOCK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The file descriptor I<sockfd> does not refer to a socket."
msgstr "Файловый дескриптор I<sockfd> указывает не на каталог."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "POSIX.1-2001, POSIX.1-2008, SVr4, 4.4BSD (B<getpeername>()  first "
#| "appeared in 4.2BSD)."
msgid "POSIX.1-2001, SVr4, 4.4BSD (first appeared in 4.2BSD)."
msgstr ""
"POSIX.1-2001, POSIX.1-2008, SVr4, 4.4BSD, (B<getpeername>() впервые появился "
"в 4.2BSD)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For stream sockets, once a B<connect>(2)  has been performed, either socket "
"can call B<getpeername>()  to obtain the address of the peer socket.  On the "
"other hand, datagram sockets are connectionless.  Calling B<connect>(2)  on "
"a datagram socket merely sets the peer address for outgoing datagrams sent "
"with B<write>(2)  or B<recv>(2).  The caller of B<connect>(2)  can use "
"B<getpeername>()  to obtain the peer address that it earlier set for the "
"socket.  However, the peer socket is unaware of this information, and "
"calling B<getpeername>()  on the peer socket will return no useful "
"information (unless a B<connect>(2)  call was also executed on the peer).  "
"Note also that the receiver of a datagram can obtain the address of the "
"sender when using B<recvfrom>(2)."
msgstr ""
"Для потоковых сокетов после выполнения B<connect>(2) любой сокет может "
"вызвать B<getpeername>() для получения адреса соседа (peer). С другой "
"стороны, дейтаграмные сокеты не устанавливают соединения. Вызов "
"B<connect>(2) для дейтаграмного сокета просто задаст адрес соседа в "
"исходящих дейтаграммах, посылаемых с помощью B<write>(2) или B<recv>(2). "
"Вызывающий B<connect>(2) может использовать B<getpeername>() для получения "
"адреса соседа., который был установлен у сокета ранее. Однако, сокет соседа "
"не знает про это и  вызов B<getpeername>() для сокета соседа не вернёт "
"полезной информации (если сосед также не вызвал B<connect>(2)). Заметим, что "
"получатель дейтаграммы может получить адрес отправителя, если использует "
"B<recvfrom>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМОТРИТЕ ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<accept>(2), B<bind>(2), B<getsockname>(2), B<ip>(7), B<socket>(7), "
"B<unix>(7)"
msgstr ""
"B<accept>(2), B<bind>(2), B<getsockname>(2), B<ip>(7), B<socket>(7), "
"B<unix>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-10-30"
msgstr "30 октября 2022 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm
msgid ""
"POSIX.1-2001, POSIX.1-2008, SVr4, 4.4BSD (B<getpeername>()  first appeared "
"in 4.2BSD)."
msgstr ""
"POSIX.1-2001, POSIX.1-2008, SVr4, 4.4BSD, (B<getpeername>() впервые появился "
"в 4.2BSD)."

#. type: Plain text
#: debian-bookworm
msgid "For background on the I<socklen_t> type, see B<accept>(2)."
msgstr "Описание типа I<socklen_t> смотрите в B<accept>(2)."

#. type: TH
#: fedora-40 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-04-03"
msgstr "3 апреля 2023 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

# Ukrainian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Andrij Mizyk <andm1zyk@proton.me>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:22+0200\n"
"PO-Revision-Date: 2022-09-24 20:47+0300\n"
"Last-Translator: Andrij Mizyk <andm1zyk@proton.me>\n"
"Language-Team: Ukrainian\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "clear_console"
msgstr "clear_console"

#. type: ds n
#: debian-bookworm debian-unstable
#, no-wrap
msgid "5"
msgstr "5"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "НАЗВА"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<clear_console> - clear the console"
msgstr "B<clear_console> - очищає консоль"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "КОРОТКИЙ ОПИС"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<clear_console>"
msgstr "B<clear_console>"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"B<clear_console> clears your console if this is possible.  It looks in the "
"environment for the terminal type and then in the B<terminfo> database to "
"figure out how to clear the screen. To clear the buffer, it then changes the "
"foreground virtual terminal to another terminal and then back to the "
"original terminal."
msgstr ""
"B<clear_console> очищає вашу консоль, якщо це можливо. Щоб зʼясувати, як "
"очистити екран, сопчатку шукає тип терміналу в середовищі, а потім в базі "
"B<terminfo>. Щоб очистити буфер, змінюється віртуальний термінал на "
"передньому плані на інший термінал, а потім повертається до оригінального "
"терміналу."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "ДИВ. ТАКОЖ"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<clear>(1), B<chvt>(1)"
msgstr "B<clear>(1), B<chvt>(1)"

# Ukrainian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Yuri Chornoivan <yurchor@ukr.net>, 2022, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-05-01 15:59+0200\n"
"PO-Revision-Date: 2024-03-02 19:21+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 23.04.1\n"

#. type: TH
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "UUIDD"
msgstr "UUIDD"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2024-03-28"
msgstr "28 березня 2024 року"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "System Administration"
msgstr "Керування системою"

#. type: SH
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "НАЗВА"

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid "uuidd - UUID generation daemon"
msgstr "uuidd — фонова служба створення UUID"

#. type: SH
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr "КОРОТКИЙ ОПИС"

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid "B<uuidd> [options]"
msgstr "B<uuidd> [параметри]"

#. type: SH
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid ""
"The B<uuidd> daemon is used by the UUID library to generate universally "
"unique identifiers (UUIDs), especially time-based UUIDs, in a secure and "
"guaranteed-unique fashion, even in the face of large numbers of threads "
"running on different CPUs trying to grab UUIDs."
msgstr ""
"Фонову службу B<uuidd> використовує бібліотека UUID для створення "
"універсально унікальних ідентифікаторів (UUID), особливо заснованих на "
"часових позначках UUID, у безпечний і гарантовано унікальний спосіб, навіть "
"для великої кількості потоків обробки на різних процесорах, яким потрібні "
"UUID."

#. type: SH
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "OPTIONS"
msgstr "ПАРАМЕТРИ"

#. type: Plain text
#: debian-bookworm
msgid "B<-C>, B<--cont-clock> I<opt_arg>"
msgstr "B<-C>, B<--cont-clock> I<необ_арг>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Activate continuous clock handling for time based UUIDs. B<uuidd> could use "
"all possible clock values, beginning with the daemon\\(cqs start time. The "
"optional argument can be used to set a value for the max_clock_offset. This "
"gurantees, that a clock value of a UUID will always be within the range of "
"the max_clock_offset. \\*(Aq-C\\*(Aq or \\*(Aq--cont-clock\\*(Aq enables the "
"feature with a default max_clock_offset of 2 hours. \\*(Aq-"
"CE<lt>NUME<gt>[hd]\\*(Aq or \\*(Aq--cont-clock=E<lt>NUME<gt>[hd]\\*(Aq "
"enables the feature with a max_clock_offset of NUM seconds. In case of an "
"appended h or d, the NUM value is read in hours or days. The minimum value "
"is 60 seconds, the maximum value is 365 days."
msgstr ""
"Задіяти неперервну обробку даних годинника для заснованих на часі UUID. "
"B<uuidd> може використовувати усі можливі значення годинника, починаючи з "
"моменту запуску фонової служби. Необов'язковим аргументом можна скористатися "
"для встановлення значення для max_clock_offset. Це гарантує, що значення "
"годинника для UUID завжди перебуватиме у діапазоні max_clock_offset. \\*(Aq-"
"C\\*(Aq або \\*(Aq--cont-clock\\*(Aq вмикає можливість із типовим default "
"max_clock_offset у 2 години. \\*(Aq-CE<lt>ЧИСЛОE<gt>[hd]\\*(Aq або \\*(Aq--"
"cont-clock=E<lt>ЧИСЛОE<gt>[hd]\\*(Aq вмикає можливість зі значенням "
"max_clock_offset у ЧИСЛО секунд. Якщо додати h або d, програма вважатиме "
"ЧИСЛО значення у годинах або днях. Мінімальним є значення 60 секунд, а "
"максимальним є значення у 365 днів."

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid "B<-d>, B<--debug>"
msgstr "B<-d>, B<--debug>"

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid ""
"Run B<uuidd> in debugging mode. This prevents B<uuidd> from running as a "
"daemon."
msgstr ""
"Запустити B<uuidd> у режимі діагностики. Запобігає запуску B<uuidd> у режимі "
"фонової служби."

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid "B<-F>, B<--no-fork>"
msgstr "B<-F>, B<--no-fork>"

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid "Do not daemonize using a double-fork."
msgstr "Не створювати фонової служби за допомогою подвійного відгалуження."

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid "B<-k>, B<--kill>"
msgstr "B<-k>, B<--kill>"

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid "If currently a uuidd daemon is running, kill it."
msgstr "Якщо запущено фонову службу uuidd, завершити її роботу."

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid "B<-n>, B<--uuids> I<number>"
msgstr "B<-n>, B<--uuids> I<кількість>"

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid ""
"When issuing a test request to a running B<uuidd>, request a bulk response "
"of I<number> UUIDs."
msgstr ""
"При надсиланні запиту щодо тестування запущеному екземпляру B<uuidd>, "
"просити про пакетну відповідь з вказаної I<кількості> UUID."

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid "B<-P>, B<--no-pid>"
msgstr "B<-P>, B<--no-pid>"

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid "Do not create a pid file."
msgstr "Не створювати файл pid."

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid "B<-p>, B<--pid> I<path>"
msgstr "B<-p>, B<--pid> I<шлях>"

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid ""
"Specify the pathname where the pid file should be written. By default, the "
"pid file is written to I<{runstatedir}/uuidd/uuidd.pid>."
msgstr ""
"Вказати шлях до каталогу, до якого має бути записано файл pid. Типово, файл "
"pid буде записано до I<{runstatedir}/uuidd/uuidd.pid>."

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid "B<-q>, B<--quiet>"
msgstr "B<-q>, B<--quiet>"

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid "Suppress some failure messages."
msgstr "Придушити деякі повідомлення про помилки."

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid "B<-r>, B<--random>"
msgstr "B<-r>, B<--random>"

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid ""
"Test uuidd by trying to connect to a running uuidd daemon and request it to "
"return a random-based UUID."
msgstr ""
"Перевірити uuidd спробою з'єднання із запущеною фоновою службою uuidd і "
"надіслати до неї запит для повернення заснованого на випадкових числах UUID."

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid "B<-S>, B<--socket-activation>"
msgstr "B<-S>, B<--socket-activation>"

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid ""
"Do not create a socket but instead expect it to be provided by the calling "
"process. This implies B<--no-fork> and B<--no-pid>. This option is intended "
"to be used only with B<systemd>(1). It needs to be enabled with a configure "
"option."
msgstr ""
"Не створювати сокет, а вважати, що його буде надано процесом, звідки "
"викликано службу. При цьому неявно встановлюються B<--no-fork> і B<--no-"
"pid>. Цей параметр призначено для використання лише у поєднанні із "
"B<systemd>(1). Його має бути увімкнено за допомогою параметра налаштовування."

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid "B<-s>, B<--socket> I<path>"
msgstr "B<-s>, B<--socket> I<шлях>"

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid ""
"Make uuidd use this pathname for the unix-domain socket. By default, the "
"pathname used is I<{runstatedir}/uuidd/request>. This option is primarily "
"for debugging purposes, since the pathname is hard-coded in the B<libuuid> "
"library."
msgstr ""
"Наказати uuidd використовувати цей шлях для сокета домену unix. Типово, "
"використаним шляхом буде I<{runstatedir}/uuidd/request>. Цей параметр, в "
"основному, призначено для діагностики, оскільки шлях жорстко вказано у коді "
"бібліотеки B<libuuid>."

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid "B<-T>, B<--timeout> I<number>"
msgstr "B<-T>, B<--timeout> I<число>"

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid "Make B<uuidd> exit after I<number> seconds of inactivity."
msgstr ""
"Наказати B<uuidd> завершити роботу після вказаного I<числа> секунд "
"бездіяльності."

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid "B<-t>, B<--time>"
msgstr "B<-t>, B<--time>"

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid ""
"Test B<uuidd> by trying to connect to a running uuidd daemon and request it "
"to return a time-based UUID."
msgstr ""
"Перевірити B<uuidd> спробою з'єднання із запущеною фоновою службою uuidd і "
"надіслати до неї запит для повернення заснованого на часовій мітці UUID."

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid "Display help text and exit."
msgstr "Вивести текст довідки і завершити роботу."

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid "Print version and exit."
msgstr "Вивести дані щодо версії і завершити роботу."

#. type: SH
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLE"
msgstr "ПРИКЛАДИ"

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid "Start up a daemon, print 42 random keys, and then stop the daemon:"
msgstr ""
"Запустити фонову службу, вивести 42 випадкових ключі, а потім завершити "
"роботу фонової служби:"

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid ""
"uuidd -p /tmp/uuidd.pid -s /tmp/uuidd.socket\n"
"uuidd -d -r -n 42 -s /tmp/uuidd.socket\n"
"uuidd -d -k -s /tmp/uuidd.socket\n"
msgstr ""
"uuidd -p /tmp/uuidd.pid -s /tmp/uuidd.socket\n"
"uuidd -d -r -n 42 -s /tmp/uuidd.socket\n"
"uuidd -d -k -s /tmp/uuidd.socket\n"

#. type: SH
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "AUTHOR"
msgstr "АВТОР"

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid "The B<uuidd> daemon was written by"
msgstr "Фонову службу B<uuidd> було написано"

#. type: SH
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr "ДИВ. ТАКОЖ"

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid "B<uuid>(3), B<uuidgen>(1)"
msgstr "B<uuid>(3), B<uuidgen>(1)"

#. type: SH
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ЗВІТИ ПРО ВАДИ"

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid "For bug reports, use the issue tracker at"
msgstr "Для звітування про вади використовуйте систему стеження помилками на"

#. type: SH
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "AVAILABILITY"
msgstr "ДОСТУПНІСТЬ"

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid ""
"The B<uuidd> command is part of the util-linux package which can be "
"downloaded from"
msgstr "B<uuidd> є частиною пакунка util-linux, який можна отримати з"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2024-03-20"
msgstr "20 березня 2024 року"

#. type: TH
#: mageia-cauldron
#, fuzzy, no-wrap
#| msgid "util-linux 2.37.4"
msgid "util-linux 2.40"
msgstr "util-linux 2.37.4"

#. type: Plain text
#: mageia-cauldron opensuse-leap-15-6
msgid "B<-C>, B<--cont-clock>[=I<time>]"
msgstr "B<-C>, B<--cont-clock>[=I<час>]"

#. type: Plain text
#: mageia-cauldron opensuse-leap-15-6
msgid ""
"Activate continuous clock handling for time based UUIDs. B<uuidd> could use "
"all possible clock values, beginning with the daemon\\(cqs start time. The "
"optional argument can be used to set a value for the max_clock_offset. This "
"gurantees, that a clock value of a UUID will always be within the range of "
"the max_clock_offset."
msgstr ""
"Задіяти неперервну обробку даних годинника для заснованих на часі UUID. "
"B<uuidd> може використовувати усі можливі значення годинника, починаючи з "
"моменту запуску фонової служби. Необов'язковим аргументом можна скористатися "
"для встановлення значення для max_clock_offset. Це гарантує, що значення "
"годинника для UUID завжди перебуватиме у діапазоні max_clock_offset."

#. type: Plain text
#: mageia-cauldron
#, fuzzy
#| msgid ""
#| "The option \\*(Aq-C\\*(Aq or \\*(Aq--cont-clock\\*(Aq enables the feature "
#| "with a default max_clock_offset of 2 hours."
msgid ""
"The option B<-C> or B<--cont-clock> enables the feature with a default "
"max_clock_offset of 2 hours."
msgstr ""
"Параметр \\*(Aq-C\\*(Aq або \\*(Aq--cont-clock\\*(Aq вмикає можливість зі "
"типовим значенням max_clock_offset у 2 години."

#. type: Plain text
#: mageia-cauldron
#, fuzzy
#| msgid ""
#| "The option \\*(Aq-CE<lt>NUME<gt>[hd]\\*(Aq or \\*(Aq--cont-"
#| "clock=E<lt>NUME<gt>[hd]\\*(Aq enables the feature with a max_clock_offset "
#| "of NUM seconds. In case of an appended h or d, the NUM value is read in "
#| "hours or days. The minimum value is 60 seconds, the maximum value is 365 "
#| "days."
msgid ""
"The option B<-CE<lt>NUME<gt>[hd]> or B<--cont-clock=E<lt>NUME<gt>[hd]> "
"enables the feature with a max_clock_offset of NUM seconds. In case of an "
"appended h or d, the NUM value is read in hours or days. The minimum value "
"is 60 seconds, the maximum value is 365 days."
msgstr ""
"Параметр \\*(Aq-CE<lt>NUME<gt>[hd]\\*(Aq або \\*(Aq--cont-"
"clock=E<lt>ЧИСЛОE<gt>[hd]\\*(Aq вмикає можливість зі значенням "
"max_clock_offset у ЧИСЛО секунд. Якщо буде додано h або d, значення ЧИСЛО "
"буде прочитано як кількість годин або днів. Мінімальним є значення у 60 "
"секунд, а максимальним ­­— у 365 днів."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-11-21"
msgstr "21 листопада 2023 року"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "util-linux 2.39.3"
msgstr "util-linux 2.39.3"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The option \\*(Aq-C\\*(Aq or \\*(Aq--cont-clock\\*(Aq enables the feature "
"with a default max_clock_offset of 2 hours."
msgstr ""
"Параметр \\*(Aq-C\\*(Aq або \\*(Aq--cont-clock\\*(Aq вмикає можливість зі "
"типовим значенням max_clock_offset у 2 години."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The option \\*(Aq-CE<lt>NUME<gt>[hd]\\*(Aq or \\*(Aq--cont-"
"clock=E<lt>NUME<gt>[hd]\\*(Aq enables the feature with a max_clock_offset of "
"NUM seconds. In case of an appended h or d, the NUM value is read in hours "
"or days. The minimum value is 60 seconds, the maximum value is 365 days."
msgstr ""
"Параметр \\*(Aq-CE<lt>NUME<gt>[hd]\\*(Aq або \\*(Aq--cont-"
"clock=E<lt>ЧИСЛОE<gt>[hd]\\*(Aq вмикає можливість зі значенням "
"max_clock_offset у ЧИСЛО секунд. Якщо буде додано h або d, значення ЧИСЛО "
"буде прочитано як кількість годин або днів. Мінімальним є значення у 60 "
"секунд, а максимальним ­­— у 365 днів."

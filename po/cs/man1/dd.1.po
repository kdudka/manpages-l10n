# Czech translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Petr Kolář <Petr.Kolar@vslib.cz>, 2001.
# Kamil Dudka <kdudka@redhat.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-05-01 15:37+0200\n"
"PO-Revision-Date: 2009-09-02 20:06+0100\n"
"Last-Translator: Kamil Dudka <kdudka@redhat.com>\n"
"Language-Team: Czech <translation-team-cs@lists.sourceforge.net>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.08.2\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DD"
msgstr "DD"

#. type: TH
#: archlinux debian-unstable
#, no-wrap
msgid "March 2024"
msgstr "Březen 2024"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.5"
msgstr "GNU coreutils 9.5"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Příručka uživatele"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "JMÉNO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "dd - convert and copy a file"
msgstr "dd - konvertuje a kopíruje soubor"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "POUŽITÍ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<dd> [I<\\,OPERAND\\/>]..."
msgstr "B<dd> [I<\\,OPERAND\\/>]..."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<dd> I<\\,OPTION\\/>"
msgstr "B<dd> I<\\,VOLBA\\/>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "POPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Copy a file, converting and formatting according to the operands."
msgstr "Kopíruje soubor a konvertuje a formátuje jej podle zadaných operandů."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "bs=BYTES"
msgstr "bs=BAJTŮ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
msgid ""
"read and write up to BYTES bytes at a time (default: 512); overrides ibs and "
"obs"
msgstr "vynutí ibs=BAJTŮ a obs=BAJTŮ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "cbs=BYTES"
msgstr "cbs=BAJTŮ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "convert BYTES bytes at a time"
msgstr "určuje délku bloku, který bude konvertován najednou"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "conv=CONVS"
msgstr "conv=CONVS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "convert the file as per the comma separated symbol list"
msgstr "Konvertuje soubor podle čárkami oddělovaného seznamu symbolů"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "count=BLOCKS"
msgid "count=N"
msgstr "count=BLOKŮ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "copy only BLOCKS input blocks"
msgid "copy only N input blocks"
msgstr "Kopíruje pouze zadaný počet bloků"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ibs=BYTES"
msgstr "ibs=BAJTŮ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "read BYTES bytes at a time (default: 512)"
msgid "read up to BYTES bytes at a time (default: 512)"
msgstr "načítá bloky zadané délky"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "if=FILE"
msgstr "if=SOUBOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "read from FILE instead of stdin"
msgstr "čte ze zadaného SOUBORu místo ze standardního vstupu"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "iflag=FLAGS"
msgstr "iflag=PŘÍZNAKY"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "read as per the comma separated symbol list"
msgstr "čte podle čárkami oddělovaného seznamu symbolů"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "obs=BYTES"
msgstr "obs=BAJTŮ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "write BYTES bytes at a time (default: 512)"
msgstr "zapisuje bloky zadané délky"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "of=FILE"
msgstr "of=SOUBOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "write to FILE instead of stdout"
msgstr "zapisuje do SOUBORu místo na standardní výstup"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "oflag=FLAGS"
msgstr "oflag=PŘÍZNAKY"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "write as per the comma separated symbol list"
msgstr "zapisuje podle čárkami oddělovaného seznamu symbolů"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "seek=N"
msgstr "seek=N"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "(or oseek=N) skip N obs-sized output blocks"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "skip=N"
msgstr "skip=N"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "(or iseek=N) skip N ibs-sized input blocks"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "status=LEVEL"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The LEVEL of information to print to stderr; \\&'none' suppresses everything "
"but error messages, \\&'noxfer' suppresses the final transfer statistics, "
"\\&'progress' shows periodic transfer statistics"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "BLOCKS and BYTES may be followed by the following multiplicative "
#| "suffixes: c =1, w =2, b =512, kB =1000, K =1024, MB =1000*1000, M "
#| "=1024*1024, xM =M GB =1000*1000*1000, G =1024*1024*1024, and so on for T, "
#| "P, E, Z, Y."
msgid ""
"N and BYTES may be followed by the following multiplicative suffixes: c=1, "
"w=2, b=512, kB=1000, K=1024, MB=1000*1000, M=1024*1024, xM=M, "
"GB=1000*1000*1000, G=1024*1024*1024, and so on for T, P, E, Z, Y, R, Q.  "
"Binary prefixes can be used, too: KiB=K, MiB=M, and so on.  If N ends in "
"'B', it counts bytes not blocks."
msgstr ""
"Údaje označující počet BLOKŮ a BAJTŮ mohou být doplněny následujícími "
"násobiteli: c =1, w =2, b =512, kB =1000, K =1024, MB =1000*1000, M "
"=1024*1024, xM =M GB =1000*1000*1000, G =1024*1024*1024, atd. pro T, P, E, "
"Z, Y."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Each CONV symbol may be:"
msgstr "Platné symboly pro conv jsou:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ascii"
msgstr "ascii"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "from EBCDIC to ASCII"
msgstr "z EBCDIC do ASCII"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ebcdic"
msgstr "ebcdic"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "from ASCII to EBCDIC"
msgstr "z ASCII do EBCDIC"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ibm"
msgstr "ibm"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "from ASCII to alternate EBCDIC"
msgstr "z ASCII do alternativního EBCDIC"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "block"
msgstr "block"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "pad newline-terminated records with spaces to cbs-size"
msgstr ""
"doplňuje záznamy ukončené znakem newline koncovými mezerami na délku cbs"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "unblock"
msgstr "unblock"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "replace trailing spaces in cbs-size records with newline"
msgstr "nahrazuje koncové mezery v každém bloku délky cbs znakem newline"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "lcase"
msgstr "lcase"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "change upper case to lower case"
msgstr "mění velká písmena na malá"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ucase"
msgstr "ucase"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "change lower case to upper case"
msgstr "mění malá písmena na velká"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "sparse"
msgstr "sparse"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "try to seek rather than write all-NUL output blocks"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "swab"
msgstr "swab"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "swap every pair of input bytes"
msgstr "prohazuje sudé a liché bajty"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "sync"
msgstr "sync"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"pad every input block with NULs to ibs-size; when used with block or "
"unblock, pad with spaces rather than NULs"
msgstr ""
"doplní každý vstupní blok koncovými nulovými bajty na velikost ibs; je=li "
"použito s block nebo unblock,doplňuje mezerami"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "excl"
msgstr "excl"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "fail if the output file already exists"
msgstr "selže, pokud výstupní soubor už existuje"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "nocreat"
msgstr "nocreat"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "do not create the output file"
msgstr "nevytváří výstupní soubor"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "notrunc"
msgstr "notrunc"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "do not truncate the output file"
msgstr "nezkracuje výstupní soubor"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "noerror"
msgstr "noerror"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "continue after read errors"
msgstr "ignoruje chyby při čtení"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "fdatasync"
msgstr "fdatasync"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "physically write output file data before finishing"
msgstr "fyzicky zapíše data před ukončením"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "fsync"
msgstr "fsync"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "likewise, but also write metadata"
msgstr "to samé, ale zapíše také metadata"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Each FLAG symbol may be:"
msgstr "PŘÍZNAKY mohou být:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "append"
msgstr "append"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "append mode (makes sense only for output; conv=notrunc suggested)"
msgstr ""
"režim přidávání na konec souboru (pouze zápis; doporučuje se kombinovat s "
"conv=notrunc)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "direct"
msgstr "direct"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "use direct I/O for data"
msgstr "přímé čtení/zápis dat"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "directory"
msgstr "directory"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "fail unless a directory"
msgstr "selže, pokud daný soubor (vstupní/výstupní) není adresář"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "dsync"
msgstr "dsync"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "use synchronized I/O for data"
msgstr "použije synchronizovaný vstup/výstup dat"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "likewise, but also for metadata"
msgstr "to samé, ale platí i pro metadata"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "fullblock"
msgstr "fullblock"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "accumulate full blocks of input (iflag only)"
msgstr "akumuluje kompletní vstupní bloky (jen pro iflag)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "nonblock"
msgstr "nonblock"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "use non-blocking I/O"
msgstr "použije neblokující vstup/výstup"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "noatime"
msgstr "noatime"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "do not update access time"
msgstr "neaktualizuje čas posledního přístupu souboru"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "nocache"
msgstr "nocache"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Request to drop cache.  See also oflag=sync"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "noctty"
msgstr "noctty"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "do not assign controlling terminal from file"
msgstr "nepřiřazuje řídící terminál ze souboru"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "nofollow"
msgstr "nofollow"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "do not follow symlinks"
msgstr "nenásleduje symbolické odkazy"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Sending a USR1 signal to a running 'dd' process makes it print I/O "
"statistics to standard error and then resume copying."
msgstr ""
"Zaslání signálu USR1 běžícímu procesu 'dd' zapříčiní výpis vstup/výstupních "
"statistik na standardní chybový výstup, proces potom normálně pokračuje v "
"kopírování."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Options are:"
msgstr "Jeho volby jsou:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "vypíše návod k použití na standardní výstup a bezchybně skončí"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "Vypíše informaci o verzi programu a skončí"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Written by Paul Rubin, David MacKenzie, and Stuart Kemp."
msgstr "Napsal Paul Rubin, David MacKenzie a Stuart Kemp."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "HLÁŠENÍ CHYB"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"On-line nápověda GNU coreutils: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Chyby v překladu hlaste na E<lt>https://translationproject.org/team/cs."
"htmlE<gt> (česky)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2024 Free Software Foundation, Inc.  Licence GPLv3+: GNU "
"GPLverze 3 nebo novější E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Toto je volné programové vybavení: můžete jej měnit a šířit. Je zcela BEZ "
"ZÁRUKY, v rozsahu povoleném zákonem."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "DALŠÍ INFORMACE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Full documentation E<lt>https://www.gnu.org/software/coreutils/ddE<gt>"
msgstr ""
"Úplná dokumentace je na: E<lt>https://www.gnu.org/software/coreutils/ddE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) dd invocation\\(aq"
msgstr "nebo dostupná lokálně skrze: info \\(aq(coreutils) dd invocation\\(aq"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "September 2022"
msgstr "Září 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "BLOCKS and BYTES may be followed by the following multiplicative "
#| "suffixes: c =1, w =2, b =512, kB =1000, K =1024, MB =1000*1000, M "
#| "=1024*1024, xM =M GB =1000*1000*1000, G =1024*1024*1024, and so on for T, "
#| "P, E, Z, Y."
msgid ""
"N and BYTES may be followed by the following multiplicative suffixes: c=1, "
"w=2, b=512, kB=1000, K=1024, MB=1000*1000, M=1024*1024, xM=M, "
"GB=1000*1000*1000, G=1024*1024*1024, and so on for T, P, E, Z, Y.  Binary "
"prefixes can be used, too: KiB=K, MiB=M, and so on.  If N ends in 'B', it "
"counts bytes not blocks."
msgstr ""
"Údaje označující počet BLOKŮ a BAJTŮ mohou být doplněny následujícími "
"násobiteli: c =1, w =2, b =512, kB =1000, K =1024, MB =1000*1000, M "
"=1024*1024, xM =M GB =1000*1000*1000, G =1024*1024*1024, atd. pro T, P, E, "
"Z, Y."

#. type: Plain text
#: debian-bookworm
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  Licence GPLv3+: GNU "
"GPLverze 3 nebo novější E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: debian-unstable fedora-40 mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: Plain text
#: debian-unstable fedora-40 mageia-cauldron
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  Licence GPLv3+: GNU "
"GPLverze 3 nebo novější E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: fedora-40 opensuse-leap-15-6
#, no-wrap
msgid "January 2024"
msgstr "Leden 2024"

#. type: TH
#: fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "April 2024"
msgstr "Dubna 2024"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "August 2023"
msgstr "Srpen 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid "skip BLOCKS obs-sized blocks at start of output"
msgid "skip N obs-sized blocks at start of output"
msgstr ""
"přeskočí zadaný počet bloků velikosti obs od začátku výstupního souboru"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid "skip BLOCKS ibs-sized blocks at start of input"
msgid "skip N ibs-sized blocks at start of input"
msgstr "přeskočí zadaný počet bloků velikosti ibs od začátku vstupního souboru"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "BLOCKS and BYTES may be followed by the following multiplicative "
#| "suffixes: c =1, w =2, b =512, kB =1000, K =1024, MB =1000*1000, M "
#| "=1024*1024, xM =M GB =1000*1000*1000, G =1024*1024*1024, and so on for T, "
#| "P, E, Z, Y."
msgid ""
"N and BYTES may be followed by the following multiplicative suffixes: c=1, "
"w=2, b=512, kB=1000, K=1024, MB=1000*1000, M=1024*1024, xM=M, "
"GB=1000*1000*1000, G=1024*1024*1024, and so on for T, P, E, Z, Y.  Binary "
"prefixes can be used, too: KiB=K, MiB=M, and so on."
msgstr ""
"Údaje označující počet BLOKŮ a BAJTŮ mohou být doplněny následujícími "
"násobiteli: c =1, w =2, b =512, kB =1000, K =1024, MB =1000*1000, M "
"=1024*1024, xM =M GB =1000*1000*1000, G =1024*1024*1024, atd. pro T, P, E, "
"Z, Y."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "count_bytes"
msgstr "count_bytes"

#. type: Plain text
#: opensuse-leap-15-6
msgid "treat 'count=N' as a byte count (iflag only)"
msgstr ""

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "skip_bytes"
msgstr "skip_bytes"

#. type: Plain text
#: opensuse-leap-15-6
msgid "treat 'skip=N' as a byte count (iflag only)"
msgstr ""

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "seek_bytes"
msgstr "seek_bytes"

#. type: Plain text
#: opensuse-leap-15-6
msgid "treat 'seek=N' as a byte count (oflag only)"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  Licence GPLv3+: GNU "
"GPLverze 3 nebo novější E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

# Czech translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Petr Kolář <Petr.Kolar@vslib.cz>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-02-09 17:14+0100\n"
"PO-Revision-Date: 2001-09-02 20:06+0100\n"
"Last-Translator: Petr Kolář <Petr.Kolar@vslib.cz>\n"
"Language-Team: Czech <translation-team-cs@lists.sourceforge.net>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.08.2\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "W"
msgstr "W"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2020-06-04"
msgstr "4. června 2020"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "procps-ng"
msgstr "procps-ng"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "User Commands"
msgstr "Příručka uživatele"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "JMÉNO"

#. type: Plain text
#: opensuse-leap-15-6
msgid "w - Show who is logged on and what they are doing."
msgstr "w - Zobrazí, kdo je přihlášený k systému a co dělá."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr "POUŽITÍ"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<w> [I<options>] I<user> [...]"
msgstr "B<w> [I<volby>] I<uživatel> [...]"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "POPIS"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<w> displays information about the users currently on the machine, and "
"their processes.  The header shows, in this order, the current time, how "
"long the system has been running, how many users are currently logged on, "
"and the system load averages for the past 1, 5, and 15 minutes."
msgstr ""
"B<w> zobrazí informace o uživatelích přihlášených k počítači a jejich "
"procesech.  V záhlaví se zobrazí (v tomto pořadí): aktuální čas, jak dlouho "
"už systém běží, kolik uživatelů je právě přihlášených a průměrné zatížení "
"systému za posledních 1, 5 a 15 minut."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The following entries are displayed for each user: login name, the tty name, "
"the remote host, login time, idle time, JCPU, PCPU, and the command line of "
"their current process."
msgstr ""
"V následujících řádcích je pro každého uživatele vypsáno: přihlašovací "
"jméno, jméno konzoly, počítač, odkud je uživatel přihlášen, čas přihlášení, "
"délka trvání nečinnosti, JCPU, PCPU a příkazový řádek jeho aktuálního "
"procesu."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The JCPU time is the time used by all processes attached to the tty.  It "
"does not include past background jobs, but does include currently running "
"background jobs."
msgstr ""
"Čas JCPU je čas použitý všemi procesy spuštěnými z dané konzoly (tty).  "
"Nejsou v něm zahrnuty úlohy spuštěné v minulosti na pozadí, ale zahrnuje i "
"právě bežící úlohy na pozadí."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The PCPU time is the time used by the current process, named in the \"what\" "
"field."
msgstr "PCPU čas je čas, použitý aktuálním procesem uvedeným v posledním poli."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "COMMAND-LINE OPTIONS"
msgstr "VOLBY PŘÍKAZOVÉ ŘÁDKY"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-h>, B<--no-header>"
msgstr "B<-h>, B<--no-header>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Don't print the header."
msgstr "Nezobrazuje záhlaví."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-u>, B<--no-current>"
msgstr "B<-u>, B<--no-current>"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "Ignores the username while figuring out the current process and cpu "
#| "times.  To demonstrate this, do a \"su\" and do a \"w\" and a \"w -u\"."
msgid ""
"Ignores the username while figuring out the current process and cpu times.  "
"To demonstrate this, do a B<su> and do a B<w> and a B<w -u>."
msgstr ""
"Ignoruje uživatelské jméno při zjišťování aktuálního procesu a času cpu.  "
"Pro demonstraci zkuste \"su\" a \"w\" a \"w -u\"."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-s>, B<--short>"
msgstr "B<-s>, B<--short>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Use the short format.  Don't print the login time, JCPU or PCPU times."
msgstr ""
"Použije krátký formát výpisu.  Nezobrazuje čas přihlášení ani časy JCPU a "
"PCPU."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-n>, B<--no-truncat>"
msgstr "B<-n>, B<--no-truncat>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Do not truncate the output format. This option might become renamed in "
"future versions."
msgstr ""

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-f>, B<--from>"
msgstr "B<-f>, B<--from>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Toggle printing the B<from> (remote hostname) field.  The default as "
"released is for the B<from> field to not be printed, although your system "
"administrator or distribution maintainer may have compiled a version in "
"which the B<from> field is shown by default."
msgstr ""
"Přepíná zobrazení pole B<from> (jméno vzdáleného počítače). Implicitně se "
"pole B<from> nezobrazuje, ale správce počítače nebo udržovatel distribuce "
"může zkompilovat program tak, že pole B<from> se implicitně zobrazuje."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display help text and exit."
msgstr "Zobrazí nápovědu a skončí."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-i>, B<--ip-addr>"
msgstr "B<-i>, B<--ip-addr>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display IP address instead of hostname for B<from> field."
msgstr ""

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display version information."
msgstr "Vypíše číslo verze."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<-o>, B<--old-style>"
msgstr "B<-o>, B<--old-style>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Old style output.  Prints blank space for idle times less than one minute."
msgstr ""

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<user >"
msgstr "B<uživatel >"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Show information about the specified user only."
msgstr "Vypíše informace pouze o zadaném uživateli."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "ENVIRONMENT"
msgstr "PROSTŘEDÍ"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "PROCPS_USERLEN"
msgstr "PROCPS_USERLEN"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Override the default width of the username column.  Defaults to 8."
msgstr ""

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "PROCPS_FROMLEN"
msgstr "PROCPS_FROMLEN"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Override the default width of the from column.  Defaults to 16."
msgstr ""

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "FILES"
msgstr "SOUBORY"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "I</var/run/utmp>"
msgstr "I</var/run/utmp>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "information about who is currently logged on"
msgstr "informace o tom, kdo je aktuálně přihlášený"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "I</proc>"
msgstr "I</proc>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "process information"
msgstr "informace o procesech"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr "DALŠÍ INFORMACE"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<free>(1), B<ps>(1), B<top>(1), B<uptime>(1), B<utmp>(5), B<who>(1)"
msgstr "B<free>(1), B<ps>(1), B<top>(1), B<uptime>(1), B<utmp>(5), B<who>(1)"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "AUTHORS"
msgstr "AUTORI"

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid ""
#| "B<w> was re-written almost entirely by Charles Blake, based on the "
#| "version by Larry Greenfield E<lt>greenfie@gauss.rutgers.eduE<gt> and "
#| "Michael K. Johnson E<lt>johnsonm@redhat.comE<gt>."
msgid ""
"B<w> was re-written almost entirely by Charles Blake, based on the version "
"by E<.UR greenfie@\\:gauss.\\:rutgers.\\:edu> Larry Greenfield E<.UE> and E<."
"UR johnsonm@\\:redhat.\\:com> Michael K. Johnson E<.UE>"
msgstr ""
"B<w> byl téměř zcela přepsán Charlesem Blakem. Vychází z programu od Larryho "
"Greenfielda E<lt>greenfie@gauss.rutgers.eduE<gt> a Michaela K. Johnsona "
"E<lt>johnsonm@redhat.comE<gt>."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "REPORTING BUGS"
msgstr "HLÁŠENÍ CHYB"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Please send bug reports to E<.UR procps@freelists.org> E<.UE>"
msgstr ""
"Chyby týkající se programu prosím zasílejte na E<.UR procps@freelists.org> "
"E<.UE>"

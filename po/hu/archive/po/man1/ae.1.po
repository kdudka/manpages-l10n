# Hungarian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Tevesz Tamás <ice@rulez.org>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2021-07-11 08:57+0200\n"
"PO-Revision-Date: 2001-01-05 12:34+0100\n"
"Last-Translator: Tevesz Tamás <ice@rulez.org>\n"
"Language-Team: Hungarian <>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: original/man1/ae.1:16
#, no-wrap
msgid "AE"
msgstr "AE"

#. type: TH
#: original/man1/ae.1:16
#, no-wrap
msgid "Editors"
msgstr ""

#. type: SH
#: original/man1/ae.1:17
#, no-wrap
msgid "NAME"
msgstr "NÉV"

#. type: Plain text
#: original/man1/ae.1:19
msgid "ae - tiny full-screen text editor"
msgstr "ae - pici teljesképernyős szövegszerkesztő"

#. type: SH
#: original/man1/ae.1:19
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÁTTEKINTÉS"

#. type: Plain text
#: original/man1/ae.1:22
msgid "B<ae> [-f config_file ] [ file ]"
msgstr "B<ae> [-f config_file ] [ file ]"

#. type: SH
#: original/man1/ae.1:22
#, no-wrap
msgid "DESCRIPTION"
msgstr "LEÍRÁS"

#. type: Plain text
#: original/man1/ae.1:30
msgid ""
"B<ae> is a tiny full-screen text editor with both modual (vi-like)  and "
"modeless (emacs-like) editing modes, determined by an ae.rc config file.  "
"Keybindings are configurable in the config file.  The default config file is "
"located in /etc/ae.rc.  If an ae.rc file exists in current working directory "
"or in the user's home directory, that file is used instead of the default "
"file."
msgstr ""
"Az B<ae> egy pici teljesképernyős szövegszerkesztő, kétmódú (vi-szerű) és "
"egymódú (emacs-szerű) szerkesztési móddal. A szerkesztési módot az ae.rc "
"konfigurációs fájl beállításai határozzák meg.  A billentyűkombinációk a "
"konfigurációs fájlban állíthatók be.  Az alapértelmezésű konfigurációs fájl "
"az /etc/ae.rc.  Ha az aktuális könyvtárban vagy a felhasználó home "
"könyvtárában van ae.rc fájl, akkor az alapértelmezésű konfigurációs fájl "
"helyett az abban a fájlban tárolt beállítások lesznek érvényesek."

#. type: Plain text
#: original/man1/ae.1:32
msgid "A users manual and sample config files may be found in /usr/doc/ae."
msgstr ""
"A felhasználói kézikönyv és példa konfigurációs fájlok az /usr/doc/ae "
"könyvtárban találhatók."

#. type: SS
#: original/man1/ae.1:32
#, no-wrap
msgid "FILES"
msgstr "FÁJLOK"

#. type: Plain text
#: original/man1/ae.1:38
#, no-wrap
msgid ""
"/etc/ae.rc\n"
"/usr/doc/ae/ae.man\n"
"/usr/doc/ae/mode.rc\n"
"/usr/doc/ae/modeless.rc\n"
msgstr ""
"/etc/ae.rc\n"
"/usr/doc/ae/ae.man\n"
"/usr/doc/ae/mode.rc\n"
"/usr/doc/ae/modeless.rc\n"
